var upload_type = '';

$(function() { // Document Ready
    // Use the "Upload" button to bring up the file select
    $('button[name="do_upload"]').on('click', function() {
        $(this).prev('input[type="file"]').click();
    });
    
    $('input[name="image_uploader"], input[name="file_uploader"]').on('change', function(e) {
        var files = e.target.files;
        
        upload_type = $(this).attr('name').substr(0,$(this).attr('name').length - 9);
                
        handleFileUpload($(this).attr('id'),files);
    });
    
});

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var image_types = ["jpg","jpeg","gif","png"];
var file_types = ["png","doc","docx","pdf","csv","xls","xlsx","jpg","jpeg","gif","png","zip","rar","7z","s7z","tif","psd"];

var file_icon = '';
var image_type = 'image';

var allowedfiletypes = image_types.concat(file_types);

var totalBadFiles = 0;
var totalGoodFiles = 0;
var totalFilesUploaded = 0;
function handleFileUpload(fieldname,files) {

	totalBadFiles = 0;
	totalGoodFiles = 0;
	totalFilesUploaded = 0;

	numfiles = files.length;
	// check for bad files first
	for (var i = 0; i < numfiles; i++) {
		var filename = files[i].name;
		var ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
		
		// Set the post_type field value
		if ( image_types.indexOf(ext) >= 0 ) {
            image_type = 'image';
		} else if ( file_types.indexOf(ext) >= 0 ) {
    		
    		if ( upload_type == 'image' ) {
    		    $('#image_uploader_destfolder').val('/images/'+ext+'/');
            } else if ( upload_type == 'file' ) {
                $('#file_uploader_destfolder').val('/files/'+ext+'/');
            }
    		image_type = 'file';
		}
		
		if ( upload_type == 'image' ) {
		    $('#image_post_type').val(image_type);
        } else if ( upload_type == 'file' ) {
            $('#file_post_type').val(image_type);
        }
		
		if (allowedfiletypes.indexOf(ext) >= 0) {
		}else{
			totalBadFiles++;
		}
	}

	totalGoodFiles = numfiles-totalBadFiles;
	if (totalGoodFiles==0) {
		if (totalBadFiles>0) {
			alert("Your file could not be uploaded.\nReason: Invalid file type");
			totalBadFiles = 0;
		}
	}else{

		var fnames = [];

		for (var i = 0; i < numfiles; i++) {
			var findex = 1;
			var filename = files[i].name;
			var ext = filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
			
			if (allowedfiletypes.indexOf(ext) >= 0) {
				while(fnames.indexOf(filename) >= 0 && findex < 100) {
					filename = files[i].name.replace("." + ext, "-" + findex + "." + ext);
					findex++;
				}
				
				switch (ext) {
        			case 'doc':
        			case 'docx':
        			    file_icon = 'file-text-o';
        			    break;
                    case 'pdf':
                        file_icon = 'file-pdf-o';
                        break;
                    case 'csv':
                    case 'xls':
                    case 'xlsx':
                        file_icon = 'file-excel-o';
                        break;
                    case 'jpg':
                    case 'jpeg':
                    case 'gif':
                    case 'png':
                    case 'tif':
                    case 'psd':
                        file_icon = 'file-image-o';
                        break;
                    case 'zip':
                    case 'rar':
                    case '7z':
                    case 's7z':
                        file_icon = 'file-archive-o';
                        break;
                    default:
                        file_icon = 'file-o';
    			}
				
				var cu = new chunkUploader(fieldname, filename, files[i]);

				if (image_types.indexOf(ext) >= 0) {
					// load image and get dimensions
                    readImage(fieldname,files[i] );
					$('#'+fieldname+"_deletebutton").val("Remove New Image");
				}

				fnames.push(filename);
			}
		}

	}

}

function readImage(fieldname,file) {

	var reader = new FileReader();
	var image  = new Image();

	reader.readAsDataURL(file);  
	reader.onload = function(_file) {
		image.src    = _file.target.result;              // url.createObjectURL(file);
		image.onload = function() {

			// show preview
			$('#'+fieldname+'_preview img').attr("src",this.src);
			
		};
		image.onerror= function() {
			alert('Invalid file type: '+ file.type);
		};
	};

}



function chunkUploader(fieldname, fn, file) {
	this._destfolder 	= $("#"+fieldname+"_destfolder").val();
	$("#"+fieldname+"_filename").val(file.name.replace(/\s/g,'-'));
	this.progressContainer 		= $("#"+fieldname+"_progress");
	this.progressBar 		= $("#"+fieldname+"_progress span");
	this.progressDetails = $("."+upload_type+"_progress_details");

	this.file = file;
	this.fn = fn;
	this.uploadedcount = 0;

	this.blobs = [];
	
	var numblobs = 0;
	var self = this;

	self.progressBar.css('width', 0).removeClass("processing").removeClass("done");
	self.progressDetails.css('opacity',0).html('&nbsp;');
	
    $('.'+upload_type+'_upload_btn').hide();
    $('.'+upload_type+'_uploader[data-step="2"]').hide();
    
/*
    $('.file_upload_btn').hide();
    $('.file_uploader[data-step="2"]').hide();
*/
    
	this.setProgress = function(progress) {
		this.progressContainer.show();
		if (parseInt(progress)==100) {
			progressTxt = '<i class="fa fa-'+file_icon+'"></i> Processing ' + fn + ' - ' + progress + '%';
			addclass = "done";
		} else if(parseInt(progress)==200) {
			progressTxt = '<i class="fa fa-'+file_icon+'"></i> Processed ' + fn + ' - ' + (progress / 2) + '%';
			progress = 100;
			addclass="done";
		} else {
			progressTxt = '<i class="fa fa-'+file_icon+'"></i> Processing ' + fn + ' - ' + progress + '%';
			addclass = "processing";
		}
		self.progressDetails.css('opacity','1').html(progressTxt);
		self.progressBar.animate({'width': parseInt(progress) +"%"}, 10).addClass(addclass);
		if(parseInt(progress) >= 100) {
			//self.abort.html('');
		}
	}

	this.setAbort = function(jqXHR) {
		var sb = self.selfbar;
		self.abort.unbind('click');
		self.abort.click(function() {
			totalGoodFiles--;
			jqXHR.abort();
			sb.hide();

			var fda = new FormData();
			fda.append('numblobs', numblobs);
			fda.append('filename', self.fn);
			$.ajax({
				url: '/admin/scripts/abort-upload.php',
				type: "POST",
				contentType:false,
				processData: false,
				cache: false,
				data: fda,
				success: function (data) {
					
				}
			});
		});
	}

	this.makeChunks = function () {

		var numchunks = 10;
		var filesize = self.file.size;
		var sizeStr="";
		var sizeKB = filesize/1024;

		if(parseInt(sizeKB) > 1024) {
			var sizeMB = sizeKB/1024;
			sizeStr = sizeMB.toFixed(2)+" MB";
		} else {
			sizeStr = sizeKB.toFixed(2)+" KB";
		}

		// get chunking

		if (filesize <= (1 * 1024 * 1024) ) {
			numchunks = 3;
		}else if (filesize <= (5 * 1024 * 1024) ) {
			numchunks = 5;
		}else if (filesize <= (10 * 1024 * 1024) ) {
			numchunks = 10;
		}else if (filesize <= (50 * 1024 * 1024) ) {
			numchunks = 25;
		}else if (filesize <= (100 * 1024 * 1024) ) {
			numchunks = 50;
		}else{
			numchunks = 100;
		}

		var bytes_per_chunk = Math.ceil(filesize / numchunks) ;

		var start = 0;
		var end = bytes_per_chunk;
		while (start < filesize) {
			self.blobs.push(file.slice(start, end));
			start = end;
			end = start + bytes_per_chunk;
		}

		var currentblob = 1;
		numblobs = self.blobs.length;

		var blob;
		if(blob = self.blobs.shift()) {
			self.uploadChunk(blob, currentblob, numblobs);
		}
	}

	this.uploadChunk = function(blob, currentblob, numblobs){

		var fd = new FormData();
		fd.append('file', blob);
		fd.append('currentblob', currentblob);
		fd.append('filename', self.fn);

		fd.append('destfolder', self._destfolder);
		

		var uploadURL ="/admin/scripts/upload-chunks.php"; //Upload URL
		var jqXHR=$.ajax({
			url: uploadURL,
			type: "POST",
			contentType:false,
			processData: false,
			cache: false,
			data: fd,
			success: function(data){

				self.uploadedcount++;
				percent = Math.ceil(self.uploadedcount/(numblobs/100));
				if (self.uploadedcount==numblobs) {
					// all parts uploaded
					self.setProgress(100);

					// send an ajax call to the recombiner script
					var fdd = new FormData();
					fdd.append('numblobs', numblobs);
					fdd.append('filename', self.fn);
					fdd.append('destfolder', self._destfolder);
										
					$.ajax({
						url: '/admin/scripts/combine-chunks.php',
						type: "POST",
						contentType:false,
						processData: false,
						cache: false,
						data: fdd,
						dataType: 'json',
						success: function (data) {
							self.setProgress(200);
							
							//console.log(data);
                            
							var destFolder = $('#'+upload_type+'_uploader_destfolder').val().replace(/^\/+|\/+$/g,'');
							
							var newSlug = '/'+destFolder+'/'+data.filename;
																					
							$('.upload_slug input[name="'+upload_type+'_post_slug"]').val(newSlug);
							
							// blank the upload input so it doesn't upload again on submit
							$('#'+fieldname).val('');
							
							// show the title and alt fields
							$('.'+upload_type+'_uploader[data-step="2"]').slideDown(300);
						
							$('.'+upload_type+'_upload_btn').fadeIn(300).html('Choose a different '+upload_type);
						
							$('.'+upload_type+'_save_btn').html('Save ' + capitalizeFirstLetter(upload_type));
							
							// Auto-populate Title and Description fields with name of image without extension
							if ( upload_type == 'image' ) {
								//console.log('upload_type',upload_type);
								//console.log('name',data.filename);
								var imageTitle = data.filename.substring(0,data.filename.lastIndexOf("."));
								$('div[data-step="2"] form').find('input[name="image_post_title"]').val(imageTitle);
								$('div[data-step="2"] form').find('input[name="image_post_alt_title"]').val(imageTitle);
							}
							
							totalFilesUploaded++;
							if (totalGoodFiles == totalFilesUploaded) {
								if (totalBadFiles>0) {
									alert("One or more of your files could not be uploaded.\nReason: Invalid file type");
								}
								totalBadFiles = 0;
								totalGoodFiles = 0;
								totalFilesUploaded = 0;
								//setTimeout(function() { location.reload(); }, 500);
							}
						},
						error: function(err) {
    						//console.error(err);
						}
					});


				}else{
					self.setProgress(percent);
					currentblob++;
					if(blob = self.blobs.shift()) {
						self.uploadChunk(blob, currentblob, numblobs);
					}
				}

			}
		});
		//self.setAbort(jqXHR);
	}

	this.makeChunks();
}