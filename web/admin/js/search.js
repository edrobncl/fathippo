$(function () {

  if ($('#admin-search').length) {

    $('#admin-search').append(
        '<div class="admin-search__form">' +
            '<div class="admin-search__inner">' +
                '<label for="searchtext" class="admin-search__label">Search</label>' +
                '<input type="text" id="search-text" placeholder="What are you looking for?" class="admin-search__input" required />' +
                '<div id="autocomplete"></div>' +
            '</div>' +
            '<div class="close-admin-search" role="button">' +
                '<span></span><span></span>' +
            '</div>' +
        '</div>' +
        '<div class="admin-search__history">' +
            '<div class="admin-search__inner">' +
                '<h3 class="admin-search__title">Recently viewed items</h3>' +
                '<div id="search-history"></div>' +
            '</div>' +
        '</div>'
    );

    $.getJSON('/admin/scripts/search.php', function (response) {

        $.widget("custom.catcomplete", $.ui.autocomplete, {
  
            _create: function () {
                this._super();
                this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
            },
  
            _renderMenu: function (ul, items) {
                var that = this;
                var currentCategory = "";
  
                $.each(items, function (index, item) {
                    var li;
  
                    if (item.category != currentCategory) {
                        ul.append('<li class="ui-autocomplete-category">' + item.category.toUpperCase() + '</li>');
                        currentCategory = item.category;
                    }
  
                    li = that._renderItemData(ul, item);
  
                    if (item.category) {
                        li.attr('aria-label', item.category + ' : ' + item.label);
                    }
                });
            }
  
        });
            
        $("#search-text").catcomplete({
            delay: 0,
            source: response,
            appendTo: '#autocomplete',
            select: function (event, ui) {
                // define variables
                var baseURL = '/admin/modules';
                var page, module = '';
                var store = [];
                var inStore = 0;
  
                // switch the category and crate matchin urls
                switch (ui.item.category) {
                    case 'page':
                        page = baseURL + '/core/pages/pages-form.php?_id=' + ui.item._id;
                        module = baseURL + '/core/pages';
                        break;
                    case 'product':
                        page = baseURL + '/products/products-form.php?_id=' + ui.item._id;
                        module = baseURL + '/products';
                        break;
                    // expand as needed below
                    // add new post_type to search.php too
                }

                // check if the local storage is set
                if (localStorage.getItem('history')) {
                    // get data from the local storage
                    store = JSON.parse(localStorage.getItem('history'));
                }

                // loop through each item in the store
                store.forEach(function (item) {
                    // return if matching item was found
                    // we don't want to store the same item twice
                    if (item._id === ui.item._id) {
                        inStore = 1;
                        return;
                    }
                });
  
                // execute only if the item wasn't found in the store
                if (!inStore) {
    
                    // remove last item from the store if more than 12
                    if (store.length > 12) {
                        store.pop();
                    }

                    // append page url to the ui.item object
                    ui.item.page = page;
                    // append module url to the ui.item object
                    ui.item.module = module;
  
                    // push the ui.item to the store
                    store.push(ui.item)
    
                    // update the local storage with new history
                    localStorage.setItem('history', JSON.stringify(store));
                }
  
                // redirect to searched page
                self.location = page;
            }
        });
  
    })
  
    $("#admin-search-toggle").click(function() {

        $('.css-search').toggleClass('active');
        $("#admin-search").toggleClass('active');
        $("body").toggleClass('freeze');
        $("#search-text").trigger('focus')
  
        if (localStorage.getItem('history')) {
  
            var store = JSON.parse(localStorage.getItem('history'));
  
            $('#search-history').html('');
  
            $.each(store.reverse(), function (index, value) {
                $('#search-history').append(
                    '<div class="admin-search__history-item">' +
                        '<a href="' + value.page + '" title="' + value.label + '" class="admin-search__history-item-lbl">' + value.label + '</a>' +
                        '<a href="' + value.module + '" title="' + value.category + '" class="admin-search__history-item-cat">' + value.category + '</a>' +
                    '</div>'
                );
            });
        }
    });
    
    $(".close-admin-search").click(function() {
        $('.css-search').toggleClass('active');
        $("#admin-search").toggleClass('active');
        $("body").toggleClass("freeze");
    });
  }

});