<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// print_r($_POST);
// exit();

// Run the save or delete action
if ( !isset($_POST['delete_post']) ) {
    // If the action isn't delete...
    
    // If the "module" post var has been set
    if ( isset($_POST['module']) ) {
        // Create variable from post var
        $module = $_POST['module'];
        
        // Unset post var so it doesn't save as meta
        // (should also happen in $unset_array below, just in case)
        unset($_POST['module']);
        
        // Check for functions file of the current module, and include if it exists
        if ( file_exists(FS_ROOT.'admin/modules/'.$module.'/mod_funcs.php') ) {
            include_once FS_ROOT.'admin/modules/'.$module.'/mod_funcs.php';
        } else if ( file_exists(FS_ROOT.'admin/modules/core/'.$module.'/mod_funcs.php') ) {
            include_once FS_ROOT.'admin/modules/core/'.$module.'/mod_funcs.php';
        }
        
        // If the module has a beforeSave function, run it to manipulate the post vars
        if ( function_exists('beforeSave') ) {
            $_POST = beforeSave($_POST);
        }
    }
    
    // Determine if this is a new post to be inserted, or a currently existing post to be updated.
    // - If post_id is greater than zero, then it must be a post.
    $form_type = ($_POST['post_id'] > 0) ? 'edit' : 'add';
    
    // If post_alt_title is blank, use post_title in it's place (used on some modules)
    if ( !isset($_POST['post_alt_title']) || $_POST['post_alt_title'] == '' ) {
        $_POST['post_alt_title'] = $_POST['post_title'];
    }
    
    if ( $form_type == 'edit' ) {        
        $doSQL = $pdo->prepare("UPDATE er_posts SET
            post_status = :post_status,
            post_type = :post_type,
            parent_id = :parent_id,
            date_modified = NOW(),
            post_slug = :post_slug,
            menu_order = :menu_order,
            post_author = :post_author,
            post_title = :post_title,
            post_alt_title = :post_alt_title
            WHERE _id = :post_id
        ");

        $doSQL->execute([
            'post_status' => $_POST['post_status'],
            'post_type' => $_POST['post_type'],
            'parent_id' => $_POST['parent_id'],
            'post_slug' => $_POST['post_slug'],
            'menu_order' => $_POST['menu_order'],
            'post_author' => $_SESSION['eruid'],
            'post_title' => $_POST['post_title'],
            'post_alt_title' => $_POST['post_alt_title'],
            'post_id' => $_POST['post_id']
        ]);
        
        // Store the post_id as a variable.
        $post_id = $_POST['post_id'];
        
        // Set the action for the success message.
        $action = 'updated';
        
    } else if ( $form_type == 'add' ) {
        $doSQL = $pdo->prepare("INSERT INTO er_posts(
            date_created,
            post_status,
            post_type,
            parent_id,
            date_modified,
            post_slug,
            menu_order,
            post_author,
            post_title,
            post_alt_title
        )
        VALUES (
            NOW(),
            :post_status,
            :post_type,
            :parent_id,
            NOW(), 
            :post_slug,
            :menu_order,
            :post_author,
            :post_title,
            :post_alt_title
        )");

        $doSQL->execute([
            'post_status' => $_POST['post_status'],
            'post_type' => $_POST['post_type'],
            'parent_id' => $_POST['parent_id'],
            'post_slug' => $_POST['post_slug'],
            'menu_order' => $_POST['menu_order'],
            'post_author' => $_POST['post_author'],
            'post_title' => $_POST['post_title'],
            'post_alt_title' => $_POST['post_alt_title']
        ]);
        
        // Store the newly generated post_id as a variable.
        $post_id = $pdo->lastInsertId();
        
        // Set the action for the success message.
        $action = 'created';
    }
    
    $return_to = $_POST['return_to'] . '?_id=' . $post_id;
    
    // Store the post_type for use in the success message.
    $post_type = $_POST['post_type'];

    // Clear out all of the main post keys/values and button keys/values, leaving only the postmeta keys/values remaining in $_POST.
    $unset_array = ['post_status', 'post_type', 'parent_id', 'post_slug', 'menu_order', 'post_author', 'post_title', 'post_alt_title', 'post_id', 'save_form', 'save_form_return', 'delete_post', 'return_to', 'module'];
    foreach ( $unset_array as $item ) {
        unset($_POST[$item]);
    }
    
    // Array to store changed / submitted meta
    $changedMeta = [];
    
    // Loop through the remaining $_POST keys/values, which should all be postmeta
    foreach ( $_POST as $meta_name => $meta_value ) {
        
        $checkMeta = $pdo->prepare("SELECT * FROM er_postmeta WHERE post_id = ? AND meta_name = ? LIMIT 1");
        $checkMeta->execute([$post_id, $meta_name]);
        
        // If the meta is an array, convert it to json
        /*
            This will store an array as JSON by default, but should you prefer to store it a different
            way then create a beforeSave function in the module's mod_funcs.php file and manipulate it
            there, then store it overwriting the $_POST variable. Ask me if you're unsure!      - James
        */
        if ( is_array($meta_value) ) {
            $meta_value = json_encode($meta_value, JSON_UNESCAPED_SLASHES);
        }
        
        if ( $checkMeta->rowCount() > 0 ) {
            
            // If the meta_name already exists, update it.
            if ( trim($meta_value) == '' ) {
                // If the meta_value to be saved is empty, just delete the record from the table to save space.
                $putMeta = $pdo->prepare("DELETE FROM er_postmeta WHERE post_id = :post_id AND meta_name = :meta_name");
                $needsVal = false;
            } else {
                // If the meta_value to be saved is not empty, update the record.
                $putMeta = $pdo->prepare("UPDATE er_postmeta SET meta_value = :meta_value WHERE post_id = :post_id AND meta_name = :meta_name");
                $needsVal = true;
            }
        } else {
            // If the meta_name doesn't exist, insert it.
            if ( trim($meta_value) != '' ) {
                $putMeta = $pdo->prepare("INSERT INTO er_postmeta(post_id, meta_name, meta_value) VALUES (:post_id, :meta_name, :meta_value)");
                $needsVal = true;
            }
        }
        
        // If there's a query to run, go ahead.
        if ( isset($putMeta) ) {
            // Bind the parameters for the insert/update/delete query.
            $putMeta->bindParam(':post_id',$post_id,PDO::PARAM_INT);
            $putMeta->bindParam(':meta_name',$meta_name,PDO::PARAM_STR);
            
            // If not deleting, bind the meta_value parameter too.
            if ( isset($needsVal) && $needsVal == true ) {
                $putMeta->bindParam(':meta_value',$meta_value,PDO::PARAM_STR);
            }
            
            // Run the query with the correct parameters.
            $putMeta->execute();
            
            // Clear out the $putMeta variable, ready for the next postmeta.
            unset($putMeta);
        }
        
        // Make a record of changed meta
        $changedMeta[] = $meta_name;
        
        // Clear out this meta from $_POST
        unset($_POST[$meta_name]);
    }
    
    // Check database for any meta tied to the post that hasn't been submitted, and delete it
    if ( count($changedMeta) > 0 ) {
        $in = str_repeat('?, ', count($changedMeta) - 1) . '?';
        $cleanupMeta = $pdo->prepare("DELETE from er_postmeta WHERE post_id = ? AND meta_name NOT IN ($in)");
        array_unshift($changedMeta, $post_id);
        $cleanupMeta->execute($changedMeta);
    }
    
} else {
    // If the action is delete...
    $deleteSQL = ["DELETE FROM er_postmeta WHERE post_id = :post_id", "DELETE FROM er_posts WHERE _id = :post_id"];
    
    // Run a query to delete the main post from er_posts, and another query to delete any relevent meta from er_postmeta.
    foreach( $deleteSQL as $query ) {
        $doDelete = $pdo->prepare($query);
        $doDelete->execute(['post_id' => $_POST['post_id']]);
    }
    
    // Set the action for the success message.
    $action = 'deleted';
    
    // Store the post_type for use in the success message.
    $post_type = $_POST['post_type'];
    
    $return_to = $_POST['return_to'];
}

// If post_type is image, output json response
if ( $post_type == 'image' ) {
    echo json_encode([
        'img_src'=>$img_src,
        'img_alt' => $img_alt,
        'is_modal' => ( (isset($is_modal)) ? true : false ),
        'success_msg' => 'Image successfully uploaded',
        'img_id' => $post_id
    ]);
    exit();
} else {
    // If not an image, set success message
    if ( isset($action) ) {
        $_SESSION['success'] = ucfirst($post_type) . ' successfully ' . $action;
    }
}

// Run the redirect (as set above).
header("Location: ".$return_to);