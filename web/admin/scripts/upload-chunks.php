<?
require_once("../includes/inc_config.php");
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

	$CurrentBlob = XSStrapper($_POST['currentblob']);
	$Filename = $_POST['filename'];//XSStrapper($_POST['filename']);
	$destfolder = XSStrapper($_POST['destfolder']);
	
	$destpath = explode('/', trim($destfolder,'/'));
	
	// Check if each folder in the path exists
	foreach ( $destpath as $key => $folder ) {
    	$prev = '';
    	if ( $key > 0 ) {
        	for ( $i = 0; $i < $key; $i++ ) {
            	$prev .= $destpath[$i] . '/';
        	}
        }
        
        // Check if folder exists
        if ( !is_dir(FS_ROOT.$prev.$folder) ) {
            mkdir(FS_ROOT.$prev.$folder, 0777);
        }
        
        // If this is the last in the line, check / add the temp folder, too.
        if ( $key == count($destpath) - 1 ) {
            if ( !is_dir(FS_ROOT.$prev.$folder.'/temp') ) {
                mkdir(FS_ROOT.$prev.$folder.'/temp');
            }
        }
	}

/*
	// check folder exists
	if (!is_dir( FS_ROOT.$destfolder )) {
		mkdir(FS_ROOT.$destfolder, 0777);
	}

	//check folder exists
	$destfolder = $destfolder . "/temp";
	if (!is_dir( FS_ROOT.$destfolder )) {
		mkdir(FS_ROOT.$destfolder, 0777);
	}
*/

	foreach ($_FILES as $fieldname => $file) {

		if (move_uploaded_file($_FILES[$fieldname]['tmp_name'], FS_ROOT.$destfolder."/temp/" . $Filename . ".part" . $CurrentBlob )) {
			chmod( FS_ROOT.$destfolder."/temp/" . $Filename . ".part" . $CurrentBlob ,0777);
		}

	}



?>