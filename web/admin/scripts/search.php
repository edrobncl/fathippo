<?php

// Author: Chris
// Created: 2019
// Modified: 2/08/2019

// Include configuration file
include "../includes/inc_config.php";

// Define required variables
$results = []; // holds query results
$binds = []; // holds binding parameters
$modules = []; // holds name of the modules
$post_types = []; // holds all avaialble post types

// will ignore files/directories with these names
$ignore = [
  ".",
  "..",
  ".DS_Store",
  "._.DS_Store",
  // "categories" // you can ignore specific directories if you wish
];

// Scan available modules
$dir = scandir(FS_ADMIN_ROOT."/modules");

// Loop through each file/folder name found in the modules directory
foreach ($dir as $key => $module) {
  // If name is not in $ignore array
  if (!in_array($module, $ignore)) {
    // If name is core, step inside
    if ($module == "core") {
      // Scan core directory
      $dir = scandir(FS_ADMIN_ROOT."/modules/$module");
      // Loop through each file/folder name found in the core directory
      foreach ($dir as $ckey => $cmodule) {
        // If name is not in $ignore array
        if (!in_array($cmodule, $ignore)) {
          // Add module name to $modules variable
          $modules[] = "core/$cmodule";
        }
      }
    } else {
      // Add module name to $modules variable
      $modules[] = $module;
    }
  }
}

// Loop through $modules variable
foreach ($modules as $key => $module) {
  // Check if mod_settings.php file exists
  if (file_exists(FS_ADMIN_ROOT."modules/$module/mod_settings.php")) {
    // Include the module's settings file
    include FS_ADMIN_ROOT."/modules/$module/mod_settings.php";
    // Check if module settings post type has been defined
    if (isset($module_settings["post_type"])) {
      // Grab the post type
      $post_types[] = $module_settings["post_type"];
    }
  }
}

// Build SQL query
$query = "
  SELECT
    _id,
    post_title,
    post_type
  FROM
    er_posts
  WHERE ";

  // Loop through each post_type in $post_types array
  foreach ($post_types as $key => $post_type) {
    // Add condition to query
    $query .= " post_type = ? OR ";
    // Add binding parameter for this condition
    $binds[] = $post_type;
  }

// Remove last ' OR ' from the query
$query = rtrim($query, " OR ");

// Append 'ORDER BY' to the query
$query .= "
  ORDER BY
    post_type ASC,
    post_title ASC
";

// TESTING
// var_dump($query);
// var_dump($binds);

// Prepare PDO statement
$stmt = $pdo->prepare($query);

// Bind and execute PDO statement
$stmt->execute($binds);

// Loop through results
// Warning: it will fetch ALL results... could be 1000s... needs improvement, anyone?
while ($row = $stmt->fetch()) {
  // Add result to results array
  $results[] = [
    "label" => $row['post_title'],
    "category" => $row['post_type'],
    "_id" => $row['_id']
  ];
}

// Set content type
header('Content-Type: application/json');
// Encode results and print
echo json_encode($results);