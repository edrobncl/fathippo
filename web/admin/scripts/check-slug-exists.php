<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

if ( $_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['slug']) && isset($_POST['parent']) && isset($_POST['id']) ) {
    $slug = $_POST['slug'];
    $parent = $_POST['parent'];
    $post_id = $_POST['id'];

    $findSlug = $pdo->prepare("SELECT * FROM er_posts WHERE post_slug = ? AND parent_id = ? AND _id != ?");
    
    function findSlug($slug,$parent,$num = 0) {
        global $pdo;
        global $findSlug;
        global $post_id;

        if ( $num > 0 ) {
            $newSlug = $slug . '-' . $num;
        } else {
            $newSlug = $slug;
        }

        $findSlug->execute([$newSlug,$parent,$post_id]);

        if ( $findSlug->rowCount() == 0 ) {
            return $newSlug;
        } else {
            return findSlug($slug,$parent,$num+1);
        }
    }

    $response['slug'] = findSlug($slug,$parent);
    echo json_encode($response);
    exit();
} else {
    http_response_code(400);
    exit();
}