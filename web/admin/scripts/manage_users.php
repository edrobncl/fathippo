<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

// Run the save or delete action
if ( !isset($_POST['delete_user']) ) {
    // If the action isn't delete...
    
    // Determine if this is a new post to be inserted, or a currently existing post to be updated.
    // - If post_id is greater than zero, then it must be a post.
    $form_type = ($_POST['user_id'] > 0) ? 'edit' : 'add';
    
    if ( isset($_POST['password']) && $_POST['password'] != '' ) {
        $pass = password_hash($_POST['password'], PASSWORD_BCRYPT);
    } else {
        $pass = '';
    }
    
    if ( $form_type == 'edit' ) {
        
        if ( $pass != '' ) {
            $extraSQL = ", password = :password";
        } else {
            $extraSQL = '';
        }
        
        $doSQL = $pdo->prepare("UPDATE er_users SET username = :username, active = :active, level = :level".$extraSQL." WHERE _id = :user_id");
        
        if ( $pass != '' ) {
            $doSQL->execute(['username'=>$_POST['username'], 'password' => $pass, 'active' => $_POST['active'], 'level' => $_POST['level'], 'user_id' => $_POST['user_id']]);
        } else {
            $doSQL->execute(['username'=>$_POST['username'], 'active' => $_POST['active'], 'level' => $_POST['level'], 'user_id' => $_POST['user_id']]);
        }
        
        // Store the user_id as a variable.
        $user_id = $_POST['user_id'];
        
        // Set the action for the success message.
        $action = 'updated';
        
    } else if ( $form_type == 'add' ) {
                
        $doSQL = $pdo->prepare("INSERT INTO er_users(
            username,
            password,
            active,
            level,
            created_at,
            last_login
        )
        VALUES (
            :username,
            :password,
            :active,
            :level,
            NOW(),
            NOW()
        )");

        $doSQL->execute([
            'username' => $_POST['username'],
            'password' => $pass,
            'active' => $_POST['active'],
            'level' => $_POST['level']
        ]);
        
        // Store the newly generated user_id as a variable.
        $user_id = $pdo->lastInsertId();
        
        // Set the action for the success message.
        $action = 'created';
    }
    
    // Store the username as a variable
    $username = $_POST['username'];
    
    // Set $return_to, which is where to go after the script runs  [ needs work for ajax! ]
    if ( isset($_POST['save_form']) ) {
        $return_to = $_POST['return_to'] . '?_id=' . $user_id;
    } else if ( isset($_POST['save_form_return']) || isset($_POST['delete_post']) ) {
        $return_to = dirname($_POST['return_to']).'/';
    }
    
    // Clear out all permissions (and add back the ones we want below)
    $removePermissions = $pdo->prepare("DELETE FROM er_permissions WHERE _id = :user_id");
    $removePermissions->execute(['user_id'=>$user_id]);

    // Loop through permissions and add them to database
    if ( isset($_POST['permissions']) && !empty($_POST['permissions']) ) {
        
        foreach ( $_POST['permissions'] as $key => $module ) {
            $putModule = $pdo->prepare("INSERT INTO er_permissions(_id,module_name) VALUES (:user_id,:module)");
            $putModule->execute(['user_id'=>$user_id,'module'=>$module]);
        }
    }
    
    // Clear out all of the main user keys/values and button keys/values, leaving only the usermeta keys/values remaining in $_POST.
    $unset_array = ['user_id','username', 'password', 'active', 'level', 'created_at', 'last_login','save_form', 'save_form_return', 'delete_post', 'return_to' ,'permissions'];
    foreach ( $unset_array as $item ) {
        unset($_POST[$item]);
    }
    
    // Loop through the remaining $_POST keys/values, which should all be postmeta
    foreach ( $_POST as $meta_name => $meta_value ) {
        
        $checkMeta = $pdo->prepare("SELECT * FROM er_usermeta WHERE user_id = ? AND meta_name = ? LIMIT 1");
        $checkMeta->execute([$user_id, $meta_name]);
        
        // If the meta is an array, convert it to json
        if ( is_array($meta_value) ) {
            $meta_value = json_encode($meta_value);
        }
        
        if ( $checkMeta->rowCount() > 0 ) {
            
            // If the meta_name already exists, update it.
            if ( trim($meta_value) == '' ) {
                // If the meta_value to be saved is empty, just delete the record from the table to save space.
                $putMeta = $pdo->prepare("DELETE FROM er_usermeta WHERE user_id = :user_id AND meta_name = :meta_name");
                $needsVal = false;
            } else {
                // If the meta_value to be saved is not empty, update the record.
                $putMeta = $pdo->prepare("UPDATE er_usermeta SET meta_value = :meta_value WHERE user_id = :user_id AND meta_name = :meta_name");
                $needsVal = true;
            }
        } else {
            // If the meta_name doesn't exist, insert it.
            if ( trim($meta_value) != '' ) {
                $putMeta = $pdo->prepare("INSERT INTO er_usermeta(user_id, meta_name, meta_value) VALUES (:user_id, :meta_name, :meta_value)");
                $needsVal = true;
            }
        }
        
        // If there's a query to run, go ahead.
        if ( isset($putMeta) ) {
            // Bind the parameters for the insert/update/delete query.
            $putMeta->bindParam(':user_id',$user_id,PDO::PARAM_INT);
            $putMeta->bindParam(':meta_name',$meta_name,PDO::PARAM_STR);
            
            // If not deleting, bind the meta_value parameter too.
            if ( isset($needsVal) && $needsVal == true ) {
                $putMeta->bindParam(':meta_value',$meta_value,PDO::PARAM_STR);
            }
            
            // Run the query with the correct parameters.
            $putMeta->execute();
            
            // Clear out the $putMeta variable, ready for the next postmeta.
            unset($putMeta);
        }
        
        // Clear out this meta from $_POST
        unset($_POST[$meta_name]);
    }
    
} else {
    $user_id = $_POST['user_id'];
    
    // Get the username for the "X successfully deleted" message
    $getUsername = $pdo->prepare("SELECT username FROM er_users WHERE _id = :user_id LIMIT 1");
    $getUsername->execute(['user_id'=>$user_id]);
    $user = $getUsername->fetch();
    $username = $user['username'];
    
    // Remove all permissions for this user
    $removePermissions = $pdo->prepare("DELETE FROM er_permissions WHERE _id = :user_id");
    $removePermissions->execute(['user_id'=>$user_id]);
    
    // Remove all meta for this user
    $removeUserMeta = $pdo->prepare("DELETE FROM er_usermeta WHERE user_id = :user_id");
    $removeUserMeta->execute(['user_id'=>$user_id]);
    
    // Remove the user
    $removeUser = $pdo->prepare("DELETE FROM er_users WHERE _id = :user_id LIMIT 1");
    $removeUser->execute(['user_id'=>$user_id]);
    
    // Set the action for the success message.
    $action = 'deleted';
    
    $return_to = $_POST['return_to'];
}


// Set success message
if ( isset($action) ) {
    $_SESSION['success'] = '<strong>' . $username . '</strong> successfully ' . $action;
}

// Run the redirect (as set above).
header("Location: ".$return_to);