<?
require_once("../includes/inc_config.php");
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

$response = [];


	// GET POSTED VALUES
	$NumBlobs = XSStrapper($_POST['numblobs']);
	$Filename = $_POST['filename'];//XSStrapper($_POST['filename']);
	$destfolder = XSStrapper($_POST['destfolder']);

	// SET PATHS
	$temp_dir = FS_ROOT .$destfolder."/temp/";
	$final_dir = FS_ROOT .$destfolder."/temp/";

	// RECOMBINE PARTS
	// when combined add it to the database and move the file
	if (($fp = fopen($temp_dir.$Filename, 'w')) !== false) {
		for ($i=1; $i<=$NumBlobs; $i++) {
			if (file_exists($temp_dir.'/'.$Filename.'.part'.$i)) {
				fwrite($fp, file_get_contents($temp_dir.'/'.$Filename.'.part'.$i));
				unlink($temp_dir.'/'.$Filename.'.part'.$i);
			}
		}
		fclose($fp);

		// get some file details
		$this_file_name = $Filename;
		$temp = explode(".",$this_file_name);
		$this_file_ext = strtolower(array_pop($temp));
		$this_file_size = filesize($temp_dir.$Filename);

        // rename to timestamp and move to final_dir
        // $new_filename = preg_replace("/[\s]/", "-",preg_replace("/[\s-]+/"," ",$Filename));//time() . "." . $this_file_ext;
        $new_filename = strtolower(trim(preg_replace('/\s+/', '-', $Filename)));

        if (rename($temp_dir.$Filename, $final_dir.$new_filename)) {
            chmod( $final_dir.$new_filename ,0777);
        }

        // return temp filename
        echo json_encode(['filename'=>$new_filename]);

	} else {
		return false;
	}




?>