<?php

if ( $_SERVER['REQUEST_METHOD'] === 'POST' && !isset($_POST['delete_post']) ) {

    $revPost = $_POST;

    $revDate = date('Y-m-d H:i:s');

    // Create revision to be saved
    $revToSave = [
        'rev_date' => $revDate,
        'post_id' => $revPost['post_id'],
        'post_data' => json_encode([
            'post_status' => $revPost['post_status'],
            'post_type' => $revPost['post_type'],
            'parent_id' => $revPost['parent_id'],
            'date_modified' => $revDate,
            'post_slug' => $revPost['post_slug'],
            'menu_order' => $revPost['menu_order'],
            'post_author' => $revPost['post_author'],
            'post_title' => $revPost['post_title'],
            'post_alt_title' => $revPost['post_alt_title']
        ]),
        'post_meta' => []
    ];

    // Clear out non-meta
    $rev_unset_array = ['post_status', 'post_type', 'parent_id', 'post_slug', 'menu_order', 'post_author', 'post_title', 'post_alt_title', 'post_id', 'save_form', 'save_form_return', 'delete_post', 'return_to', 'module'];
    foreach ( $rev_unset_array as $item ) {
        unset($revPost[$item]);
    }

    // Add meta to revision
    $revToSave['post_meta'] = json_encode($revPost);

    $insertRev = $pdo->prepare("INSERT INTO er_revisions (post_id, rev_date, post_data, post_meta) VALUES (?,?,?,?)");
    $insertRev->execute([$revToSave['post_id'], $revToSave['rev_date'], $revToSave['post_data'], $revToSave['post_meta']]);

    $checkRevs = $pdo->prepare("SELECT * FROM er_revisions WHERE post_id = ? ORDER BY rev_date ASC");
    $checkRevs->execute([$revToSave['post_id']]);

    if ( $checkRevs->rowCount() > 5 ) {
        $revs = $checkRevs->fetchAll();
        $delRev = $pdo->prepare("DELETE FROM er_revisions WHERE _id = ?");
        $delRev->execute([$revs[0]['_id']]);
    }
}

/*require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

    // Save revision

    // Store the main post_id to use as the revision's parent_id
    $original_post_id = $_POST['post_id'];

    // Run "beforeSave" if it exists in the module's functions
    if ( isset($_POST['module']) ) {
        $module = $_POST['module'];
        unset($_POST['module']);

        if ( file_exists(FS_ROOT.'admin/modules/'.$module.'/mod_funcs.php') ) {
            include_once FS_ROOT.'admin/modules/'.$module.'/mod_funcs.php';
        } else if ( file_exists(FS_ROOT.'admin/modules/core/'.$module.'/mod_funcs.php') ) {
            include_once FS_ROOT.'admin/modules/core/'.$module.'/mod_funcs.php';
        }

        if ( function_exists('beforeSave') ) {
            $_POST = beforeSave($_POST);
        }
    }

    // Unset "post_id" as no longer required
    unset($_POST['post_id']);

    $saveRev = $pdo->prepare("INSERT INTO er_posts (
        date_created, post_status, post_type,
        parent_id, date_modified, post_slug,
        menu_order, post_author, post_title,
        post_alt_title
    ) VALUES (
        NOW(), :post_status, :post_type,
        :parent_id, NOW(), :post_slug,
        :menu_order, :post_author, :post_title,
        :post_alt_title
    )");

    // $saveRev->execute([
    //     'post_status' => $original_post_id,
    //     'post_type' => 'revision',
    //     'parent_id' => $_POST['parent_id'],
    //     'post_slug' => $_POST['post_slug'],
    //     'menu_order' => $_POST['menu_order'],
    //     'post_author' => $_POST['post_author'],
    //     'post_title' => $_POST['post_title'],
    //     'post_alt_title' => $_POST['post_alt_title']
    // ]);

    // Count revisions for this post
    // If more than 5, delete the oldest

    $_SESSION['posted'] = $_POST;
    echo json_encode(['saved' => true, 'time' => date('Y-m-d H:i:s')]);

} else {

    echo '<pre style="clear:both;">';
    print_r($_SESSION['posted']);
    echo '</pre>';

    unset($_SESSION['posted']);

}

// $data = [];
// if ( $_SERVER['REQUEST_METHOD'] === 'GET' ) {
//     $_SESSION['rev_data'] = $_GET;
// } else if ( isset($_SESSION['rev_data']) ) {
//     $data = $_SESSION['rev_data'];
// }

// // Check if post is coming via ajax (as a revision)
// $source = '';
// if ( isset($data['source']) && $data['source'] == 'rev_js' ) {
//     $source = 'rev_js';
// }

// // Print out data for testing
// if ( !empty($data) && $source != 'rev_js' ) {
//     echo '<pre style="clear:both;">';
//     print_r($data);
//     echo '</pre>';
// }

// // Run through save


// // If saving via ajax, return the date/time of the save
// if ( $source == 'rev_js' ) {
//     echo json_encode(['saved' => true, 'time' => date('Y-m-d H:i:s')]);
// }*/