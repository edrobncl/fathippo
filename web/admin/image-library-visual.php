
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
    	<meta name="HandheldFriendly" content="True">
    	<meta name="MobileOptimized" content="320">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    	<meta name="author" content="Edward Robertson - www.edwardrobertson.co.uk" />
        <meta name="description" content="The Edward Robertson CMS Dashboard" />
        
        <title>Image library : Site Name : Dashboard</title>
                
        <link rel="stylesheet" href="/admin/css/core.min.css?v=1615999713" media="all" />
        <link rel="stylesheet" href="/admin/css/additional.min.css?v=1615999421" media="all" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">        
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/admin/js/fancybox/source/jquery.fancybox.css" media="screen" />
        
                                <meta name="msapplication-TileColor" content="#fff" />
        
        <meta name="robots" content="noindex">
    </head>
    <body class="dev-site superuser">
        <div id="admin-search"></div>
        
        <div id="page-wrap">
            
            <header id="header">
                <a class="header__logo" target="_blank" href="https://www.edwardrobertson.co.uk"><img src="/admin/images/logo-white-blue-dots.png" /></a>
                <div id="nav-toggle"><i class="fa fa-bars"></i><b>Menu</b></div>
                <a class="header__link header__link--dashboard" href="/admin/"><i class="fa fa-dashboard"></i><b>Dashboard</b></a>
                <a class="header__link" href="/admin/modules/core/settings/"><i class="fa fa-cog"></i><b>Settings</b></a>
                <a class="header__link" id="admin-search-toggle" target="_blank">
                    <div class="css-shape css-search" role="button"><span></span><span></span><span></span></div>
                    <b>Search</b>
                </a>
                <a class="header__link" href="/" target="_blank"><i class="fa fa-globe"></i><b>Website</b></a>
                <div class="dev-site-notice">Dev Admin</div>                <a class="header__link header__link--signout" href="/admin/logout"><i class="fa fa-sign-out"></i><b>Logout</b></a>
            </header>
            
            <nav id="nav" class="nav-">
    <ul class="main-nav">
        <li class="nav__item core"><a class="nav__link" href="/admin/modules/core/articles/"><i class="fa fa-newspaper-o"></i> Articles</a></li><li class="nav__item core"><a class="nav__link" href="/admin/modules/core/file-manager/"><i class="fa fa-file-o"></i> File Manager</a></li><li class="nav__item core active"><a class="nav__link" href="/admin/modules/core/image-library/"><i class="fa fa-picture-o"></i> Image Library</a></li><li class="nav__item core"><a class="nav__link" href="/admin/modules/core/pages/"><i class="fa fa-file-text-o"></i> Pages</a></li><li class="nav__item core"><a class="nav__link" href="/admin/modules/core/social-media/"><i class="fa fa-share-alt"></i> Social Media</a></li><li class="nav__item core"><a class="nav__link" href="/admin/modules/core/users/"><i class="fa fa-users"></i> Users</a></li><li class="nav__item "><a class="nav__link" href="/admin/modules/homepage-slider/"><i class="fa fa-picture-o"></i> Homepage Slider</a></li>    </ul>
</nav>			<div id="dashboard"><div class="image-library-wrap wrap" id="image-library">
   
        
        <?php if(!$_GET['image']) { ?>
            <div class="image-library-nav">
                <a class="button" href="#upload" style="display: none;"><i class="fa fa-upload"></i> Upload New Image</a><a class="button" href="#manage" style="display: none;"><i class="fa fa-folder-o"></i> Manage Images</a>
                </div>
        <? } ?>
            
    
    <div class="wrap image-library-tab upload">        
        <?php if(!$_GET['image']) { ?><div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-upload"></i> Upload Image</h1>
    	</div><? } ?>
        
        
    </div><!-- upload tab -->
    
    <div class="wrap image-library-tab manage active">
    <div class="page-headers">
        <h1 class="page-headers__title"><i class="fa fa-picture-o"></i> <?php if(!$_GET['image']) { ?>Image Library<? } else { ?>Name of Image<? } ?></h1>
        <?php if($_GET['image']) { ?><a class="button" href="/admin/image-library-visual.php"><i class="fa fa-angle-left"></i> Back to Image Library</a><? } ?>
        <?php if(!$_GET['image']) { ?><div class="image-sorting">
            <div class="image-sorting__title">Sort by:</div>
            <div class="image-sorting__option">
                <input class="image-sorting__option__radio" type="radio" name="sort-images" id="sort-by-date" checked>
                <label class="image-sorting__option__label" for="sort-by-date">Date</label>
                <div class="image-sorting__option__fancyradio"></div>
            </div>
            <div class="image-sorting__option">
                <input class="image-sorting__option__radio" type="radio" name="sort-images" id="sort-by-name" >
                <label class="image-sorting__option__label" for="sort-by-name">Image Name</label>
                <div class="image-sorting__option__fancyradio"></div>
            </div>
        </div><? } ?>
    </div>
        
    <style>
        .image-listings {
            display: grid;
            grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
            grid-gap: 1.5rem;
            padding:0 1rem 1rem 1rem;
        }
        
        .image-listings__item {
            background:#fff;
            border:1px #ccc solid;
            overflow:hidden;
            border-radius:4px;
            box-shadow:2px 2px 2px rgba(0,0,0,.05);
            cursor:pointer;
        }
        .image-listings__item:hover {
            box-shadow:0px 0px 10px rgba(0,0,0,.15);

        }
        .image-listings__item__image  {
            background-image: linear-gradient(45deg, #f6f6f5 25%, transparent 25%), linear-gradient(-45deg, #f6f6f5 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #f6f6f5 75%), linear-gradient(-45deg, transparent 75%, #f6f6f5 75%);
            background-size: 20px 20px;
            background-position: 0 0, 0 10px, 10px -10px, -10px 0px;
            padding:1rem;
            height:180px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .image-listings__item__image img {
            filter: drop-shadow(0 2px 2px rgba(0, 0, 0, 0.2));
        }
        .image-listings__details {
            border-top:1px #ccc solid;
            padding:1rem;
        }
        .image-listings__details__title {
            font-weight:bold;
            color:#263237;
        }
        .image-listings__details__date {
            color:#666;
            font-size:.875rem;
        }
        .image-full-details {
            display: grid;
            grid-template-columns: 1fr 2fr;
            grid-gap: 3rem;
        }
        .image-full-details__overview {
            background:#fff;
            border:1px #2d3b42 solid;
            overflow:hidden;
            border-radius:4px;
            box-shadow:2px 2px 2px rgba(0,0,0,.05);
            text-align:center;
        }
        .image-full-details__image {
            min-height:300px;
        }
        .image-full-details__image,
        .image-full-details__option__image {
            background-image: linear-gradient(45deg, #f6f6f5 25%, transparent 25%), linear-gradient(-45deg, #f6f6f5 25%, transparent 25%), linear-gradient(45deg, transparent 75%, #f6f6f5 75%), linear-gradient(-45deg, transparent 75%, #f6f6f5 75%);
            background-size: 20px 20px;
            background-position: 0 0, 0 10px, 10px -10px, -10px 0px;
            padding:2rem;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .image-full-details__option__image img,
        .image-full-details__image img {
            filter: drop-shadow(0 2px 2px rgba(0, 0, 0, 0.2));
        }
        .image-full-details__content {
            background:#2d3b42;
            color:#fff;
            padding:1.5rem;
        }
        .image-full-details__title{
            font-weight:bold;
        }
        .image-full-details__summary {
            margin-bottom:1em;
        }
        .image-full-details__optionstitle h3 {
            font-size:1.3em;
            margin:0;
        }
        .image-full-details__option {
            background:#fff;
            border:1px #ccc solid;
            overflow:hidden;
            border-radius:4px;
            box-shadow:2px 2px 2px rgba(0,0,0,.05);
            display: grid;
            grid-template-columns: 1fr 2fr;
            margin:1rem 0;
        }
        .image-full-details__option__details {
            padding:1.5rem;
            
        }
        .image-full-details__option__details strong {
            color:#263237;
        }

        @media screen and (max-width: 1580px){
            .image-listings {
                grid-template-columns: 1fr 1fr 1fr 1fr;
            }
        }
        @media screen and (max-width: 1280px){
            .image-listings {
                grid-template-columns: 1fr 1fr 1fr;
            }
            .image-full-details {
                display:block;
            }
            .__image-full-details__overview {
                margin-bottom:1.5em;
            }
        }
        @media screen and (max-width: 800px){
            .image-listings {
                grid-template-columns: 1fr 1fr;
            }
        }
    </style>

        <div class="wrap tab-content normal-overflow">
            <div class="wrap tab-content__content">
                
                <?php if($_GET['image']) { ?>
                    <div class="image-full-details">
                        <div class="__image-full-details__overview">
                            <div class="image-full-details__overview">
                                <div class="image-full-details__image">
                                    <img src="https://www.edwardrobertson.co.uk/images/edward-robertson-logo-white.png" alt="Example Title" />
                                </div>
                                <div class="image-full-details__content">
                                    <div class="image-full-details__title">
                                    Title of Image
                                    </div>
                                    <div class="image-full-details__summary">
                                    Uploaded: Date here
                                    </div>
                                    <a href="asdas" class="button action delete button--delete" style="cursor:pointer;background: #e74035;border-color: #e74035;">Delete Image</a>
                                </div>
                            </div>
                        </div>
                        <div class="image-full-details__options">
                            <div class="image-full-details__optionstitle">
                                <h3>3 Image options</h3>
                            </div>
                            <div class="image-full-details__option">
                                <div class="image-full-details__option__image">
                                    <img src="https://www.edwardrobertson.co.uk/images/edward-robertson-logo-white.png" alt="Example Title" />
                                </div>
                                <div class="image-full-details__option__details">
                                    <strong>Original Upload</strong>
                                    <br />/images/page/title-of-image/original.jpg
                                    <br />1600 x  800 pixels (jpg)
                                </div>
                            </div>
                            <div class="image-full-details__option">
                                <div class="image-full-details__option__image">
                                    <img src="https://www.edwardrobertson.co.uk/images/edward-robertson-logo-white.png" alt="Example Title" />
                                </div>
                                <div class="image-full-details__option__details">
                                    <strong>Thumbnail Version</strong>
                                    <br />/images/page/title-of-image/thumbnail.jpg
                                    <br />600 x  300 pixels (jpg)
                                </div>
                            </div>
                            <div class="image-full-details__option">
                                <div class="image-full-details__option__image">
                                    <img src="/admin/images/hoach-le-dinh-91107.jpg" alt="Example Title" />
                                </div>
                                <div class="image-full-details__option__details">
                                    <strong>Cropped Version</strong>
                                    <br />/images/page/title-of-image/cropped.jpg
                                    <br />900 x  200 pixels (jpg)
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                <div class="image-listings">
                    <a href="/admin/image-library-visual.php?image=1" class="image-listings__item">
                        <div class="image-listings__item__image">
                            <img src="/admin/images/hoach-le-dinh-91107.jpg" alt="Example Title" />
                        </div>
                        <div class="image-listings__details">
                            <div class="image-listings__details__title">
                                Title of Image
                            </div>
                            <div class="image-listings__details__date">
                                Uploaded 4 days ago
                            </div>
                        </div>
                    </a>
                    <a href="/admin/image-library-visual.php?image=1" class="image-listings__item">
                        <div class="image-listings__item__image">
                            <img src="https://www.edwardrobertson.co.uk/images/services-32.jpg" alt="Example Title" />
                        </div>
                        <div class="image-listings__details">
                            <div class="image-listings__details__title">
                                Title of Image
                            </div>
                            <div class="image-listings__details__date">
                                Uploaded 4 days ago
                            </div>
                        </div>
                    </a>
                    <a href="/admin/image-library-visual.php?image=1" class="image-listings__item">
                        <div class="image-listings__item__image">
                            <img src="https://www.edwardrobertson.co.uk/images/edward-robertson-logo-white.png" alt="Example Title" />
                        </div>
                        <div class="image-listings__details">
                            <div class="image-listings__details__title">
                                Title of Image
                            </div>
                            <div class="image-listings__details__date">
                                Uploaded 4 days ago
                            </div>
                        </div>
                    </a>
                    <a href="/admin/image-library-visual.php?image=1" class="image-listings__item">
                        <div class="image-listings__item__image">
                            <img src="https://www.edwardrobertson.co.uk/images/work_google_cloud.jpg" alt="Example Title" />
                        </div>
                        <div class="image-listings__details">
                            <div class="image-listings__details__title">
                                Title of Image
                            </div>
                            <div class="image-listings__details__date">
                                Uploaded 4 days ago
                            </div>
                        </div>
                    </a>
                    <a href="/admin/image-library-visual.php?image=1" class="image-listings__item">
                        <div class="image-listings__item__image">
                        <img src="/admin/images/hoach-le-dinh-91107.jpg" alt="Example Title" />
                        </div>
                        <div class="image-listings__details">
                            <div class="image-listings__details__title">
                                Title of Image
                            </div>
                            <div class="image-listings__details__date">
                                Uploaded 4 days ago
                            </div>
                        </div>
                    </a>
                    <a href="/admin/image-library-visual.php?image=1" class="image-listings__item">
                        <div class="image-listings__item__image">
                        <img src="/admin/images/hoach-le-dinh-91107.jpg" alt="Example Title" />
                        </div>
                        <div class="image-listings__details">
                            <div class="image-listings__details__title">
                                Title of Image
                            </div>
                            <div class="image-listings__details__date">
                                Uploaded 4 days ago
                            </div>
                        </div>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div><!-- manage tab -->
</div>    </div><!--dashboard-->
    <footer>
        <div id="footer">
            <p>Content Management System provided by <a href="https://www.edwardrobertson.co.uk" target="_blank">Edward Robertson web design</a></p>
        </div>
    </footer>
</div><!--page_wrap-->        
        
    <script src="/admin/js/jquery-3.2.1.min.js"></script>
    <script src="/admin/js/fancybox/source/jquery.fancybox.pack.js"></script>
        
    <script src="/admin/js/uploader.js"></script>
    
    <script src="/admin/modules/core/image-library/jquery-ui-sortable.min.js"></script>
<script src="/admin/modules/core/image-library/image_library.js?v=1608554906"></script>    
    <script src="/admin/js/admin.js?v=1606834973"></script>
    
    <script src="/admin/modules/core/file-manager/file_manager.js?v=1600182964"></script>
    <script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="/admin/js/search.js"></script>

</body>
</html>
