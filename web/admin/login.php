<?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
    
    $site_name = 'Site Name';
    
    // Define variables
    $userInfo = '';
    $permissions = [];
    
    // Do logout action if query string is set
    if ( $_SERVER['QUERY_STRING'] == 'logout' ) {
        clearLogin();
    }
    
    if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
        
        // Set username & password variables
        $username = XSSTrapper($_POST['username']);
        $password = XSSTrapper($_POST['password']);

        // If username & password variables are empty, set error
        if ( !trapCheck($username) || !trapCheck($password) ) {
            $_SESSION['loginerror'] = 'You must enter a username and password';
            clearLogin();
        } else {
                        
            // Set up initial tables, if required
            require_once(FS_ADMIN_INCLUDES.'create_initial_tables.php');
            
            // Check if the user exists, is active and is a super user or admin
            $getUser = $pdo->prepare("SELECT * FROM er_users WHERE username = ? AND active = ? LIMIT 1");
            $getUser->execute([$username, 'Yes']);
            
            if ( $getUser->rowCount() == 0 ) {
                // If the user doesn't exist, is inactive or is not a super user or admin, set an error.
                $_SESSION['loginerror'] = 'Invalid username or password';
                clearLogin();
            } else {
                $userInfo = $getUser->fetch();
                
                if ( in_array($userInfo['level'], ['superuser','admin']) ) {
                    // The account is a super user or admin, so try to login
                    
                    if ( password_verify($password, $userInfo['password']) ) {
                        
                        // Do login
                        $_SESSION['loggedin'] = 'Yes';
                        $_SESSION['userlevel'] = $userInfo['level'];
                        $_SESSION['eruid'] = $userInfo['_id'];
                        
                        if ( $userInfo['level'] == 'superuser' ) {
                            // Allow all modules
                            $items = [];
                            $core_list = scandir(FS_ADMIN_ROOT.'modules/core');
                            foreach ( $core_list as $key => $module ) {
                                if ( substr($module,0,1) != '.' && is_dir(FS_ADMIN_ROOT.'modules/core/'.$module) ) {
                                    $permissions[] = $module;
                                }
                            }
                            $module_list = scandir(FS_ADMIN_ROOT.'modules/');
                            foreach ( $module_list as $key => $module ) {
                                if ( substr($module,0,1) != '.' && $module != 'core' && is_dir(FS_ADMIN_ROOT.'modules/'.$module) ) {
                                    $permissions[] = $module;
                                }
                            }
                            $core_list = scandir(FS_ADMIN_ROOT.'modules/menus');
                            foreach ( $core_list as $key => $module ) {
                                if ( substr($module,0,1) != '.' && is_dir(FS_ADMIN_ROOT.'modules/menus/'.$module) ) {
                                    $permissions[] = $module;
                                }
                            }
                        } else if ( $userInfo['level'] == 'admin' ) {
                            // Get allocated modules for this admin
                            $getPerms = $pdo->prepare("SELECT module_name FROM er_permissions WHERE _id = ?");
                            $getPerms->execute([$userInfo['_id']]);
                            $permissions = $getPerms->fetchAll(PDO::FETCH_COLUMN);
                        }
                        
                        $_SESSION['permissions'] = $permissions;
                        
                        $doLogin = $pdo->prepare("UPDATE er_users SET last_login = NOW() WHERE _id = ?");
                        $doLogin->execute([$userInfo['_id']]);
                        
                        header("Location: /admin/");
                        exit();
                        
                    } else {
                        // Failed login
                        $_SESSION['loginerror'] = 'Invalid username or password';
                        clearLogin();
                    }
                } else {
                    // Not authorised to login
                    $_SESSION['loginerror'] = 'Sorry, you\'re not authorised to login here.';
                    clearLogin();
                }
            }
        }

    }

    // Check if tables have been created. If not, set superuser name for login box
    $init_check = $pdo->query("SHOW TABLES LIKE 'er_users'")->fetch();
    if ( !$init_check ) {
        $uname = 'edrob_';
        $sub = substr($_SERVER['HTTP_HOST'],0,3);
        $uname .= $sub;
    } else {
        $uname = '';
    }
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>

	<meta charset="utf-8" />
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet">  
	<title><?=($site_name != 'Site Name') ? $site_name : 'EdRob CMS';?> - Login</title>
	<meta name="author" content="Edward Robertson - www.edwardrobertson.co.uk" />	
    <link rel="stylesheet" href="<?=ADMIN_FOLDER.'css/core.min.css?v='.filemtime(FS_ADMIN_ROOT.'css/core.min.css');?>" media="all" />
    <link rel="stylesheet" href="<?=ADMIN_FOLDER.'css/additional.min.css?v='.filemtime(FS_ADMIN_ROOT.'css/additional.min.css');?>" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    </head>
    <body>
        
        <div class="cms-login">
            <div class="inner cms-login__inner">
                <form action="login.php" method="post" class="cms-login__form validate-form">
                    
                        
                            <h1 class="cms-login__logo"><img src="<?=$admin_logo?>" alt="<?=$site_name?> - CMS Login" /></h1>
                            <? if ( isset($_SESSION['loginerror']) ) { ?>
                                <h2 class="cms-login__error"><i class="fa fa-warning"></i> <?=$_SESSION['loginerror'];?></h2>
                                <? unset($_SESSION['loginerror']); ?>
                            <? } ?>
                            <div class="cms-login__form__field">
                                <label class="cms-login__form__label<? if($username) { ?> active<? } ?>" for="username"><i class="fa fa-user fa-lg"></i> Username</label>
                                <input autofocus tabindex="1" name="username" id="username" autocomplete="username" class="cms-login__form__input" value="<?=$uname;?>" required />
                            </div>
                            <div class="cms-login__form__field">
                                <label class="cms-login__form__label<? if($password) { ?> active<? } ?>" for="password"><i class="fa fa-unlock-alt fa-lg"></i> Password</label>
                                <div class="wrap password-wrapper">
                                <button id="toggle-password" type="button" aria-label="Show password as plain text. Warning: this will display your password on the screen."><i class="fa fa-eye"></i></button>
                                <input tabindex="2" autocomplete="password" type="password" name="password" id="password" class="cms-login__form__input"  required />
                                </div>
                            </div>
                            <a href="https://www.edwardrobertson.co.uk/contact/" target="_blank" class="need_help">Need Help?</a>
                            <button type="submit" class="button cms-login__form__button" value="Submit">Log in</button>
                </form>
            </div>            
    	</div>

        <script src="/admin/js/jquery-3.2.1.min.js"></script>
        <script src="/admin/js/validate.js"></script>
        <script>
                const passwordInput = document.getElementById('password');

                const togglePasswordButton = document.getElementById('toggle-password');
                togglePasswordButton.addEventListener('click', togglePassword);

                function togglePassword() {
                if (passwordInput.type === 'password') {
                    passwordInput.type = 'text';
                    togglePasswordButton.innerHTML = '<i class="fa fa-eye-slash"></i>';
                    togglePasswordButton.setAttribute('aria-label',
                    'Hide password.');
                } else {
                    passwordInput.type = 'password';
                    togglePasswordButton.innerHTML = '<i class="fa fa-eye"></i>';
                    togglePasswordButton.setAttribute('aria-label',
                    'Show password as plain text. ' +
                    'Warning: this will display your password on the screen.');
                }
                }
            </script>
    </body>
</html>