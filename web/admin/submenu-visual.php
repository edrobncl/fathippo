
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
    	<meta name="HandheldFriendly" content="True">
    	<meta name="MobileOptimized" content="320">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    	<meta name="author" content="Edward Robertson - www.edwardrobertson.co.uk" />
        <meta name="description" content="The Edward Robertson CMS Dashboard" />
        
        <title>Admin : Site Name : Dashboard</title>
                
        <link rel="stylesheet" href="/admin/css/core.min.css?v=1606391352" media="all" />
        <link rel="stylesheet" href="/admin/css/additional.min.css?v=1598015159" media="all" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">        
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/admin/js/fancybox/source/jquery.fancybox.css" media="screen" />
        
        <link rel="shortcut icon" href="/images/favicon.ico" />        <link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon-precomposed.png">        <meta name="msapplication-TileImage" content="/images/apple-touch-icon-precomposed.png" />        <meta name="msapplication-TileColor" content="#fff" />
        
        <meta name="robots" content="noindex">
    </head>
    <body class="dev-site superuser">
        <div id="admin-search"></div>
        
        <div id="page-wrap">
            
            <header id="header">
                <a class="header__logo" target="_blank" href="https://www.edwardrobertson.co.uk"><img src="/admin/images/logo-white-blue-dots.png" /></a>
                <div id="nav-toggle"><i class="fa fa-bars"></i><b>Menu</b></div>
                <a class="header__link header__link--dashboard" href="/admin/"><i class="fa fa-dashboard"></i><b>Dashboard</b></a>
                <a class="header__link" href="/admin/modules/core/settings/"><i class="fa fa-cog"></i><b>Settings</b></a>
                <a class="header__link" id="admin-search-toggle" target="_blank">
                    <div class="css-shape css-search" role="button"><span></span><span></span><span></span></div>
                    <b>Search</b>
                </a>
                <a class="header__link" href="/" target="_blank"><i class="fa fa-globe"></i><b>Website</b></a>
                <div class="dev-site-notice">Dev Admin</div>                <a class="header__link header__link--signout" href="/admin/logout"><i class="fa fa-sign-out"></i><b>Logout</b></a>
            </header>
            
            <nav id="nav" class="nav-">
    <ul class="main-nav">
        <li class="nav__item core"><a class="nav__link" href="/admin/modules/core/articles/"><i class="fa fa-newspaper-o"></i> Articles</a></li>
        <li class="nav__item core"><a class="nav__link" href="/admin/modules/core/file-manager/"><i class="fa fa-file-o"></i> File Manager</a></li>
        <li class="nav__item core"><a class="nav__link" href="/admin/modules/core/image-library/"><i class="fa fa-picture-o"></i> Image Library</a></li>
        <li class="nav__item core"><a class="nav__link" href="/admin/modules/core/pages/"><i class="fa fa-file-text-o"></i> Pages</a></li>
        <li class="nav__item core"><a class="nav__link" href="/admin/modules/core/social-media/"><i class="fa fa-share-alt"></i> Social Media</a></li>
        <li class="nav__item core"><a class="nav__link" href="/admin/modules/core/users/"><i class="fa fa-users"></i> Users</a></li>
        <li class="nav__item nav__item--group"><div class="nav__item__title"> Competitions <i class="fa fa-caret-down"></i></div>
            <ul class="sub-nav">
                <li class="nav__item"><a class="nav__link" href="asdsadsa"><i class="fa fa-star"></i> Competitions</a></li>
                <li class="nav__item"><a class="nav__link" href="asdsadsa"><i class="fa fa-users"></i> Entries</a></li>
            </ul></li>
        <li class="nav__item nav__item--group"><div class="nav__item__title active">Shop  <i class="fa fa-caret-up"></i></div>
            <ul class="sub-nav" style="display:block;">
                <li class="nav__item"><a class="nav__link" href="asdsadsa"><i class="fa fa-list"></i> Orders</a></li>
                <li class="nav__item"><a class="nav__link" href="asdsadsa"><i class="fa fa-tags"></i> Discount Codes</a></li>
                <li class="nav__item"><a class="nav__link" href="asdsadsa"><i class="fa fa-shopping-cart"></i> Products</a></li>
                <li class="nav__item"><a class="nav__link" href="asdsadsa"><i class="fa fa-shopping-cart"></i> Product Options</a></li>
            </ul>
        </li>
        <li class="nav__item "><a class="nav__link" href="/admin/modules/homepage-slider/"><i class="fa fa-picture-o"></i> Homepage Slider</a></li>    
    </ul>
</nav>		
<div id="dashboard">    
    <div class="dashboard-box pg-day">
    <div class="dashboard-pad">
        <div class="wrap welcome">
            <div class="welcome__logo" style="background-image:url(/images/logo-long.png)"></div>            <h1 class="welcome__title">Good afternoon!</h1>
            <div class="welcome__text">How are you going to update the website today?</div>
        </div>
        
                
        <div class="wrap last-updated">
                <h3 class="last-updated__title">Recent items</h3>
				<div id="row-list" class="wrap"><div class="row">
    					<div class="row__title">
                                <div class="row__title__type">page</div>
                                <a class="row__title__name" href="/admin/modules/core/pages/pages-form.php?_id=3">Example Page with a stupidly long title so we can see how it wraps</a><em class="row__title__lastupdated">Last Updated: 1:10pm on Wednesday 18th November 2020</em>
                            </div>
                            <div class="row__actions">
                                <a href="/admin/modules/core/pages/pages-form.php?_id=3" class="button"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="button button--outlined" href="/example-page/" target="_blank"><i class="fa fa-eye"></i> View</a>
                            </div>
                            <div class="row__status">
                                                        <div class="row__status__title"><i class="fa fa-check"></i> live</div>
                        </div>
    					</div><div class="row">
    					<div class="row__title">
                                <div class="row__title__type">page</div>
                                <a class="row__title__name" href="/admin/modules/core/pages/pages-form.php?_id=65">Contact page</a><em class="row__title__lastupdated">Last Updated: 11:56am on Friday 9th October 2020</em>
                            </div>
                            <div class="row__actions">
                                <a href="/admin/modules/core/pages/pages-form.php?_id=65" class="button"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="button button--outlined" href="/contact-page/" target="_blank"><i class="fa fa-eye"></i> View</a>
                            </div>
                            <div class="row__status">
                                                        <div class="row__status__title"><i class="fa fa-check"></i> live</div>
                        </div>
    					</div><div class="row">
    					<div class="row__title">
                                <div class="row__title__type">page</div>
                                <a class="row__title__name" href="/admin/modules/core/pages/pages-form.php?_id=4">Child Page</a><em class="row__title__lastupdated">Last Updated: 5:02pm on Thursday 9th April 2020</em>
                            </div>
                            <div class="row__actions">
                                <a href="/admin/modules/core/pages/pages-form.php?_id=4" class="button"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="button button--outlined" href="/example-page/child-page/" target="_blank"><i class="fa fa-eye"></i> View</a>
                            </div>
                            <div class="row__status">
                                                        <div class="row__status__title"><i class="fa fa-check"></i> live</div>
                        </div>
    					</div><div class="row">
    					<div class="row__title">
                                <div class="row__title__type">page</div>
                                <a class="row__title__name" href="/admin/modules/core/pages/pages-form.php?_id=67">Subpage</a><em class="row__title__lastupdated">Last Updated: 10:41am on Tuesday 19th November 2019</em>
                            </div>
                            <div class="row__actions">
                                <a href="/admin/modules/core/pages/pages-form.php?_id=67" class="button"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="button button--outlined" href="/contact-page/subpage/" target="_blank"><i class="fa fa-eye"></i> View</a>
                            </div>
                            <div class="row__status">
                                                        <div class="row__status__title"><i class="fa fa-check"></i> live</div>
                        </div>
    					</div><div class="row">
    					<div class="row__title">
                                <div class="row__title__type">page</div>
                                <a class="row__title__name" href="/admin/modules/core/pages/pages-form.php?_id=66">Subpage</a><em class="row__title__lastupdated">Last Updated: 10:41am on Tuesday 19th November 2019</em>
                            </div>
                            <div class="row__actions">
                                <a href="/admin/modules/core/pages/pages-form.php?_id=66" class="button"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="button button--outlined" href="/contact-page/subpage/" target="_blank"><i class="fa fa-eye"></i> View</a>
                            </div>
                            <div class="row__status">
                                                        <div class="row__status__title"><i class="fa fa-check"></i> live</div>
                        </div>
    					</div>                </div>
            </div>		
        

    </div>
    </div>
    

    </div><!--dashboard-->
    <footer>
        <div id="footer">
            <p>Content Management System provided by <a href="https://www.edwardrobertson.co.uk" target="_blank">Edward Robertson web design</a></p>
        </div>
    </footer>
</div><!--page_wrap-->        
        
    <script src="/admin/js/jquery-3.2.1.min.js"></script>
    <script src="/admin/js/fancybox/source/jquery.fancybox.pack.js"></script>
        
    <script src="/admin/js/uploader.js"></script>
    
        
    <script src="/admin/js/admin.js?v=1606152794"></script>
    
    <script src="/admin/modules/core/image-library/jquery-ui-sortable.min.js"></script>
<script src="/admin/modules/core/image-library/image_library.js?v=1606224502"></script><script src="/admin/modules/core/file-manager/file_manager.js?v=1600182964"></script>
    <script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="/admin/js/search.js"></script>

</body>
</html>
