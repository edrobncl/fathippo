<?php
global $pdo;

$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'FAQ',
    'plural' => 'FAQs',
    'form_link' => 'faq-form.php',
    'post_type' => 'faq',
    'icon' => 'question',
    'sort_order' => 'menu_order ASC'
];
?>
