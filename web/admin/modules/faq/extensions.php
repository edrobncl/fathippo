<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Additional Details</legend>
            
            <div class="form__field">
                <label class="form__field__label" for="answer">Answer</label>
                <textarea name="answer" id="answer" class="form__field__textarea richtext_editor"><?= isset($answer) ? $answer : '' ?></textarea>
            </div>
<!-- 
            <?php
                if (isset($faq_page)) {
                    $faq_page_array = json_decode($faq_page, true);
                } else {
                    $faq_page_array = [];
                }
            ?>

            <div class="form__field">
                <label class="form__field__label" for="offer_page_save">Link to Page(s)</label>
                <div class="input-group">
                    <input class="form__field__input" type="text" id="select-page__filter" class="input-group__input" placeholder="Page Filter" />
                    
                </div> 
                <label class="form__field__accent form__field__label">
                    Show only selected page(s) <input type="checkbox" id="show_selected_checkboxes">
                </label>
                <div class="tag-input-box-wrap">
                    <div class="wrap tag-input-boxes">
                       <?php buildPathList($faq_page_array); ?>
                    </div>
                </div>
            </div> -->
        </fieldset>
    </div>
</div>

<?php
    function buildPathList($faq_page_array, $parent_id = 0, $indent = 0) {
        global $pdo;
        global $last_indent;
        global $row_count;
        
        $getPages = $pdo->prepare("SELECT * FROM er_posts WHERE parent_id = :parent_id AND post_type IN ('page', 'category', 'article', 'project', 'product') ORDER BY menu_order ASC, _id ASC");
        $getPages->execute(['parent_id'=>$parent_id]);
        $pages = $getPages->fetchAll();
        
        foreach ( $pages as $row ) {
            if ($row['_id'] != 2) {
                $row_count++;  ?>
                <label class="tag-input-box ir-<?= $indent ?>">
                    <input type="checkbox" name="faq_page[]" value="<?= $row['_id']; ?>" <?= (in_array($row['_id'], $faq_page_array)) ? ' checked' : ''; ?>>
                    <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                    <?= $row['post_title'] ?> (<?=$row['post_type'];?>)
                </label>
                <?php
                $last_indent = $indent;
                buildPathList($faq_page_array, $row['_id'], $indent + 1);
            }
        } ?>

        <?php
    }
?>