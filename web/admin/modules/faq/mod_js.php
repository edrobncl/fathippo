<script>
    // Search files
    $(document).on('keyup', '#search-bar-form__faqs input', function() {
        let searchInput = $(this);
        let searchVal = searchInput.val().toLowerCase();

        if ( $.trim(searchVal).length > 0 ) {
            // Show rows that contain the search value
            $('#row-list .row').each(function() {
                let thisProduct = $(this).find('.title:first').text();
                let productPages = $(this).data('pages').toLowerCase();
               
                if ( thisProduct.toLowerCase().indexOf(searchVal) >= 0 || productPages.indexOf(searchVal) > -1 ) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        } else {
            // If the search box is empty, show all products
            $('#row-list .row').show();
        }
    });


    // ===============================================
    //  SELECT PAGE ADDITIONAL FUNCTIONALITY
    // ===============================================
    if ( $('#select-page__filter').length ) {
        $(document).ready(function() {

            // ===============================================
            //  Page Filter Text Input
            // ===============================================
            $('#select-page__filter').on('keyup', linkFilterHandler);
            $('#show_selected_checkboxes').on('change', linkFilterHandler);


            function linkFilterHandler() {
                var filter_text = $('#select-page__filter').val().toLowerCase().trim();
                var show_only_selected = $('#show_selected_checkboxes').is(':checked');

                $('.tag-input-boxes > label').each(function() {
                    var label_text = $(this).text().toLowerCase().trim();
                    
                    // Handle the Text Filter Section
                    if ( filter_text == '' || label_text.indexOf(filter_text) > -1 ) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                    // Handle the selected page part
                    if ( show_only_selected && ! $(this).find('input:first').is(':checked') ) {
                        $(this).hide();
                    }  
                });
            }

        });
    }


</script>
