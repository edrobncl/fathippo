<?php

if ( !function_exists('beforeSave') ) {
    function beforeSave($post) {
        
        // Change the array of features to a string
        $post['key_features'] = implode(', ', $post['key_features']);     
        $post['application_areas'] = implode(', ', $post['application_areas']);
        $post['markets'] = json_encode($post['markets']);

        return $post;
    }
}



if ( !function_exists('idRecordConvert') ) {

    /**
     * Return the record of the ID from er_posts
     * 
     * @param interger $id - The ID of the post
     * @return array The record of the ID
     */
    function idRecordConvert($id) {
        global $pdo;

        $q = $pdo->prepare('SELECT * FROM er_posts WHERE _id = ?');
        $q->execute([$id]);
        return $q->fetch();
    }
}


if (!function_exists('beforeRowOutput')) {
    function beforeRowOutput($row) {
        global $pdo;
        
        $meta_query = $pdo->prepare('
            SELECT meta_value
                FROM er_postmeta
                WHERE post_id = ' . $row['_id'] . '
                    AND meta_name = ?   
        ');


        // Check for offer page 
        $meta_query->execute(['faq_page']);
        $offer_page = $meta_query->fetch();

        // Create the pages list
        $row['pages'] = '';
        if (isset($offer_page['meta_value'])) {
            $pages = json_decode($offer_page['meta_value'], true);

            $page_names = $pdo->query('
                SELECT post_title
                FROM er_posts
                WHERE _id IN (' . implode(',', $pages) . ')
            ')->fetchAll();

            $pages = [];
            foreach ($page_names as $pg) {
                $pages[] = $pg['post_title'];
            }

            $row['pages'] = implode(', ', $pages);
        }


        // Check that there are answers
        $meta_query->execute(['answer']);
        $answer = $meta_query->fetch();

        $row['no_answer'] = true;
        if (isset($answer['meta_value']) && !empty(trim($answer['meta_value']))) {
            $row['no_answer'] = false;
        }

        return $row;
    }
}