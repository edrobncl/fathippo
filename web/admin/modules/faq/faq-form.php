<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

require_once(FS_ADMIN_INCLUDES.'inc_form_header.php');

if ( in_array($admin_page_type,['add','edit']) ) {

    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['singular'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;

    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>

    <div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-<?=($admin_page_type=='edit')?'pencil':'plus';?>"></i> <?=$admin_page_title;?></h1>
        <a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a>
    </div>

    <?
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success">'.$_SESSION['success'].'</div>';
        }
    ?>

    <form class="validate-form wrap form-with-actions" method="post" action="/admin/scripts/manage_post.php" enctype="multipart/form-data">

        <input type="hidden" name="parent_id" value="1">
        <input type="hidden" name="post_id" value="<?=$_id;?>">
        <input type="hidden" name="return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
        <input type="hidden" name="post_type" value="<?=$module_form_settings['post_type']?>">
        <input type="hidden" name="post_author" value="<?=$_SESSION['eruid'];?>">
        <input type="hidden" name="post_slug" value="<?=(isset($post_slug)) ? $post_slug : ''?>">

        <div class="__form__group">
            <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>
                    <div class="form__field with_help">
                        <label class="form__field__label" for="post_title">Question <span class="required">*</span></label>
                        <input class="form__field__input" name="post_title" id="post_title" type="text" required value="<?=$post_title ?? '';?>" placeholder="Question" />
                    </div>
                </fieldset>
            </div>

            <div class="form__group">
                <fieldset class="display-options">
                    <legend class="form__group__legend">Display Options</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="post_status">Display Status<span class="required">*</span></label>
                        <select class="form__select" name="post_status" id="post_status" required>
                            <option value="">Please choose...</option>
                            <option value="draft"<?=($post_status=='draft')?' selected':'';?>>Draft</option>
                            <option value="private"<?=($post_status=='private')?' selected':'';?>>Private</option>
                            <option value="live"<?=($post_status=='live')?' selected':'';?>>Live</option>
                        </select>
                    </div>
    
                    <div class="form__field">
                        <label class="form__field__label" for="menu_order">Display Order<span class="required">*</span></label>
                        <input class="form__field__input" name="menu_order" id="menu_order" type="number" required value="<?=$menu_order;?>" />
                    </div>

                    <div class="form__field">
                      <label class="form__field__label" for="show_on_page">Show on Page<span class="required">*</span></label>
                      <select class="form__select" id="show_on_page" name="post_alt_title" required>
                        <option value="" hidden>Please select...</option>
                        <option value="16"<?=!empty($post_alt_title)&&$post_alt_title==16?' selected':'';?>>FAQs</option>
                        <option value="887"<?=!empty($post_alt_title)&&$post_alt_title==887?' selected':'';?>>DIY FAQs</option>
                      </select>
                    </div>
                </fieldset>
            </div>
        </div>

        <?php if ( file_exists(__DIR__ . '/extensions.php') ) {
            include __DIR__ . '/extensions.php';
        }  ?>

        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>

    </form>


    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // If you're not editing or adding a post, you shouldn't really be here!
    $path = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($path);
    $path = implode('/',$path);
    // Go to the main index of this module.
    header("Location: ".$path.'/');
}
?>
