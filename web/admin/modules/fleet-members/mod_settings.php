<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Fleet Member',
    'plural' => 'Fleet Members',
    'form_link' => 'fleet-members-form.php',
    'post_type' => 'fleet-member',
    'hide_slug_on_index' => true,
    'icon' => 'car'
];

?>
