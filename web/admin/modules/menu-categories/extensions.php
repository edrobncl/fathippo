<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Extra Details</legend>
            
            <div class="form__field">
                <label class="form__field__label" for="menu_category_description">Description</label>
                <textarea class="richtext_editor" name="menu_category_description" id="menu_category_description"><?= $menu_category_description ?? '' ?></textarea>
            </div>

            <div class="form__field">
                <label class="form__field__label" for="show_cta">Show Call To Action?</label>
                <select class="form__field__input" name="show_cta" id="show_cta">
                    <option value="">No</option>
                    <option value="1" <?= ! empty($show_cta) && $show_cta == 1 ? 'selected' : '' ?>>Yes</option>
                </select>
            </div>
        </fieldset>
    </div>
</div>