<?php

if ( ! function_exists('beforeSave') ) {
    function beforeSave($post) {
        return $post;
    }
}

if ( ! function_exists('beforeRowOutput') ) {
    function beforeRowOutput($post) {
        global $pdo;

        // Get Name of Menu
        $post['menu_name'] = showName($post['parent_id']);

        return $post;
    }
}
