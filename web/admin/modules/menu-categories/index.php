<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
	require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");
    
    // Include this module's functions file, if it has one.
    if ( file_exists('mod_funcs.php') ) {
        include('mod_funcs.php');
    }
    
    require_once(FS_ADMIN_INCLUDES."inc_header.php");
    
    // Set sort order
    $sort_order = ( isset($module_form_settings['sort_order'])) ? $module_form_settings['sort_order'] : 'menu_order ASC';
    
    // Run query for jobs
	$fetchPosts = $pdo->prepare("SELECT * FROM er_posts WHERE post_type = ? ORDER BY $sort_order");
    $fetchPosts->execute([$module_form_settings['post_type']]);
    
    // Status Icons Storage
    $statusIcons = [
        'live' => 'check',
        'private' => 'lock',
        'draft' => 'file-text-o'
    ];
?>

<style>
    .filters {
        display: flex;
        justify-content: flex-start;
        align-items: center;
    }

    .filter {
        margin-right: 1em;
    }

    .filter > label {
        display: block;
    }

    .filter > input,
    .filter > select {
        width: fit-content;
    }
</style>

    <div class="page-headers">
        <h1 class="page-headers__title"><i class="fa fa-<?=$module_form_settings['icon']?>"></i> <?=$module_form_settings['plural']?></h1>
        <a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a>
    </div>
    <div class="dashboard-pad">
        <? if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? } ?>
         <div class="wrap filters">
            <div class="filter">
                <label for="search">Search</label>
                <input type="text" id="search" />
            </div>
            <div class="filter">
                <label for="menu">Menu</label>
                <select class="form__select" id="menu">
                    <option value="">All</option>
                    <?php
                        foreach ( getMenus(true) as $menu ) { ?>
                            <option value="<?= $menu['_id'] ?>"><?= $menu['name'] ?></option>
                            <?php
                        }
                    ?>
                </select>
            </div>
        </div>

    <div id="row-list" class="wrap card jobs-index">
        <?php
            while ( $row = $fetchPosts->fetch() ) {
                if ( function_exists('beforeRowOutput') ) { $row = beforeRowOutput($row); } ?>
                
                <div class="row" data-menu_id="<?= $row['parent_id'] ?>">
                    <div class="row__title">
                        <a class="row__title__name" href="<?= $module_form_settings['form_link'] ?>?_id=<?= $row['_id'] ?>"><?= $row['post_title'] ?></a>
                        <br /><span class="row__title__slug"><strong>Menu</strong>: <?= $row['menu_name'] ?></span>
                    </div>
                    <div class="row__status">
                        <div class="status"><i class="fa fa-<?= $statusIcons[$row['post_status']] ?>"></i> <?= $row['post_status'] ?></div>
                    </div>
                    <div class="row__actions">
                        <a href="<?= $module_form_settings['form_link'] ?>?_id=<?= $row['_id'] ?>" class="button button--edit action"><i class="fa fa-pencil"></i> Edit</a>
                        <a id="inline" href="#pop_delete_<?= $row['_id'] ?>" class="button button--delete action delete"><i class="fa fa-trash"></i> Delete</a>
                    </div>
                </div>
                <div style="display:none">
                    <div id="pop_delete_<?= $row['_id'] ?>">
                        <h3>Are you sure?</h3>
                        <p>This will delete <strong><?= $row['post_title'] ?></strong> forever - please be sure before you delete this record.</p>
                        <form id="delete-post-<?= $row['_id'] ?>" method="post" action="/admin/scripts/manage_post.php">
                            <input type="hidden" name="post_id" value="<?= $row['_id'] ?>" />
                            <input type="hidden" name="return_to" value="<?= $_SERVER['REQUEST_URI'] ?>" />
                            <input type="hidden" name="post_type" value="<?= $row['post_type'] ?>" />
                            <button type="button" name="cancel_delete" class="button"><i class="fa fa-arrow-left"></i> No, don't delete it!</button>
                            <button type="submit" name="delete_post" class="button button--delete"><i class="fa fa-trash"></i> Yes! Delete it!</button>
                        </form>
                    </div>
                </div>
                <?php
            }
            
            if ($fetchPosts->rowCount() == 0) {
                ?>There are no <?=strtolower($module_form_settings['plural']);?>. Why not <a href="<?=$module_form_settings['form_link'];?>">add one</a>?<?
            }
            ?>
    </div>
</div>

<?php require_once(FS_ADMIN_INCLUDES.'inc_footer.php'); ?>
<?php require_once(FS_ADMIN_INCLUDES.'inc_js.php'); ?>