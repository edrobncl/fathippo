<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Menu Category',
    'plural' => 'Menu Categories',
    'form_link' => 'menu-categories-form.php',
    'post_type' => 'menu_category',
    'hide_slug_on_index' => true,
    'icon' => 'list',
    'module_group' => '1',
    'sort_order' => 'parent_id ASC, menu_order ASC'
];
