<script>
    // =============================
    //  Index Filters
    // =============================
    (function() {

        document.querySelector('#search').addEventListener('keyup', () => handleFilters());
        document.querySelector('#menu').addEventListener('change', () => handleFilters());

        // Handle Filtering the rows down depending on values selected
        function handleFilters() {
            const textSearch = document.querySelector('#search').value.toLowerCase();
            const menuSearch = parseInt(document.querySelector('#menu option:checked').value);

            document.querySelectorAll('#row-list > .row').forEach(row => {
                let showRow = true;

                // Check Text Search
                if ( textSearch !== '' && row.innerText.toLowerCase().indexOf(textSearch) === -1 ) {
                    showRow = false;
                }

                // Check Menu Search
                if ( ! isNaN(menuSearch) && parseInt(row.attributes['data-menu_id'].value) !== menuSearch ) {
                    showRow = false;
                }

                // Decide whether to show or hide the row
                if ( showRow ) {
                    row.style.display = 'block';
                } else {
                    row.style.display = 'none';
                }
            });
        }

    })();
</script>