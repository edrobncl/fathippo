<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Homepage Slide',
    'plural' => 'Homepage Slides',
    'form_link' => 'homepage-slider-form.php',
    'post_type' => 'homepage_slider',
    'icon' => 'picture-o',
    'listing_thumbnail' => 'homepage_slide_image',
    'module_group' => '1'
];

//$homepage_card = [
//    'h3' => 'Why not add a <b>Team Member?</b>',
//    'p' => 'New content is great for keeping your users updated with your latest news and for showing search engines that your website is active.',
//    'manage-link' => '/admin/modules/'.$module_settings['module_name'] . '/',
//    'add-link' => '/admin/modules/'.$module_settings['module_name'].'/'.$module_settings['form_link'],
//    'manage-text' => 'Manage ' . $module_settings['plural'],
//    'add-text' => 'Add a ' . $module_settings['singular']
//];
?>
