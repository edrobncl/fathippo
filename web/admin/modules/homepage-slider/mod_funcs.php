<?php
    
// Modify the row values before output onto module index page
if ( !function_exists('beforeRowOutput') ) {
    function beforeRowOutput($row) {
        global $pdo;
        
        // $getImageMeta = $pdo->prepare("SELECT meta_value FROM er_postmeta WHERE post_id = ? AND meta_name = ?");
        // $getImageMeta->execute([$row['_id'],'homepage_slide_image']);
        
        // if ( $thumb = $getImageMeta->fetch() ) {
        //     $thumb = json_decode($thumb['meta_value'], true);
        //     $row['listing_thumbnail'] = $thumb['post_slug'];
        // } else {
        //     $row['listing_thumbnail'] = '/admin/images/no-thumbnail.jpg';
        // }

        $row['href'] = getMeta($row['_id'], 'homepage_slide_href');        

        // Making sure post_alt_title isn't empty
        $row['post_alt_title'] = $row['post_title'];

        return $row;
    }
}
?>