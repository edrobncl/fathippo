<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

require_once(FS_ADMIN_INCLUDES.'inc_form_header.php');

if ( in_array($admin_page_type,['add','edit']) ) {

    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['singular'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;

    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>

    <div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=($admin_page_type=='edit')?'pencil':'plus';?>"></i> <div class="page-headers__title__parentlink"><a href="/admin/modules/<?=$module_form_settings['module_name']?>/"><?=$module_form_settings['plural']?></a> <i class="fa fa-angle-right"></i></div> <?=$admin_page_title;?></h1>
    </div>

    <?
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success">'.$_SESSION['success'].'</div>';
        }
    ?>

    <form class="validate-form wrap form-with-actions" method="post" action="/admin/scripts/manage_post.php" enctype="multipart/form-data" id="event-form">

        <input type="hidden" name="post_id" value="<?=$_id;?>">
        <input type="hidden" name="post_type" value="<?=$module_form_settings['post_type'];?>">
        <input type="hidden" name="post_author" value="<?=$_SESSION['eruid'];?>">
        <input type="hidden" name="return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
        <input type="hidden" name="menu_order" value="0">
        <input type="hidden" name="post_layout" value="event">
        <input type="hidden" name="parent_id" value="0">
        <input type="hidden" name="post_slug" value="<?=$post_slug?>">
        
        <div class="wrap __form__group">
            <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>
                    <div class="form__field with_help">
                        <label class="form__field__label" for="post_title">Title<span class="required">*</span></label>
                        <input class="form__field__input" name="post_title" id="post_title" type="text" required value="<?=(isset($post_title))?$post_title:'';?>" placeholder="Full Title of Event" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="post_alt_title">Start Date<span class="required">*</span></label>
                        <input class="form__field__input" name="post_alt_title" id="post_alt_title" type="date" required value="<?=(isset($post_alt_title))?$post_alt_title:'';?>" placeholder="For example, 10/08/2021" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="event_end_date">End Date</label>
                        <input class="form__field__input" name="event_end_date" id="event_end_date" type="date" value="<?=(isset($event_end_date))?$event_end_date:'';?>" placeholder="For example, 12/08/2021" />
                        <div id="event_end_date__error" class="input_errors"></div>
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="event_time">Time<span class="required">*</span></label>
                        <input class="form__field__input" type="text" id="event_time" name="event_time" placeholder="e.g.: 19:30" required value="<?=(isset($event_time))?$event_time:'';?>" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="event_address">Full Address<span class="required">*</span></label>
                        <input class="form__field__input" type="text" id="event_address" name="event_address" placeholder="e.g.: Full address" required value="<?=(isset($event_address) ? $event_address : '');?>" />
                    </div>

                    
                    <div class="form__field">
                        <label class="form__field__label" for="event_link_text">Link Text</label>
                        <input class="form__field__input" type="text" id="event_link_text" name="event_link_text" value="<?=(isset($event_link_text) ? $event_link_text : '');?>" />
                    </div>

                    <div class="form__field event-link__div">
                        <label class="form__field__label" for="event_link">Link For Menu</label>
                        <input class="form__field__input" type="text" id="event_link" name="event_link" value="<?=(isset($event_link) ? $event_link : '');?>" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="pdf_for_menu">Use PDF For Menu</label>
                        <select class="form__select" name="pdf_for_menu" id="pdf_for_menu">
                            <option value="">No</option>
                            <option value="yes"<?=($pdf_for_menu=='yes')?' selected':'';?>>Yes</option>
                        </select>
                    </div>

                </fieldset>
            </div>

            <div class="menu-file-uploader" <? if(empty($menu_pdf) && $pdf_for_menu != 'yes'){ ?>style="display: none" <? } ?>>
                <? file_uploader('menu_pdf', 'Menu PDF'); ?>
            </div>
        
            <div class="form__group">
                <fieldset class="display-options">
                    <legend class="form__group__legend">Display Options</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="post_status">Page Status<span class="required">*</span></label>
                        <select class="form__select" name="post_status" id="post_status" required>
                            <option value="">Please choose...</option>
                            <option value="draft"<?=($post_status=='draft')?' selected':'';?>>Draft</option>
                            <option value="private"<?=($post_status=='private')?' selected':'';?>>Private</option>
                            <option value="live"<?=($post_status=='live')?' selected':'';?>>Live</option>
                        </select>
                    </div>
                    <div class="form__field">
                        <label class="form__field__label" for="menu_order">Order<span class="required">*</span></label>
                        <input class="form__field__input" name="menu_order" id="menu_order" type="number" required value="<?=$menu_order;?>" />
                    </div>
                </fieldset>
            </div>
        </div>
        
        <?php if ( file_exists(FS_ADMIN_ROOT.'/modules/'.$module_form_settings['module_name'].'/extensions.php') ) {
            include(FS_ADMIN_ROOT.'/modules/'.$module_form_settings['module_name'].'/extensions.php'); 
        } ?>

        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>

    </form>

    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // If you're not editing or adding a post, you shouldn't really be here!
    $path = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($path);
    $path = implode('/',$path);
    // Go to the main index of this module.
    header("Location: ".$path.'/');
}
?>

