<div class="wrap __form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Event Details for Schema</legend>

            <div class="form__field">
                <label class="form__field__label" for="event_streetaddress">Event Street Address</label>
                <input class="form__field__input" type="text" id="event_streetaddress" name="event_streetaddress" placeholder="e.g. first part of the address" value="<?=(isset($event_streetaddress) ? $event_streetaddress : '');?>" />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="event_addresslocality">Event Address Locality</label>
                <input class="form__field__input" type="text" id="event_addresslocality" name="event_addresslocality" placeholder="e.g. Town/City" value="<?=(isset($event_addresslocality) ? $event_addresslocality : '');?>" />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="event_postalcode">Event Postal Code</label>
                <input class="form__field__input" type="text" id="event_postalcode" name="event_postalcode" placeholder="e.g. NE1 2AB" value="<?=(isset($event_postalcode) ? $event_postalcode : '');?>" />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="event_addressregion">Event Address Region</label>
                <input class="form__field__input" type="text" id="event_addressregion" name="event_addressregion" placeholder="e.g. County" value="<?=(isset($event_addressregion) ? $event_addressregion : '');?>" />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="event_addresscountry">Event Address Country</label>
                <input class="form__field__input" type="text" id="event_addresscountry" name="event_addresscountry" placeholder="e.g. UK" value="<?=(isset($event_addresscountry) ? $event_addresscountry : '');?>" />
            </div>

        </fieldset>
        </div>
    </div>
<? 
// single_image('event_image', 'Event Image', '600px wide');
// single_image('thumbnail_image', 'Event Thumbnail', '360px x 180px');
?>
