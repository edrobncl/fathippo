<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Event',
    'plural' => 'Events',
    'form_link' => 'event-form.php',
    'post_type' => 'event',
    'hide_slug_on_index' => true,
    'icon' => 'calendar'
];

?>
