<style type="text/css">
    .ui-state-highlight { display: block; width: 100%; float: left; height: 5em; line-height: 1.2em; background: rgba(0,0,0,0.3); border: 1px solid #000; margin: .25% 0 0; }
    .moving { box-shadow: 1px 1px 5px rgba(0,0,0,.3); }
</style>
<script src="/admin/js/jquery-ui-sortable.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.jobs-index').sortable({
            'handle': '.drag-handle',
            'placeholder': 'ui-state-highlight',
            'start': function(event, ui) {
                ui.item.addClass('moving');
            },
            'stop': function(event, ui) {
                ui.item.removeClass('moving');
            },
            'update': function(event, ui) {
                var rowList = [];
                $('.jobs-index').find('.row').each(function() {
                    rowList.push($(this).attr('data-job-id'));
                });
                $.ajax({
                    url: '/admin/scripts/jobs_order.php',
                    method: 'post',
                    data: { jobs: rowList }
                });
            }
        });
    });

    $(document).on('change', 'select[name=pdf_for_menu]', function() {
        var pdfdiv = $('.menu-file-uploader');
        var linkdiv = $('.event-link__div');
        if ($(this).val() == 'yes') {
            pdfdiv.css('display', 'block');
            linkdiv.css('display', 'none');
            linkdiv.find('input').val(null);
        } else {
            pdfdiv.css('display', 'none');
            pdfdiv.find('input').val(null);
            pdfdiv.find('.files-list').html('<li class="no-files-msg"><i class="fa fa-info-circle"></i> This event has no files.</li>');
            linkdiv.css('display', 'block');
        }
    });


    // ===============================================
    //  End Date Checking & Verification
    // ===============================================
    if ( document.querySelector('#event-form').length ) {

        document.querySelector('#event-form').addEventListener('submit', e => {
            const endDateError = document.querySelector('#event_end_date__error');
            endDateError.innerHTML = '';

            // Test the end date against start date to make sure it is equal to or greater
            const startDate = new Date(document.querySelector('#post_alt_title').value);
            const endDate = new Date(document.querySelector('#event_end_date').value);

            // Date Comparison
            if ( endDate !== '' && endDate < startDate ) {
                // Stop Form from submitting
                e.preventDefault();

                // Show Error Message
                endDateError.innerHTML = '<div class="error"><i class="fa fa-times"></i> End date cannot be set before start date</div>';
            }
        });

    }

</script>