<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

// Include module's functions
if ( file_exists('mod_funcs.php') ) {
    require_once('mod_funcs.php');
}

// check if edit or script
$admin_page_type = ($_SERVER['REQUEST_METHOD'] === 'POST') ? 'script' : 'edit';

switch ($admin_page_type) {
    case 'edit':        
        // Build any postmeta variables
        $postMeta = $pdo->query("SELECT * FROM er_options")->fetchAll();
        foreach ( $postMeta as $field ) {
            $option_name = $field['option_name'];
            $$option_name = $field['option_value'];
        }
        break;
        
    case 'script':
        break;
}

if ( $admin_page_type == 'edit' ) {
    
    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['plural'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;
        
    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>
     
    <div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-<?=$module_form_settings['icon']?>"></i> <?=$admin_page_title;?></h1>
    </div>
    
    <?
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success">'.$_SESSION['success'].'</div>';
        }
    ?>
    
    <form class="validate-form wrap form-with-actions" method="post" action="<?=$_SERVER['REQUEST_URI'];?>" enctype="multipart/form-data">
        <input type="hidden" name="return_to" value="<?=$_SERVER['REQUEST_URI'];?>">
        
        <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="facebook_handle">Facebook <i class="fa fa-facebook-official social-handle" style="color: #3b5998;"></i></label>
                        <input class="form__field__input" name="facebook_handle" id="facebook_handle"   type="text" value="<?=(isset($facebook_handle))?$facebook_handle:'';?>" placeholder="The link to your Facebook page" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="twitter_handle">Twitter <i class="fa fa-twitter social-handle" style="color: #00b6f1;"></i></label>
                        <input class="form__field__input" name="twitter_handle" id="twitter_handle"   type="text" value="<?=(isset($twitter_handle))?$twitter_handle:'';?>" placeholder="@YourTwitterHandle" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="instagram_handle">Instagram <i class="fa fa-instagram social-handle" style="color: #c32aa3;"></i></label>
                        <input class="form__field__input" name="instagram_handle" id="instagram_handle"   type="text" value="<?=(isset($instagram_handle))?$instagram_handle:'';?>" placeholder="@YourInstagramHandle" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="whatsapp_handle">WhatsApp <i class="fa fa-whatsapp social-handle" style="color: #25d366;"></i></label>
                        <input class="form__field__input" name="whatsapp_handle" id="whatsapp_handle"   type="text" value="<?=(isset($whatsapp_handle))?$whatsapp_handle:'';?>" placeholder="Your WhatsApp Number (in international format e.g. +44)" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="pinterest_handle">Pinterest <i class="fa fa-pinterest social-handle" style="color: #cb2027;"></i></label>
                        <input class="form__field__input" name="pinterest_handle" id="pinterest_handle"   type="text" value="<?=(isset($pinterest_handle))?$pinterest_handle:'';?>" placeholder="Link to your Pinterest page" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="linkedin_handle">LinkedIn <i class="fa fa-linkedin social-handle" style="color: #007bb6;"></i></label>
                        <input class="form__field__input" name="linkedin_handle" id="linkedin_handle"   type="text" value="<?=(isset($linkedin_handle))?$linkedin_handle:'';?>" placeholder="Link to your LinkedIn page" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="flickr_handle">Flickr <i class="fa fa-flickr social-handle" style="color: #f40083;"></i></label>
                        <input class="form__field__input" name="flickr_handle" id="flickr_handle"   type="text" value="<?=(isset($flickr_handle))?$flickr_handle:'';?>" placeholder="Link to your Flickr page" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="youtube_handle">YouTube <i class="fa fa-youtube social-handle" style="color: #b31217;"></i></label>
                        <input class="form__field__input" name="youtube_handle" id="youtube_handle"   type="text" value="<?=(isset($youtube_handle))?$youtube_handle:'';?>" placeholder="Link to your YouTube page" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="spotify_handle">Spotify <i class="fa fa-spotify social-handle" style="color: #1DB954;"></i></label>
                        <input class="form__field__input" name="spotify_handle" id="spotify_handle"   type="text" value="<?=(isset($spotify_handle))?$spotify_handle:'';?>" placeholder="Your Spotify Link" />
                    </div>
                    
                </fieldset>
        </div>
        

        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>
        
    </form>

    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // Save the submitted settings

    // Set return address and remove return_to and save_form from $_POST
    $return_to = $_POST['return_to'];
    unset($_POST['return_to']);
    unset($_POST['save_form']);

    foreach ( $_POST as $option_name => $option_value ) {
        // Check if the option_name is already in the table
        $checkOption = $pdo->prepare("SELECT * FROM er_options WHERE option_name = ? LIMIT 1");
        $checkOption->execute([$option_name]);
        $checkOption->fetch();
        
        echo $option_name . ' = ' . $option_value . '<br/>';
                
        if ( $checkOption->rowCount() > 0 ) {
            // If the option_name is currently in the table...
            if ( trim($option_value) == '' ) {
                // If the new option_value is empty then delete the record
                $deleteOption = $pdo->prepare("DELETE FROM er_options WHERE option_name = ?");
                $deleteOption->execute([$option_name]);
            } else {
                // if the new option_value is not empty then update the record
                $putOption = $pdo->prepare("UPDATE er_options SET option_value = ? WHERE option_name = ?");
                $putOption->execute([$option_value,$option_name]);
            }
        } else {
            
            // If the option_name doesn't already exist...
            if ( trim($option_value) != '' ) {
                // If the option_value is not empty, insert the new record
                $putOption = $pdo->prepare("INSERT INTO er_options (option_name, option_value) VALUES (?, ?)");
                $putOption->execute([$option_name, $option_value]);
            }
        }
    }
    
    // Set success message
    $_SESSION['success'] = 'Social Media updated successfully';

    // Refresh page
    header("Location: ".$return_to);
}
?>