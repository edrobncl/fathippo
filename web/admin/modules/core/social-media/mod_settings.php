<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Socials',
    'plural' => 'Socials',
    'form_link' => '',
    'main_menu' => [
        'option' => 'secondary',
        'order' => 3
    ],
    'icon' => 'share-alt'
];
?>