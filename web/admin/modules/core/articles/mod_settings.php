<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Article',
    'plural' => 'Articles',
    'form_link' => 'articles-form.php',
    'main_menu' => [
        'option' => 'primary',
        'order' => 2
    ],
    'post_type' => 'article',
    'icon' => 'newspaper-o',
	'sort_order' => 'post_alt_title DESC'
];

$homepage_card = [
    'h3' => 'Why not add an <b>article?</b>',
    'p' => 'New content is great for keeping your users updated with your latest news and for showing search engines that your website is active.',
    'manage-link' => '/admin/modules/'.$module_settings['module_name'] . '/',
    'add-link' => '/admin/modules/'.$module_settings['module_name'].'/'.$module_settings['form_link'],
    'manage-text' => 'Manage ' . $module_settings['plural'],
    'add-text' => 'Add an ' . $module_settings['singular'],
    'icon' => 'newspaper-o'
];
?>