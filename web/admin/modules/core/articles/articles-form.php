<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

require_once(FS_ADMIN_INCLUDES.'inc_form_header.php');

if ( in_array($admin_page_type,['add','edit']) ) {
    
    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['singular'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;
        
    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>
     
    <div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=($admin_page_type=='edit')?'pencil':'plus';?>"></i> <div class="page-headers__title__parentlink"><a href="/admin/modules/core/<?=$module_form_settings['module_name']?>/"><?=$module_form_settings['plural']?></a> <i class="fa fa-angle-right"></i></div> <?=$admin_page_title;?></h1>
    </div>
    
    <?
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success">'.$_SESSION['success'].'</div>';
        }
    ?>
    
    <form class="validate-form wrap form-with-actions" method="post" action="/admin/scripts/manage_post.php" enctype="multipart/form-data">

        <input type="hidden" name="post_id" value="<?=$_id;?>">
        <input type="hidden" name="post_type" value="<?=$module_form_settings['post_type'];?>">
        <input type="hidden" name="post_author" value="<?=$_SESSION['eruid'];?>">
        <input type="hidden" name="return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
        <input type="hidden" name="menu_order" value="0">
        <input type="hidden" name="parent_id" value="<?=(isset($articles_parent))?$articles_parent:0;?>">
        <input type="hidden" name="post_layout" value="article">
        
        <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>                    
                    <div class="form__field with_help">
                        <label class="form__field__label" for="post_title">Article Title<span class="required">*</span><? if ($_id > 0) { ?><a tabindex="-1" id="inline" href="#pop_post_title" class="admin-help"><i class="fa fa-question-circle"></i> Help?</a><? } ?></label>
                        <input class="form__field__input" name="post_title" id="post_title" type="text" required value="<?=(isset($post_title))?$post_title:'';?>" placeholder="The main identifer for your page" />
                    </div>
					<div style="display:none">
    					<div id="pop_post_title">
                    	    <h3>Article Title</h3>
                            <p>This will be the main heading on the page.</p>
                            <a target="_blank" href="<?=buildPath($_id) ?>?show=labels">Show me the page! <i class="fa fa-external-link"></i></a>
                        </div>
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="post_slug">Slug / URL<span class="required">*</span></label>
                        <input class="form__field__input" name="post_slug" id="post_slug" class="disable_slug" type="text" required value="<?=$post_slug;?>" tabindex="-1" placeholder="e.g. /your-page-name/" />
                    </div>
                
                    <div class="form__field">
                        <label class="form__field__label" for="post_alt_title">Date Published<span class="required">*</span></label>
                        <input class="form__field__input" name="post_alt_title" id="post_alt_title" type="date" required value="<?=(isset($post_alt_title))?$post_alt_title:date('Y-m-d');?>" placeholder="yyyy-mm-dd" />
                    </div>
                
                    <div class="form__field">
                        <label class="form__field__label" for="date_updated">Date Updated</label>
                        <input class="form__field__input" name="date_updated" id="v" type="date" value="<?=date('Y-m-d');?>" placeholder="yyyy-mm-dd" />
                    </div>
                    
                </fieldset>
            </div>
            <div class="form__group">
                <fieldset class="display-options">
                    <legend class="form__group__legend">Display Options</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="post_status">Page Status<span class="required">*</span></label>
                        <select class="form__select" name="post_status" id="post_status" required>
                            <option value="">Please choose...</option>
                            <option value="draft"<?=($post_status=='draft')?' selected':'';?>>Draft</option>
                            <option value="private"<?=($post_status=='private')?' selected':'';?>>Private</option>
                            <option value="live"<?=($post_status=='live')?' selected':'';?>>Live</option>
                        </select>
                    </div>
                </fieldset>
            </div>
            
        <?php         
        if ( file_exists(FS_ADMIN_ROOT.'/includes/inc_seo_content.php') && $admin_page_type == "edit" ) {
            include(FS_ADMIN_ROOT.'/includes/inc_seo_content.php'); 
        } ?>

		<?php if ( file_exists(FS_ADMIN_ROOT.'/modules/core/'.$module_form_settings['module_name'].'/extensions.php') ) {
            include(FS_ADMIN_ROOT.'/modules/core/'.$module_form_settings['module_name'].'/extensions.php'); 
        } ?>
        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>
        
    </form>

    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // If you're not editing or adding a post, you shouldn't really be here!
    $path = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($path);
    $path = implode('/',$path);
    // Go to the main index of this module.
    header("Location: ".$path.'/');
}
?>