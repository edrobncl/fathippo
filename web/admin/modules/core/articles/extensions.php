<div class="form__group">
  <fieldset>
    <legend  class="form__group__legend">Article Info</legend>
    
    <div class="form__field">
      <label class="form__field__label" for="article_abstract">Abstract</label>
      <input class="form__field__input" name="article_abstract" id="article_abstract" type="text" value="<?=$article_abstract??'';?>" />
    </div>

    <div class="form__field">
      <label class="form__field__label" for="article_content">Content</label>
      <textarea class="form__field__textarea richtext_editor" name="article_content" id="article_content" ><?=(isset($article_content))?$article_content:'';?></textarea>
    </div>
    
    <div class="form__field">
      <label class="form__field__label" for="article_content2">Content 2</label>
      <textarea class="form__field__textarea richtext_editor" name="article_content2" id="article_content2" ><?=(isset($article_content2))?$article_content2:'';?></textarea>
    </div>
  </fieldset>
</div>

<div class="form__group">
  <fieldset>
    <legend  class="form__group__legend">Call To Action</legend>

    <div class="form__field">
      <label class="form__field__label" for="show_cta">Show Call To Action</label>
      <input type="checkbox" id="show_cta" name="show_cta" value="1" <?= isset($show_cta) && $show_cta == 1 ? 'checked' : '' ?> />
    </div>

    <div class="form__field">
      <label class="form__field__label" for="cta_text">Text</label>
      <textarea class="form__field__textarea richtext_editor" name="cta_text" id="cta_text" ><?= $cta_text ?? '' ?></textarea>
    </div>

    <div class="form__field">
      <label class="form__field__label" for="cta_button_text">Button Text</label>
      <input type="text" class="form__field__input" name="cta_button_text" id="cta_button_text" value="<?= $cta_button_text ?? ''  ?>" />
    </div>

    <div class="form__field">
      <label class="form__field__label" for="cta_linked_page">Linked Page</label>
      <select class="form__field__input form__select" name="cta_linked_page" id="cta_linked_page">
        <option value="">Please select a page</option>
        <?php
          $pages = [];
          getPages(0, $pages);
        ?>
      </select>
    </div>
  </fieldset>
</div>

<?php 
  single_image('thumbnail_image', 'Article Thumbnail', '660px x 440px');

  function getPages( $parent_id, &$pages, $index = 0 ) {
    global $pdo, $cta_linked_page;

    $getPages = $pdo->prepare('
      SELECT * 
      FROM er_posts 
      WHERE post_type = "page"
      AND parent_id = :parent_id
    ');
    $getPages->execute([
      'parent_id' => $parent_id
    ]);
    $nextIndex = $index + 1;
    while ( $row = $getPages->fetch() ) { ?>
      <option value="<?= $row['_id'] ?>"<?=isset($cta_linked_page) && $cta_linked_page == $row['_id'] ? ' selected' : '';?>><?= str_repeat('-', $index) ?><?= $row['post_title'] ?></option>
      <?php
      getPages($row['_id'], $pages, $nextIndex);
    }
  }
