<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
	require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");
	
	// Create the identifier for this module - used to set the parent page's id for articles, events, etc.
	$parent_identifier = $module_form_settings['module_name'] . '_parent';
	
	// Save the parent page's ID for this post type
	if ( $_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST[$parent_identifier]) && trapCheck($_POST[$parent_identifier]) && isSuper() ) {
    	$parent_check = $_POST[$parent_identifier];
	    $checkParent = $pdo->query("SELECT * FROM er_options WHERE option_name = '$parent_identifier'")->fetch();
        if ( $checkParent ) {
            $saveParent = $pdo->prepare("UPDATE er_options SET option_value = ? WHERE option_name = ?");
        } else {
            $saveParent = $pdo->prepare("INSERT INTO er_options (option_value, option_name) VALUES (?,?)");
        }
        $saveParent->execute([$parent_check,$parent_identifier]);
	}
	
	// Set the value of the parent identifier variable (created above) - taken from either the submitted value, the database value or just set to zero
	$$parent_identifier = (isset($parent_check)) ? $parent_check : ( (isset($$parent_identifier)) ? $$parent_identifier : 0 );
    
    // Include this module's functions file, if it has one.
    if ( file_exists('mod_funcs.php') ) {
        include('mod_funcs.php');
    }
    
    require_once(FS_ADMIN_INCLUDES."inc_header.php");
    
    // Set sort order
    $sort_order = ( isset($module_form_settings['sort_order'])) ? $module_form_settings['sort_order'] : 'menu_order ASC';

    // Run query for articles
	$fetchArticles = $pdo->prepare("SELECT * FROM er_posts WHERE post_type = ? ORDER BY $sort_order");
	$fetchArticles->execute([$module_form_settings['post_type']]);
?>

<div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=$module_form_settings['icon']?>"></i> <?=$module_form_settings['plural']?></h1>
    <? if ( $$parent_identifier > 0 ) { ?><a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a><? } ?>
</div>
<div class="dashboard-pad">

<? if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? } ?>



<div id="row-list" class="wrap">
	
    <?
        if ( $$parent_identifier > 0 ) {
            while ( $row = $fetchArticles->fetch() ) {
                if ( function_exists('beforeRowOutput') ) { $row = beforeRowOutput($row); } ?>
                <div class="row row-<?=$row['post_status']?>">
                    <div class="row__title">
                        <a class="row__title__name" href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>"><?=$row['post_title']?></a>
                        <br /><div class="row__title__slug"><?=date("jS M Y",strtotime($row['post_alt_title']));?></div>
                    </div>
                    
                    <div class="row__actions">
                        <a href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>" class="button button--edit"><i class="fa fa-pencil"></i> Edit</a>
                        <a id="inline" href="#pop_delete_<?=$row['_id']?>" class="button button--delete"><i class="fa fa-trash"></i> Delete</a>
                    </div>
                    <div class="row__status">
                    	<?
    						switch ($row['post_status']) {
    							case 'live':
    							    $postIcon = 'check';
    							    break;
    							case 'draft':
    							    $postIcon = 'file-text-o';
    							    break;
    							case 'private':
    							    $postIcon = 'lock';
                                    break;
    						}
    					?>
                        <div class="row__status__title"><i class="fa fa-<?=$postIcon?>"></i> <?=$row['post_status']?></div>
                    </div>
                </div>
    			<div style="display:none">
    				<div id="pop_delete_<?=$row['_id']?>">
                	    <h3>Are you sure?</h3>
                        <p>This will delete <strong><?=$row['post_alt_title']?></strong> forever - please be sure before you delete this record.</p>
                    	<form id="delete-post-<?=$row['_id'];?>" method="post" action="/admin/scripts/manage_post.php">
                            <input type="hidden" name="post_id" value="<?=$row['_id'];?>">
                            <input type="hidden" name="return_to" value="<?=$_SERVER['REQUEST_URI'];?>">
                            <input type="hidden" name="post_type" value="<?=$row['post_type'];?>">
                            <button type="button" name="cancel_delete" class="button"><i class="fa fa-arrow-left"></i> No, don't delete it!</button>
                            <button type="submit" name="delete_post" class="button button--delete"><i class="fa fa-trash"></i> Yes! Delete it!</button>
                        </form>
                    </div>
                </div><?
            }
            
            if ($fetchArticles->rowCount() == 0) {
            ?><div class="none-found">There are no <?=strtolower($module_form_settings['plural']);?>. Why not <a href="<?=$module_form_settings['form_link'];?>">add one</a>?</div><?
            }
        } else {
        ?><div class="none-found"><i class="fa fa-warning"></i> The main <?=$module_form_settings['module_name'];?> page must be set before any <?=strtolower($module_form_settings['plural']);?> can be created.<?=(!isSuper())?' Please <a href="https://www.edwardrobertson.co.uk/contact">contact us</a> if you\'re seeing this message!':'';?></div><?
        }
    ?>
    
    <? // If logged in as a SuperUser, show the parent selector form
    if ( isSuper() ) { parentSelector($parent_identifier, $$parent_identifier); } ?>
</div>
</div>
<?php require_once(FS_ADMIN_INCLUDES.'inc_footer.php'); ?>
<?php require_once(FS_ADMIN_INCLUDES.'inc_js.php'); ?>