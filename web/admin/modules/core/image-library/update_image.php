<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    
    $data = [];
    
    $image_id = XSSTrapper($_POST['image_id']);
    $image_title = XSSTrapper($_POST['image_title']);
    $image_alt = XSSTrapper($_POST['image_alt']);
    
    if ( trapCheck($image_id) ) {
        
        $imageQuery = $pdo->prepare("UPDATE er_posts SET post_title = :image_title, post_alt_title = :image_alt, date_modified = NOW() WHERE _id = :image_id");
        $imageQuery->execute(['image_title' => $image_title, 'image_alt' => $image_alt, 'image_id' => $image_id]);
        
        if ( $imageQuery->rowCount() > 0 ) {
            $data['success'] = true;
            $data['image'] = ['_id' => $image_id, 'title' => $image_title, 'alt_title' => $image_alt];
        } else {
            $data['success'] = false;
            $data['error'] = 'There was a problem updating the details';
        }
        
    } else {
        $data['success'] = false;
        $data['error'] = 'No valid image ID was provided';
    }
    
    echo json_encode($data);
    
}