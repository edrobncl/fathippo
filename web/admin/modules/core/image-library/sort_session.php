<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');

if ( isset($_POST['sortimages']) ) {
    $_SESSION['sortimages'] = 'name';
} else {
    unset($_SESSION['sortimages']);
}

http_response_code(200);
exit();