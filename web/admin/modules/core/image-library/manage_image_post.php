<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

// Get the source (either js or nothing)
$source = XSSTrapper($_POST['source']);
unset($_POST['source']);

// Run the save or delete action
if ( !isset($_POST['delete_post']) ) {
    // If the action isn't delete...
    
    // Set destination folder var
    $destfolder = trim(rtrim($_POST['image_uploader_destfolder'][0],'/'),'/');
    unset($_POST['image_uploader_destfolder']);
    
    // Make sure the destination folder exists
    if ( !is_dir(FS_ROOT.$destfolder) ) {
        mkdir(FS_ROOT.$destfolder);
    }
    
    // Set filename var
    $filename = strtolower($_POST['image_uploader_filename'][0]);
    unset($_POST['image_uploader_filename']);
    
    // Clear out is_modal from $_POST
    if ( isset($_POST['is_modal']) ) {
        $is_modal = true;
        unset($_POST['is_modal']);
    }
    
    if ( $_POST['image_post_type'] == 'image' ) {
        // If it's an image
        
        // Create new image from temp
        $image = new Imagick(FS_ROOT.$destfolder."/temp/".$filename);
		$ext = $image->getImageFormat();
				
		// Check the format
		switch ($ext) {
			case 'JPEG':
				$format = "jpg";
				break;
            case 'PNG':
			case 'PNG24':
				$format = "png";
				break;
			case 'GIF':
				$format = "gif";
				break;
		}
		
		// Get the filename without it's extension
		$filename_noext = basename($filename, '.'.( substr_compare($filename, '.jpeg', -5) === 0 ? 'jpeg' : $format));
        
        // If the format isn't one of the above, set it as jpg
		if ( !$format ) {
            $image->setImageFormat('jpg');
            $format = 'jpg';
        }

        // Check image width
        $defaultGeo = $image->getImageGeometry();
        $defaultWidth = $defaultGeo['width'];

        $filenames = [];

        // Multiple file sizes
        if ( $defaultWidth > 1920 ) {
            // If the uploaded image is wider than 1920px (!?) set 3 sizes
            $filenames['large'] = $filename_noext.'--large';
            $filenames['1920'] = $filename_noext;
            $filenames['768'] = $filename_noext.'--600px';

        } else if ( $defaultWidth > 768 ) {
            // If the uploaded image is wider than 768px (but less than 1920) set 2 sizes
            $filenames['standard'] = $filename_noext;
            $filenames['768'] = $filename_noext.'--600px';

        } else {
            // File size is less than / equal to 768px just set one size
            $filenames['standard'] = $filename_noext;
        }

        // Loop through checking if file exists. If it does exist, add a number and check again.
        // When the file doesn't exist, write it
        function checkAndWrite($size,$count = 0) {
            global $format;
            global $nameToUse;
            global $destfolder;
            global $image;
            global $_POST;
            global $filename;
            global $filenames;
            
            if ( $count > 0 ) {
                $file = $destfolder.'/'.$nameToUse.'('.$count.').'.$format;
            } else {
                $file = $destfolder.'/'.$nameToUse.'.'.$format;
            }
            
            if ( file_exists(FS_ROOT.$file) ) {
                $count = $count + 1;
                checkAndWrite($size,$count);
            } else {
                // Write file to proper place
                if ( $size != 'large' && $size != 'standard' ) {
                    $image->resizeImage($size,0,Imagick::FILTER_LANCZOS,1);
                }
                $image->writeImage(FS_ROOT.$file);
        		chmod(FS_ROOT.$file ,0777);
                
                //if ( $size == 'standard' || $size == '1920' ) {
        		    // Update the post_slug
                    //$_POST['image_post_slug'] = '/'.$file;
                    $filenames[$size] = '/'.$file;
                //}
            }
        }

        // Loop through sizes and write file for each
        foreach ( $filenames as $size => $fname ) {

            $sizeToUse = $size;
            $nameToUse = $fname;

            checkAndWrite($sizeToUse);

            unset($sizeToUse);
            unset($nameToUse);
        }

        // Delete temp file
        unlink(FS_ROOT.$destfolder."/temp/".$filename);
        
		// Destroy canvas
		$image->clear();
		$image->destroy();
        
    }
    
    // Determine if this is a new post to be inserted, or a currently existing post to be updated.
    // - If post_id is greater than zero, then it must be a post.
    $form_type = ($_POST['image_post_id'] > 0) ? 'edit' : 'add';
    
    if ( $form_type == 'edit' ) {        
        $doSQL = $pdo->prepare("UPDATE er_posts SET
            post_status = :post_status,
            post_type = :post_type,
            parent_id = :parent_id,
            date_modified = NOW(),
            post_slug = :post_slug,
            menu_order = :menu_order,
            post_author = :post_author,
            post_title = :post_title,
            post_alt_title = :post_alt_title
            WHERE _id = :post_id
        ");

        $doSQL->execute([
            'post_status' => $_POST['image_post_status'],
            'post_type' => $_POST['image_post_type'],
            'parent_id' => $_POST['image_parent_id'],
            'post_slug' => $_POST['image_post_slug'],
            'menu_order' => $_POST['image_menu_order'],
            'post_author' => $_SESSION['eruid'],
            'post_title' => $_POST['image_post_title'],
            'post_alt_title' => $_POST['image_post_alt_title'],
            'post_id' => $_POST['image_post_id']
        ]);
        
        // Store the post_id as a variable.
        $post_id = $_POST['image_post_id'];
        
        // Set the action for the success message.
        $action = 'updated';
        
    } else if ( $form_type == 'add' ) {

        $doSQL = $pdo->prepare("INSERT INTO er_posts(
            date_created,
            post_status,
            post_type,
            parent_id,
            date_modified,
            post_slug,
            menu_order,
            post_author,
            post_title,
            post_alt_title
        )
        VALUES (
            NOW(),
            :post_status,
            :post_type,
            :parent_id,
            NOW(), 
            :post_slug,
            :menu_order,
            :post_author,
            :post_title,
            :post_alt_title
        )");

        foreach ( $filenames as $size => $slug ) {
        
            $doSQL->execute([
                'post_status' => $_POST['image_post_status'],
                'post_type' => $_POST['image_post_type'],
                'parent_id' => $_POST['image_parent_id'],
                'post_slug' => $slug,
                'menu_order' => $_POST['image_menu_order'],
                'post_author' => $_POST['image_post_author'],
                'post_title' => $_POST['image_post_title'],
                'post_alt_title' => $_POST['image_post_alt_title']
            ]);

            // Store the newly generated post_id as a variable.
            /*if ( $size == '1920' || $size == 'standard' ) {
                $post_id = $pdo->lastInsertId();
            }*/
            
            // Update $filenames to contain new post ids
            $filenames[$size] = $pdo->lastInsertId();

        }
        
        // Set the action for the success message.
        $action = 'created';
    }
    
    // Set $return_to, which is where to go after the script runs
    if ( isset($_POST['save_form']) ) {
        $return_to = $_POST['image_return_to'] . '?_id=' . $_POST['image_parent_id'];
    } else if ( isset($_POST['save_form_return']) || isset($_POST['delete_post']) ) {
        $return_to = dirname($_POST['image_return_to']).'/';
    }
    
    // Store the post_type for use in the success message.
    $post_type = $_POST['image_post_type'];
    
    // Clear out all of the main post keys/values and button keys/values, leaving only the postmeta keys/values remaining in $_POST.
    /*$unset_array = ['image_post_status', 'image_post_type', 'image_parent_id', 'image_post_slug', 'image_menu_order', 'image_post_author', 'image_post_title', 'image_post_alt_title', 'image_post_id', 'save_form', 'save_form_return', 'delete_post', 'image_return_to'];
    foreach ( $unset_array as $item ) {
        unset($_POST[$item]);
    }
    
    // Loop through the remaining $_POST keys/values, which should all be postmeta
    foreach ( $_POST as $meta_name => $meta_value ) {
        
        $checkMeta = $pdo->prepare("SELECT * FROM er_postmeta WHERE post_id = ? AND meta_name = ? LIMIT 1");
        $checkMeta->execute([$post_id, $meta_name]);
        
        if ( $checkMeta->rowCount() > 0 ) {
            // If the meta_name already exists, update it.
            if ( trim($meta_value) == '' ) {
                // If the meta_value to be saved is empty, just delete the record from the table to save space.
                $putMeta = $pdo->prepare("DELETE FROM er_postmeta WHERE post_id = :post_id AND meta_name = :meta_name");
                $needsVal = false;
            } else {
                // If the meta_value to be saved is not empty, update the record.
                $putMeta = $pdo->prepare("UPDATE er_postmeta SET meta_value = :meta_value WHERE post_id = :post_id AND meta_name = :meta_name");
                $needsVal = true;
            }
        } else {
            // If the meta_name doesn't exist, insert it.
            if ( trim($meta_value) != '' ) {
                $putMeta = $pdo->prepare("INSERT INTO er_postmeta(post_id, meta_name, meta_value) VALUES (:post_id, :meta_name, :meta_value)");
                $needsVal = true;
            }
        }
        
        // If there's a query to run, go ahead.
        if ( isset($putMeta) ) {
            // Bind the parameters for the insert/update/delete query.
            $putMeta->bindParam(':post_id',$post_id,PDO::PARAM_INT);
            $putMeta->bindParam(':meta_name',$meta_name,PDO::PARAM_STR);
            
            // If not deleting, bind the meta_value parameter too.
            if ( isset($needsVal) && $needsVal == true ) {
                $putMeta->bindParam(':meta_value',$meta_value,PDO::PARAM_STR);
            }
            
            // Run the query with the correct parameters.
            $putMeta->execute();
            
            // Clear out the $putMeta variable, ready for the next postmeta.
            unset($putMeta);
        }
        
        // Clear out this meta from $_POST
        unset($_POST[$meta_name]);
    }*/
    
} else {
    // If the action is delete...
    
    $deleteSQL = ["DELETE FROM er_postmeta WHERE post_id = :post_id", "DELETE FROM er_posts WHERE _id = :post_id"];
    
    // Run a query to delete the main post from er_posts, and another query to delete any relevent meta from er_postmeta.
    foreach( $deleteSQL as $query ) {
        $doDelete = $pdo->prepare($query);
        $doDelete->execute(['post_id' => $_POST['image_post_id']]);
    }
    
    // Set the action for the success message.
    $action = 'deleted';
    
    // Store the return_to value
    $return_to = $_POST['image_return_to'];
}

if ( isset($source) && $source == 'js' ) {
    // If the source was js, pass back the details - provided the post_id exists (image saved)
    
    foreach ( $filenames as $size => $post_id ) {
        if ( isset($post_id) && $post_id > 0 ) {
            $imageQuery = $pdo->prepare("SELECT * FROM er_posts WHERE _id = ?");
            $imageQuery->execute([$post_id]);
            $image = $imageQuery->fetch();
            
            if ( file_exists(FS_ROOT.$image['post_slug']) ) {
                $fs = filesize('/'.trim(FS_ROOT,'/').$image['post_slug']);
                $image['filesize'] = formatBytes($fs,0);
            }
            
            if ( $image['post_type'] == 'image' ) {
                $magick = new Imagick('/'.trim(FS_ROOT,'/').$image['post_slug']);
                $geo = $magick->getImageGeometry();
                $image['dimensions'] = ['width'=>$geo['width'],'height'=>$geo['height']];
                $magick->clear();
                $magick->destroy();
            }
            
            $data['images'][$size] = $image;
            
        }
    }

    if ( isset($is_modal) && $is_modal === true ) { $data['is_modal'] = true; }
    echo json_encode($data);
    
    exit();
} else {
    // If the source isn't js and the action is set, store the success message in session
    if ( isset($action) ) {
        $_SESSION['success'] = ucfirst($post_type) . ' successfully ' . $action;
    }
}

// Redirect to the return_to page, if not using AJAX
header("Location: ".$return_to);