<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    
    $data = [];
    
    $image_id = XSSTrapper($_POST['image_id']);
    $image_slug = XSSTrapper($_POST['image_slug']);
        
    if ( trapCheck($image_id) && trapCheck($image_slug) ) {
        
        // Sort out any issues with html entities
        $image_slug = html_entity_decode($image_slug);
        
        // Run the delete query
        $doDelete = $pdo->prepare("DELETE FROM er_posts WHERE _id = :post_id AND post_slug = :post_slug");
        $doDelete->execute(['post_id' => $image_id, 'post_slug' => $image_slug]);
        
        if ( $doDelete->rowCount() > 0 ) {
            $data['success'] = true;
            
            // Delete the file
            if ( file_exists(FS_ROOT.$image_slug) ) {
                unlink(FS_ROOT.$image_slug);
            }
            
            // Find any uses of the image in postmeta and remove any reference to it
            $search = "%$image_slug%";
            $searchMeta = $pdo->prepare("SELECT * FROM er_postmeta WHERE meta_value LIKE ?");
            $searchMeta->execute([$search]);
            
            while ( $result = $searchMeta->fetch() ) {
                //print_r($result);
                $meta_array = json_decode($result['meta_value'], true);
                
                if ( isset($meta_array['_id']) ) {
                    
                    // Just the one here, so it can be deleted
                    $metaDeleteQuery = $pdo->prepare("DELETE FROM er_postmeta WHERE _id = ?");
                    $metaDeleteQuery->execute([$result['_id']]);
                    
                } else {
                    // More than one item
                    // Loop through and remove the one we're deleting
                    /*for ( $i = 0; $i < count($meta_array); $i++ ) {
                        if ( $meta_array[$i]['post_slug'] == $image_slug ) {
                            unset($meta_array[$i]);
                        }
                    }*/

                    $data['response'][] = $result;

                    /*$meta_array = array_values($meta_array);
                    
                    // If the one we're deleting is the last one, just remove the row entirely, otherwise save the updated row
                    if ( count($meta_array) == 0 ) {
                        $metaDeleteQuery = $pdo->prepare("DELETE FROM er_postmeta WHERE _id = ?");
                        $metaDeleteQuery->execute([$result['_id']]);
                    } else {
                        
                        $save_meta = json_encode($meta_array, JSON_UNESCAPED_SLASHES);
                                    
                        $metaUpdateQuery = $pdo->prepare("UPDATE er_postmeta SET meta_value = ? WHERE _id = ?");
                        $metaUpdateQuery->execute([$save_meta,$result['_id']]);
                    }*/
                            
                }
            }
            
        } else {
            $data['success'] = false;
            $data['error'] = 'There was a problem deleting the image. Please try again';
        }
        
    } else {
        $data['success'] = false;
        $data['error'] = 'No valid image ID was provided';
    }
    
    echo json_encode($data);
    
}