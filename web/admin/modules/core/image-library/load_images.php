<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

$imageQuery = $pdo->prepare("SELECT * FROM er_posts WHERE post_type = ? ORDER BY date_created DESC");
$imageQuery->execute(['image']);
$images = $imageQuery->fetchAll();

$output = [];

foreach ( $images as $k => $image ) {
    if ( file_exists(FS_ROOT.$image['post_slug']) ) {
        $fs = filesize('/'.trim(FS_ROOT,'/').$image['post_slug']);
        $image['filesize'] = formatBytes($fs,0);

        $imageSize = getimagesize(FS_ROOT.$image['post_slug']);
        $image['dimensions'] = ['width'=>$imageSize[0],'height'=>$imageSize[1]];

        $expl = explode('/',$image['post_slug']);
        $image['filename'] = strtolower(array_pop($expl));

        $output[] = $image;
    }
}

echo json_encode($output);