<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Image',
    'plural' => 'Images',
    'form_link' => '',
    'main_menu' => [
        'option' => 'secondary',
        'order' => 1
    ],
    'post_type' => 'image',
    'icon' => 'picture-o'
];
?>