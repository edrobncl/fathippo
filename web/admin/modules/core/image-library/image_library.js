/*
    Variable definitions
                                        */

// Define variable for the image library's manage tab - content area
var $manageContent = $('.image-library-tab.manage').find('.tab-content').find('.tab-inner');

// Define variable for the image editor
var $imageEditor = $('.image-library-tab.manage').find('.image-editor');

// Define variable for the form section to be populated (header image, gallery, download, etc.)
var $uploadOrigin = null;

// Define variable for gallery item when selected to edit
var $galleryItem = null;

// Define variable to store the list of images (filled by loadimages() function)
var images = [];

// Define variable to store the list of images sorted alphabetically by filename
var imagesByName = [];

// Define whether working in a popup (true) or the main image library (false)
var in_image_library = ($('.image_library_modal').length) ? true : false;

// Define variable to hold TinyMCE editor instance when required
var tinymceEditor;

// Define variable to determine if editing directly from an image gallery
var editFromGallery = false;

/*
    Main functions
                                */
                                
// Function to show "you've made a change" warning
function showChangeWarning() {
    var $formActions = $('.form-actions');
    if ( !$formActions.hasClass('visible') ) {
        $formActions.addClass('visible');
    }
}
                                
// Function to return image html
function imageHtml(image) {
    switch ( image.post_type ) {
        case 'image':
            output = '<div class="image-box" data-type="'+image.post_type+'" data-id="'+image._id+'"><div class="image-box-image" style="background-image: url(\''+image.post_slug+'\');"></div><p>'+image.post_title+'';
            if(image.dimensions.width == 600) {
                output += '<strong><i class="fa fa-mobile"></i> Mobile</strong>';
            }
            output += '</p></div>';
            break;
            
        case 'file':
            var icon = imageIcon(image.post_slug);
            var filename = imageFilename(image.post_slug);
            output = '<div class="image-box" data-type="'+image.post_type+'" data-id="'+image._id+'"><i class="fa fa-'+icon+'"></i> <span>'+filename+'</span></div>';
            break;
            
        default:
            output = '<div class="image-box" data-type="'+image.post_type+'" data-id="'+image._id+'"><i class="fa fa-question-circle-o"></i> '+image.post_title+' ('+image.post_slug+')</div>';
    }
    
    return output;
}

// Function to load images into content box
function importimages(images) {
    var imageCount = 0;
    var files = 0;
    
    // Load in each image
    $.each(images, function(k, image) {
        
        var output = imageHtml(image);
        
        $manageContent.append(output);
    });
}

// Function to determine icon for image
function imageIcon(post_slug) {
    
    var ext = post_slug.substring(post_slug.lastIndexOf(".")+1).toLowerCase();
    
    switch (ext) {
		case 'doc':
		case 'docx':
		    file_icon = 'file-text-o';
		    break;
        case 'pdf':
            file_icon = 'file-pdf-o';
            break;
        case 'csv':
        case 'xls':
        case 'xlsx':
            file_icon = 'file-excel-o';
            break;
        case 'jpg':
        case 'jpeg':
        case 'gif':
        case 'png':
        case 'tif':
        case 'psd':
            file_icon = 'file-image-o';
            break;
        case 'zip':
        case 'rar':
        case '7z':
        case 's7z':
            file_icon = 'file-archive-o';
            break;
        default:
            file_icon = 'file-o';
	}
	
	return file_icon;
}

// Function to get filename from slug
function imageFilename(post_slug) {
    return post_slug.substring(post_slug.lastIndexOf("/")+1);
}

// Function to select an image from the list by it's ID
function selectimage(images, id) {
    for ( i = 0; i < images.length; i++ ) {
        if ( images[i]._id == id ) {
            return images[i];
        }
    }
}

// Function to load all images from the database
function loadimages() {
    
    // Show "loading" message
    $manageContent.html('<i class="fa fa-spin fa-spinner"></i> Loading...');
    
    $.get( '/admin/modules/core/image-library/load_images.php', function(res) {
        // Clear "loading" message
        $manageContent.html('');
        
        //console.log(res);
        
        // If there are results, load them in. Otherwise, show "no images" message.
        if ( res.length ) {
            //console.log(res);
            images = res;

            for (var i = 0; i < images.length; i++) {
                imagesByName.push(images[i]);
            }

            imagesByName.sort(function(a,b) {
                if ( a.filename != undefined && b.filename != undefined ) {
                    var nameA = a.filename.toLowerCase();
                    var nameB = b.filename.toLowerCase();
                    return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
                }

                return 1;
            });

            if ( $('input[name="sort-images"]:checked').attr('id') == 'sort-by-date' ) {
                importimages(images);
            } else if ( $('input[name="sort-images"]:checked').attr('id') == 'sort-by-name' ) {
                importimages(imagesByName);
            } else {
                importimages(images);
            }
            $manageContent.trigger('imagesloaded');
        } else {
            // There aren't any images.
            $manageContent.html('No images!');
        }
    }, 'json')
    .fail(function(err) {
        console.error(err);
    });
}

// Function to determine which tab is active, and show the appropriate nav buttons
function getActiveTab() {
    var activeTab = '';
    if ( $('.image-library-tab.active').hasClass('manage') ) {
        activeTab = 'manage';
        $('.image-library-nav a[href="#upload"]').show();
        $('.image-library-nav a[href="#manage"]').hide();
    } else if ( $('.image-library-tab.active').hasClass('upload') ) {
        activeTab = 'upload';
        $('.image-library-nav a[href="#upload"]').hide();
        $('.image-library-nav a[href="#manage"]').html('Manage images').show();
    } else if ( $('.image-library-tab.active').hasClass('edit') ) {
        activeTab = 'edit';
        $('.image-library-nav a[href="#upload"]').hide();
        $('.image-library-nav a[href="#manage"]').html('Back to images').show();
    }
    
    return activeTab;
}

// Function to open image editor
function openimageEditor(image) {
    // Fade the image library out
    $manageContent.animate({'opacity':'0.1'},300);
    
    var aDetails = '';
    var filename = imageFilename(image.post_slug);
    var icon = imageIcon(image.post_slug);
    
    aDetails += '<div class="wrap image-details-preview" style="background-image: url(\''+image.post_slug+'\')"></div>';
    
    aDetails += '<div class="wrap image-details-text"><h2><span>'+filename+'</span></h2><div class="image-detail-snippet"><b>Size:</b> '+image.filesize+((image.dimensions)?'</div><div class="image-detail-snippet"><b>Dimensions:</b> '+image.dimensions.width+'px by '+image.dimensions.height + 'px':'')+'</div></div><div class="image-details-fields"><fieldset><div class="form__field"><label class="form__field__label" for="image-details-title">Title <span class="required">*</span></label><input class="form__field__input" type="text" name="image-details-title" id="image-details-title" value="'+image.post_title+'" required></div><div class="form__field"><label class="form__field__label" for="image-details-alt">Description <span class="required">*</span></label><input class="form__field__input" type="text" name="image-details-alt" id="image-details-alt" value="'+image.post_alt_title+'" required></div></fieldset></div><div class="wrap  form__field image-details-options"><button type="button" class="button" id="cancel-image">Cancel</button>'+((!in_image_library) ? '<button class="button action delete button--delete" type="button" id="delete-image">Delete Image</button><button class="button" type="button" id="update-image">Update image Details</button>':'<button class="button" type="button" class="button" id="select-image">'+((editFromGallery)?'Update Gallery':'Select Image')+'</button>')+'</div>';
    
    // Populate the editor content
    $imageEditor.find('.image-editor-details').html(aDetails);
    
    // Assign the image ID to the editor
    $imageEditor.attr('data-image',image._id);
    
    // Add class to slide open
    $imageEditor.addClass('open');
    
    // Stop page scroll
    $('body').css('overflow','hidden');
}

// Function to close image editor
function closeimageEditor() {
    // Fade the image library in
    $manageContent.animate({'opacity':'1'},300);
    
    // Remove class to slide closed
    $imageEditor.removeClass('open');
    
    // Remove the image ID from the editor
    $imageEditor.removeAttr('data-image');
    
    // Remove current editor image details
    $imageEditor.find('.image-editor-details').html('');
    
    // Remove the delete confirmation popup if it's open
    $('.confirm-image-delete').remove();
    
    if ( !in_image_library ) {
        $('body').css('overflow','auto');
    }
}
    
/*
    Main features
                            */
$(function() { // Document Ready     
    // If not in "modal" mode, load images on doc.ready
    if ( $('#image-library').length ) {
        loadimages();
        getActiveTab();
    }
});

// Switch tabs in image library
$(document).on('click', '.image-library-nav a', function(e) {
    e.preventDefault();
    var tab = $(this).attr('href').slice(1);

    if ( !$('.image-library-tab.active').hasClass(tab) && tab != 'close' ) {
        $('.image-library-tab.active').fadeOut(200, function() {
            $(this).removeClass('active');
            $('.image-library-tab.'+tab).fadeIn(200).addClass('active');
        });
    }
    
    if ( tab != 'close' ) {
        var otherTab = (tab=='manage') ? 'upload' : 'manage';
        $('.image-library-nav a[href="#'+otherTab+'"]').show();
        $('.image-library-nav a[href="#'+tab+'"]').hide();
    }
});

// Remove the success message after an upload
$(document).on('removeuploadalert', function() {
    $(".alert.success").delay(1000).fadeOut(1000, function() { $(this).remove(); });
});

// Handle image upload form submission with ajax
$(document).on('submit', '#image-uploader-form', function(e) {
    e.preventDefault();

    var imageForm = $(this);

    $.ajax({
        method: 'post',
        url: imageForm.attr('action'),
        data: imageForm.serialize() + '&source=js',
        dataType: 'json',
        success: function(res) {
            if ( res.is_modal === true ) {
                // Show success message
                // $('body').append('<div class="alert success"><i class="fa fa-check"></i> '+res.success_msg+'</div>').trigger('removeuploadalert');
                
                var imgToUse = null;

                if ( res.images.standard != undefined ) {
                    imgToUse = res.images.standard;
                } else if ( res.images['1920'] != undefined ) {
                    imgToUse = res.images['1920'];
                }

                // Place new content onto module form page
                useimage(imgToUse, 'upload');
                
                // Push new images into images array and update library management tab
                $.each(res.images, function(k,v) {
                    var output = imageHtml(v);
                    $manageContent.prepend(output);
                    images.push(v);
                })
                
                // Close image library
                closeimagelibrary();
                
                // Alert that something's changed
                showChangeWarning();
                
                // Clear upload tab
                $('#image_uploader_progress span, .image_uploader[data-step="2"]').removeAttr('style');
                $('.image_progress_details').html('&nbsp;');
                $('.upload_btn_wrap button').html('Select a file');
                $('#image_post_id').val('0');
                $('.upload_btn_wrap input[name="image_uploader"], #image_post_type, #image_uploader_filename, #image_post_slug, #image_post_title, #image_post_alt_title').val('');

            } else {
                // Reload page onto library tab
                window.location.href = '/admin/modules/core/image-library/';
            }
        },
        error: function(err) {
            console.error('error',err);
        }
    });
});

// Open image editor
$(document).on('click', '.image-box', function() {
    // Pull image details
    var imageId = $(this).attr('data-id');
    var imageDetails = selectimage(images, imageId);
    
    // Open the editor
    openimageEditor(imageDetails);
});

// Close image editor
$(document).on('click', '.image-editor-bg, #cancel-image', function() {
    if ( editFromGallery ) {
        closeimagelibrary();
        editFromGallery = false;
        image = undefined;
    } else {
        closeimageEditor();
    }
});

// Update image info
$(document).on('click', '#update-image', function() {
    var image_id = $imageEditor.attr('data-image');
    var image_title = $('#image-details-title').val();
    var image_alt = $('#image-details-alt').val();
    
    $.ajax({
        method: 'post',
        url: '/admin/modules/core/image-library/update_image.php',
        data: {
            image_id : image_id,
            image_title : image_title,
            image_alt : image_alt
        },
        dataType: 'json',
        beforeSend: function() {
            $('.image-update-msg').remove();
        },
        success: function(res) {
            if ( res.success == true ) {
                
                // Update this image in the images array
                for ( i = 0; i < images.length; i++ ) {
                    if ( images[i]._id == res.image._id ) {
                        images[i].post_title = res.image.title;
                        images[i].post_alt_title = res.image.alt_title;
                        break;
                    }
                }
                
                // Show success message
                $('.image-details-options').before('<div class="wrap  form__field image-update-msg success"><i class="fa fa-check"></i> Details updated successfully.</div>');
                $('.image-update-msg').slideDown(200);
                
            } else {
                // Show error message
                $('.image-details-options').before('<div class="wrap  form__field image-update-msg error"><i class="fa fa-times"></i> '+((res.error)? res.error : 'There was a problem updating the details.')+'</div>');
                $('.image-update-msg').show();
            }
        },
        error: function(err) {
            // Show error message
            $('.image-details-options').before('<div class="wrap  form__field image-update-msg error"><i class="fa fa-times"></i> There was a problem updating the details.</div>');
            $('.image-update-msg').show();
        }
    });
});

// Delete image from library
$(document).on('click','#delete-image', function() {
    var image_id = $imageEditor.attr('data-image');
    var imageToDelete;
    
    for ( var i = 0; i < images.length; i++ ) {
        if ( images[i]._id == image_id ) {
            imageToDelete = i;
        }
    }
    
    $imageEditor.find('.image-editor-inner').append('<div class="confirm-image-delete"><div class="confirm-inner"><p>Are you sure you want to delete this image?</p><div class="confirm-delete-buttons"><button type="button" class="button cancel" id="cancel-image-delete">No</button><button type="button" class="button delete" id="accept-delete">Yes, I\'m sure</button></div></div></div>');

    // Cancel delete and remove the confirmation popup
    $(document).on('click', '#cancel-image-delete', function() {
        $('.confirm-image-delete').remove();
    });
    
    // Confirm delete and proceed
    $(document).on('click', '#accept-delete', function() {
        $.ajax({
            method: 'post',
            url: '/admin/modules/core/image-library/delete_image.php',
            data: {
                image_id: images[imageToDelete]._id,
                image_slug: images[imageToDelete].post_slug
            },
            dataType: 'json',
            beforeSend: function() {
                $('.image-update-msg').remove();
            },
            success: function(res) {
                if ( res.error ) {
                    // Show error message
                    $('.image-details-options').before('<div class="wrap  form__field image-update-msg error"><i class="fa fa-times"></i> '+res.error+'.</div>');
                    $('.image-update-msg').show();
                } else {

                    //console.log(res);
                    
                    // Remove the image from the library
                    $('.image-box[data-id="'+images[imageToDelete]._id+'"]').remove();
                    
                    // Remove the image from the images array
                    images.splice(imageToDelete,1);
                    
                    // Close the image editor
                    closeimageEditor();
                }
            },
            error: function(err) {
                console.error(err.responseText);
            }
        });
    });

});

// Sort images by name (a-z) or by date uploaded (desc - default)
$(document).on('change', 'input[name="sort-images"]', function() {
    var sortAction = $(this).attr('id');
    var sortData = {};

    // Clear "loading" message
    $manageContent.html('');

    if ( sortAction == 'sort-by-date' ) {
        importimages(images);
    } else if ( sortAction == 'sort-by-name' ) {
        importimages(imagesByName);
        sortData = { sortimages: 1 };
    }

    // Set / unset session so sorting is retained
    $.ajax({
        url: '/admin/modules/core/image-library/sort_session.php',
        method: 'post',
        data: sortData
    });

    $manageContent.trigger('imagesloaded');
});

/*
    Functions & features only required when image library is loaded into a popup
                                                                                                                                        */
                                                                                                                    
if ( $('.image_library_modal').length ) {

    /*
        Popup functions
                                    */

    // Function to open the image library
    function openimagelibrary(imageAction, editor) {
        $('.image_library_modal, .image_library_bg').fadeIn(300);
        $('.image-library-tab.active').removeClass('active').removeAttr('style');
        $('.image-library-tab.'+imageAction).removeAttr('style').addClass('active');
        
        $('body').css('overflow','hidden');
        
        // If loaded from TinyMCE, store editor instance. Otherwise it'll be null
        tinymceEditor = editor;
        
        // If images haven't already been loaded, run the function to load them
        if ( !$manageContent.find('.image-box').length ) {
            loadimages();
        } else {
            setTimeout(function() {
                $manageContent.trigger('imagesloaded');
            }, 500);
        }
    }
    
    // Function to close the image library
    function closeimagelibrary() {
        $('.image_library_modal, .image_library_bg').fadeOut(300);
        $('body').css('overflow','auto');
        
        // Scroll to top of library ready for next use
        $manageContent.parent().animate({scrollTop: 0}, 50);
        
        closeimageEditor();
    }
    
    // Function to update the hidden input that saves a gallery
    function updateGalleryInput(origin) {
        var multi_list = [];
        
        origin.find('.gallery-list').find('.actual-gallery-item').each(function() {
            var attr = $(this).attr('data-item');
            // If there's an item, add it to the array
            if ( typeof attr !== typeof undefined && attr !== false ) {
                multi_list.push(attr);
            }
        });
        
        multi_list = '['+multi_list.join(',')+']';
        
        // Show the message explaining sorting if there's more than one image. Hide it if there's less than 2.
        if ( $('.actual-gallery-item').length > 1 ) {
            origin.find('.gallery-drag-info').show();
        } else {
            origin.find('.gallery-drag-info').hide();
        }
        
        origin.find('input[type="hidden"]').val(multi_list).trigger('change');
    }
    
    // Function to select an image to use
    function useimage(image, method) {

        var newTitle = (method == 'upload') ? image.post_title : $('#image-details-title').val();
        var newAlt = (method == 'upload') ? image.post_alt_title : $('#image-details-alt').val();
        var imageSplit = image.post_slug.split('/');
        var imageFilename = imageSplit[imageSplit.length-1];
        
        var insertimage = {
            "_id": image._id,
            "post_slug": image.post_slug,
            "post_title": newTitle,
            "post_alt_title": newAlt
        };

        if ( $uploadOrigin == 'tinymce' ) {
            
            // If loaded from TinyMCE, insert an image into the editor
            tinymceEditor.insertContent('<img src="'+image.post_slug+'" title="'+newTitle+'" alt="'+newAlt+'" />');
            
        } else {
            
            var insert_json = JSON.stringify(insertimage);
                        
            if ( $uploadOrigin.data('type') == 'single_image' ) { // If this is a single image upload
                
                // Insert the image object into the page form
                $uploadOrigin.find('input[type="hidden"]').val(insert_json);
                
                // If there's already an image set, just update it. Otherwise, add it to the page
                if ( $uploadOrigin.find('.current-image img').length ) {
                    $uploadOrigin.find('.current-image img').attr({'src':image.post_slug,'alt':newAlt});
                    $uploadOrigin.find('.current-image .ci-filename').text(imageFilename);
                    $uploadOrigin.find('.current-image .geo-width').text(image.dimensions.width);
                    $uploadOrigin.find('.current-image .geo-height').text(image.dimensions.height);
                    $uploadOrigin.find('.current-image .ci-filesize').text(image.filesize);
                } else {
                    var imgOutput = '<div class="current-image__image"><img src="'+image.post_slug+'" alt="'+newAlt+'" /></div><div class="current-image__details"><p><b>Current Image</b></p><p><i class="fa fa-image-text-o"></i> <span class="ci-filename">'+imageFilename+'</span></p><ul><li><b>Dimensions:</b> <span class="geo-width">'+image.dimensions.width+'</span> x <span class="geo-height">'+image.dimensions.height+'</span> pixels</li><li><b>Size:</b> <span class="ci-filesize">'+image.filesize+'</span></li></ul><button class="button action delete button--delete remove-single-image" type="button"><i class="fa fa-times"></i> Remove</button></div>';

                    $uploadOrigin.find('.current-image .pad').append(imgOutput);
                    $uploadOrigin.find('.current-image, .update-image').slideDown(300);
                }
                
            } else if ( $uploadOrigin.data('type') == 'gallery' ) { // If it's a gallery image
                
                var galleryItemHtml = '<li class="actual-gallery-item" data-item=\''+insert_json.replace(/'/g,"\\'")+'\'><div class="gallery-image-handle"><i class="fa fa-sort"></i></div><div class="gallery-image-preview" style="background-image: url(\''+insertimage.post_slug+'\');"></div><div class="gallery-image-text"><p><strong>'+insertimage.post_title+'</strong></p><p>'+insertimage.post_alt_title+'</p></div><div class="gallery-image-actions"><button type="button" class="button action edit button--edit"><i class="fa fa-pencil"></i> Edit</button><button type="button" class="button action delete button--delete"><i class="fa fa-times"></i> Remove</button></div></li>';
                
                if ( editFromGallery ) {
                    
                    $galleryItem.replaceWith(galleryItemHtml);
                    
                } else {
                    // Hide the "Gallery is empty" message
                    $uploadOrigin.find('.gallery-list').removeClass('empty-gallery').find('li:first-child').hide();
                    
                    // Add the new item to the images list
                    $uploadOrigin.find('.gallery-list').append(galleryItemHtml);
                }
                
                
                // Update the input field with the correct order of images
                updateGalleryInput($uploadOrigin);
                
                // Check sorting is enabled if needed
                checkSorting($uploadOrigin);
                
            }
        
        }
        
        closeimagelibrary();
    }
    
    // Sorting
    function checkSorting(origin) {
        // If there's only one item left, disable sorting. Otherwise activate it.
        if ( origin.find('.actual-gallery-item').length == 1  ) {
            // Disable sorting
            if ( origin.find('.gallery-list').hasClass('ui-sortable') ) {
                origin.find('.gallery-list').sortable('destroy');
            }
        } else {
            // Activate / handle sorting
            origin.find('.gallery-list').sortable({
                'handle': '.gallery-image-handle',
                'start': function(event, ui) {
                    ui.item.addClass('moving');
                },
                'stop': function(event, ui) {
                    ui.item.removeClass('moving');
                },
                'update': function(event, ui) {
                    updateGalleryInput(origin);
                }
            });
        }
    }
    
    /*
        Popup features
                                    */
    $('.gallery-list').each(function() {
        if ( $(this).find('.actual-gallery-item').length > 1 ) {
            var galleryOrigin = $(this).closest('.frm_uploader');
            $(this).sortable({
                'handle': '.gallery-image-handle',
                'start': function(event, ui) {
                    ui.item.addClass('moving');
                },
                'stop': function(event, ui) {
                    ui.item.removeClass('moving');
                },
                'update': function(event, ui) {
                    updateGalleryInput(galleryOrigin);
                }
            });
        }
    });
    
    // Open the image library
    $('.open-images').on('click', function(e) {
        e.preventDefault();
        var imageAction = $(this).attr('id').substr(0, $(this).attr('id').length-7);
        
        // Get the section of the form that will be populated
        $uploadOrigin = $(this).closest('.frm_uploader');
        
        // If we don't know what type of uploader is being used, show error in console. If it's set, all good!
        if ( !$uploadOrigin.data('type') ) {
            console.error('No type on "'+$uploadOrigin.find('legend:first').text()+'" uploader');
        } else {
            openimagelibrary(imageAction, null);
            getActiveTab();
        }
    });

    // Close the image library
    $(document).on('click', '.image-library-nav a[href="#close"], .image_library_bg', closeimagelibrary);
    
    // Select image to use
    $(document).on('click', '#select-image', function() {
        var imageId = $imageEditor.attr('data-image');
        var image = selectimage(images, imageId);
        
        useimage(image, 'select');
        
        // Alert that something's changed
        showChangeWarning();
    });

    var imageInput = null;
    var currentImage = null;
    
    // Remove an image (single image)
    $(document).on('click', '.remove-single-image', function() {
        imageInput = $(this).closest('.card').find('input');
        currentImage = $(this).parents('.current-image');
        var count = 0;
        
        currentImage.append('<div class="rsi-pop"><h3>Are you sure you\'d like to remove this image?</h3><button type="button" class="button button--cancel rsi-cancel">No, don\'t do it</button><button type="button" class="button confirm-rsi">Yes, remove the image</button></div>');
    });

    // Cancel removing the image
    $(document).on('click', '.rsi-cancel', function() {
        $('.rsi-pop').remove();
    });

    // Remove the image
    $(document).on('click', '.confirm-rsi', function() {
        $('.rsi-pop').remove();
        imageInput.val('').trigger('change'); 
        currentImage.hide().find('img').closest('.pad').find('div').remove();
        currentImage.next('.update-image').hide();
    });
    
    // Remove an image (multi image)
    $(document).on('click', '.gallery-image-actions button.delete', function() {
        var origin = $(this).closest('.frm_uploader');
        // Remove the image from the list
        $(this).closest('.actual-gallery-item').remove();
        
        // If there are no images left in the list, clear the input value and show the "empty gallery" message
        if ( origin.find('.actual-gallery-item').length <= 0 ) {
            origin.find('input[type="hidden"]').val('').trigger('change');
            origin.find('.gallery-drag-info').hide();
            origin.find('.gallery-list').addClass('empty-gallery').find('li:first-child').show();
            if ( origin.find('.gallery-list').hasClass('ui-sortable') ) {
                origin.find('.gallery-list').sortable('destroy');
            }
        } else {
            // Update the input field with the correct order of images
            updateGalleryInput(origin);
            // Check sorting is enabled if needed
            checkSorting(origin);
        }
    });
    
    // Edit an image (multi image)
    $(document).on('click', '.gallery-image-actions button.edit', function() {
        // Get the section of the form that will be populated
        $uploadOrigin = $(this).closest('.frm_uploader');
        
        // Set this gallery item variable for use later
        $galleryItem = $(this).closest('.actual-gallery-item');
        
        // Say we're editing from a gallery
        editFromGallery = true;
        
        var image = JSON.parse($galleryItem.attr('data-item'));
        
        // Open Image Library to "manage" tab
        openimagelibrary('manage', null);
        getActiveTab();
        
        $manageContent.on('imagesloaded', function() {
            // Get full image details from array 
            orig_image = selectimage(images,image._id);
            
            // Add extra info from db for image to show up correctly in editor
            image.dimensions = orig_image.dimensions;
            image.filesize = orig_image.filesize;
            
            // Open editor
            openimageEditor(image);
        });
        
    });
    
}
