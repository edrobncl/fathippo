<div class="image-library-wrap wrap"<?=(isset($in_image_library) && $in_image_library === true)?' id="image-library"':'';?>>
   
    <div class="image-library-nav">
        <a class="button" href="#upload" style="display: none;"><i class="fa fa-upload"></i> Upload New Image</a><a class="button" href="#manage" style="display: none;"><i class="fa fa-folder-o"></i> Manage Images</a>
        <? if ( !isset($in_image_library) ) { ?><a href="#close" class="close-btn"><i class="fa fa-times"></i> Close</a><? } ?>
    </div>
    
    <div class="wrap image-library-tab upload">        
        <div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-upload"></i> Upload Image</h1>
    	</div>
        
        <div class="wrap tab-content">
            <div class="wrap image_uploader" data-step="1">

                <? // Specifies which uploader activated the image library, so we know which one to update after the upload is complete ?>
                <? if ( !isset($in_image_library) ) { ?>
                    <input type="hidden" name="upload_origin" id="upload_origin" value="">
                <? } ?>
                
                <div class="form__group">
                    <div class="popup_column">
                        <fieldset>
                            <legend class="form__group__legend">Step 1: Select an image to upload</legend>
                            
                            <div class="image_uploader_wrap">
                                <? // Initial upload (to "temp" folder) progress bar ?>
                                <div id="image_uploader_progress">
                                    <span></span>
                                </div>
                                <div class="image_progress_details">
                                    &nbsp;
                                </div>
                            </div>
                            
                            <div class="form__field upload_btn_wrap">
                                <? // This is where the image is selected and uploaded to a "temp" folder, until the below form saves ?>
                                <input type="file" name="image_uploader" id="image_uploader">
                                <button type="button" name="do_upload" class="button image_upload_btn">Select an image</button>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="form__group">
                    <div class="popup_column image_uploader" data-step="2">
                <? // Form to save image as a post in the database. Also moves selected image from the "temp" folder into the proper place ?>
                <form method="post" class="wrap" id="image-uploader-form" action="/admin/modules/core/image-library/manage_image_post.php">
                    
                    <? // post_id will be zero for a new upload ?>
                    <input type="hidden" name="image_post_id" id="image_post_id" value="0">
                    
                    <? // post_status may as well always be live ?>
                    <input type="hidden" name="image_post_status" id="image_post_status" value="live">
                    
                    <? // post_type probably needs modified to "file" if it's a download ?>
                    <input type="hidden" name="image_post_type" id="image_post_type" value="">
                    
                    <? // The following uses a default folder but should get overwritten if working in an iframe from a module ?>
                    <input type="hidden" name="image_uploader_destfolder[]" id="image_uploader_destfolder" 	value="/images/<?=(isset($in_image_library))?'gallery':$module_form_settings['post_type'];?>/" />
                    
                    <? // The filename will be populated after choosing an image above ?>
                    <input type="hidden" name="image_uploader_filename[]" id="image_uploader_filename" value="" />
                    
                    <? // post_slug is the complete path to the image / file. Gets populated once the image / file is selected above ?>
                    <span class="upload_slug"><input type="hidden" name="image_post_slug" id="image_post_slug" value=""></span>
                    
                    <? // post_author speaks for itself ?>
                    <input type="hidden" name="image_post_author" id="image_post_author" value="<?=$_SESSION['eruid'];?>">
                    
                    <? // return_to will reload the window once the upload saves, if the ajax script doesn't work ?>
                    <input type="hidden" name="image_return_to" id="image_return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
                    
                    <? // parent_id is the _id of the post that initiated the upload, if working from a module in an iframe ?>
                    <input type="hidden" name="image_parent_id" id="image_parent_id" value="<?=(!isset($in_image_library)&&isset($_id))?$_id:0;?>">
                    
                    <? // menu_order only really required if this is a gallery image, to specify the order of the images. Need to work this out. ?>
                    <input type="hidden" name="image_menu_order" id="image_menu_order" value="0">
                    
                    <? // is_modal just says whether you're working in an iframe or not ?>
                    <? if ( !isset($in_image_library) ) { ?><input type="hidden" name="is_modal" id="is_modal" value="yes"><? } ?>
                    
                            <fieldset>
                                <legend class="form__group__legend">Step 2: Enter image details</legend>
                                <div class="form__field">
                                    <label class="form__field__label" for="image_post_title">Title <span class="required">*</span></label>
                                    <input class="form__field__input" type="text" name="image_post_title" id="image_post_title" value="" required>
                                </div>
                                <div class="form__field">
                                    <label class="form__field__label" for="image_post_alt_title">Description <span class="required">*</span></label>
                                    <input class="form__field__input" type="text" name="image_post_alt_title" id="image_post_alt_title" value="" required>
                                </div>
                                <div class="form__field">
                                    <button type="submit" name="save_form" class="button image_save_btn"><i class="fa fa-save"></i> Save Image</button>
                                </div>
                            </fieldset>
                        
                    
                </form>
            </div>
                    
                    
                    
                </div>
                
            </div>
            
            
        </div>
        
    </div><!-- upload tab -->
    
    <div class="wrap image-library-tab manage active">
    <div class="page-headers">
        <h1 class="page-headers__title"><i class="fa fa-picture-o"></i> Image Library</h1>
        <div class="image-sorting">
            <div class="image-sorting__title">Sort by:</div>
            <div class="image-sorting__option">
                <input class="image-sorting__option__radio" type="radio" name="sort-images" id="sort-by-date" <?= !isset($_SESSION['sortimages']) ? 'checked' : '';?>>
                <label class="image-sorting__option__label" for="sort-by-date">Date</label>
                <div class="image-sorting__option__fancyradio"></div>
            </div>
            <div class="image-sorting__option">
                <input class="image-sorting__option__radio" type="radio" name="sort-images" id="sort-by-name" <?= isset($_SESSION['sortimages']) ? 'checked' : '';?>>
                <label class="image-sorting__option__label" for="sort-by-name">Filename</label>
                <div class="image-sorting__option__fancyradio"></div>
            </div>
        </div>
    </div>
        
        <div class="wrap tab-content<?=(isset($in_image_library))?' normal-overflow':'';?>">
            <div class="wrap tab-inner">
                
            </div>
            <div class="wrap image-editor<?=(isset($in_image_library))?' full-height':'';?>" data-image="">
                <div class="image-editor-bg"></div>
                <div class="image-editor-inner">
                    <div class="image-editor-details">
                        
                    </div>
                </div>
            </div>
        </div>
    </div><!-- manage tab -->
</div>