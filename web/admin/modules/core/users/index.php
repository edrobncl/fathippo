<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
	require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");
    
    // Include this module's functions file, if it has one.
    if ( file_exists('mod_funcs.php') ) {
        include('mod_funcs.php');
    }
    
    require_once(FS_ADMIN_INCLUDES."inc_header.php");
?>

<div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=$module_form_settings['icon']?>"></i> <?=$module_form_settings['plural']?></h1>
    <a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a>
</div>
<div class="dashboard-pad">
<? if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? } ?>

<div id="row-list" class="wrap card">
    <? buildUserList(); ?>
</div>

<?
function buildUserList() {
    global $pdo;
    global $module_form_settings;

    if ( isSuper() ) {
        $in = "'superuser','admin','standard'";
    } else {
        $in = "'admin','standard'";
    }
    
    $users = $pdo->query("SELECT * FROM er_users WHERE level IN ($in) ORDER BY _id ASC");
    while ( $user = $users->fetch() ) {
        ?><div class="row">
            <div class="row__title">
                <a class="row__title__name" href="<?=$module_form_settings['form_link'];?>?_id=<?=$user['_id'];?>"><?=$user['username'];?></a>
                <br /><div class="row__title__slug"><?=$user['level'];?></div>
            </div>
            
            <div class="row__actions">
                <a href="<?=$module_form_settings['form_link']?>?_id=<?=$user['_id']?>" class="button button--edit"><i class="fa fa-pencil"></i> Edit</a>
                <a id="inline" href="#pop_delete_<?=$user['_id']?>" class="button button--delete"><i class="fa fa-trash"></i> Delete</a>
            </div>
            <div class="row__status">
                <? if ( $user['active'] == 'Yes' ) { $postIcon = 'check'; } else { $postIcon = 'times'; } ?>
                <div class="row__status__title"><i class="fa fa-<?=$postIcon;?>"></i> <?=($user['active'] == 'Yes')?'Active':'Inactive';?></div>
            </div>
            <? if ( in_array($user['level'], ['superuser','admin']) ) { ?>
                <div style="display:none">
                    <div id="pop_delete_<?=$user['_id']?>">
                    	<h3>Oops!</h3>
                    	<p>You can't delete the <?=($user['_id']==1)?'Super User':'Admin account';?>. Who would manage this place?</p>
                    	<form id="delete-post-<?=$user['_id'];?>" method="post" action="#">
                            <button type="button" name="cancel_delete" class="button"><i class="fa fa-arrow-left"></i> Okay, I understand</button>
                        </form>
                    </div>
                </div>
            <? } else { ?>
                <div style="display:none">
                    <div id="pop_delete_<?=$user['_id']?>">
                    	<h3>Are you sure?</h3>
                    	<p>This will delete <strong><?=$user['username']?></strong> forever - please be sure before you delete this record.</p>
                    	<form id="delete-post-<?=$user['_id'];?>" method="post" action="/admin/scripts/manage_users.php">
                            <input type="hidden" name="user_id" value="<?=$user['_id'];?>">
                            <input type="hidden" name="return_to" value="<?=$_SERVER['REQUEST_URI'];?>">
                            <button type="button" name="cancel_delete" class="button"><i class="fa fa-arrow-left"></i> No, don't delete it!</button>
                            <button type="submit" name="delete_user" class="button delete"><i class="fa fa-trash"></i> Yes! Delete it!</button>
                        </form>
                    </div>
                </div>
            <? } ?>
        </div><?
    }
}
?>
</div>
<?php require_once(FS_ADMIN_INCLUDES.'inc_footer.php'); ?>
<?php require_once(FS_ADMIN_INCLUDES.'inc_js.php'); ?>