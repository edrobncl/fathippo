<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

require_once(FS_ADMIN_INCLUDES.'inc_form_header.php');

if ( in_array($admin_page_type,['add','edit']) ) {

    // Get this user's permissions as they're currently set
    $getUserPermissions = $pdo->prepare("SELECT * FROM er_permissions WHERE _id = :id");
    $getUserPermissions->execute(['id'=>$_id]);
    $userPermissions = $getUserPermissions->fetchAll();
    
    $permissions = [];
    
    foreach ( $userPermissions as $module ) {
        $permissions[] = $module['module_name'];
    }
    
    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['singular'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;
        
    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>
     
    <div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=($admin_page_type=='edit')?'pencil':'plus';?>"></i> <div class="page-headers__title__parentlink"><a href="/admin/modules/core/<?=$module_form_settings['module_name']?>/"><?=$module_form_settings['plural']?></a> <i class="fa fa-angle-right"></i></div> <?=$admin_page_title;?></h1>
    </div>
    
    <?
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success">'.$_SESSION['success'].'</div>';
        }
    ?>
    
    <form class="validate-form wrap form-with-actions" method="post" action="/admin/scripts/manage_users.php" enctype="multipart/form-data">

        <input type="hidden" name="user_id" value="<?=$_id;?>">
        <input type="hidden" name="return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
        
        <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="username">Username<span class="required">*</span></label>
                        <input class="form__field__input" name="username" id="username"   type="text" required value="<?=(isset($username))?$username:'';?>" placeholder="The main identifer for this user" />
                    </div>
                    
                    <div class="form__field frm_getpass">
                        <label class="form__field__label" for="password">Password</label>
                        <input class="form__field__input" name="password" id="password"   type="text" />
                        <button class="button" type="button" id="generate_password">Generate Password</button>
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="active">Active Account?<span class="required">*</span></label>
                        <select class="form__select" name="active" id="active" required>
                            <option value="Yes"<?=($active=='Yes')?' selected':'';?>>Yes</option>
                            <option value="No"<?=($active=='No')?' selected':'';?>>No</option>
                        </select>
                    </div>
                
                    <? if ( isSuper() && $_id != 1 ) { ?>
                        <div class="form__field">
                            <label class="form__field__label" for="level">Access Level<span class="required">*</span></label>
                            <select class="form__select" name="level" id="level" required>
                                <option value="">Please choose...</option>
                                <option value="standard"<?=($level=='standard')?' selected':'';?>>Standard</option>
                                <option value="admin"<?=($level=='admin')?' selected':'';?>>Admin</option>
                            </select>
                        </div>
                    <? } else { ?>
                        <div class="form__field">
                            <label class="form__field__label" for="level">Access Level</label>
                            <div  class="form__field__input faux-field"><?=($level=='superuser')?'Super User': ( ($level=='admin') ? 'Admin' : 'Standard');?></div>
                            <input type="hidden" name="level" id="level" value="<?=(isset($level))?$level:'standard';?>" />
                        </div>
                    <? } ?>
                </fieldset>
            </div>
            
            <? if ( isSuper() && $_id != 1 ) { ?>
                <div class="form__group">
                        <fieldset>
                            <legend class="form__group__legend">Permissions</legend>
                            <div class="form__field">
                                <label class="form__field__label">Core</label>
                                <?
                                    $modules = [];
                                    $core = scandir(FS_ADMIN_ROOT.'modules/core');
                                    foreach ( $core as $module ) {
                                        // For core modules, most of them are okay so only list the modules that may be restricted
                                        if ( substr($module,0,1) != '.' && is_dir(FS_ADMIN_ROOT.'modules/core/'.$module) ) {
                                            $modules[] = $module;
                                        }
                                    }
                                    sort($modules);
                                ?>
                                <div class="permissions-list">
                                <? foreach ( $modules as $module ) {
                                    ?><input type="checkbox" id="check-<?=$module;?>" name="permissions[]" value="<?=$module;?>"<?=( isset($permissions) && in_array($module,$permissions) ) ?' checked':'';?> /><label for="check-<?=$module;?>"><?=ucwords(str_replace('-',' ',$module));?></label><br/><?
                                } ?>
                                </div>
                            </div>
                            <div class="form__field">
                                <label class="form__field__label">Additional</label>
                                <?
                                    $modules = [];
                                    $additional = scandir(FS_ADMIN_ROOT.'modules');
                                    foreach ( $additional as $module ) {
                                        // For core modules, most of them are okay so only list the modules that may be restricted
                                        if ( substr($module,0,1) != '.' && $module != 'core' && $module != 'menus' && is_dir(FS_ADMIN_ROOT.'modules/'.$module) ) {
                                            $modules[] = $module;
                                        }
                                    }
                                    sort($modules);
                                    $additional_menus = scandir(FS_ADMIN_ROOT.'modules/menus');
                                    foreach ( $additional_menus as $module ) {
                                        // For core modules, most of them are okay so only list the modules that may be restricted
                                        if ( substr($module,0,1) != '.' && $module != 'core' && is_dir(FS_ADMIN_ROOT.'modules/menus/'.$module) ) {
                                            $menus[] = $module;
                                        }
                                    }
                                    sort($menus);
                                ?>
                                <div class="permissions-list">
                                <? foreach ( $modules as $module ) {
                                    ?><input type="checkbox" id="check-<?=$module;?>" name="permissions[]" value="<?=$module;?>"<?=( isset($permissions) && in_array($module,$permissions) ) ?' checked':'';?> /><label for="check-<?=$module;?>"><?=ucwords(str_replace('-',' ',$module));?></label><br/><?
                                }
                                foreach ( $menus as $module ) {
                                    ?><input type="checkbox" id="check-<?=$module;?>" name="permissions[]" value="<?=$module;?>"<?=( isset($permissions) && in_array($module,$permissions) ) ?' checked':'';?> /><label for="check-<?=$module;?>"><?=ucwords(str_replace('-',' ',$module));?></label><br/><?
                                } ?>
                                </div>
                            </div>
                        </fieldset>
                    
                </div>
            <? } ?>
        
        
        
        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>
        
    </form>

    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // If you're not editing or adding a post, you shouldn't really be here!
    $path = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($path);
    $path = implode('/',$path);
    // Go to the main index of this module.
    header("Location: ".$path.'/');
}
?>