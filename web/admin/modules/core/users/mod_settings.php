<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'User',
    'plural' => 'Users',
    'form_link' => 'users-form.php',
    'main_menu' => [
        'option' => 'secondary',
        'order' => 4
    ],
    'post_type' => 'user',
    'table_name' => 'er_users',
    'icon' => 'users'
];
?>