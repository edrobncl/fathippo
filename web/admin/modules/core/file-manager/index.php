<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

// Include module's functions
if ( file_exists(FS_ADMIN_ROOT.'modules/core/file-manager/mod_funcs.php') ) {
    require_once('/admin/modules/core/file-manager/mod_funcs.php');
}

include(FS_ADMIN_ROOT.'includes/inc_header.php');

$in_file_manager = true;

// Actual manager code
include('file_manager.php');


if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? }

include(FS_ADMIN_ROOT.'includes/inc_footer.php');
include(FS_ADMIN_ROOT.'includes/inc_js.php');
?>