<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    
    $data = [];
    
    $file_id = XSSTrapper($_POST['file_id']);
    $file_title = XSSTrapper($_POST['file_title']);
    
    if ( trapCheck($file_id) ) {
        
        $fileQuery = $pdo->prepare("UPDATE er_posts SET post_title = :file_title, post_alt_title = :alt_file_title, date_modified = NOW() WHERE _id = :file_id");
        $fileQuery->execute(['file_title' => $file_title, 'alt_file_title' => $file_title, 'file_id' => $file_id]);
        
        if ( $fileQuery->rowCount() > 0 ) {
            $data['success'] = true;
            $data['file'] = ['_id' => $file_id, 'title' => $file_title];
        } else {
            $data['success'] = false;
            $data['error'] = 'There was a problem updating the details';
        }
        
    } else {
        $data['success'] = false;
        $data['error'] = 'No valid file ID was provided';
    }
    
    echo json_encode($data);
    
}