<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

$fileQuery = $pdo->prepare("SELECT * FROM er_posts WHERE post_type = ? ORDER BY date_created DESC");
$fileQuery->execute(['file']);
$files = $fileQuery->fetchAll();

foreach ( $files as $k => $file ) {
    if ( file_exists(FS_ROOT.$file['post_slug']) ) {
        $fs = filesize('/'.trim(FS_ROOT,'/').$file['post_slug']);
        $files[$k]['filesize'] = formatBytes($fs,0);
    }
}

echo json_encode($files);