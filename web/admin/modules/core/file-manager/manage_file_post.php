<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

// Get the source (either js or nothing)
$source = XSSTrapper($_POST['source']);
unset($_POST['source']);

// Run the save or delete action
if ( !isset($_POST['delete_post']) ) {
    // If the action isn't delete...
    
    // Set destination folder var
    $destfolder = trim(rtrim($_POST['file_uploader_destfolder'][0],'/'),'/');
    unset($_POST['file_uploader_destfolder']);
    
    // Make sure the destination folder exists
    if ( !is_dir(FS_ROOT.$destfolder) ) {
        mkdir(FS_ROOT.$destfolder);
    }
    
    // Set filename var
    $filename = strtolower($_POST['file_uploader_filename'][0]);
    unset($_POST['file_uploader_filename']);
    
    // Clear out is_modal from $_POST
    if ( isset($_POST['is_modal']) ) {
        $is_modal = true;
        unset($_POST['is_modal']);
    }
    
    // Move file (rename with numbers if duplicate)
    if ( $_POST['file_post_type'] == 'file' ) {
                
        // Get the filename and extension separately
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $noext = basename($filename,'.'.$ext);
        
        // Function to check if file exists and add a number to the filename if it's already there
        function checkFileAndWrite($count = 0) {
            global $_POST;
            global $ext;
            global $noext;
            global $destfolder;
            global $filename;
                        
            if ( $count > 0 ) {
                $file = $destfolder.'/'.$noext.'('.$count.').'.$ext;
            } else {
                $file = $destfolder.'/'.$noext.'.'.$ext;
            }
            
            if ( file_exists(FS_ROOT.$file) ) {
                $count = $count + 1;
                checkFileAndWrite($count);
            } else {
                rename(FS_ROOT.$destfolder.'/temp/'.$filename, FS_ROOT.$file);
                $_POST['file_post_slug'] = '/'.$file;
            }
        }
        
        // Move the file to the correct place using above function for naming
        checkFileAndWrite();
    }
    
    // Determine if this is a new post to be inserted, or a currently existing post to be updated.
    // - If post_id is greater than zero, then it must be a post.
    $form_type = ($_POST['file_post_id'] > 0) ? 'edit' : 'add';
    
    if ( $form_type == 'edit' ) {        
        $doSQL = $pdo->prepare("UPDATE er_posts SET
            post_status = :post_status,
            post_type = :post_type,
            parent_id = :parent_id,
            date_modified = NOW(),
            post_slug = :post_slug,
            menu_order = :menu_order,
            post_author = :post_author,
            post_title = :post_title,
            post_alt_title = :post_alt_title
            WHERE _id = :post_id
        ");

        $doSQL->execute([
            'post_status' => $_POST['file_post_status'],
            'post_type' => $_POST['file_post_type'],
            'parent_id' => $_POST['file_parent_id'],
            'post_slug' => $_POST['file_post_slug'],
            'menu_order' => $_POST['file_menu_order'],
            'post_author' => $_SESSION['eruid'],
            'post_title' => $_POST['file_post_title'],
            'post_alt_title' => $_POST['file_post_title'],
            'post_id' => $_POST['file_post_id']
        ]);
        
        // Store the post_id as a variable.
        $post_id = $_POST['file_post_id'];
        
        // Set the action for the success message.
        $action = 'updated';
        
    } else if ( $form_type == 'add' ) {
        $doSQL = $pdo->prepare("INSERT INTO er_posts(
            date_created,
            post_status,
            post_type,
            parent_id,
            date_modified,
            post_slug,
            menu_order,
            post_author,
            post_title,
            post_alt_title
        )
        VALUES (
            NOW(),
            :post_status,
            :post_type,
            :parent_id,
            NOW(), 
            :post_slug,
            :menu_order,
            :post_author,
            :post_title,
            :post_alt_title
        )");

        $doSQL->execute([
            'post_status' => $_POST['file_post_status'],
            'post_type' => $_POST['file_post_type'],
            'parent_id' => $_POST['file_parent_id'],
            'post_slug' => $_POST['file_post_slug'],
            'menu_order' => $_POST['file_menu_order'],
            'post_author' => $_POST['file_post_author'],
            'post_title' => $_POST['file_post_title'],
            'post_alt_title' => $_POST['file_post_title']
        ]);
        
        // Store the newly generated post_id as a variable.
        $post_id = $pdo->lastInsertId();
        
        // Set the action for the success message.
        $action = 'created';
    }
    
    // Set $return_to, which is where to go after the script runs
    if ( isset($_POST['save_form']) ) {
        $return_to = $_POST['file_return_to'] . '?_id=' . $_POST['file_parent_id'];
    } else if ( isset($_POST['save_form_return']) || isset($_POST['delete_post']) ) {
        $return_to = dirname($_POST['file_return_to']).'/';
    }
    
    // Store the post_type for use in the success message.
    $post_type = $_POST['file_post_type'];
    
    // Clear out all of the main post keys/values and button keys/values, leaving only the postmeta keys/values remaining in $_POST.
    $unset_array = ['file_post_status', 'file_post_type', 'file_parent_id', 'file_post_slug', 'file_menu_order', 'file_post_author', 'file_post_title', 'file_post_id', 'save_form', 'save_form_return', 'delete_post', 'file_return_to'];
    foreach ( $unset_array as $item ) {
        unset($_POST[$item]);
    }
    
}

if ( isset($source) && $source == 'js' ) {
    // If the source was js, pass back the details - provided the post_id exists (image saved)
    
    if ( isset($post_id) && $post_id > 0 ) {
        $fileQuery = $pdo->prepare("SELECT * FROM er_posts WHERE _id = ?");
        $fileQuery->execute([$post_id]);
        $file = $fileQuery->fetch();
        
        if ( file_exists(FS_ROOT.$file['post_slug']) ) {
            $fs = filesize('/'.trim(FS_ROOT,'/').$file['post_slug']);
            $file['filesize'] = formatBytes($fs,0);
        }
        
        $data['file'] = $file;
        if ( isset($is_modal) && $is_modal === true ) { $data['is_modal'] = true; }
        
        echo json_encode($data);
    }
    
    exit();
} else {
    // If the source isn't js and the action is set, store the success message in session
    if ( isset($action) ) {
        $_SESSION['success'] = ucfirst($post_type) . ' successfully ' . $action;
    }
}

// Redirect to the return_to page, if not using AJAX
header("Location: ".$return_to);