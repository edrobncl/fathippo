<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'File',
    'plural' => 'Files',
    'form_link' => 'files-form.php',
    'main_menu' => [
        'option' => 'secondary',
        'order' => 2
    ],
    'post_type' => 'file',
    'icon' => 'file-o'
];
?>