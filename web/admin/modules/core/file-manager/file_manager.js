$(function() { // Document Ready 
    /*
        Variable definitions
                                            */
    
    // Define variable for the image library's manage tab - content area
    var $manageContent = $('.file-manager-tab.manage').find('.tab-content').find('.tab-inner');
    
    // Define variable for the form section to be populated (header image, gallery, download, etc.)
    var $uploadOrigin = null;
    
    // Define variable to store the list of files (filled by loadfiles() function)
    var allFiles = [];
    
    // Define whether working in a popup (true) or the main image library (false)
    var in_file_manager = ($('.file_manager_modal').length) ? true : false;
    
    /*
        Main functions
                                    */
                                    
    // Function to show "you've made a change" warning
    function showChangeWarning() {
        var $formActions = $('.form-actions');
        if ( !$formActions.hasClass('visible') ) {
            $formActions.addClass('visible');
        }
    }
                                    
    // Function to return image html
    function fileHtml(file) {
        
        var icon = fileIcon(file.post_slug);
        var filename = getFilename(file.post_slug);

        output = '<div class="file-box" data-id="'+file._id+'" id="download_link_'+file._id+'"><div class="cell file-box-icon"><i class="fa fa-'+icon+'"></i></div><div class="cell file-box-details"><span class="file-box-title">'+file.post_title+'</span><a class="post-slug" href="'+file.post_slug+'" target="_blank">'+file.post_slug+'</a></div><div class="cell file-box-size">'+file.filesize+'</div><div class="cell file-box-actions">'+((!in_file_manager)?'<button type="button" id="edit-file" class="button"><i class="fa fa-pencil"></i> Edit</button><button type="button" id="delete-file" class="action delete button button--delete"><i class="fa fa-times"></i> Delete</button>':'<button type="button" id="select-file" class="button"><i class="fa fa-check"></i> Select File</button>')+'</div></div>';

        return output;
    }
    
    // Function to load files into content box
    function importFiles(allFiles) {
        var fileCount = 0;
        var files = 0;
        
        // Load in each file
        $.each(allFiles, function(k, file) {
            
            var output = fileHtml(file);
            
            $manageContent.append(output);
        });
    }
    
    // Function to determine icon for image
    function fileIcon(post_slug) {
        
        var ext = post_slug.substring(post_slug.lastIndexOf(".")+1).toLowerCase();
        
        switch (ext) {
			case 'doc':
			case 'docx':
			    file_icon = 'file-word-o';
			    break;
            case 'pdf':
                file_icon = 'file-pdf-o';
                break;
            case 'csv':
            case 'xls':
            case 'xlsx':
                file_icon = 'file-excel-o';
                break;
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'png':
            case 'tif':
            case 'psd':
                file_icon = 'file-image-o';
                break;
            case 'zip':
            case 'rar':
            case '7z':
            case 's7z':
                file_icon = 'file-archive-o';
                break;
            default:
                file_icon = 'file-o';
		}
		
		return file_icon;
    }
    
    // Function to get filename from slug
    function getFilename(post_slug) {
        return post_slug.substring(post_slug.lastIndexOf("/")+1);
    }
    
    // Function to select a file from the list by it's ID
    function selectFile(allFiles, id) {
        for ( i = 0; i < allFiles.length; i++ ) {
            if ( allFiles[i]._id == id ) {
                return allFiles[i];
            }
        }
    }
    
    // Function to load all files from the database
    function loadFiles() {
        
        // Show "loading" message
        $manageContent.html('<i class="fa fa-spin fa-spinner"></i> Loading...');
        
        $.get( '/admin/modules/core/file-manager/load_files.php', function(res) {
            // Clear "loading" message
            $manageContent.html('');
            
            //console.log(res);
            
            // If there are results, load them in. Otherwise, show "no files" message.
            if ( res.length ) {
                allFiles = res;
                importFiles(allFiles);
            } else {
                // There aren't any images.
                $manageContent.html('No files!');
            }
        }, 'json')
        .fail(function(err) {
            console.error(err);
        });
    }
    
    // Function to determine which tab is active, and show the appropriate nav buttons
    function getActiveTab() {
        var activeTab = '';
        if ( $('.file-manager-tab.active').hasClass('manage') ) {
            activeTab = 'manage';
            $('.file-manager-nav a[href="#upload"]').show();
            $('.file-manager-nav a[href="#manage"]').hide();
        } else if ( $('.file-manager-tab.active').hasClass('upload') ) {
            activeTab = 'upload';
            $('.file-manager-nav a[href="#upload"]').hide();
            $('.file-manager-nav a[href="#manage"]').html('Manage Files').show();
        } else if ( $('.file-manager-tab.active').hasClass('edit') ) {
            activeTab = 'edit';
            $('.file-manager-nav a[href="#upload"]').hide();
            $('.file-manager-nav a[href="#manage"]').html('Back to files').show();
        }
        
        return activeTab;
    }
    
    // Function to open file editor
    function openFileEditor(file, el) {
        var filename = getFilename(file.post_slug);
        var icon = fileIcon(file.post_slug);
        var doOpen = true;
        
        if ( el.next('.file-box').hasClass('file-editor') ) {
            closeFileEditor();
            doOpen = false;
        } else if ( $('.file-editor').length ) {
            closeFileEditor();
        }
        
        if ( doOpen ) {
            // Insert editor row
            el.after('<div class="wrap file-box file-editor" data-id="'+file._id+'"><label for="file-editor-input"><i class="fa fa-info-circle"></i> Change the <strong>file title</strong> and click save.</label><input type="text" value="'+file.post_title+'" name="file-editor-input" id="file-editor-input"><button type="button" class="button button--cancel" id="cancel-file-edit">Cancel</button><button type="button" class="button" id="accept-file-edit">Save</button></div>');
            $('.file-editor').slideDown(200);
        }
    }
    
    // Function to close image editor
    function closeFileEditor() {
        $('.file-editor').slideUp(200, function() {
            $(this).remove();
        });
    }
    
    /*
        Main features
                                */
    
    // If not in "modal" mode, load images on doc.ready
    if ( !$('.file_manager_modal').length ) {
        loadFiles();
        getActiveTab();
    }
    
    // Switch tabs in image library
    $(document).on('click', '.file-manager-nav a', function(e) {
        e.preventDefault();
        var tab = $(this).attr('href').slice(1);

        if ( !$('.file-manager-tab.active').hasClass(tab) && tab != 'close' ) {
            $('.file-manager-tab.active').fadeOut(200, function() {
                $(this).removeClass('active');
                $('.file-manager-tab.'+tab).fadeIn(200).addClass('active');
            });
        }
        
        if ( tab != 'close' ) {
            var otherTab = (tab=='manage') ? 'upload' : 'manage';
            $('.file-manager-nav a[href="#'+otherTab+'"]').show();
            $('.file-manager-nav a[href="#'+tab+'"]').hide();
        }
    });

    // Remove the success message after an upload
    $(document).on('removeuploadalert', function() {
        $(".alert.success").delay(1000).fadeOut(1000, function() { $(this).remove(); });
    });

    // Handle file upload form submission with ajax
    $(document).on('submit', '#file-uploader-form', function(e) {
        e.preventDefault();

        var fileForm = $(this);

        $.ajax({
            method: 'post',
            url: fileForm.attr('action'),
            data: fileForm.serialize() + '&source=js',
            dataType: 'json',
            success: function(res) {
                if ( res.is_modal === true ) {
                    // Show success message
                    // $('body').append('<div class="alert success"><i class="fa fa-check"></i> '+res.success_msg+'</div>').trigger('removeuploadalert');
                    
                    // Place new content onto module form page
                    useFile(res.file, 'upload');
                    
                    // Update content of management tab
                    var output = fileHtml(res.file);
                    $manageContent.prepend(output);
                    
                    // Push new image into images array
                    allFiles.push(res.file);
                    
                    // Close image library
                    closeFileManager();
                    
                    // Alert that something's changed
                    showChangeWarning();
                    
                    // Clear upload tab
                    $('#file_uploader_progress span, .file_uploader[data-step="2"]').removeAttr('style');
                    $('.file_progress_details').html('&nbsp;');
                    $('.upload_btn_wrap button').html('Select a file');
                    $('#file_post_id').val('0');
                    $('.upload_btn_wrap input[name="file_uploader"], #file_post_type, #file_uploader_filename, #file_post_slug, #file_post_title, #file_post_alt_title').val('');

                } else {
                    // Reload page onto library tab
                    window.location.href = '/admin/modules/core/file-manager/';
                }
            },
            error: function(err) {
                console.error('error',err);
            }
        });
    });
    
    // Open file editor
    $(document).on('click', '#edit-file', function() {
        // Pull file details
        var fileId = $(this).closest('.file-box').attr('data-id');
        var fileDetails = selectFile(allFiles, fileId);
        var thisFileBox = $(this).closest('.file-box');
        
        // Open the editor
        openFileEditor(fileDetails, thisFileBox);
    });
    
    // Close file editor
    $(document).on('click', '.file-editor-bg, #cancel-file-edit', closeFileEditor);
    
    // Update file info
    $(document).on('click', '#accept-file-edit', function() {
        var file_id = $('.file-editor').attr('data-id');
        var file_title = $('#file-editor-input').val();
        
        // Remove any success messages
        $('.file-update-msg').remove();
        
        $.ajax({
            method: 'post',
            url: '/admin/modules/core/file-manager/update_file.php',
            data: {
                file_id : file_id,
                file_title : file_title
            },
            dataType: 'json',
            success: function(res) {
                if ( res.success == true ) {
                    
                    // Update this file in the files array
                    for ( i = 0; i < allFiles.length; i++ ) {
                        if ( allFiles[i]._id == res.file._id ) {
                            allFiles[i].post_title = res.file.title;
                            allFiles[i].post_alt_title = res.file.title;
                            break;
                        }
                    }
                    
                    // Update the title on-screen
                    $('.file-box[data-id="'+res.file._id+'"]').find('.file-box-title').html(res.file.title);
                    
                    // Insert the success message, then remove it after X seconds
                    $('<div class="file-box file-update-msg success"><i class="fa fa-check"></i> File title was successfully updated.</div>').insertBefore('.file-editor');
                    setTimeout(function() {
                        $('.file-update-msg').slideUp(200, function() {
                            $(this).remove();
                        });
                    }, 4000);
                    
                    // Close the editor
                    closeFileEditor();
                    
                } else {
                    // Show error message
                    $('<div class="file-box file-update-msg error"><i class="fa fa-times"></i> '+((res.error)? res.error : 'There was a problem updating the file title.')+'</div>').insertBefore('.file-editor');
                }
            },
            error: function(err) {
                console.error(err);
                // Show error message
                $('<div class="file-box file-update-msg error"><i class="fa fa-times"></i> There was a problem updating the file title.</div>').insertBefore('.file-editor');
            }
        });
    });

    // Delete file from manager
    $(document).on('click','#delete-file', function() {
        var file_id = $(this).closest('.file-box').attr('data-id');
        var fileToDelete;
        var slugToDelete;
        var indexToDelete;
        
        for ( var i = 0; i < allFiles.length; i++ ) {
            if ( allFiles[i]._id == file_id ) {
                indexToDelete = i;
                idToDelete = allFiles[i]._id;
                slugToDelete = allFiles[i].post_slug;
            }
        }
        
        // Remove any messages that already exist
        $('.file-update-msg').remove();
        
        $(this).closest('.file-box').append('<div class="rsi-pop"><h3>Are you sure you\'d like to delete this file?</h3><button type="button" class="button cancel rsi-cancel">No, don\'t do it</button><button type="button" class="button confirm-rsi" id="accept-delete" data-id="'+idToDelete+'">Yes, delete the file</button></div>');
        
        // Cancel delete and remove the confirmation popup
        $(document).on('click', '.rsi-cancel', function() {
            $('.rsi-pop').remove();
        });
        
        // Confirm delete and proceed
        $(document).on('click', '#accept-delete', function() {
            
            $.ajax({
                method: 'post',
                url: '/admin/modules/core/file-manager/delete_file.php',
                data: {
                    file_id: idToDelete,
                    file_slug: slugToDelete
                },
                dataType: 'json',
                success: function(res) {
                    if ( res.error ) {
                        // Show error message
                        $('.file-box[data-id="'+idToDelete+'"]').after('<div class="file-box file-update-msg error"><i class="fa fa-times"></i> '+res.error+'.</div>');
                        $('.file-update-msg').show();
                    } else {
                        // Remove the file from the files array
                        allFiles.splice(indexToDelete,1);
                        
                        // Show success message
                        if ( $('.file-box[data-id="'+idToDelete+'"]').length ) {
                            $('.file-box[data-id="'+idToDelete+'"]').after('<div class="file-box file-update-msg success"><i class="fa fa-check"></i> File was successfully deleted.</div>');
                        }
                        
                        // Remove the file from the manager
                        $('.file-box[data-id="'+idToDelete+'"]').remove();
                        
                        // Remove the success message after X seconds
                        setTimeout(function() {
                            $('.file-update-msg').slideUp(200, function() {
                                $(this).remove();
                            });
                        }, 5000);
                    }
                },
                error: function(err) {
                    console.error(err.responseText);
                }
            });
        });

    });

    /*
        Functions & features only required when image library is loaded into a popup
                                                                                                                                            */
                                                                                                                        
    if ( $('.file_manager_modal').length ) {

        /*
            Popup functions
                                        */

        // Function to open the file manager
        function openFileManager(fileAction) {
            $('.file_manager_modal, .file_manager_bg').fadeIn(300);
            $('.file-manager-tab.active').removeClass('active').removeAttr('style');
            $('.file-manager-tab.'+fileAction).removeAttr('style').addClass('active');
            
            $('body').css('overflow','hidden');
            
            // If files haven't already been loaded, run the function to load them
            if ( !$manageContent.find('.file-box').length ) {
                loadFiles();
            }
        }
        
        // Function to close the file manager
        function closeFileManager() {
            $('.file_manager_modal, .file_manager_bg').fadeOut(300);
            $('body').css('overflow','auto');
            
            // Scroll to top of manager ready for next use
            $manageContent.parent().animate({scrollTop: 0}, 50);
            
            closeFileEditor();
        }
        
        // Function to update the hidden input that saves a file list
        function updateFilesInput(origin) {
            var multi_list = [];
            
            origin.find('.files-list').find('.file-item').each(function() {
                var attr = $(this).attr('data-item');
                // If there's an item, add it to the array
                if ( typeof attr !== typeof undefined && attr !== false ) {
                    multi_list.push(attr);
                }
            });
            
            multi_list = '["'+multi_list.join('","')+'"]';
            
            // Show the message explaining sorting if there's more than one image. Hide it if there's less than 2.
            if ( $('.file-item').length > 1 ) {
                origin.find('.files-drag-info').show();
            } else {
                origin.find('.files-drag-info').hide();
            }
            
            origin.find('input[type="hidden"]').val(multi_list).trigger('change');
        }
        
        // Function to select an image to use
        function useFile(file, method) {

            var newTitle = (method == 'upload') ? file.post_title : $('#file-details-title').val();
            
            var icon = fileIcon(file.post_slug);
            var filename = getFilename(file.post_slug);
            
            // Hide the "Gallery is empty" message
            $uploadOrigin.find('.files-list').removeClass('no-files').find('li:first-child').hide();
            
            // Add the new item to the images list
            $uploadOrigin.find('.files-list').append('<li class="file-item" data-item="'+file._id+'"><div class="file-item-handle"><i class="fa fa-sort"></i></div><div class="file-item-icon"><i class="fa fa-'+icon+'"></i></div><div class="file-item-text"><p><strong>'+file.post_title+'</strong><br/>'+filename+'</p></div><div class="file-item-actions"><button type="button" class="button action delete button--delete"><i class="fa fa-times"></i> Remove</button></div></li>');
            
            // Update the input field with the correct order of images
            updateFilesInput($uploadOrigin);
            
            // Check sorting is enabled if needed
            checkFileSorting($uploadOrigin);
            
            closeFileManager();
        }
        
        // Sorting
        function checkFileSorting(origin) {
            // If there's only one item left, disable sorting. Otherwise activate it.
            if ( origin.find('.file-item').length == 1  ) {
                // Disable sorting
                if ( origin.find('.files-list').hasClass('ui-sortable') ) {
                    origin.find('.files-list').sortable('destroy');
                }
            } else {
                // Activate / handle sorting
                origin.find('.files-list').sortable({
                    'handle': '.file-item-handle',
                    'start': function(event, ui) {
                        ui.item.addClass('moving');
                    },
                    'stop': function(event, ui) {
                        ui.item.removeClass('moving');
                    },
                    'update': function(event, ui) {
                        updateFilesInput(origin);
                    }
                });
            }
        }
        
        /*
            Popup features
                                        */
        $('.files-list').each(function() {
            if ( $(this).find('.file-item').length > 1 ) {
                $(this).sortable({
                    'handle': '.file-item-handle',
                    'start': function(event, ui) {
                        ui.item.addClass('moving');
                    },
                    'stop': function(event, ui) {
                        ui.item.removeClass('moving');
                    },
                    'update': function(event, ui) {
                        updateFilesInput(ui.item.closest('.frm_uploader'));
                    }
                });
            }
        });
        
        // Open the file manager
        $('.open-files').on('click', function(e) {
            e.preventDefault();
            var fileAction = $(this).attr('id').substr(0, $(this).attr('id').length-6);
            
            // Get the section of the form that will be populated
            $uploadOrigin = $(this).closest('.frm_uploader');
            
            // If we don't know what type of uploader is being used, show error in console. If it's set, all good!
            if ( !$uploadOrigin.data('type') ) {
                console.error('No type on "'+$uploadOrigin.find('h4:first').text()+'" uploader');
            } else {
                openFileManager(fileAction);
                getActiveTab();
            }
        });

        // Close the image library
        $(document).on('click', '.file-manager-nav a[href="#close"], .file_manager_bg', closeFileManager);
        
        // Select image to use
        $(document).on('click', '#select-file', function() {
            var fileId = $(this).closest('.file-box').attr('data-id');
            var file = selectFile(allFiles, fileId);
            
            useFile(file, 'select');
            
            // Alert that something's changed
            showChangeWarning();
        });
        
        // Remove a file (files list)
        $(document).on('click', '.file-item-actions button.delete', function() {
            var origin = $(this).closest('.frm_uploader');
            // Remove the image from the list
            $(this).closest('.file-item').remove();
            
            // If there are no images left in the list, clear the input value and show the "empty gallery" message
            if ( origin.find('.file-item').length <= 0 ) {
                origin.find('input[type="hidden"]').val('').trigger('change');
                origin.find('.files-drag-info').hide();
                origin.find('.files-list').addClass('no-files').find('li:first-child').show();
                if ( origin.find('.files-list').hasClass('ui-sortable') ) {
                    origin.find('.files-list').sortable('destroy');
                }
            } else {
                // Update the input field with the correct order of images
                updateFilesInput(origin);
                // Check sorting is enabled if needed
                checkFileSorting(origin);
            }
        });

        // Edit an image (multi image)
/*
        $(document).on('click', '.gallery-image-actions button.edit', function() {
            
        });
*/
        
        
    }

});