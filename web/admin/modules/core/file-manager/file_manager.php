<div class="file-manager-wrap wrap"<?=(isset($in_file_manager) && $in_file_manager === true)?' id="file-manager"':'';?>>
    
    <div class="file-manager-nav">
        <a class="button" href="#upload" style="display: none;"><i class="fa fa-upload"></i> Upload New File</a><a class="button" href="#manage" style="display: none;"><i class="fa fa-folder-o"></i> Manage Files</a>
        <? if ( !isset($in_file_manager) ) { ?><a href="#close" class="close-btn"><i class="fa fa-times"></i> Close</a><? } ?>
    </div>

    <div class="wrap file-manager-tab upload">
        <div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-upload"></i> Upload File</h1>
    	</div>
        
        <div class="wrap tab-content">
            <div class="wrap file_uploader" data-step="1">
                <? if ( !isset($in_file_manager) ) { ?>
                    <input type="hidden" name="upload_origin" id="upload_origin" value="">
                <? } ?>
                <div class="form__group">
                        <fieldset>
                            <legend class="form__group__legend">Step 1: Select a file to upload</legend>
                            
                            <div class="file_uploader__group">
                                <? // Initial upload (to "temp" folder) progress bar ?>
                                <div id="file_uploader_progress">
                                    <span></span>
                                </div>
                                <div class="file_progress_details">
                                    &nbsp;
                                </div>
                            </div>
                            
                            <div class="form__field upload_btn_wrap">
                                <? // This is where the file is selected and uploaded to a "temp" folder, until the below form saves ?>
                                <input type="file" name="file_uploader" id="file_uploader">
                                <button type="button" name="do_upload" class="button file_upload_btn">Select a file</button>
                            </div>
                        </fieldset>
                </div>
            </div>
            
            <div class="wrap file_uploader" data-step="2">
                
                <form method="post" id="file-uploader-form" action="/admin/modules/core/file-manager/manage_file_post.php">
                    <input type="hidden" name="file_post_id" id="file_post_id" value="0">
                    <input type="hidden" name="file_post_status" id="file_post_status" value="live">
                    <input type="hidden" name="file_post_type" id="file_post_type" value="file">
                    <input type="hidden" name="file_uploader_destfolder[]" id="file_uploader_destfolder" value="/files/<?=(isset($in_file_manager))?'misc':$module_form_settings['post_type'];?>">
                    <input type="hidden" name="file_uploader_filename[]" id="file_uploader_filename" value="">
                    <span class="upload_slug">
                        <input type="hidden" name="file_post_slug" id="file_post_slug" value="">
                    </span>
                    <input type="hidden" name="file_post_author" id="file_post_author" value="<?=$_SESSION['eruid'];?>">
                    <input type="hidden" name="file_return_to" id="file_return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
                    <input type="hidden" name="file_parent_id" id="file_parent_id" value="<?=(!isset($in_file_manager)&&isset($_id))?$_id:0;?>">
                    <input type="hidden" name="file_menu_order" id="file_menu_order" value="0">
                    <? if ( !isset($in_file_manager) ) { ?><input type="hidden" name="is_modal" id="is_modal" value="yes"><? } ?>
                    
                    <div class="form__group">
                            <fieldset>
                                <legend class="form__group__legend">Step 2: Enter file details</legend>
                                <div class="form__field">
                                    <label class="form__field__label" for="file_post_title">Title <span class="required">*</span></label>
                                    <input class="form__field__input" type="text" name="file_post_title" id="file_post_title" value="" required>
                                </div>
                                <div class="form__field">
                                    <button type="submit" name="save_form" class="button file_save_btn">Save File</button>
                                </div>
                            </fieldset>
                    </div>
                </form>
                
            </div>
        </div>
    </div><!-- upload tab -->
    
    <div class="wrap file-manager-tab manage active">
        <div class="page-headers">
    		<h1 class="page-headers__title"><i class="fa fa-file-o"></i> File Manager</h1>
        </div>

        <div class="search-bar wrap" style="margin: 1.25em 0 1em 1.75em">
            <form id="search-bar-form__files">
            <label for="post_search"><i class="fa fa-search"></i> Search</label>
                <input type="text" name="search" id="post_search" placeholder="Start typing..." />
            </form>
        </div>

        <div class="wrap tab-content<?=(isset($in_file_manager))?' normal-overflow':'';?>">
            <div class="wrap tab-inner" id="file_wrap">
            </div>
            <div class="wrap file-editor<?=(isset($in_file_manager))?' full-height':'';?>" data-file="">
                <div class="file-editor-bg"></div>
                <div class="file-editor-inner"></div>
            </div>
        </div>
    </div><!-- manage tab -->

    
</div>