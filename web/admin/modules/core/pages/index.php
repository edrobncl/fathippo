<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
	require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");
    
    // Include this module's functions file, if it has one.
    if ( file_exists('mod_funcs.php') ) {
        include('mod_funcs.php');
    }
    
    require_once(FS_ADMIN_INCLUDES."inc_header.php");
?>

<div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=$module_form_settings['icon']?>"></i> <?=$module_form_settings['plural']?></h1>
    <a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a>
</div>
<div class="dashboard-pad">
<div class="search-bar wrap">
    <form id="search-bar-form">
        <label for="post_search"><i class="fa fa-search"></i> Search</label>
        <input type="text" name="search" id="post_search" placeholder="Start typing..." />
    </form>
</div>

<? if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? } ?>

<div id="row-list" class="wrap card">
    <? buildPageList(); ?>
</div>
</div>
<?
function buildPageList($parent_id = 0, $indent = 0) {
    global $pdo;
    global $module_form_settings;
    global $last_indent;
    global $indent_count;
    global $row_count;
    
    $getPages = $pdo->prepare("SELECT *,
    (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'post_layout') AS post_layout
     FROM er_posts WHERE parent_id = :parent_id AND post_type = :post_type ORDER BY menu_order ASC, _id ASC");
    $getPages->execute(['parent_id'=>$parent_id, 'post_type' => 'page']);
    $pages = $getPages->fetchAll();
    
    foreach ( $pages as $row ) {
    $row_count++;  ?>
    
        <div class="row<? if ( $indent > 0 ) { echo ' indent-row ir-'.$indent; } ?> row-<?=$row['post_status']?> <? if($row_count % 2 == 0) { echo " rw-alt"; } ?>">
			<div class="row__title">
                <a class="row__title__name" href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>"><?=$row['post_alt_title']?><?php if($_SESSION['userlevel'] == 'superuser') { ?> (<?=$row['menu_order']?>)<?php } ?></a>
                <br /><a href="<?=($row['_id']==1)?'/':buildPath($row['_id']);?>" class="row__title__slug" target="_blank"><?=($row['_id']==1)?'/':buildPath($row['_id']);?></a> <?php if($_SESSION['userlevel'] == 'superuser' && $row['post_layout'] != '') { ?><br /><span class="row__title__slug" style="margin-top:4px"><i class="fa fa-info-circle"></i> post_layout is <strong><?=$row['post_layout']?></strong></span><?php } ?>
                
            </div>
            
            <div class="row__actions">
                <a href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>" class="button button--edit"><i class="fa fa-pencil"></i> Edit</a>
                <? if ( isSuper() ) { ?><a id="inline" href="#pop_delete_<?=$row['_id']?>" class="button button--delete"><i class="fa fa-trash"></i> Delete</a><? } ?>
            </div>
            <div class="row__status">
                <?
				// Live, Draft
				if($row['post_status'] == "live") { $postIcon = "check"; }
				if($row['post_status'] == "private") { $postIcon = "lock"; }
				if($row['post_status'] == "draft") { $postIcon = "file-text-o"; }
				?>
				<div class="row__status__title"><i class="fa fa-<?=$postIcon?>"></i> <?=$row['post_status']?></div>
            </div>
            <? if ( isSuper() ) { ?>
                <? if ( in_array($row['_id'], [1,2]) ) { ?>
                    <div style="display:none">
                        <div id="pop_delete_<?=$row['_id']?>">
                        	<h3>Oops!</h3>
                        	<p>You can't delete the home page. Everything is based around this page, so if you delete it you'll break the whole website!</p>
                        	<form id="delete-post-<?=$row['_id'];?>" method="post" action="#">
                                <button type="button" name="cancel_delete" class="button"><i class="fa fa-arrow-left"></i> Okay, I understand</button>
                            </form>
                        </div>
                    </div>
                <? } else { ?>
                    <div style="display:none">
                        <div id="pop_delete_<?=$row['_id']?>">
                        	<h3>Are you sure?</h3>
                        	<p>This will delete <strong><?=$row['post_alt_title']?></strong> forever - please be sure before you delete this record.</p>
                        	<form id="delete-post-<?=$row['_id'];?>" method="post" action="/admin/scripts/manage_post.php">
                                <input type="hidden" name="post_id" value="<?=$row['_id'];?>">
                                <input type="hidden" name="return_to" value="<?=$_SERVER['REQUEST_URI'];?>">
                                <input type="hidden" name="post_type" value="<?=$row['post_type'];?>">
                                <button type="button" name="cancel_delete" class="button"><i class="fa fa-arrow-left"></i> No, don't delete it!</button>
                                <button type="submit" name="delete_post" class="button button--delete"><i class="fa fa-trash"></i> Yes! Delete it!</button>
                            </form>
                        </div>
                    </div>
                <? } ?>
            <? } ?>
		</div>
    <?
        $last_indent = $indent;
        buildPageList($row['_id'], $indent+1);
    } ?>
    
    <?
}
?>

<?php require_once(FS_ADMIN_INCLUDES.'inc_footer.php'); ?>
<?php require_once(FS_ADMIN_INCLUDES.'inc_js.php'); ?>