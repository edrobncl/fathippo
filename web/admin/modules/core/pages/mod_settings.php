<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Page',
    'plural' => 'Pages',
    'form_link' => 'pages-form.php',
    'main_menu' => [
        'option' => 'primary',
        'order' => 1
    ],
    'post_type' => 'page',
    'icon' => 'file-text-o',
    'header_uploader' => true,
    'gallery_uploader' => true,
    'file_uploader' => true
];
?>