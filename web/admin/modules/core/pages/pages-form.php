<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

require_once(FS_ADMIN_INCLUDES.'inc_form_header.php');

if ( in_array($admin_page_type,['add','edit']) ) {
    
    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['singular'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;
        
    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>
   
     
    <div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=($admin_page_type=='edit')?'pencil':'plus';?>"></i> <div class="page-headers__title__parentlink"><a href="/admin/modules/core/<?=$module_form_settings['module_name']?>/"><?=$module_form_settings['plural']?></a> <i class="fa fa-angle-right"></i></div> <?=$admin_page_title;?></h1>
    <? if ($_SESSION['userlevel'] == 'superuser' && isset($post_id)) { ?><a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a><? } ?>
    </div>
    
    <? if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? } ?>
    
    
    <form class="validate-form wrap form-with-actions checkslug" method="post" action="/admin/scripts/manage_post.php" enctype="multipart/form-data">
        <input type="hidden" name="post_id" value="<?=$_id;?>">
        <input type="hidden" name="post_type" value="<?=$module_form_settings['post_type'];?>">
        <input type="hidden" name="post_author" value="<?=$_SESSION['eruid'];?>">
        <input type="hidden" name="return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
        
        <div class="wrap __form__group">
            <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>
                    <div class="form__field with_help">
                        <label class="form__field__label" for="post_title">Page Title<span class="required">*</span><? if ($_id > 0) { ?><a tabindex="-1" id="inline" href="#pop_post_title" class="admin-help"><i class="fa fa-question-circle"></i> Help?</a><? } ?></label>
                        <input class="form__field__input" name="post_title" id="post_title" type="text" required value="<?=(isset($post_title))?$post_title:'';?>" placeholder="The main identifer for your page" />
                    </div>
                    <div style="display:none"><div id="pop_post_title">
                    	<h3>Page Title</h3>
                    	<p>This will be the main heading on the page.</p>
                    	<a target="_blank" href="<?=buildPath($_id) ?>?show=labels">Show me the page! <i class="fa fa-external-link"></i></a>
                    </div></div>

                    <div class="form__field">
                        <label class="form__field__label" for="post_alt_title">Alternative / Short Title<span class="required">*</span></label>
                        <input class="form__field__input" name="post_alt_title" id="post_alt_title" type="text" required value="<?=$post_alt_title;?>" />
                    </div>

                    <?php if($_SESSION['userlevel'] == 'superuser') { ?>
                      <div class="form__field">
                          <label class="form__field__label" for="construction_mode">Construction Mode</label>
                          <select class="form__select" name="construction_mode">
                              <option name="construction_mode" value="">Show this page</option>
                              <option name="construction_mode" value="yes" <?php if(!empty($construction_mode) && $construction_mode == "yes") { ?> selected<?php } ?>>Hide it for clients</option>
                          </select>
                      </div>
                    <?php } ?>
                    
					          <? if ( !in_array($_id, [1,2]) ) { // If homepage or 404, don't allow change of slug ?>
                        <div class="form__field with_help">
                          <label class="form__field__label" for="post_slug"><a href="<?=buildPath($post_id)?>" target="_blank">Internal Page Slug</a> <span class="required">*</span><? if ($_id > 0) { ?><a tabindex="-1" id="inline" href="#pop_post_slug" class="admin-help"><i class="fa fa-question-circle"></i> Help?</a><? } ?></label>
                              <input class="form__field__input disable_slug" name="post_slug" id="post_slug" type="text" required value="<?=(isset($post_slug))?$post_slug:'';?>" placeholder="The main identifer for your page" />
                          </div>
                          <div style="display:none"><div id="pop_post_slug">
                              <h3>Internal Page Slug</h3>
                              <p>This is the snippet that is used to build the URL of the page.</p>
                              <p>e.g. /category/subcategory/<b>this-page-slug</b>.
                              <p>Please do not enter external URLs in this section.</p>
                          </div>
                        </div>

                        <div class="form__field with_help">
                          <label class="form__field__label" for="external_slug"><a href="<?=$external_slug;?>" target="_blank">External Page Slug</a><? if ($_id > 0) { ?><a tabindex="-1" id="inline" href="#pop_external_slug" class="admin-help"><i class="fa fa-question-circle"></i> Help?</a><? } ?></label>
                          <input class="form__field__input" name="external_slug" id="external_slug" type="text" value="<?=$external_slug??'';?>" placeholder="The url to external page, e.g., Deliveroo" />
                          <div style="display:none"><div id="pop_external_slug">
                            <h3>External Page Slug</h3>
                            <p>This is the snippet that is used to point your page to external website.</p>
                            <p>e.g. https://fathippo.deliveroo.com/menu/.</p>
                            <p>Please do not enter internal URLs in this section.</p>
                          </div>
                        </div>
                    <? } else { ?>
                        <input type="hidden" name="post_slug" value="<?=$post_slug;?>">
                    <? } ?>

                    <?php
                    // Only show post_layout edit ability if superuser
                    if($_SESSION['userlevel'] == 'superuser' && !in_array($_id, [1,2])) {
                    ?>
                        <div class="form__field">
                            <label class="form__field__label" for="post_layout">Post Layout</label>
                            <input class="form__field__input" name="post_layout" id="post_layout" type="text" value="<?=(isset($post_layout))?$post_layout:'';?>" />
                        </div>
                        <div class="form__field">
                          <label class="form__field__label" for="post_nav_icon">Post Nav Icon</label>
                          <input class="form__field__input" name="post_nav_icon" id="post_nav_icon" type="text" value="<?=$post_nav_icon ?? '';?>" />
                        </div>
                    <?php } else { ?>
                        <input type="hidden" name="post_layout" value="<?=$post_layout ?? '';?>">
                        <input type="hidden" name="post_nav_icon" value="<?=$post_nav_icon ?? '';?>">
                    <?php } ?>
                
                    <? if ( !in_array($_id, [1,2]) ) { // If homepage or 404, don't allow change of parent ?>
                        <div class="form__field">
                            <label class="form__field__label" for="parent_id">Parent Page<span class="required">*</span></label>
                            <select class="form__select" name="parent_id" id="parent_id" required>
                                <option value="">Please choose...</option>
                                <option value="0"<? if($parent_id == 0) { ?> selected<? } ?>>No parent page</option>
                                <? page_options(0,0,$parent_id,$post_id,'page'); ?>
                            </select>
                        </div>
                    <? } else { ?>
                        <input type="hidden" name="parent_id" value="<?=$parent_id;?>">
                    <? } ?>


                    
                                </fieldset>
                                </div>
            
        
            <? if ( !in_array($_id, [1,2]) ) { // If homepage or 404, don't allow change of status ?>
                <div class="form__group">
                    <fieldset class="display-options">
                        <legend class="form__group__legend">Display Options</legend>
                        <div class="form__field">
                            <label class="form__field__label" for="post_status">Page Status<span class="required">*</span></label>
                            <select class="form__select" name="post_status" id="post_status" required>
                                <option value="">Please choose...</option>
                                <option value="draft"<?=($post_status=='draft')?' selected':'';?>>Draft</option>
                                <option value="private"<?=($post_status=='private')?' selected':'';?>>Private</option>
                                <option value="live"<?=($post_status=='live')?' selected':'';?>>Live</option>
                            </select>
                        </div>
                        <div class="form__field">
                            <label class="form__field__label" for="menu_order">Order<span class="required">*</span></label>
                            <input class="form__field__input" name="menu_order" id="menu_order" type="number" required value="<?=$menu_order;?>" />
                        </div>
                        <div class="form__field">
                            <label class="form__field__label" for="show_footer">Show In Footer<span class="required">*</span></label>
                            <select class="form__select" name="show_footer" id="show_footer" required>
                                <option value="">Please choose...</option>
                                <option value="no"<?=($show_footer=='no')?' selected':'';?>>No</option>
                                <option value="yes"<?=($show_footer=='yes')?' selected':'';?>>Yes</option>
                            </select>
                        </div>
                        <div class="form__field">
                            <label class="form__field__label" for="footer_section">Footer Section</label>
                            <select class="form__select" name="footer_section" id="footer_section">
                                <option value="">None</option>
                                <option value="1"<?=($footer_section=='1')?' selected':'';?>>Eat With Us</option>
                                <option value="2"<?=($footer_section=='2')?' selected':'';?>>Learn About Us</option>
                            </select>
                        </div>
                    </fieldset>
                </div>
            <? } else { ?>
                <input type="hidden" name="post_status" value="<?=$post_status;?>">
                <input type="hidden" name="menu_order" value="<?=$menu_order;?>">
                <? if ($post_id == 1) { ?>
                    <input type="hidden" name="show_footer" value="yes">
                <? } else { ?>
                    <input type="hidden" name="show_footer" value="no">
                <? } ?>
            <? } ?>
        </div>
        
        <?php if ( file_exists(FS_ADMIN_ROOT.'/includes/inc_seo_content.php') && $admin_page_type == "edit" ) {
            include(FS_ADMIN_ROOT.'/includes/inc_seo_content.php'); 
        } ?>
        
        <?php if ( file_exists(FS_ADMIN_ROOT.'/modules/core/'.$module_form_settings['module_name'].'/extensions.php') ) {
            include(FS_ADMIN_ROOT.'/modules/core/'.$module_form_settings['module_name'].'/extensions.php'); 
        } ?>
        
        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>
        
    </form>
    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // If you're not editing or adding a post, you shouldn't really be here!
    $path = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($path);
    $path = implode('/',$path);
    // Go to the main index of this module.
    header("Location: ".$path.'/');
}
?>