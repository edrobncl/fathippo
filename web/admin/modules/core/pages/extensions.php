<?php

include_once(FS_ROOT.'layouts/'.(isset($post_layout)?$post_layout:($_id==1?'homepage':($_id==2?'404':'page'))).'.php');

// Grab the post editables for the post_id and turn it into an array so we can check if the editable exists in the array
$post_editables_list = '';
if (isset($post_editables)) {
    $post_editables_list = json_decode($post_editables,true);
 ?>
<div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Page Content</legend>

            <? if(2 == 1) { ?>
            <div class="form__field">
                <label class="form__field__label" for="hero_title">Hero Title</label>
                <input class="form__field__input" name="hero_title" id="hero_title" type="text" value="<?=(isset($hero_title))?$hero_title:'';?>" />
            </div>
            
            <div class="form__field">
                <label class="form__field__label" for="hero_tagline">Hero Tagline (h1)</label>
                <input class="form__field__input" name="hero_tagline" id="hero_tagline" type="text" value="<?=(isset($hero_tagline))?$hero_tagline:'';?>" />
            </div>

            <? } ?>

            <?php if ( in_array("post_tagline",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline">Tagline</label>
                    <input class="form__field__input" name="post_tagline" id="post_tagline" type="text" value="<?=(isset($post_tagline))?$post_tagline:'';?>" />
                </div>
            <? } ?>

            

            <?php if ( in_array("page_content",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content">Page Content</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content" id="page_content"><?=(isset($page_content))?$page_content:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline2",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline2">Tagline 2</label>
                    <input class="form__field__input" name="post_tagline2" id="post_tagline2" type="text" value="<?=(isset($post_tagline2))?$post_tagline2:'';?>" />
                </div>
            <? } ?>

            <?php if ( in_array("page_content2",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content2">Page Content 2</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content2" id="page_content2"><?=(isset($page_content2))?$page_content2:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline3",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline3">Tagline 3</label>
                    <input class="form__field__input" name="post_tagline3" id="post_tagline3" type="text" value="<?=(isset($post_tagline3))?$post_tagline3:'';?>" />
                </div>
            <? } ?>

            <?php if ( in_array("page_content3",$post_editables_list) ) { ?>
            <div class="form__field">
                <label  class="form__field__label" for="page_content3">Page Content 3</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content3" id="page_content3" ><?=(isset($page_content3))?$page_content3:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline4",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline4">Tagline 4</label>
                    <input class="form__field__input" name="post_tagline4" id="post_tagline4" type="text" value="<?=(isset($post_tagline4))?$post_tagline4:'';?>" />
                </div>
            <? } ?>
            
            <?php if ( in_array("page_content4",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content4">Page Content 4</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content4" id="page_content4" ><?=(isset($page_content4))?$page_content4:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline5",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline5">Tagline 5</label>
                    <input class="form__field__input" name="post_tagline5" id="post_tagline5" type="text" value="<?=(isset($post_tagline5))?$post_tagline5:'';?>" />
                </div>
            <? } ?>

            <?php if ( in_array("page_content5",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content5">Page Content 5</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content5" id="page_content5" ><?=(isset($page_content5))?$page_content5:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline6",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline6">Tagline 6</label>
                    <input class="form__field__input" name="post_tagline6" id="post_tagline6" type="text" value="<?=(isset($post_tagline6))?$post_tagline6:'';?>" />
                </div>
            <? } ?>

            <?php if ( in_array("page_content6",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content6">Page Content 6</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content6" id="page_content6" ><?=(isset($page_content6))?$page_content6:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline7",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline7">Tagline 7</label>
                    <input class="form__field__input" name="post_tagline7" id="post_tagline7" type="text" value="<?=(isset($post_tagline7))?$post_tagline7:'';?>" />
                </div>
            <? } ?>

            <?php if ( in_array("page_content7",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content7">Page Content 7</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content7" id="page_content7" ><?=(isset($page_content7))?$page_content7:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline8",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline8">Tagline 8</label>
                    <input class="form__field__input" name="post_tagline8" id="post_tagline8" type="text" value="<?=(isset($post_tagline8))?$post_tagline8:'';?>" />
                </div>
            <? } ?>

            <?php if ( in_array("page_content8",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content8">Page Content 8</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content8" id="page_content8" ><?=(isset($page_content8))?$page_content8:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline9",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline9">Tagline 9</label>
                    <input class="form__field__input" name="post_tagline9" id="post_tagline9" type="text" value="<?=(isset($post_tagline9))?$post_tagline9:'';?>" />
                </div>
            <? } ?>

            <?php if ( in_array("page_content9",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content9">Page Content 9</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content9" id="page_content9" ><?=(isset($page_content9))?$page_content9:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("post_tagline10",$post_editables_list) ) { ?>
                <div class="form__field">
                    <label class="form__field__label" for="post_tagline10">Tagline 10</label>
                    <input class="form__field__input" name="post_tagline10" id="post_tagline10" type="text" value="<?=(isset($post_tagline10))?$post_tagline10:'';?>" />
                </div>
            <? } ?>
            
            <?php if ( in_array("page_content10",$post_editables_list) ) { ?>
            <div class="form__field">
                <label class="form__field__label" for="page_content10">Page Content 10</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content10" id="page_content10" ><?=(isset($page_content10))?$page_content10:'';?></textarea>
            </div>
            <? } ?>

            <?php if ( in_array("menus",$post_editables_list) ) { 
                $menu_list = json_decode($menu_list ?? '[]', true);
                ?>
                <div class="form__field">
                    <label class="form__field__label" for="menus">Menus</label>
                    <div class="tag-input-box-wrap">
                        <div class="wrap tag-input-boxes">
                            <?php foreach ( getMenus(true) as $menu ) { ?>
                                <label class="tag-input-box"><input type="checkbox" name="menu_list[]" value="<?=$menu['_id'];?>"<?=(in_array($menu['_id'],$menu_list))?' checked':'';?>><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> <?=$menu['post_title']?></label>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <? if(2 == 1) { ?>
                    <? if ( isset($homepage_categories) ) {
                        $homepage_categories_array = json_decode($homepage_categories,true);
                    } else {
                        $homepage_categories_array = [];
                    } 
                    
                    $typeList = $pdo->query("SELECT _id, parent_id, post_title FROM er_posts WHERE (post_type = 'category' OR post_type = 'page') AND post_status = 'live' AND (parent_id <> 0) ORDER BY post_title ASC")->fetchAll(); ?>
                    <div class="form__field">
                        <label class="form__field__label" for="homepage_categories_save">Homepage Categories</label>
                        <div class="tag-input-box-wrap">
                            <div class="wrap tag-input-boxes">
                                <? foreach ( $typeList as $page ) { ?>
                                    <label class="tag-input-box"><input type="checkbox" name="homepage_categories[]" value="<?=$page['_id'];?>"<?=(in_array($page['_id'],$homepage_categories_array))?' checked':'';?>><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> <?=$page['post_title']?></label>
                                <? } ?>
                            </div>
                        </div>
                    </div>
            <? } ?>

        </fieldset>
        
    
</div>
<? } ?>
<?php 
// ====================
// STANDARD IMAGE OPTIONS
// ====================
if ( in_array("header_image",$post_editables_list) ) { 
    single_image('header_image', 'Header Image', '1920px by 800px'); 
} 
if ( in_array("thumbnail_image",$post_editables_list) ) { 
    single_image('thumbnail_image', 'Thumbnail/Portal Image', '600px by 466px');
}
if ( in_array("image_gallery",$post_editables_list) ) { 
    multi_image('image_gallery', 'Image Gallery');
}
// ====================
// POST CONTENT IMAGES
// ====================
if ( in_array("page_content_image",$post_editables_list) ) { 
    single_image('page_content_image', 'Page Content Image', '900px by 500px'); 
}
if ( in_array("page_content2_image",$post_editables_list) ) { 
    single_image('page_content2_image', 'Page Content 2 Image', '900px by 500px'); 
}
if ( in_array("page_content3_image",$post_editables_list) ) { 
    single_image('page_content3_image', 'Page Content 3 Image', '900px by 500px'); 
}
if ( in_array("page_content4_image",$post_editables_list) ) { 
    single_image('page_content4_image', 'Page Content 4 Image', '900px by 500px'); 
}
if ( in_array("page_content5_image",$post_editables_list) ) { 
    single_image('page_content5_image', 'Page Content 5 Image', '900px by 500px'); 
}
if ( in_array("page_content6_image",$post_editables_list) ) { 
    single_image('page_content6_image', 'Page Content 6 Image', '900px by 500px'); 
}
if ( in_array("page_content7_image",$post_editables_list) ) { 
    single_image('page_content7_image', 'Page Content 7 Image', '900px by 500px'); 
}
if ( in_array("page_content8_image",$post_editables_list) ) { 
    single_image('page_content8_image', 'Page Content 8 Image', '900px by 500px'); 
}
if ( in_array("page_content9_image",$post_editables_list) ) { 
    single_image('page_content9_image', 'Page Content 9 Image', '900px by 500px'); 
}
if ( in_array("page_content10_image",$post_editables_list) ) { 
    single_image('page_content10_image', 'Page Content 10 Image', '900px by 500px'); 
}
// ====================
// FILES
// ====================
if ( in_array("downloads",$post_editables_list) ) { 
    file_uploader('downloads', 'Downloads');
}
// ====================
// FACEBOOK PIXEL
// ====================
if ( in_array("facebook_pixel",$post_editables_list) ) { ?>
    <div class="form__group">
            <fieldset>
                <legend class="form__group__legend">Tracking</legend>
                <div class="form__field">
                    <label class="form__field__label" for="facebook_pixel">Facebook Pixel Code</label>
                    <textarea class="form__field__textarea richtext_editor" name="facebook_pixel" id="facebook_pixel"><?=(isset($facebook_pixel))?$facebook_pixel:'';?></textarea>
                </div>
            </fieldset>
    </div>
    <?php } ?>