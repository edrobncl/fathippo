<script>
// Fetch the longitude and latitude based on postcode
$(document).on('click', '#get_lat_long', function() {
    var thePostcode = $('input[name="site_postcode"]').val();
    
    if ( $.trim(thePostcode).length > 0 ) {
        $.ajax({
            method: 'get',
            url: 'https://api.postcodes.io/postcodes/'+thePostcode,
            dataType: 'json',
            success: function(res) {
                if ( res.status == 200 ) {
                    $('input[name="site_lat_long"]').val(res.result.longitude + ', ' + res.result.latitude);
                }
            }
        });
    }
});

// Toggle errors in admin during session
$(document).on('change', '#show_errors', function(e) {
    var switchAction;
    if ( $(this).prop('checked') == true ) {
        switchAction = 'on';
    } else {
        switchAction = 'off';
    }
    $.ajax({
        type: 'post',
        url: '/admin/modules/core/settings/errors_toggle.php',
        data: {action: switchAction}
    });
});
</script>