<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

// Include module's functions
if ( file_exists('mod_funcs.php') ) {
    require_once('mod_funcs.php');
}

// check if edit or script
$admin_page_type = ($_SERVER['REQUEST_METHOD'] === 'POST') ? 'script' : 'edit';

switch ($admin_page_type) {
    case 'edit':        
        // Build any postmeta variables
        $postMeta = $pdo->query("SELECT * FROM er_config")->fetch();
        if ( $postMeta ) {
            extract($postMeta);
        }
        break;
        
    case 'script':
        break;
}

if ( $admin_page_type == 'edit' ) {
    
    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['plural'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;
        
    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>
     
    <div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-cog"></i> <?=$admin_page_title;?></h1>
    </div>
    
    <?
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success">'.$_SESSION['success'].'</div>';
        }
    ?>
    
    <form class="validate-form wrap form-with-actions" method="post" action="<?=$_SERVER['REQUEST_URI'];?>" enctype="multipart/form-data">
        <input type="hidden" name="return_to" value="<?=$_SERVER['REQUEST_URI'];?>">
        
        <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="site_name">Site Name<span class="required">*</span></label>
                        <input class="form__field__input" name="site_name" id="site_name"   type="text" required value="<?=(isset($site_name))?$site_name:'';?>" placeholder="The name of your website (usually your company's name)" />
                    </div>

                    <? if ( isSuper() ) { ?>
                        <div class="form__field">
                            <label class="form__field__label" for="admin_logo">Admin Logo<span class="required">*</span></label>
                            <input class="form__field__input" name="admin_logo" id="admin_logo"   type="text" required value="<?=(isset($admin_logo))?$admin_logo:'';?>" placeholder="Logo for admin login" />
                        </div>
                    <? } ?>
                
                    <div class="form__field">
                        <label class="form__field__label" for="append_meta_title">Append Meta Title<span class="required">*</span></label>
                        <input class="form__field__input" name="append_meta_title" id="append_meta_title"   required type="text" value="<?=(isset($append_meta_title))?$append_meta_title:'';?>" placeholder="This will be added to the end of Meta titles" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="append_meta_description">Append Meta Description<span class="required">*</span></label>
                        <input class="form__field__input" name="append_meta_description" id="append_meta_description"   required type="text" value="<?=(isset($append_meta_description))?$append_meta_description:'';?>" placeholder="This will be added to the end of Meta description" />
                    </div>
                
                    <div class="form__field">
                        <label class="form__field__label" for="emailer_from">Send Emails From<span class="required">*</span></label>
                        <input class="form__field__input" name="emailer_from" id="emailer_from"   type="email" required value="<?=(isset($emailer_from))?$emailer_from:'';?>" placeholder="Contact forms will use this address to send emails to you" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="emailer_to">Send Emails To<span class="required">*</span></label>
                        <input class="form__field__input" name="emailer_to" id="emailer_to"  type="email" required value="<?=(isset($emailer_to))?$emailer_to:'';?>" placeholder="Contact forms will be sent to this address" />
                    </div>
                
                    <div class="form__field">
                        <label class="form__field__label" for="ga_code">Google Analytics Code</label>
                        <input class="form__field__input" name="ga_code" id="ga_code" type="text" value="<?=(isset($ga_code))?$ga_code:'';?>" placeholder="Google Analytics code beginning with UA-" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="gst_code">Global Site Tag Code</label>
                        <input class="form__field__input" name="gst_code" id="ga_code" type="text" value="<?=(isset($gst_code))?$gst_code:'';?>" placeholder="Modern way to track Google Analytics. Code begins G-" />
                    </div>
                </fieldset>
            </div>
            <?php if ( isSuper() ) { // Show options for SuperUser account ?>
                <div class="form__group">
                    <fieldset class="display-options">
                        <legend class="form__group__legend">Super User Options</legend>
                        <div class="form__field">
                            <label class="form__field__label" for="css_js_filename">CSS/JS<span class="required">*</span></label>
                            <input class="form__field__input" name="css_js_filename" id="css_js_filename" type="text" required value="<?=(isset($css_js_filename))?$css_js_filename:'';?>" placeholder="Main CSS and JS filename" />
                        </div>
                        <div class="form__field">
                            <label class="form__field__label" for="database_live_date">Database live date / Author</label>
                            <input class="form__field__input" name="database_live_date" id="database_live_date"   type="text" value="<?=(isset($database_live_date))?$database_live_date:'';?>" placeholder="e.g. 1st Jan 2020 / Graham" />
                        </div>
                        <div class="form__field">
                            <input type="checkbox" id="show_errors" class="hide_switch_input"<?=(isset($_SESSION['show_errors']))?' checked':'';?>>
                            <label for="show_errors"><div class="form__field__label">Show CMS Errors</div><span class="toggle_switch"></span></label>
                        </div>
                    </fieldset>
                </div>
            <?php } ?>
       
                
        <div class="form__group">
            	<fieldset>
                    <legend class="form__group__legend">Contact Details</legend>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="site_address">Address</label>
                        <input class="form__field__input" name="site_address" id="site_address"   type="text" value="<?=(isset($site_address))?$site_address:'';?>" placeholder="Your address" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="site_postcode">Postcode</label>
                        <input class="form__field__input" class="form__field__input" name="site_postcode" id="site_postcode" type="text" value="<?=(isset($site_postcode))?$site_postcode:'';?>" placeholder="Your postcode" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="reg_number">Company Reg Number</label>
                        <input class="form__field__input" name="reg_number" id="reg_number" type="text" value="<?=(isset($reg_number))?$reg_number:'';?>" placeholder="Company registration number" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="site_maplink">Google Map Link</label>
                        <input class="form__field__input" name="site_maplink" id="site_maplink" type="text" value="<?=(isset($site_maplink))?$site_maplink:'';?>" placeholder="Your map link" />
                    </div>
                    
                    <div class="form__field frm_latlong">
                        <label class="form__field__label" for="site_lat_long">Longitude / Latitude</label>
                        <input class="form__field__input" name="site_lat_long" id="site_lat_long" type="text" value="<?=(isset($site_lat_long))?$site_lat_long:'';?>" placeholder="The longitude and latitude of you address on a map" />
                        <? if(isset($site_postcode)) { ?><button type="button" name="get_lat_long" id="get_lat_long" class="button"><i class="fa fa-map-marker"></i> Fetch Longitude / Latitude</button><? } ?>
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="site_telephone">Telephone</label>
                        <input class="form__field__input" name="site_telephone" id="site_telephone" type="text" value="<?=(isset($site_telephone))?$site_telephone:'';?>" placeholder="Your contact telephone number" />
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="site_email">Email</label>
                        <input class="form__field__input" name="site_email" id="site_email" type="text" value="<?=(isset($site_email))?$site_email:'';?>" placeholder="Your contact email address" />
                    </div>
                </fieldset>
        </div>
        
        <div class="wrap form-actions">
            <div class="msg change-alert"><i class="fa fa-warning"></i> Please <b>save</b> your settings before navigating elsewhere or you will lose your changes.</div>
            <button class="form-actions__save" type="submit" name="save_form"><i class="fa fa-save"></i> Save and Continue</button>
        </div>
        
    </form>

    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // Save the submitted settings

    // Set return address and remove return_to and save_form from $_POST
    $return_to = $_POST['return_to'];
    unset($_POST['return_to']);
    unset($_POST['save_form']);

    // Make postcode uppercase, just in case ;)
    $_POST['site_postcode'] = strtoupper($_POST['site_postcode']);

    // Build the query string
    $query = '';
    foreach ($_POST as $key => $val) {
        $query .= $key . ' = :'.$key.', ';
    }
    $query = 'UPDATE er_config SET ' . rtrim($query,", ");

    // Save settings
    $saveSettings = $pdo->prepare($query);

    if ( isset($_POST['css_js_filename']) ) {
        $saveSettings->bindParam(':css_js_filename', $_POST['css_js_filename'], PDO::PARAM_STR);
    }

    if ( isset($_POST['admin_logo']) ) {
        $saveSettings->bindParam(':admin_logo', $_POST['admin_logo'], PDO::PARAM_STR);
    }

    if ( isset($_POST['database_live_date']) ) {
        $saveSettings->bindParam(':database_live_date', $_POST['database_live_date'], PDO::PARAM_STR);
    }

    $saveSettings->bindParam(':site_name', $_POST['site_name'], PDO::PARAM_STR);
    $saveSettings->bindParam(':append_meta_title', $_POST['append_meta_title'], PDO::PARAM_STR);
    $saveSettings->bindParam(':append_meta_description', $_POST['append_meta_description'], PDO::PARAM_STR);
    $saveSettings->bindParam(':emailer_from', $_POST['emailer_from'], PDO::PARAM_STR);
    $saveSettings->bindParam(':emailer_to', $_POST['emailer_to'], PDO::PARAM_STR);
    $saveSettings->bindParam(':ga_code', $_POST['ga_code'], PDO::PARAM_STR);
    $saveSettings->bindParam(':gst_code', $_POST['gst_code'], PDO::PARAM_STR);
    $saveSettings->bindParam(':reg_number', $_POST['reg_number'], PDO::PARAM_STR);
    $saveSettings->bindParam(':site_address', $_POST['site_address'], PDO::PARAM_STR);
    $saveSettings->bindParam(':site_postcode', $_POST['site_postcode'], PDO::PARAM_STR);
    $saveSettings->bindParam(':site_maplink', $_POST['site_maplink'], PDO::PARAM_STR);
    $saveSettings->bindParam(':site_lat_long', $_POST['site_lat_long'], PDO::PARAM_STR);
    $saveSettings->bindParam(':site_telephone', $_POST['site_telephone'], PDO::PARAM_STR);
    $saveSettings->bindParam(':site_email', $_POST['site_email'], PDO::PARAM_STR);

    $saveSettings->execute();

    $_SESSION['success'] = 'Settings saved successfully';

    // Refresh page
    header("Location: ".$return_to);
}
?>