<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Setting',
    'plural' => 'Settings',
    'form_link' => '',
    'icon' => 'cog'
];
?>