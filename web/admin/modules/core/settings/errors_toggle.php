<?php
// Show or hide errors in admin during session
if ( $_POST['action'] == 'on' ) {
    session_start();
    $_SESSION['show_errors'] = 1;
} else if ($_POST['action'] == 'off' ) {
    session_start();
    unset($_SESSION['show_errors']);
}