<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Content</legend>

            <div class="form__field">
                <label class="form__field__label" for="popup_title">Title <span class="required">*</span></label>
                <input type="text" class="form__field__input" name="popup_title" id="popup_title" value="<?= $popup_title ?? '' ?>" required />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="popup_tagline">Tagline <span class="required">*</span></label>
                <input type="text" class="form__field__input" name="popup_tagline" id="popup_tagline" value="<?= $popup_tagline ?? '' ?>" required />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="popup_body">Main Text <span class="required">*</span></label>
                <textarea class="form__field__textarea richtext_editor" name="popup_body" id="popup_body" required><?= $popup_body ?? '' ?></textarea>
            </div>

            
            <div class="form__field">
                <label class="form__field__label" for="popup_footer">Footer <span class="required">*</span></label>
                <textarea class="form__field__textarea richtext_editor" name="popup_footer" id="popup_footer" required><?= $popup_footer ?? '' ?></textarea>
            </div>

        </fieldset>
    </div>
</div>

<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Where to show</legend>

            <?php
                $pageList = json_decode($popup_page_list ?? '[]', true);
            ?>
            <div class="form__field">
                <label class="form__field__label">Pages</label>
                <div class="tag-input-box-wrap">
                    <input type="text" placeholder="Search..." data-search="popup_page_list[]" />

                    <div class="wrap tag-input-boxes">
                        <?php foreach ( getPages() as $page ) { ?>
                            <label class="tag-input-box"><input type="checkbox" name="popup_page_list[]" value="<?=$page['_id'];?>"<?=(in_array($page['_id'], $pageList))?' checked':'';?>><i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> <?=$page['post_title']?></label>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </fieldset>
    </div>
</div>



<?php
