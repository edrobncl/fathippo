<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Pop Up',
    'plural' => 'Pop Ups',
    'form_link' => 'popup-form.php',
    'post_type' => 'popup',
    'hide_slug_on_index' => true,
    'icon' => 'window-restore'
];

$homepage_card = [
    'h3' => 'Why not add a <b>Pop Up?</b>',
    'p' => 'Pop Ups can convey and push users to areas of the site you wish to advertise more.',
    'manage-link' => '/admin/modules/'.$module_settings['module_name'] . '/',
    'add-link' => '/admin/modules/'.$module_settings['module_name'].'/'.$module_settings['form_link'],
    'manage-text' => 'Manage ' . $module_settings['plural'],
    'add-text' => 'Add a ' . $module_settings['singular'],
    'icon' => 'window-restore'
];
?>
