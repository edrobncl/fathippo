<?php

if ( !function_exists('beforeSave') ) {
    function beforeSave($post) {
        
        return $post;
    }
}

if ( !function_exists('beforeRowOutput') ) {
    function beforeRowOutput($post) {
        
        return $post;
    }
}

if ( !function_exists('getPages') ) {

/**
 * Retrieve all Pages
 * 
 * @return array Returns all pages
 */
function getPages(): array {
    global $pdo;

    // Get Pages
    return $pdo->query('SELECT _id, post_title FROM er_posts WHERE post_type = "page"')->fetchAll();
}
}