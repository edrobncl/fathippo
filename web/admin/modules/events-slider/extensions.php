<div class="form__group">
  <fieldset>
    <legend class="form__group__legend">Customisation</legend>

    <div class="form__field">
      <label class="form__field__label" for="post_status">Text Color<span class="required">*</span></label>
      <select class="form__select" name="text_color" id="text_color" required>
        <option value="" hidden>Please choose...</option>
        <option value="#fff"<?=($text_color=='#fff')?' selected':'';?>>White</option>
        <option value="#000"<?=($text_color=='#000')?' selected':'';?>>Black</option>
      </select>
    </div>
  </fieldset>
</div>

<? single_image('homepage_slide_image', 'Homepage Slider Image', '1980px by 1309px'); ?>
