<?php

$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Event Slide',
    'plural' => 'Event Slides',
    'form_link' => 'events-slider-form.php',
    'post_type' => 'event_slider',
    'icon' => 'picture-o',
    'listing_thumbnail' => 'event_slide_image',
    'module_group' => '1'
];
