<?php
    
// Modify the row values before output onto module index page
if ( !function_exists('beforeRowOutput') ) {
    function beforeRowOutput($row) {
        global $pdo;
        
        $row['href'] = getMeta($row['_id'], 'homepage_slide_href');        

        // Making sure post_alt_title isn't empty
        $row['post_alt_title'] = $row['post_title'];

        return $row;
    }
}
?>