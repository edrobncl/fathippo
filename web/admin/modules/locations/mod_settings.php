<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Location',
    'plural' => 'Locations',
    'form_link' => 'locations-form.php',
    'post_type' => 'location',
    'hide_slug_on_index' => true,
    'icon' => 'building',
    'module_group' => '1'
];

?>
