<div class="__form__group">
            <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Extra Details</legend>

            
            <div class="form__field">
              <label class="form__field__label" for="office_address1">Address Line 1 <span class="required">*</span></label>
              <input class="form__field__input" name="office_address1" id="office_address1" type="text" required value="<?=(isset($office_address1))?$office_address1:'';?>" placeholder="Address Line 1" />
            </div>
            
            <div class="form__field">
              <label class="form__field__label" for="office_address2">Address Line 2</label>
              <input class="form__field__input" name="office_address2" id="office_address2" type="text" value="<?=(isset($office_address2))?$office_address2:'';?>" placeholder="Address Line 2" />
            </div>

            <div class="form__field">
              <label class="form__field__label" for="office_address3">Address Line 3</label>
              <input class="form__field__input" name="office_address3" id="office_address3" type="text" value="<?=(isset($office_address3))?$office_address3:'';?>" placeholder="Address Line 3" />
            </div>
            
            <div class="form__field">
              <label class="form__field__label" for="office_city">Town/City <span class="required">*</span></label>
              <input class="form__field__input" name="office_city" id="office_city" type="text" required value="<?=(isset($office_city))?$office_city:'';?>" placeholder="Town/City" />
            </div>

            <div class="form__field">
              <label class="form__field__label" for="office_postcode">Postcode <span class="required">*</span></label>
              <input class="form__field__input" name="office_postcode" id="office_postcode" type="text" required value="<?=(isset($office_postcode))?$office_postcode:'';?>" placeholder="Postcode" />
            </div>

            <div class="form__field">
              <label class="form__field__label" for="office_country">Country <span class="required">*</span></label>
              <select class="form__select" name="office_country" id="office_country" required>
                <option value="" hidden>Please select...</option>
                <option value="england"<?php echo !empty($office_country) && $office_country == 'england' ? ' selected' : ''; ?>>England</option>
                <option value="scotland"<?php echo !empty($office_country) && $office_country == 'scotland' ? ' selected' : ''; ?>>Scotland</option>
                <option value="wales"<?php echo !empty($office_country) && $office_country == 'wales' ? ' selected' : ''; ?>>Wales</option>
                <option value="northern-ireland"<?php echo !empty($office_country) && $office_country == 'northern-ireland' ? ' selected' : ''; ?>>Northern Ireland</option>
              </select>
            </div>

            <div class="form__field">
              <label class="form__field__label" for="office_phone">Phone Number</label>
              <input class="form__field__input" name="office_phone" id="office_phone" type="text" value="<?=(isset($office_phone))?$office_phone:'';?>" placeholder="Contact/Phone number" />
            </div>

            <div class="form__field">
              <label class="form__field__label" for="office_email">Email Address</label>
              <input class="form__field__input" name="office_email" id="office_email" type="text"  value="<?=(isset($office_email))?$office_email:'';?>" placeholder="Email Address" />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="location_amenities[]">Amenities</label>
                <div class="tag-input-box-wrap">
                    <div class="wrap tag-input-boxes">
                        <?php $location_amenities = ! empty($location_amenities) ? json_decode($location_amenities, true) : []; ?>
                        <label class="tag-input-box">
                            <input type="checkbox" name="location_amenities[]" value="1" <?= in_array(1, $location_amenities) ? 'checked' : '' ?> />
                            <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                            Child Friendly
                        </label>
                        <label class="tag-input-box">
                            <input type="checkbox" name="location_amenities[]" value="2" <?= in_array(2, $location_amenities) ? 'checked' : '' ?> />
                            <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                            Dog Friendly
                        </label>
                        <label class="tag-input-box">
                            <input type="checkbox" name="location_amenities[]" value="3" <?= in_array(3, $location_amenities) ? 'checked' : '' ?> />
                            <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                            Free Wifi
                        </label>
                        <label class="tag-input-box">
                            <input type="checkbox" name="location_amenities[]" value="4" <?= in_array(4, $location_amenities) ? 'checked' : '' ?> />
                            <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                            Halal Options
                        </label>
                        <label class="tag-input-box">
                            <input type="checkbox" name="location_amenities[]" value="5" <?= in_array(5, $location_amenities) ? 'checked' : '' ?> />
                            <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                            Outdoor Seating
                        </label>
                        <label class="tag-input-box">
                            <input type="checkbox" name="location_amenities[]" value="6" <?= in_array(6, $location_amenities) ? 'checked' : '' ?> />
                            <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                            Wheelchair Accesible
                        </label>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>

<div class="__form__group">
  <div class="form__group">
    <fieldset>
      <legend class="form__group__legend">Social Media</legend>

      <div class="form__field">
        <label class="form__field__label" for="facebook_link">Twitter</label>
        <input class="form__field__input" name="facebook_link" id="facebook_link" type="text"  value="<?=$twitter_link ?? '';?>" placeholder="e.g. https://www.twitter.com/fathippojesmond" />
      </div>

      <div class="form__field">
        <label class="form__field__label" for="instagram_link">Instagram</label>
        <input class="form__field__input" name="instagram_link" id="instagram_link" type="text"  value="<?=$instagram_link ?? '';?>" placeholder="e.g. https://www.instagram.com/fathippojesmond" />
      </div>

      <div class="form__field">
        <label class="form__field__label" for="facebook_link">Facebook</label>
        <input class="form__field__input" name="facebook_link" id="facebook_link" type="text"  value="<?=$facebook_link ?? '';?>" placeholder="e.g. https://www.facebook.com/fathippojesmond" />
      </div>

      <div class="form__field">
        <label class="form__field__label" for="spotify_link">Spotify</label>
        <input class="form__field__input" name="spotify_link" id="spotify_link" type="text"  value="<?=$spotify_link ?? '';?>" placeholder="e.g. https://open.spotify.com/playlist/your-playlist-id" />
      </div>
    </fieldset>
  </div>
</div>

<div class="__form__group">
  <div class="form__group">
    <fieldset>
      <legend class="form__group__legend">Available Services</legend>

      <div class="form__field">
        <label class="form__field__label" for="book_table">Book a Table</label>
        <input class="form__field__input" name="book_table" id="book_table" type="text" value="<?=$book_table ?? '';?>" />
      </div>

      <div class="form__field">
        <label class="form__field__label" for="click_collect">Click &amp; Collect</label>
        <input class="form__field__input" name="click_collect" id="click_collect" type="text" value="<?=$click_collect ?? '';?>" />
      </div>

      <div class="form__field">
        <label class="form__field__label" for="order_delivery">Order Delivery</label>
        <input class="form__field__input" name="order_delivery" id="order_delivery" type="text" value="<?=$order_delivery ?? '';?>" />
      </div>

      <div class="form__field">
        <label class="form__field__label" for="menu">Menu</label>
        <input class="form__field__input" name="menu" id="menu" type="text" value="<?=$menu ?? '';?>" />
      </div>
    </fieldset>
  </div>
</div>

<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Opening Hours</legend>
                <div class="form__field">
                    <label class="form__field__label" for="opening_monday">Monday</label>
                    <input class="form__field__input" name="opening_monday" id="opening_monday" type="text" value="<?=(isset($opening_monday))?$opening_monday:'';?>" placeholder="10:00 - 22:00" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="opening_tuesday">Tuesday</label>
                    <input class="form__field__input" name="opening_tuesday" id="opening_tuesday" type="text" value="<?=(isset($opening_tuesday))?$opening_tuesday:'';?>" placeholder="10:00 - 22:00" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="opening_wednesday">Wednesday</label>
                    <input class="form__field__input" name="opening_wednesday" id="opening_wednesday" type="text" value="<?=(isset($opening_wednesday))?$opening_wednesday:'';?>" placeholder="10:00 - 22:00" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="opening_thursday">Thursday</label>
                    <input class="form__field__input" name="opening_thursday" id="opening_thursday" type="text" value="<?=(isset($opening_thursday))?$opening_thursday:'';?>" placeholder="10:00 - 22:00" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="opening_friday">Friday</label>
                    <input class="form__field__input" name="opening_friday" id="opening_friday" type="text" value="<?=(isset($opening_friday))?$opening_friday:'';?>" placeholder="10:00 - 22:00" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="opening_saturday">Saturday</label>
                    <input class="form__field__input" name="opening_saturday" id="opening_saturday" type="text" value="<?=(isset($opening_saturday))?$opening_saturday:'';?>" placeholder="10:00 - 22:00" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="opening_sunday">Sunday</label>
                    <input class="form__field__input" name="opening_sunday" id="opening_sunday" type="text" value="<?=(isset($opening_sunday))?$opening_sunday:'';?>" placeholder="10:00 - 22:00" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="opening_bank_holiday">Bank Holiday</label>
                    <input class="form__field__input" name="opening_bank_holiday" id="opening_bank_holiday" type="text" value="<?=(isset($opening_bank_holiday))?$opening_bank_holiday:'';?>" placeholder="10:00 - 22:00, leave blank if N/A" />
                </div>
                <div class="form__field">
                    <label class="form__field__label" for="opening_christmas">Christmas</label>
                    <input class="form__field__input" name="opening_christmas" id="opening_christmas" type="text" value="<?=(isset($opening_christmas))?$opening_christmas:'';?>" placeholder="10:00 - 22:00, leave blank if N/A" />
                </div>

                
        </fieldset>
    </div>
</div>
<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Location Details</legend>

                <div class="form__field">
                    <label class="form__field__label" for="office_latitude">Latitude</label>
                    <input class="form__field__input" name="office_latitude" id="office_latitude" type="text" value="<?=(isset($office_latitude))?$office_latitude:'';?>" placeholder="Office Latitude" />
                </div>
                <div class="form__field">
                    <label class="form__field__label" for="office_longitude">Longitude</label>
                    <input class="form__field__input" name="office_longitude" id="office_longitude" type="text" value="<?=(isset($office_longitude))?$office_longitude:'';?>" placeholder="Office Longitude" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="office_map_link">Google Maps URL</label>
                    <input class="form__field__input" name="office_map_link" id="office_map_link" type="text" value="<?=(isset($office_map_link))?$office_map_link:'';?>" placeholder="Google Maps URL" />
                </div>

                <div class="form__field">
                    <label class="form__field__label" for="office_map_embed">Google Maps Embed URL</label>
                    <input class="form__field__input" name="office_map_embed" id="office_map_embed" type="text" value="<?=(isset($office_map_embed))?$office_map_embed:'';?>" placeholder="Go to embed, copy and paste the iframe src here..." />
                </div>
        </fieldset>
    </div>
</div>

<?php
  single_image('location_header', 'Header Image', '1920px by 1263px');
  single_image('location_image', 'Location Thumbnail', '320px by 211px');
  single_image('location_image2', 'Location Image', '590px by 388px');
  single_image('location_image3', 'Location Illustration', '160px by 160px');
  single_image('location_image4', 'Location Illustration 2', '480px by 270px');
  multi_image('location_gallery', 'Location Carousel', '1920px by 1263px');
?>