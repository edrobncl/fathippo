<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

require_once(FS_ADMIN_INCLUDES.'inc_form_header.php');

if ( in_array($admin_page_type,['add','edit']) ) {

    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['singular'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;

    include(FS_ADMIN_ROOT.'includes/inc_header.php'); ?>

    <div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-<?=($admin_page_type=='edit')?'pencil':'plus';?>"></i> <?=$admin_page_title;?></h1>
        <? if (!empty($post_id)) { ?><a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a><? } ?>
    </div>

    <?
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success">'.$_SESSION['success'].'</div>';
        }
    ?>

    <form class="validate-form wrap form-with-actions" method="post" action="/admin/scripts/manage_post.php" enctype="multipart/form-data">

        <input type="hidden" name="module" value="<?=$module_form_settings['module_name'];?>">
        <input type="hidden" name="post_id" value="<?=$_id;?>">
        <input type="hidden" name="post_type" value="<?=$module_form_settings['post_type'];?>">
        <input type="hidden" name="post_author" value="<?=$_SESSION['eruid'];?>">
        <input type="hidden" name="return_to" value="<?=$_SERVER['SCRIPT_NAME'];?>">
        <input type="hidden" name="menu_order" value="0">
        <input type="hidden" name="parent_id" value="6">
        <input type="hidden" name="post_alt_title" value="<?=$post_title;?>">

        
        <div class="__form__group">
            <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="post_title">Name <span class="required">*</span></label>
                        <input class="form__field__input" name="post_title" id="post_title" type="text" required value="<?=$post_title;?>" placeholder="E.g. Newcastle" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="post_alt_title">Alternative Name <span class="required">*</span></label>
                        <input class="form__field__input" name="post_alt_title" id="post_alt_title" type="text" required value="<?=$post_alt_title;?>" placeholder="E.g. Blackwood House, Sunderland" />
                    </div>

					<? if ($_SESSION['userlevel'] == 'superuser') { ?>
                        <div class="form__field">
                            <label class="form__field__label" for="post_slug"><a href="<?=buildPath($post_id)?>" target="_blank">Internal Page Slug</a> <span class="required">*</span><? if ($_id > 0) { ?><? } ?></label>
                            <input class="form__field__input" name="post_slug" id="post_slug" type="text" class="disable_slug" required value="<?=(isset($post_slug))?$post_slug:'';?>" placeholder="The main identifer for your location" />
                        </div>
                    <? } else { ?>
                        <input type="hidden" name="post_slug" value="<?=$post_slug;?>">
                    <? } ?>

                    <div class="form__field">
                        <label class="form__field__label" for="page_tagline">Tagline</label>
                        <input class="form__field__input" name="page_tagline" id="page_tagline" type="text" value="<?=$page_tagline;?>" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="page_content">Content</label>
                        <textarea class="form__field__textarea richtext_editor" name="page_content" id="page_content"><?=(isset($page_content))?$page_content:'';?></textarea>
                    </div>
                </fieldset>
            </div>
        
            <div class="form__group">
                <fieldset class="display-options">
                    <legend class="form__group__legend">Display Options</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="post_status">Status <span class="required">*</span></label>
                        <select class="form__select" name="post_status" id="post_status" required>
                            <option value="">Please choose...</option>
                            <option value="draft"<?=($post_status=='draft')?' selected':'';?>>Draft</option>
                            <option value="live"<?=($post_status=='live')?' selected':'';?>>Live</option>
                        </select>
                    </div>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="menu_order">Order <span class="required">*</span></label>
                        <input class="form__field__input" name="menu_order" id="menu_order" type="number" required value="<?=$menu_order;?>" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="coming_soon">Coming Soon? <span class="required">*</span></label>
                        <select class="form__select" name="coming_soon" id="coming_soon" required>
                            <option value="no"<?= isset($coming_soon) && $coming_soon=='no' ? ' selected' : '' ?>>No</option>
                            <option value="yes"<?= isset($coming_soon) && $coming_soon=='yes' ? ' selected' : '' ?>>Yes</option>
                        </select>
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="location_concession">Concession? <span class="required">*</span></label>
                        <select class="form__select" name="location_concession" id="location_concession" required>
                            <option value="no"<?= isset($location_concession) && $location_concession=='no' ? ' selected' : '' ?>>No</option>
                            <option value="yes"<?= isset($location_concession) && $location_concession=='yes' ? ' selected' : '' ?>>Yes</option>
                        </select>
                    </div>
                </fieldset>
            </div>


        </div>

        <?php if ( file_exists(FS_ADMIN_ROOT.'/modules/'.$module_form_settings['module_name'].'/extensions.php') ) {
            include(FS_ADMIN_ROOT.'/modules/'.$module_form_settings['module_name'].'/extensions.php'); 
        } ?>

        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>

    </form>

    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // If you're not editing or adding a post, you shouldn't really be here!
    $path = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($path);
    $path = implode('/',$path);
    // Go to the main index of this module.
    header("Location: ".$path.'/');
}
?>


