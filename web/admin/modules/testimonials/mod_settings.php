<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Testimonial',
    'plural' => 'Testimonials',
    'form_link' => 'testimonials-form.php',
    'post_type' => 'testimonial',
    'hide_slug_on_index' => true,
    'icon' => 'comments',
    'group' => 'tools',
];

$homepage_card = [
    'h3' => 'Why not add a <b>testimonial?</b>',
    'p' => 'Testimonials help show users what real people think about your company and serve as a great supporting tool for sales.',
    'manage-link' => '/admin/modules/'.$module_settings['module_name'] . '/',
    'add-link' => '/admin/modules/'.$module_settings['module_name'].'/'.$module_settings['form_link'],
    'manage-text' => 'Manage ' . $module_settings['plural'],
    'add-text' => 'Add a ' . $module_settings['singular'],
    'icon' => 'comments'
];
?>
