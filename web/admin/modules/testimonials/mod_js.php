<script>
	// ===============================================
	//  Form Search
	// ===============================================
	if ( document.querySelector('[data-search]') ) {

		// Go through all data searches
		document.querySelectorAll('[data-search]').forEach(filter => {

			// Add an event listener to the element
			filter.addEventListener('keyup', () => {
				// Get the target to filter on
				const target = filter.attributes['data-search'].value.toLowerCase();
				const searchValue = filter.value.toLowerCase();

				document.querySelectorAll('input[name="' + target + '"]').forEach(row => {
					const parent = row.parentNode;
					const rowText = parent.innerText.toLowerCase();

					if ( rowText.indexOf(searchValue) > -1 ) {
						parent.style.display = 'block';
					} else {
						parent.style.display = 'none';
					}
				});

			});

		});

	}
</script>
