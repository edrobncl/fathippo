<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Testimonial Content</legend>

            <div class="form__field">
                <label class="form__field__label" for="testimonial_content">Full Testimonial<span class="required">*</span></label>
                <textarea name="testimonial_content" id="testimonial_content" class="richtext_editor" required><?=(isset($testimonial_content))?$testimonial_content:'';?></textarea>
            </div>

        </fieldset>
    </div>
</div>

<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Where to show</legend>

            <?php
                $pageList = json_decode($testimonial_page_list ?? '[]', true);
                $pagesWhitelist = [
                    8, // Fleet Events Page
                    884, // Hire the Fleet Page
                    883 // Wedding Page
                ]
            ?>
            <div class="form__field">
                <label class="form__field__label">Pages</label>
                <div class="tag-input-box-wrap">
                    <input type="text" placeholder="Search..." data-search="testimonial_page_list[]" />

                    <div class="wrap tag-input-boxes">
                        <?php 
                            foreach ( $pagesWhitelist as $page ) { ?>
                                <label class="tag-input-box">
                                    <input type="checkbox" name="testimonial_page_list[]" value="<?= $page ?>"<?=(in_array($page, $pageList))?' checked':'';?>>
                                    <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> <?= showName($page) ?>
                                </label>
                                <?php 
                            } 
                        ?>

                    </div>
                </div>
            </div>

        </fieldset>
    </div>
</div>



<?php single_image('testimonial_image', 'Testimonial Image', '600px by 600px');  ?>
