<?php

if ( !function_exists('beforeSave') ) {
    function beforeSave($post) {
        
        // Store $testimonial_on_pages as a comma separated string
        $post['testimonial_on_pages'] = implode(',', $post['testimonial_on_pages']);
        
        return $post;
    }
}


if ( !function_exists('getPages') ) {
/**
 * Retrieve all Pages
 * 
 * @return array Returns all pages
 */
function getPages(): array {
    global $pdo;

    // Get Pages
    return $pdo->query('SELECT _id, post_title FROM er_posts WHERE post_type IN ("page", "menu") ORDER BY post_type DESC')->fetchAll();
}
}