<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Content</legend>

            <? 
                $locations = $pdo->query("SELECT * FROM er_posts WHERE post_type = 'location' AND post_status = 'live'")->fetchAll();
            ?>

            <div class="form__field">
                <label class="form__field__label" for="job_location">Job Location<span class="required">*</span></label>
                <select class="form__select" name="job_location" id="job_location" required>
                    <option value="">Please Select...</option>
                    <option value="-1"<?=($job_location==-1)?' selected':'';?>>Other</option>
                    <? foreach ($locations as $location) { ?>
                        <option value="<?=$location['_id'];?>"<?=($job_location==$location['_id'])?' selected':'';?>><?=$location['post_alt_title'];?></option>
                    <? } ?>
                    <option value="8"<?=($job_location==8)?' selected':'';?>>Fleet</option>
                </select>
            </div>

            <div class="form__field">
                <label class="form__field__label" for="job_type">Job Type<span class="required">*</span></label>
                <input class="form__field__input" name="job_type" id="job_type" type="text"  value="<?=(isset($job_type))?$job_type:'';?>"  />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="job_salary">Job Salary</label>
                <input class="form__field__input" name="job_salary" id="job_salary" type="text"  value="<?=(isset($job_salary))?$job_salary:'';?>"  />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="job_department">Job Department</label>
                <input class="form__field__input" name="job_department" id="job_department" type="text"  value="<?=(isset($job_department))?$job_department:'';?>"  />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="job_email">Job Contact Email<span class="required">*</span></label>
                <input class="form__field__input" name="job_email" id="job_email" type="text"  value="<?=(isset($job_email))?$job_email:'';?>"  />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="job_summary">Summary</label>
                <textarea name="job_summary" id="job_summary" class="form__field__textarea richtext_editor"><?=(isset($job_summary))?$job_summary:'';?></textarea>
            </div>

            <div class="form__field">
                <label class="form__field__label" for="job_description">Description</label>
                <textarea name="job_description" id="job_description" class="form__field__textarea richtext_editor"><?=(isset($job_description))?$job_description:'';?></textarea>
            </div>  

    </div>
</div>
<? single_image('job_image', 'Job Image', '540px by 340px'); ?>