<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Job',
    'plural' => 'Jobs',
    'form_link' => 'job-form.php',
    'post_type' => 'job',
    'hide_slug_on_index' => true,
    'icon' => 'briefcase',
    'sort_order' => 'post_title'
];

?>
