<style type="text/css">
    .ui-state-highlight { display: block; width: 100%; float: left; height: 5em; line-height: 1.2em; background: rgba(0,0,0,0.3); border: 1px solid #000; margin: .25% 0 0; }
    .moving { box-shadow: 1px 1px 5px rgba(0,0,0,.3); }
</style>
<script src="/admin/js/jquery-ui-sortable.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('.jobs-index').sortable({
            'handle': '.drag-handle',
            'placeholder': 'ui-state-highlight',
            'start': function(event, ui) {
                ui.item.addClass('moving');
            },
            'stop': function(event, ui) {
                ui.item.removeClass('moving');
            },
            'update': function(event, ui) {
                var rowList = [];
                $('.jobs-index').find('.row').each(function() {
                    rowList.push($(this).attr('data-job-id'));
                });
                $.ajax({
                    url: '/admin/scripts/jobs_order.php',
                    method: 'post',
                    data: { jobs: rowList }
                });
            }
        });
    });
</script>
