<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
	require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");
    
    // Include this module's functions file, if it has one.
    if ( file_exists('mod_funcs.php') ) {
        include('mod_funcs.php');
    }
    
    require_once(FS_ADMIN_INCLUDES."inc_header.php");
    
    // Set sort order
    $sort_order = ( isset($module_form_settings['sort_order'])) ? $module_form_settings['sort_order'] : 'menu_order ASC';
    
    // Run query for jobs
	$fetchPosts = $pdo->prepare("
    SELECT *,
        (SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = 'job_location') AS job_location 
    FROM er_posts
    WHERE post_type = ?
    ORDER BY job_location ASC, menu_order ASC");
    $fetchPosts->execute([$module_form_settings['post_type']]);
    

?>

<div class="page-headers">
    <h1 class="page-headers__title"><i class="fa fa-<?=$module_form_settings['icon']?>"></i> <?=$module_form_settings['plural']?></h1>
    <a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a>
</div>
    <div class="dashboard-pad">
    <? if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? } ?>

    <div id="row-list" class="wrap card">
    <?
        while ( $row = $fetchPosts->fetch() ) {
            if ( function_exists('beforeRowOutput') ) { $row = beforeRowOutput($row); } ?>
            <div class="row row-<?=$row['post_status']?>">
                <div class="row__title">
                <a class="row__title__name" href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>"><?=$row['post_title']?></a>
                <br /><div class="row__title__slug" target="_blank">
                    <?php
                        if ( $row['job_location'] > -1 ) { ?>
                            <a href="<?=buildPath($row['job_location']);?>"><?=showAltName($row['job_location']);?></a>
                            <?php
                        } else { ?>
                            Other
                            <?php
                        }
                    ?>
                </div> 
                
            </div>
            
            <div class="row__actions">
                <a href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>&clone=1" class="button button--edit button--clone action"><i class="fa fa-copy"></i> Clone</a>
                <a href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>" class="button button--edit"><i class="fa fa-pencil"></i> Edit</a>
                <a id="inline" href="#pop_delete_<?=$row['_id']?>" class="button button--delete"><i class="fa fa-trash"></i> Delete</a>
            </div>
            <div class="row__status">
                <?
				// Live, Draft
				if($row['post_status'] == "live") { $postIcon = "check"; }
				if($row['post_status'] == "private") { $postIcon = "lock"; }
				if($row['post_status'] == "draft") { $postIcon = "file-text-o"; }
				?>
				<div class="row__status__title"><i class="fa fa-<?=$postIcon?>"></i> <?=$row['post_status']?></div>
            </div>
            <div style="display:none">
                        <div id="pop_delete_<?=$row['_id']?>">
                        	<h3>Are you sure?</h3>
                        	<p>This will delete <strong><?=$row['post_alt_title']?></strong> forever - please be sure before you delete this record.</p>
                        	<form id="delete-post-<?=$row['_id'];?>" method="post" action="/admin/scripts/manage_post.php">
                                <input type="hidden" name="post_id" value="<?=$row['_id'];?>">
                                <input type="hidden" name="return_to" value="<?=$_SERVER['REQUEST_URI'];?>">
                                <input type="hidden" name="post_type" value="<?=$row['post_type'];?>">
                                <button type="button" name="cancel_delete" class="button"><i class="fa fa-arrow-left"></i> No, don't delete it!</button>
                                <button type="submit" name="delete_post" class="button button--delete"><i class="fa fa-trash"></i> Yes! Delete it!</button>
                            </form>
                        </div>
                    </div>
		</div><?
        }
        
        if ($fetchPosts->rowCount() == 0) {
        ?><div class="none-found">There are no <?=strtolower($module_form_settings['plural']);?>. Why not <a href="<?=$module_form_settings['form_link'];?>">add one</a>?</div><?
        }
    ?>
</div>
</div>
<?php require_once(FS_ADMIN_INCLUDES.'inc_footer.php'); ?>
<?php require_once(FS_ADMIN_INCLUDES.'inc_js.php'); ?>