<div class="form__group">
    <fieldset>
        <legend class="form__group__legend">Menu Details</legend>
        <div class="form__field">
            <label class="form__field__label" for="parent_id">Menu Section<span class="required">*</span></label>
            <select class="form__field__input" name="parent_id" id="parent_id" required >
                <option value="">Please select ...</option>
                <?php
                    foreach ( $menuCategories as $category ) { ?>
                        <option value="<?= $category['_id'] ?>" <?= isset($parent_id) && $parent_id == $category['_id'] ? 'selected' : '' ?>><?= $category['post_title'] ?></option>
                        <?php
                    }
                ?>  
            </select>
        </div>

        <div class="form__field">
            <label class="form__field__label" for="drink_details[]">Drink Details</label>
            <div class="tag-input-box-wrap">
                <div class="wrap tag-input-boxes">
                    <?php $drink_details = ! empty($drink_details) ? json_decode($drink_details, true) : []; ?>
                    <label class="tag-input-box">
                        <input type="checkbox" name="drink_details[]" value="1" <?= in_array(1, $drink_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Vegetarian
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="drink_details[]" value="2" <?= in_array(2, $drink_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Vegan
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="drink_details[]" value="3" <?= in_array(3, $drink_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Gluten Free
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="drink_details[]" value="4" <?= in_array(4, $drink_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Dairy Free
                    </label>
                </div>
            </div>
        </div>
        
        <div class="form__field">
            <label class="form__field__label" for="ingredients[]">Ingredients</label>
            <div class="tag-input-box-wrap">
                <input type="text" id="ingredients_search" placeholder="Search..." />
                <div class="wrap ingredients tag-input-boxes">
                    <?php
                        $ingredients = ! empty($ingredients) ? json_decode($ingredients, true) : [];
                        $ingredient_list = $pdo->query('SELECT * FROM er_posts WHERE post_type = "ingredient" ORDER BY post_title ASC')->fetchAll();
                        foreach ( $ingredient_list as $ingredient ) { ?>
                            <label class="tag-input-box">
                                <input type="checkbox" name="ingredients[]" value="<?= $ingredient['_id'] ?>" <?= in_array($ingredient['_id'], $ingredients) ? 'checked' : '' ?> />
                                <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                                <?=$ingredient['post_title'] ?>
                            </label>
                        <?php }
                    ?>
                </div>
            </div>
        </div>

        <div class="form__field">
            <label class="form__field__label">Selected Ingredients</label>
            <div class="tag-input-box-wrap">
                <div class="wrap ingredients tag-input-boxes">
                    <? foreach ($ingredients as $ingredient) { ?>
                        <label class="tag-input-box false">
                            <?=showName($ingredient);?>
                        </label>
                    <? } ?>
                </div>
            </div>
        </div>
    </fieldset>
</div>

<?php
    single_image('drink_image', 'Drink Image', '600px by 600px'); 
?>