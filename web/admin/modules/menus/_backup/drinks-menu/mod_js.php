<script>
    $(function() {
        $(document).on('keyup', '#ingredients_search', function(){
            var searchInput = $(this);
            var searchVal = searchInput.val().toLowerCase();

            if ( $.trim(searchVal).length > 0 ) {
                // Show rows that contain the search value
                $('.ingredients.tag-input-boxes label').each(function() {
                    var thisProduct = $(this).text();console.log(thisProduct);

                    if (thisProduct.toLowerCase().indexOf(searchVal) >= 0) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            } else {
                // If the search box is empty, show all products
                $('.ingredients.tag-input-boxes label').show();
            }
        });
    });
</script>
