<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Menu Item',
    'plural' => 'Menu Items',
    'form_link' => 'drinks-menu-form.php',
    'post_type' => 'drinks_menu_item',
    'hide_slug_on_index' => true,
    'icon' => 'glass',
    'module_group' => '2'
];

?>
