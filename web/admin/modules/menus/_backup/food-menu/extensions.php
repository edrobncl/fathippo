<div class="form__group">
    <fieldset>
        <legend class="form__group__legend">Menu Details</legend>
        <div class="form__field">
            <label class="form__field__label" for="parent_id">Menu Section<span class="required">*</span></label>
            <select class="form__select" name="parent_id" id="parent_id" required>
                <option value="">Please Select...</option>
                <?php
                    foreach ( $menuCategories as $category ) { ?>
                        <option value="<?= $category['_id'] ?>" <?= ! empty($parent_id) && $parent_id === $category['_id'] ? 'selected' : '' ?>><?= $category['post_title'] ?></option>
                        <?php
                    }
                ?>
            </select>
        </div>

        <div class="form__field">
            <label class="form__field__label" for="food_details[]">Food Details</label>
            <div class="tag-input-box-wrap">
                <div class="wrap tag-input-boxes">
                    <?php $food_details = ! empty($food_details) ? json_decode($food_details, true) : []; ?>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="1" <?= in_array(1, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Vegetarian
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="2" <?= in_array(2, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Vegetarian On Request
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="3" <?= in_array(3, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Vegan
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="4" <?= in_array(4, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Vegan On Request
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="5" <?= in_array(5, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Gluten Free
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="6" <?= in_array(6, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Gluten Free On Request
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="7" <?= in_array(7, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Dairy Free
                    </label>
                    <label class="tag-input-box">
                        <input type="checkbox" name="food_details[]" value="8" <?= in_array(8, $food_details) ? 'checked' : '' ?> />
                        <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                        Dairy Free On Request
                    </label>
                </div>
            </div>
        </div>
        
        <div class="option__ingredients">
            <div class="form__field">
                <label class="form__field__label" for="ingredients[]">Ingredients</label>
                <div class="tag-input-box-wrap">
                    <input type="text" id="ingredients_search" placeholder="Search..." />
                    <div class="wrap ingredients tag-input-boxes">
                        <?php
                            $ingredients = ! empty($ingredients) ? json_decode($ingredients, true) : [];
                            $ingredient_list = $pdo->query('SELECT * FROM er_posts WHERE post_type = "ingredient" ORDER BY post_title ASC')->fetchAll();
                            foreach ( $ingredient_list as $ingredient ) { ?>
                                <label class="tag-input-box">
                                    <input type="checkbox" name="ingredients[]" value="<?= $ingredient['_id'] ?>" <?= in_array($ingredient['_id'], $ingredients) ? 'checked' : '' ?> />
                                    <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                                    <?=$ingredient['post_title'] ?>
                                </label>
                            <?php }
                        ?>
                    </div>
                </div>
            </div>

            <div class="form__field">
                <label class="form__field__label">Selected Ingredients</label>
                <div class="tag-input-box-wrap">
                    <div class="wrap ingredients tag-input-boxes">
                        <? foreach ($ingredients as $ingredient) { ?>
                            <label class="tag-input-box false">
                                <?=showName($ingredient);?>
                            </label>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>                 
        
        <div class="vegetarian__ingredients"<? if (empty($veg_ingredients)) { ?> style="display: none"<? } ?>>
            <div class="form__field">
                <label class="form__field__label" for="veg_ingredients[]">Vegetarian Ingredients</label>
                <div class="tag-input-box-wrap">
                    <input type="text" id="veg_ingredients_search" placeholder="Search..." />
                    <div class="wrap veg-ingredients tag-input-boxes">
                        <?php
                            $veg_ingredients = ! empty($veg_ingredients) ? json_decode($veg_ingredients, true) : [];
                            $ingredient_list = $pdo->query('SELECT * FROM er_posts WHERE post_type = "ingredient" ORDER BY post_title ASC')->fetchAll();
                            foreach ( $ingredient_list as $ingredient ) { ?>
                                <label class="tag-input-box">
                                    <input type="checkbox" name="veg_ingredients[]" value="<?= $ingredient['_id'] ?>" <?= in_array($ingredient['_id'], $veg_ingredients) ? 'checked' : '' ?> />
                                    <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                                    <?=$ingredient['post_title'] ?>
                                </label>
                            <?php }
                        ?>
                    </div>
                </div>
            </div>

            <div class="form__field">
                <label class="form__field__label">Selected Vegetarian Ingredients</label>
                <div class="tag-input-box-wrap">
                    <div class="wrap veg-ingredients tag-input-boxes">
                        <? foreach ($veg_ingredients as $ingredient) { ?>
                            <label class="tag-input-box false">
                                <?=showName($ingredient);?>
                            </label>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>                    
        
        <div class="vegan__ingredients"<? if (empty($vegan_ingredients)) { ?> style="display: none"<? } ?>>
            <div class="form__field">
                <label class="form__field__label" for="vegan_ingredients[]">Vegan Ingredients</label>
                <div class="tag-input-box-wrap">
                    <input type="text" id="vegan_ingredients_search" placeholder="Search..." />
                    <div class="wrap vegan-ingredients tag-input-boxes">
                        <?php
                            $vegan_ingredients = ! empty($vegan_ingredients) ? json_decode($vegan_ingredients, true) : [];
                            $ingredient_list = $pdo->query('SELECT * FROM er_posts WHERE post_type = "ingredient" ORDER BY post_title ASC')->fetchAll();
                            foreach ( $ingredient_list as $ingredient ) { ?>
                                <label class="tag-input-box">
                                    <input type="checkbox" name="vegan_ingredients[]" value="<?= $ingredient['_id'] ?>" <?= in_array($ingredient['_id'], $vegan_ingredients) ? 'checked' : '' ?> />
                                    <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                                    <?=$ingredient['post_title'] ?>
                                </label>
                            <?php }
                        ?>
                    </div>
                </div>
            </div>

            <div class="form__field">
                <label class="form__field__label">Selected Vegan Ingredients</label>
                <div class="tag-input-box-wrap">
                    <div class="wrap vegan-ingredients tag-input-boxes">
                        <? foreach ($vegan_ingredients as $ingredient) { ?>
                            <label class="tag-input-box false">
                                <?=showName($ingredient);?>
                            </label>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>                    
        
        <div class="gluten-free__ingredients"<? if (empty($gf_ingredients)) { ?> style="display: none"<? } ?>>
            <div class="form__field">
                <label class="form__field__label" for="gf_ingredients[]">Gluten Free Ingredients</label>
                <div class="tag-input-box-wrap">
                    <input type="text" id="gf_ingredients_search" placeholder="Search..." />
                    <div class="wrap gf-ingredients tag-input-boxes">
                        <?php
                            $gf_ingredients = ! empty($gf_ingredients) ? json_decode($gf_ingredients, true) : [];
                            $ingredient_list = $pdo->query('SELECT * FROM er_posts WHERE post_type = "ingredient" ORDER BY post_title ASC')->fetchAll();
                            foreach ( $ingredient_list as $ingredient ) { ?>
                                <label class="tag-input-box">
                                    <input type="checkbox" name="gf_ingredients[]" value="<?= $ingredient['_id'] ?>" <?= in_array($ingredient['_id'], $gf_ingredients) ? 'checked' : '' ?> />
                                    <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                                    <?=$ingredient['post_title'] ?>
                                </label>
                            <?php }
                        ?>
                    </div>
                </div>
            </div>

            <div class="form__field">
                <label class="form__field__label">Selected Gluten Free Ingredients</label>
                <div class="tag-input-box-wrap">
                    <div class="wrap gf-ingredients tag-input-boxes">
                        <? foreach ($gf_ingredients as $ingredient) { ?>
                            <label class="tag-input-box false">
                                <?=showName($ingredient);?>
                            </label>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="dairy-free__ingredients"<? if (empty($df_ingredients)) { ?> style="display: none"<? } ?>>
            <div class="form__field">
                <label class="form__field__label" for="df_ingredients[]">Dairy Free Ingredients</label>
                <div class="tag-input-box-wrap">
                    <input type="text" id="df_ingredients_search" placeholder="Search..." />
                    <div class="wrap df-ingredients tag-input-boxes">
                        <?php
                            $df_ingredients = ! empty($df_ingredients) ? json_decode($df_ingredients, true) : [];
                            $ingredient_list = $pdo->query('SELECT * FROM er_posts WHERE post_type = "ingredient" ORDER BY post_title ASC')->fetchAll();
                            foreach ( $ingredient_list as $ingredient ) { ?>
                                <label class="tag-input-box">
                                    <input type="checkbox" name="df_ingredients[]" value="<?= $ingredient['_id'] ?>" <?= in_array($ingredient['_id'], $df_ingredients) ? 'checked' : '' ?> />
                                    <i class="fa fa-square-o"></i><i class="fa fa-check-square-o"></i> 
                                    <?=$ingredient['post_title'] ?>
                                </label>
                            <?php }
                        ?>
                    </div>
                </div>
            </div>

            <div class="form__field">
                <label class="form__field__label">Selected Dairy Free Ingredients</label>
                <div class="tag-input-box-wrap">
                    <div class="wrap df-ingredients tag-input-boxes">
                        <? foreach ($df_ingredients as $ingredient) { ?>
                            <label class="tag-input-box false">
                                <?=showName($ingredient);?>
                            </label>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>   

        <div class="form__field">
            <button class="button vegetarian" type="button">Add Vegetarian Option</button>
            <button class="button vegan" type="button">Add Vegan Option</button>
            <button class="button gluten-free" type="button">Add Gluten Free Option</button>
            <button class="button dairy-free" type="button">Add Dairy Free Option</button>
        </div>
    </fieldset>
</div>
<?php
    // single_image('food_image', 'Food Image', '600px by 600px'); 
?>