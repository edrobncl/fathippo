<?php 

if ( function_exists('beforeRowOutput') ) { $row = beforeRowOutput($row); } ?>
<div class="row">
    
    <div class="row__title">
        <a class="row__title__name" href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>"><?=$row['post_title']?></a>
        <br /><div class="row__title__slug">£<?=number_format($row['post_alt_title'],2);?></div>
    </div>
    <div class="row__status">
        <?
            switch ($row['post_status']) {
                case 'live':
                $postIcon = 'check';
                break;
                case 'draft':
                $postIcon = 'file-text-o';
                break;
                case 'private':
                $postIcon = 'lock';
                break;
            }
            ?>
        <div class="status"><i class="fa fa-<?=$postIcon?>"></i> <?=$row['post_status']?></div>
    </div>
    <div class="row__actions">
        <a href="<?=$module_form_settings['form_link']?>?_id=<?=$row['_id']?>" class="button button--edit action"><i class="fa fa-pencil"></i> Edit</a>
        <a id="inline" href="#pop_delete_<?=$row['_id']?>" class="button button--delete action delete"><i class="fa fa-trash"></i> Delete</a>
    </div>
</div>
<div style="display:none">
    <div id="pop_delete_<?=$row['_id']?>">
        <h3>Are you sure?</h3>
        <p>This will delete <strong><?=$row['post_alt_title']?></strong> forever - please be sure before you delete this record.</p>
        <form id="delete-post-<?=$row['_id'];?>" method="post" action="/admin/scripts/manage_post.php">
            <input type="hidden" name="post_id" value="<?=$row['_id'];?>">
            <input type="hidden" name="return_to" value="<?=$_SERVER['REQUEST_URI'];?>">
            <input type="hidden" name="post_type" value="<?=$row['post_type'];?>">
            <button type="button" name="cancel_delete" class="action"><i class="fa fa-arrow-left"></i> No, don't delete it!</button>
            <button type="submit" name="delete_post" class="action delete"><i class="fa fa-trash"></i> Yes! Delete it!</button>
        </form>
    </div>
</div><?