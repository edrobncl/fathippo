<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
	require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");
    
    // Include this module's functions file, if it has one.
    if ( file_exists('mod_funcs.php') ) {
        include('mod_funcs.php');
    }
    
    require_once(FS_ADMIN_INCLUDES."inc_header.php");
    
    // Set sort order
    // $sort_order = ( isset($module_form_settings['sort_order'])) ? $module_form_settings['sort_order'] : 'menu_order ASC';
    
    // Run query for jobs
	// $fetchPosts = $pdo->prepare("SELECT *, (SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = 'menu_section') AS menu_section FROM er_posts WHERE post_type = ? ORDER BY $sort_order");
    
    // Get Categories
    $getCategories = $pdo->prepare('
        SELECT _id, post_title
        FROM er_posts
        WHERE post_type = "menu_category"
            AND parent_id = :menu_id
        ORDER BY menu_order ASC
    ');
    $getCategories->execute([
        'menu_id' => 96 // Food Menu ID
    ]);
    $categoriesList = [];
    
    // Prepare Query
    $getMenuItems = $pdo->prepare('
        SELECT *
        FROM er_posts 
        WHERE parent_id = :category_id
            AND post_type = "food_menu_item"
        ORDER BY menu_order ASC
    ');
    
    // Organise The Categories 
    while ( $category = $getCategories->fetch() ) {
        $categoriesList[$category['_id']] = [
            '_id' => $category['_id'],
            'title' => $category['post_title'],
            'menu_items' => []
        ];

        $getMenuItems->execute([
            'category_id' => $category['_id'] 
        ]);
        while ( $menuItem = $getMenuItems->fetch() ) {
            $categoriesList[$category['_id']]['menu_items'][] = $menuItem;
        }
    }

?>

    <div class="page-headers">
        <h1 class="page-headers__title"><i class="fa fa-<?=$module_form_settings['icon']?>"></i> <?=$module_form_settings['plural']?></h1>
        <a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a>
    </div>
    <div class="dashboard-pad">
        <? if ( isset($_SESSION['success']) ) { ?><div class="alert success"><i class="fa fa-check"></i> <?=$_SESSION['success']; ?></div><? } ?>

        <div id="row-list" class="wrap card">
            <?php
                foreach ( $categoriesList as $category ) { ?>
                    <div class="menu-section"><?= $category['title'] ?></div>
                    <?php
                        foreach ( $category['menu_items'] as $row ) { 
                            include 'index-listing.php';
                        }
                    ?>
                    <?php
                }
            ?>
        </div>
    </div>
</div>

<?php require_once(FS_ADMIN_INCLUDES.'inc_footer.php'); ?>
<?php require_once(FS_ADMIN_INCLUDES.'inc_js.php'); ?>