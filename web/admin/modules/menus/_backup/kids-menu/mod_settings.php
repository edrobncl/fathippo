<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Menu Item',
    'plural' => 'Menu Items',
    'form_link' => 'kids-menu-form.php',
    'post_type' => 'kids_menu_item',
    'hide_slug_on_index' => true,
    'icon' => 'cutlery',
    'module_group' => '2'
];

?>
