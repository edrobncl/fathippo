<script>
    // ==========================================
    //  Index Search Functionality
    // ==========================================
    (function() {
        if ( document.querySelector('#module-index') ) {
            document.querySelector('#search').addEventListener('keyup', () => searchHandler());
            document.querySelectorAll('#menu, #category').forEach(filter => filter.addEventListener('change', () => searchHandler()));

            function searchHandler() {
                // Get Search Values
                const textSearch = document.querySelector('#search').value.toLowerCase();
                const menuSearch = parseInt(document.querySelector('#menu option:checked').value);
                const categorySearch = parseInt(document.querySelector('#category option:checked').value);

                // Loop Through Menu Items
                document.querySelectorAll('#row-list > .row').forEach(row => {
                    let showRow = true;
                    
                    // Check Text Value
                    if ( row.querySelector('.row__title__name').innerText.toLowerCase().indexOf(textSearch) === -1 ) {
                        showRow = false;
                    }
                    
                    // Check Menu
                    if ( ! isNaN(menuSearch) && parseInt(row.attributes['data-menu_id'].value) !== menuSearch ) {
                        showRow = false;
                    }

                    // Check Category
                    if (  ! isNaN(categorySearch) && parseInt(row.attributes['data-category_id'].value) !== categorySearch ) {
                        showRow = false;
                    }
                    
                    // Descide whether to show or hide                
                    if ( showRow ) {
                        row.style.display = 'block';
                    } else {
                        row.style.display = 'none';
                    }
                });

            }
        }

    })();


    // ==========================================
    //  Menu Form Functionality
    // ==========================================
    if ( $('#menu-form').length ) {
        $(function() {
            $(document).on('keyup', '#ingredients_search', function(){
                var searchInput = $(this);
                var searchVal = searchInput.val().toLowerCase();

                if ( $.trim(searchVal).length > 0 ) {
                    // Show rows that contain the search value
                    $('.ingredients.tag-input-boxes label').each(function() {
                        var thisProduct = $(this).text();

                        if (thisProduct.toLowerCase().indexOf(searchVal) >= 0) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                } else {
                    // If the search box is empty, show all products
                    $('.ingredients.tag-input-boxes label').show();
                }
            });

            $(document).on('keyup', '#veg_ingredients_search', function(){
                var searchInput = $(this);
                var searchVal = searchInput.val().toLowerCase();

                if ( $.trim(searchVal).length > 0 ) {
                    // Show rows that contain the search value
                    $('.veg-ingredients.tag-input-boxes label').each(function() {
                        var thisProduct = $(this).text();console.log(thisProduct);

                        if (thisProduct.toLowerCase().indexOf(searchVal) >= 0) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                } else {
                    // If the search box is empty, show all products
                    $('.veg-ingredients.tag-input-boxes label').show();
                }
            });

            $(document).on('keyup', '#vegan_ingredients_search', function(){
                var searchInput = $(this);
                var searchVal = searchInput.val().toLowerCase();

                if ( $.trim(searchVal).length > 0 ) {
                    // Show rows that contain the search value
                    $('.vegan-ingredients.tag-input-boxes label').each(function() {
                        var thisProduct = $(this).text();console.log(thisProduct);

                        if (thisProduct.toLowerCase().indexOf(searchVal) >= 0) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                } else {
                    // If the search box is empty, show all products
                    $('.vegan-ingredients.tag-input-boxes label').show();
                }
            });

            $(document).on('keyup', '#gf_ingredients_search', function(){
                var searchInput = $(this);
                var searchVal = searchInput.val().toLowerCase();

                if ( $.trim(searchVal).length > 0 ) {
                    // Show rows that contain the search value
                    $('.gf-ingredients.tag-input-boxes label').each(function() {
                        var thisProduct = $(this).text();console.log(thisProduct);

                        if (thisProduct.toLowerCase().indexOf(searchVal) >= 0) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                } else {
                    // If the search box is empty, show all products
                    $('.gf-ingredients.tag-input-boxes label').show();
                }
            });

            $(document).on('keyup', '#df_ingredients_search', function(){
                var searchInput = $(this);
                var searchVal = searchInput.val().toLowerCase();

                if ( $.trim(searchVal).length > 0 ) {
                    // Show rows that contain the search value
                    $('.df-ingredients.tag-input-boxes label').each(function() {
                        var thisProduct = $(this).text();console.log(thisProduct);

                        if (thisProduct.toLowerCase().indexOf(searchVal) >= 0) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                } else {
                    // If the search box is empty, show all products
                    $('.df-ingredients.tag-input-boxes label').show();
                }
            });

            $(document).on('click', '.button.dairy-free', function() {
                var df = $('.dairy-free__ingredients');
                if (df.css('display') == 'none') {
                    df.css('display', 'unset');
                    $(this).text('Remove Dairy Free Option');
                } else {
                    df.css('display', 'none');
                    $('.df-ingredients input').prop('checked', false);
                    $(this).text('Add Dairy Free Option');
                }
            });

            $(document).on('click', '.button.gluten-free', function() {
                var gf = $('.gluten-free__ingredients');
                if (gf.css('display') == 'none') {
                    gf.css('display', 'unset');
                    $(this).text('Remove Gluten Free Option');
                } else {
                    gf.css('display', 'none');
                    $('.gf-ingredients input').prop('checked', false);
                    $(this).text('Add Gluten Free Option');
                }
            });

            $(document).on('click', '.button.vegan', function() {
                var v = $('.vegan__ingredients');
                if (v.css('display') == 'none') {
                    v.css('display', 'unset');
                    $(this).text('Remove Vegan Option');
                } else {
                    v.css('display', 'none');
                    $('.vegan-ingredients input').prop('checked', false);
                    $(this).text('Add Vegan Option');
                }
            });

            $(document).on('click', '.button.vegetarian', function() {
                var veg = $('.vegetarian__ingredients');
                if (veg.css('display') == 'none') {
                    veg.css('display', 'unset');
                    $(this).text('Remove Vegetarian Option');
                } else {
                    veg.css('display', 'none');
                    $('.veg-ingredients input').prop('checked', false);
                    $(this).text('Add Vegetarian Option');
                }
            });
        });
    }
</script>
