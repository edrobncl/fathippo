<?php

if ( ! function_exists('beforeSave') ) {
    function beforeSave($post) {
        return $post;
    }
}

if ( ! function_exists('beforeRowOutput') ) {
    function beforeRowOutput($row) {
        global $pdo;

        $getPost = $pdo->prepare('SELECT * FROM er_posts WHERE _id = :post_id');
        
        // Parent ID == Category
        $getPost->execute([
            'post_id' => $row['parent_id']
        ]);
        $category = $getPost->fetch();

        // Category Parent ID == Menu
        $getPost->execute([
            'post_id' => $category['parent_id']
        ]);
        $menu = $getPost->fetch();

        $row['category'] = $category;
        $row['menu'] = $menu;

        return $row;
    }
}