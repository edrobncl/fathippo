<?php
    // https://github.com/SortableJS/Sortable
?>
<script src="http://SortableJS.github.io/Sortable/Sortable.js"></script>

<script>

    (function() {

        // Add a Event Listener to the "Show Category Items" button
        document.querySelectorAll('.show-menu-items').forEach(showButton => {
            // Add Event Listener to every instance of the show menu item button
            showButton.addEventListener('click', () => {
                // Retrieve the items wrapper in the row
                const itemList = showButton.closest('.menu-row').querySelector('.menu-row__items');
                const icon = showButton.querySelector('i');

                // Toggle the Display
                if ( itemList.style.display === 'block' ) {
                    itemList.style.display = 'none';
                    icon.classList.remove('fa-chevron-up');
                    icon.classList.add('fa-chevron-down');
                } else {
                    itemList.style.display = 'block';
                    icon.classList.remove('fa-chevron-down');
                    icon.classList.add('fa-chevron-up');
                }
            });
        });


        // Allow Sorting of the Menu Items
        const sortableOptions = {
            // Change element when moving
            onStart: event => event.item.classList.add('moving'),
            onEnd: event => { 
                event.item.classList.remove('moving');
                
                // Pass through the wrapper so can calculate new ordering
                updateMenuOrdering(event.srcElement); 
            }
        };

        // Apply the sortable functionality
        document.querySelectorAll('.sortable').forEach(sortable => new Sortable.create(sortable, sortableOptions));

        // Calculate new ordering and update database
        function updateMenuOrdering(list) {
            const categoryOrderedList = [];

            // Get the new ordering
            [...list.children].forEach(item => categoryOrderedList.push(item.attributes['data-id'].value));

            // Update the database -- Note: Tried to use POST but wouldn't send the POSTed Variables
            fetch('scripts/updateMenuOrdering.php?list=' + JSON.stringify(categoryOrderedList))
                .then(response => response.json())
                .then(data => console.log('Succeeded', data))
                .catch(error => console.error('An error has ocurred', error));
        }

    })();

</script>