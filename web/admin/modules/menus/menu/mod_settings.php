<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Menu',
    'plural' => 'Menus',
    'form_link' => 'menu-form.php',
    'post_type' => 'menu',
    'hide_slug_on_index' => true,
    'icon' => 'file-o',
    'module_group' => '2'
];

?>
