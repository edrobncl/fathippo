<?php

if ( $_SERVER['REQUEST_METHOD'] !== 'GET' ) {
    echo json_encode([
        'error' => true,
        'message' => 'Invalid post type'
    ]);
    exit();
}

// ==========================================
//  Setup Script
// ==========================================
require_once $_SERVER['DOCUMENT_ROOT'] . '/admin/includes/inc_config.php';

// Check Values Exist
if ( ! isset($_GET['list']) ) {
    echo json_encode([
        'error' => true,
        'message' => 'List variable isnt set'
    ]);
    exit();
}

// Get POSTed Variables
$menuOrder = array_map(function($item) {
    return (int) filter_var($item, FILTER_SANITIZE_NUMBER_INT);
}, json_decode($_GET['list'], true));

// Prepare Query
$updatePost = $pdo->prepare('
    UPDATE er_posts
    SET menu_order = :menu_order
    WHERE _id = :post_id
');

   

// ==========================================
//  Update Database
// ==========================================
foreach ( $menuOrder as $index => $id ) {
    $updatePost->execute([
        'menu_order' => $index + 1,
        'post_id' => $id
    ]);
}


// ==========================================
//  Send Response
// ==========================================
echo json_encode([
    'error' => false,
    'message' => 'Order has been updated'
]);
exit();