<?php

if ( ! function_exists('beforeSave') ) {
    function beforeSave($post) {
        return $post;
    }
}

if ( ! function_exists('beforeRowOutput') ) {
    function beforeRowOutput($row) {
        return $row;
    }
}