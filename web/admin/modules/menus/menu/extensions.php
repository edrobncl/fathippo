<div class="__form__group">
    <div class="form__group">
        <fieldset>
            <legend class="form__group__legend">Content</legend>

            <div class="form__field">
                <label class="form__field__label" for="post_tagline">Post Tagline</label>
                <input type="text" class="form__field__input" name="post_tagline" id="post_tagline" value="<?= $post_tagline ?? '' ?>" />
            </div>

            <div class="form__field">
                <label class="form__field__label" for="page_content">Page Content</label>
                <textarea class="form__field__textarea richtext_editor" name="page_content" id="page_content"><?= $page_content ?? '' ?></textarea>
            </div>

        </fieldset>
    </div>
</div>

<div class="__form_group">
  <div class="form__group">
    <fieldset>
      <legend class="form__group__legend">Hippo Hour</legend>

      <div class="form__field">
        <label class="form__field__label" for="hh_title">Title</label>
        <input class="form__field__input" id="hh_title" name="hh_title" type="text" value="<?=$hh_title ?? '';?>" />
      </div>

      <div class="form__field">
        <label class="form__field__label" for="hh_content">Content</label>
        <textarea class="richtext_editor" id="hh_content" name="hh_content"><?=$hh_content ?? '';?></textarea>
      </div>
    </fieldset>
  </div>
</div>
      
<?php
    single_image('header_image', 'Header Image', '1920px x 800px');
    single_image('thumbnail_image', 'Thumbnail Image', '600px x 466px');
    // multi_image('image_gallery', 'Image Gallery');
    // file_uploader('downloads', 'Downloads');
?>


<?php
    // Load Menu Details for ease of use
    if ( ! empty($post_id) ) { ?>
        <div class="__form__group">
            <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Menu Details</legend>
                    
                    <div class="form__group__note">
                        <i class="fa fa-info-circle"></i> Click and Drag to rearrange the ordering of the categories and category items
                    </div>

                    <div class="__menu-rows sortable">
                        <?php
                            $categories = getMenuCategories($post_id, true);
                            // Handle No Categories
                            if ( count($categories) === 0 ) { ?>
                                <div class="form__row" style="padding: 1rem;">
                                    <i class="fa fa-info-circle"></i> There are no categories in this menu. Why not <a href="/admin/modules/menu-categories/menu-categories-form.php?menu=<?= $post_id ?>">add one</a>?
                                </div>
                                <?php
                            }
                            
                            foreach ( $categories as $category ) { 
                                $menuItems = getCategoryItems($category['_id'], true);
                                ?>
                                <div class="menu-row menu-row--category" data-id="<?= $category['_id'] ?>">
                                    <div class="menu-row__name">
                                        <a href="/admin/modules/menu-categories/menu-categories-form.php?_id=<?= $category['_id'] ?>" target="_blank">
                                            <?= $category['name'] ?> <i class="fa fa-external-link"></i>
                                        </a><br /> 
                                        <div class="post_slug">
                                            <?php
                                                if ( count($menuItems) === 0 ) { ?>
                                                    <i class="fa fa-info-circle"></i> There are no items in this category. Why not <a href="/admin/modules/menus/menu-items/menu-form.php?category=<?= $category['_id'] ?>">add one</a>?
                                                    <?php
                                                } else { ?>
                                                    <span class="show-menu-items">Show Items <i class="fa fa-chevron-down"></i></span>
                                                    <?php
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="menu-row__actions">
                                        <a class="action action--success" href="/admin/modules/menu-categories/menu-categories-form.php?_id=<?= $category['_id'] ?>" target="_blank">Edit</a>
                                        <!-- <a class="action action--error delete-category-item" data-id="<?= $category['_id'] ?>">Delete</a> -->
                                    </div>
                                    <div class="menu-row__status">
                                        <?= ucfirst($category['post_status']) ?>
                                    </div>
                                    <div class="menu-row__items sortable">
                                        <?php
                                            // Show Menu Items
                                            if ( count($menuItems) === 0 ) { ?>
                                                <div class="wrap" style="padding: 1rem;">
                                                    <i class="fa fa-info-circle"></i> There are no items in this category. Why not <a href="/admin/modules/menus/menu-items/menu-form.php?category=<?= $category['_id'] ?>">add one</a>?
                                                </div>
                                                <?php
                                            }
                                            foreach ( $menuItems as $menuItem ) { ?>
                                                <div class="menu-row menu-row--item" data-id="<?= $menuItem['_id'] ?>">
                                                    <div class="menu-row__name">
                                                        <a href="/admin/modules/menus/menu-items/menu-form.php?_id=<?= $menuItem['_id'] ?>" target="_blank">
                                                            <?= $menuItem['post_title'] ?> <i class="fa fa-external-link"></i>
                                                        </a><br /><div class="post_slug">Price: &pound;<?= number_format($menuItem['post_alt_title'], 2) ?></div>
                                                    </div>
                                                    <div class="menu-row__actions">
                                                        <a class="action action--success" href="/admin/modules/menus/menu-items/menu-form.php?_id=<?= $category['_id'] ?>" target="_blank">Edit</a>
                                                        <!-- <a class="action action--error delete-category-item" data-id="<?= $menuItem['_id'] ?>">Delete</a> -->
                                                    </div>
                                                    <div class="menu-row__status">
                                                        <?= ucfirst($category['post_status']) ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                    </div>

                </fieldset>
            </div>
        </div>
        <?php
    }
?>