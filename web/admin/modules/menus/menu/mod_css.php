<style>
    .__menu-rows {
        margin-top: 1em;
        padding: 0 1rem;
    }

    .menu-row {
        padding: .5em;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
    }
    
    .menu-row + .menu-row {
        box-shadow: -0px -5px 3px -5px #ccc;
    }

    .menu_row--category {

    }

    .menu-row--item {
        padding: .4em 0;
    }

    .menu-row__name {
      
    }

    .menu-row__actions {
        margin-left: auto;
        margin-right: 2rem;
    }
    
    .menu-row__actions > * {
        margin-right: 1em;
    }

    .menu-row__status {
        
    }

    .menu-row__items {
        display: none;
        width: 100%;
        flex: 1 1 100%;
        background: #f3f3f3;
        padding: .5rem;
    }

    .show-menu-items {
        cursor: pointer;
    }

    .action {
        padding: .5em 1em;
        border-radius: 4px;
    }
    
    .action--success {
        background: #08cd92;
        color: #fff;
    }
    .action--success:hover {
        background: #12f6b2;
    }

    .action--error {
        background: #e01717;
        color: #fff;
    }
    .action--error:hover {
        background: #ff3030;
    }

    .moving {
        background: rgb(210 241 200);
        border: 1px dashed #333;
        border-radius: 2px;
    }

    .form__group__note {
        padding: .5em;
        border-left: 5px solid #6894f5;
    }

    .form__group__note > i {
        color: #6894f5;
    }

</style>