<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/includes/inc_config.php');
require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");

require_once(FS_ADMIN_INCLUDES.'inc_form_header.php');

if ( in_array($admin_page_type,['add','edit']) ) {

    // Set page title and meta title
    $admin_page_title = ucwords($admin_page_type).' '.$module_form_settings['singular'];
    $admin_meta_title = $admin_page_title . ' : ' . $site_name;

    include(FS_ADMIN_ROOT.'includes/inc_header.php'); 
    include_once __DIR__ . '/mod_css.php';
    ?>

    <div class="page-headers">
    	<h1 class="page-headers__title"><i class="fa fa-<?=($admin_page_type=='edit')?'pencil':'plus';?>"></i> <?=$admin_page_title;?></h1>
        <? if (!empty($post_id)) { ?><a href="<?=$module_form_settings['form_link']?>" class="button"><i class="fa fa-plus"></i> Add <?=$module_form_settings['singular']?></a><? } ?>
    </div>

    <?php
        if ( isset($_SESSION['success']) ) {
            echo '<div class="alert success"><i class="fa fa-check"></i>' . $_SESSION['success'].'</div>';
        }
    ?>

    <form class="validate-form wrap form-with-actions" method="post" action="/admin/scripts/manage_post.php" enctype="multipart/form-data" id="menu-form">

        <input type="hidden" name="module" value="<?= $module_form_settings['module_name'] ?>" />
        <input type="hidden" name="post_id" value="<?= $_id ?? '' ?>" />
        <input type="hidden" name="post_type" value="<?= $module_form_settings['post_type'] ?>" />
        <input type="hidden" name="post_author" value="<?= $_SESSION['eruid'] ?>" />
        <input type="hidden" name="return_to" value="<?= $_SERVER['SCRIPT_NAME'] ?>" />
        <input type="hidden" name="post_slug" value="<?= $post_slug ?? '' ?>" />
        <input type="hidden" name="parent_id" value="7" /> <?php // Menu Page ID  ?>
        
        <div class="__form__group">
            <div class="form__group">
                <fieldset>
                    <legend class="form__group__legend">Base Details</legend>

                    <div class="form__field">
                        <label class="form__field__label" for="post_title">Menu Name <span class="required">*</span></label>
                        <input class="form__field__input" name="post_title" id="post_title" type="text" required value="<?=$post_title ?? '' ?>" placeholder="i.e. Special Menu" />
                    </div>

                    <div class="form__field">
                        <label class="form__field__label" for="post_alt_title">Menu Alt Name <span class="required">*</span></label>
                        <input class="form__field__input" name="post_alt_title" id="post_alt_title" type="text" required value="<?= $post_alt_title ?? '' ?>" placeholder="i.e. Specials" />
                    </div>

                    <div class="form__field">
                      <label class="form__field__label" for="post_slug">Post Slug</label>
                      <input class="form__field__input" name="post_slug" id="post_slug" type="text" required value="<?=$post_slug ?? '';?>" />
                    </div>

                    <?php if (isSuper()) { ?>
                      <div class="form__field">
                        <label class="form__field__label" for="menu_icon">Menu Icon <span class="required">*</span></label>
                        <input class="form__field__input" name="menu_icon" id="menu_icon" type="text" required value="<?=$menu_icon ?? '';?>" />
                      </div>
                    <?php } else { ?>
                      <input type="hidden" name="menu_icon" value="<?php echo $menu_icon; ?>" />
                    <?php } ?>

                </fieldset>
            </div>
            
            <div class="form__group aside">
                <fieldset class="display-options">
                    <legend class="form__group__legend">Display Options</legend>
                    <div class="form__field">
                        <label class="form__field__label" for="post_status">Post Status <span class="required">*</span> <a tabindex="-1" id="inline" href="#pop_post_status" class="admin-help"><i class="fa fa-question-circle"></i> Help?</a></label>
                        <select class="form__select" name="post_status" id="post_status" required>
                            <option value="">Please choose...</option>
                            <option value="draft"<?= isset($post_status) && $post_status=='draft' ? ' selected' : '' ?>>Draft</option>
                            <option value="private"<?= isset($post_status) && $post_status=='private' ? ' selected' : '' ?>>Private</option>
                            <option value="live"<?= isset($post_status) && $post_status=='live' ? ' selected' : '' ?>>Live</option>
                            <option value="unlisted"<?= isset($post_status) && $post_status=='unlisted' ? ' selected' : '' ?>>Unlisted</option>
                        </select>
                    </div>
                    <div style="display:none">
                        <div id="pop_post_status">
                            <h3>Post Status</h3>
                            <p><strong>Draft</strong>: The menu will not show up on the website at all.</p>
                            <p><strong>Private</strong>: The menu will ONLY show up if you are logged into the CMS.</p>
                            <p><strong>Live</strong>: The menu will show up on the website for everyone.</p>
                            <p><strong>Unlisted</strong>: The menu will not show up on the website. You can only see it if you know the exact URL for it.</p>
                        </div>
                    </div>

                    <?php
                        if ( ! empty($post_status) && $post_status === 'unlisted' ) { 
                            $unlistedUrl = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . buildPath($_id);
                            ?>
                            <div class="form__field">
                                <label class="form__field__label">Unlisted URL</label>
                                <div class="faux-field"><a href="<?= $unlistedUrl ?>" target="_blank"><?= $unlistedUrl ?></a></div>
                            </div>
                            <?php
                        }
                    ?>
                    
                    <div class="form__field">
                        <label class="form__field__label" for="menu_order">Order<span class="required">*</span></label>
                        <input class="form__field__input" name="menu_order" id="menu_order" type="number" required value="<?= $menu_order ?? '' ?>" />
                    </div>
                </fieldset>
            </div>

        </div>

        <?php 
            if ( file_exists(__DIR__ . '/extensions.php') ) {
                include_once __DIR__ . '/extensions.php'; 
            }
        ?>

        <?php include(FS_ADMIN_ROOT.'includes/inc_form_actions.php'); ?>

    </form>

    <?php include(FS_ADMIN_ROOT.'includes/inc_footer.php'); ?>
    <?php include(FS_ADMIN_ROOT.'includes/inc_js.php'); ?>
<?
} else {
    // If you're not editing or adding a post, you shouldn't really be here!
    $path = explode('/',$_SERVER['REQUEST_URI']);
    array_pop($path);
    $path = implode('/',$path);
    // Go to the main index of this module.
    header("Location: ".$path.'/');
}
?>