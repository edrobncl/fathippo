<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Allergen',
    'plural' => 'Allergens',
    'form_link' => 'allergens-form.php',
    'post_type' => 'allergen',
    'hide_slug_on_index' => true,
    'icon' => 'cutlery',
    'sort_order' => 'post_title',
    'module_group' => '2'
];

?>
