<?php
$module_settings = [
    'module_name' => basename(dirname(__FILE__)),
    'singular' => 'Ingredient',
    'plural' => 'Ingredients',
    'form_link' => 'ingredients-form.php',
    'post_type' => 'ingredient',
    'hide_slug_on_index' => true,
    'icon' => 'cutlery',
    'sort_order' => 'post_title',
    'module_group' => '2'
];

?>
