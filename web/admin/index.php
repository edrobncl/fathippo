<?php
    require_once("includes/inc_config.php");
    require_once(FS_ADMIN_INCLUDES."inc_checklogin.php");
    require_once(FS_ADMIN_INCLUDES."inc_header.php");
    
    // Show success message, if it's set
    if ( isset($_SESSION['Success']) ) {
        echo '<div class="success">'.$_SESSION['Success'].'</div>';
    }
    
    // Determine morning, afternoon, evening or night for greeting
    $greetingDate = date('H');
    $greeting = ( $greetingDate < 12 ) ? 'morning' : ( ($greetingDate >= 12 && $greetingDate < 17) ? 'afternoon' : ( ($greetingDate >= 17 && $greetingDate < 19) ? 'evening': ( ($greetingDate >= 19) ? 'night' : '' ) ) );
	
	// Greetings array
	$greetingMessages = [
	    "How are you going to update the website today?",
	    "It only takes a minute to keep your website up to date.",
	    "A fresh website is a google-friendly website."
    ];
	
	// Determine day or night for header background
	if($greetingDate < 17 && $greetingDate > 06) { $period = "day"; } else { $period = "night"; }
	
	// Create homepage cards array
    $homepage_cards = homeCards();

    // Warn ER that client has admin/database (so database needs to be brought down...)
    $site_settings = $pdo->query("SELECT * FROM er_config")->fetch();
    if ( $site_settings ) {
        $database_live_date = $site_settings['database_live_date'];
    }
?>
    
    <div class="dashboard-box pg-<?=$period;?>">
    <div class="dashboard-pad">
        <div class="wrap welcome">
            <?php if($admin_logo) { ?><div class="welcome__logo" style="background-image:url(<?=$admin_logo?>)"></div><? } ?>
            <h1 class="welcome__title">Good <?=$greeting;?>!</h1>
            <?php 
            // Warning for ER users only
            if($_SESSION['userlevel'] == "superuser" && !empty($database_live_date)) { ?><div class="welcome__text" style="font-style:italic;color:#e74035;">Client has admin access: <?=$database_live_date?></div><?php } else { ?>
            <div class="welcome__text"><?=$greetingMessages[array_rand($greetingMessages)]?></div>
            <?php } ?>
        </div>
        
        <?
            // Work out what should show up in the "lastUpdated" list (based on permissions)
            $initialExecuteVals = ['image','file'];
            $allowedTypes = array_keys($modules_by_type);
            $executeVals = array_merge($initialExecuteVals,$allowedTypes);
            $luqIn = str_repeat('?, ', count($allowedTypes) - 1) . '?';
        ?>
        
        <?
		$LastUpdatedQuery = "SELECT * FROM er_posts WHERE post_type NOT IN (?, ?) AND post_type IN (".$luqIn.")ORDER BY date_modified DESC LIMIT 10";
		$fetchNav = $pdo->prepare($LastUpdatedQuery);
		$fetchNav->execute($executeVals);
		if ( $fetchNav ) {
			?><div class="wrap last-updated">
                <h3 class="last-updated__title">Recent items</h3>
				<div id="row-list" class="wrap"><?
        			while ( $LastUpdate = $fetchNav->fetch() ) {
                        if ( function_exists('beforeLastUpdate') ) { $LastUpdate = beforeLastUpdate($LastUpdate, $modules_by_type[$LastUpdate['post_type']]); }
    					?><div class="row">
    					<div class="row__title">
                                <div class="row__title__type"><?=str_replace('_', ' ',$LastUpdate['post_type']);?></div>
                                <a class="row__title__name" href="<?=$module_folder_links[$LastUpdate['post_type']];?>?_id=<?=$LastUpdate['_id']?>"><?=$LastUpdate['post_title']?></a><em class="row__title__lastupdated">Last Updated: <?=date('g:ia \o\n l jS F Y', strtotime($LastUpdate['date_modified'])) ?></em>
                            </div>
                            <div class="row__actions">
                                <a href="<?=$module_folder_links[$LastUpdate['post_type']];?>?_id=<?=$LastUpdate['_id']?>" class="button"><i class="fa fa-pencil"></i> Edit</a>
                                <a class="button button--outlined" href="<?=($LastUpdate['_id']!=1)?buildPath($LastUpdate['_id']):"/";?>" target="_blank"><i class="fa fa-eye"></i> View</a>
                            </div>
                            <div class="row__status">
                            <?
                            // Live, Draft
                            if($LastUpdate['post_status'] == "live") { $postIcon = "check"; }
                            if($LastUpdate['post_status'] == "private") { $postIcon = "lock"; }
                            if($LastUpdate['post_status'] == "draft") { $postIcon = "file-text-o"; }
                            ?>
                            <div class="row__status__title"><i class="fa fa-<?=$postIcon?>"></i> <?=$LastUpdate['post_status']?></div>
                        </div>
    					</div><?
        			}
        			?>
                </div>
            </div><?
		}
		?>
		
        

    </div>
    </div>
    

<?php
    unset($_SESSION['Success']);
    require_once(FS_ADMIN_INCLUDES.'inc_footer.php');
    require_once(FS_ADMIN_INCLUDES.'inc_js.php');
?>