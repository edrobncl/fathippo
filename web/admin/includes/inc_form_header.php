<?php
// Include module's functions
if ( file_exists('mod_funcs.php') ) {
    require_once('mod_funcs.php');
}

// trap post id
$post_id = (isset($_GET['_id'])) ? XSSTrapper($_GET['_id']) : null;

// check if add, edit or script
$admin_page_type = getPageType($post_id);

// Set the table name based on module settings, or default to 'er_posts'
$table = (isset($module_form_settings['table_name'])) ? $module_form_settings['table_name'] : 'er_posts';

switch ($admin_page_type) {
    case 'edit':
        // Build standard post variables
        $getPost = $pdo->prepare("SELECT * FROM $table WHERE _id = :id LIMIT 1");
        $getPost->execute(['id'=>$post_id]);
        $post = $getPost->fetch();
        extract($post);
        
        if ( $table == 'er_posts' ) {
            // Build any postmeta variables
            $getPostMeta = $pdo->prepare("SELECT * FROM er_postmeta WHERE post_id = :id");
            $getPostMeta->execute(['id'=>$post_id]);
            $postMeta = $getPostMeta->fetchAll();
            foreach ( $postMeta as $field ) {
                $meta_name = $field['meta_name'];
                $$meta_name = $field['meta_value'];
            }
        }
        
        break;
        
    case 'add':
        // Populate defaults
        $getDefaults = $pdo->query("SHOW COLUMNS FROM $table");
        while ( $row = $getDefaults->fetch() ) {
            if ( is_null($row['Default']) ) {
                $$row['Field'] = '';
            } else {
                $$row['Field'] = $row['Default'];
            }
        }
        break;
        
    case 'script':
        break;
}
?>