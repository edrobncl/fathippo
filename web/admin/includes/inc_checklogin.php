<?php
if ( !isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true || !in_array($_SESSION['userlevel'],['superuser','admin']) ) {
	header('Location: /admin/login');
	exit();
}