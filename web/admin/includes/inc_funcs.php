<?php
// Prevent XSS
if ( !function_exists('XSSTrapper') ) {
    function XSSTrapper($TheString) {
        if (is_null($TheString) || trim($TheString) == "") {
            $XSSTrapper = $TheString;
        } else {
            $XSSTrapper = trim($TheString);
            $XSSTrapper = str_replace(
                array("<",">","(",")","'","\""),
                array("&lt;","&gt;","&#x28;","&#x29;","&apos;","&#x22;"),
                $XSSTrapper
            );
        }
        return $XSSTrapper;
    }
}

// Check if current user is a Super User
if ( !function_exists('isSuper') ) {
    function isSuper() {
        global $pdo;
        
        if ( isset($_SESSION['eruid']) && (int)$_SESSION['eruid'] > 0 ) {
            $id = $_SESSION['eruid'];
            
            $superCheck = $pdo->prepare("SELECT level FROM er_users WHERE _id = ? LIMIT 1");
            $superCheck->execute([$id]);
            $is_super = $superCheck->fetch();
            
            if ( $is_super['level'] == 'superuser' ) {
                return true;
            }
        }
        
        return false;
    }
}

// Check if current user is an Admin
if ( !function_exists('isAdmin') ) {
    function isAdmin() {
        global $pdo;
        
        if ( isset($_SESSION['eruid']) && (int)$_SESSION['eruid'] > 0 ) {
            $id = $_SESSION['eruid'];
            
            $adminCheck = $pdo->prepare("SELECT level FROM er_users WHERE _id = ? LIMIT 1");
            $adminCheck->execute([$id]);
            $is_admin = $adminCheck->fetch();
            
            if ( $is_admin['level'] == 'admin' ) {
                return true;
            }
        }
        
        return false;
    }
}

// Get admin page type
if ( !function_exists('getPageType') ) {
    function getPageType($var) {
        if (isset($var)) {
            // has a QUERYSTRING, must be EDIT
            return "edit";
        } else {
            // no QUERYSTRING, but is it ADD or SCRIPT? let's check for a POST value
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // sent via POST, must be SCRIPT
                return "script";
            }else{
                // no POST, no QUERYSTRING, must be ADD
                return "add";
            }
        }
    }
}

function showName($post_id) {
    global $pdo;

    // Get the
    $getPost = $pdo->prepare("SELECT post_title FROM er_posts WHERE _id = ? LIMIT 1");
    $getPost->execute([$post_id]);
    
    $post = $getPost->fetch();
    return $post['post_title'];
}

function showAltName($post_id) {
    global $pdo;

    // Get the
    $getPost = $pdo->prepare("SELECT post_alt_title FROM er_posts WHERE _id = ? LIMIT 1");
    $getPost->execute([$post_id]);
    
    $post = $getPost->fetch();
    return $post['post_alt_title'];
}

function showParentID($post_id) {
    global $pdo;

    // Get the
    $getPost = $pdo->prepare("SELECT parent_id FROM er_posts WHERE _id = ? LIMIT 1");
    $getPost->execute([$post_id]);
    
    $post = $getPost->fetch();
    return $post['parent_id'];
}

// Make sure variable is good after XSSTrapper
if ( !function_exists('trapCheck') ) {
    function trapCheck($input) {
        $output = false;
        
        if ( $input != '' && !is_null($input) ) {
            $output = true;
        }
        
        return $output;
    }
}

// Build breadcrumb <ul>
if ( !function_exists('buildBreadcrumb') ) {
    function buildBreadcrumb($thearray) {
        global $NormPath;
        echo '<ul id="breadcrumb">';
        foreach ($thearray as $value) {
            echo '<li><a href="'.$value[0].'">'.$value[1].'</a></li>';
        }
        echo '</ul>';
    }
}

// Clear login items of session
if ( !function_exists('clearLogin') ) {
    function clearLogin() {
        unset($_SESSION['loggedin']);
        unset($_SESSION['userlevel']);
        unset($_SESSION['eruid']);
        unset($_SESSION['permissions']);
    }
}

// Show the staff members who worked on the site in help list
if ( !function_exists('showHelp') ) {
    function showHelp($director, $staff = []) {
        $listToShow = [];
        
        // List of staff
        $staffList = [
            'sarah' => ['Sarah Collinson', '3'],
            'greg' => ['Greg Duckett', '4'],
            'michael' => ['Michael Price', '5'],
            'paul' => ['Paul Tissington', '13'],
            'dave' => ['David Aiken', '7'],
            'david' => ['David Zborowski', '14'],
            'katy' => ['Katy', '10'],
            'allen' => ['Allen Brindle', '11'],
            'kevan' => ['Kevan Bulmer', '12'],
            'james' => ['James Weatheritt', '16'],
            'allie' => ['Allie Parker', '15'],
            'hoda' => ['Hoda Shirzad', '17'],
            'rob' => ['Rob Sykes', '20'],
            'chris' => ['Chris Prusakiewicz', '21'],
            'ben' => ['Ben Sinca', '19'],
            'luke' => ['Luke Harvey', '22']
        ];
        
        // Set the director first
        switch ($director) {
            case 'graham':
                $listToShow[$director] = ['Graham Miller', '1'];
                break;
            case 'simon':
                $listToShow[$director] = ['Simon Crisp', '2'];
                break;
        }
        
        // Then get the staff who worked on the site
        foreach ( $staff as $name ) {
            $listToShow[$name] = $staffList[$name];
        }
        
        // Echo out the help contact list
        ?><aside id="aside">
            <h5>Need a helping hand?</h5>
            <p>If you need any help or want to make changes to your website, please contact any of the following:</p>
            <? foreach ( $listToShow as $name => $info ) { ?>
                <div class="staff">
                    <img src="/admin/images/staff/<?=$info[1]?>.jpg" alt="<?=$info[0];?>" />
                    <div class="name"><?=$info[0];?></div>
                    <a href="mailto:<?=$name;?>@edwardrobertson.co.uk" class="email"><i class="fa fa-envelope"></i> Email</a>
                </div>
            <? }
        ?></aside><?
    }
}

// Delete from array by value, rather than key
function deleteByValue($value, $array) {
    if ( ($key = array_search($value, $array)) !== false) {
        unset($array[$key]);
        return $array;
    }
}

// Create an array of all modules this user has access to, and each module's settings
function moduleSettings() {
    $permissions = ( isset($_SESSION['permissions']) && (isSuper() || isAdmin()) ) ? $_SESSION['permissions'] : [];
    $mod_settings = [];
    
    // Loop through permissions and create an array of modules with their settings
    foreach ( $permissions as $key => $module ) {
        // Check for core modules
        if ( file_exists(FS_ADMIN_ROOT.'modules/core/'.$module.'/mod_settings.php') ) {
            include_once(FS_ADMIN_ROOT.'modules/core/'.$module.'/mod_settings.php');
            $mod_settings[$module] = $module_settings;
            unset($module_settings);
        }
        // Check for additional modules
        if ( file_exists(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php') ) {
            include_once(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php');
            $mod_settings[$module] = $module_settings;
            unset($module_settings);
        }
    }
    
    return $mod_settings;
}

// Updated nav menu build. Don't include all the module stuff.
function mainMenu($mod_settings = [], $core_modules, $additional_modules) {
    $permissions = ( isset($_SESSION['permissions']) && (isSuper() || isAdmin()) ) ? $_SESSION['permissions'] : [];
    $main_menu = '';
    $core_primary = [];
    $core_secondary = [];
    $additional = [];
    
    foreach ( $core_modules as $key => $module ) {
        
        // Go through core modules and add them to the menu.
        if ( substr($module,0,1) != '.' && is_dir(FS_ADMIN_ROOT.'modules/core/'.$module) && in_array($module, $permissions) ) {
            if ( isset($mod_settings[$module]['main_menu']) ) {
                $menu_option = 'core_' . $mod_settings[$module]['main_menu']['option'];

                $menu_item = [
                    'menu_order' => $mod_settings[$module]['main_menu']['order'],
                    'code' => '<li class="core'.( ( $module == basename(getcwd()) ) ? ' active' : '' ).'"><a href="/admin/modules/core/'.$module.'/"><i class="fa fa-'.$mod_settings[$module]['icon'].'"></i> '.ucwords(str_replace('-',' ',$module)).'</a></li>'
                ];

                if ( $menu_option == 'core_primary' ) {
                    $core_primary[$module] = $menu_item;
                } else if ( $menu_option == 'core_secondary' ) {
                    $core_secondary[$module] = $menu_item;
                }
                
                $permissions = deleteByValue($module, $permissions);
                
                unset($menu_option);
                unset($menu_item);
            }
        }
    }
    
    // If there are still permissions left, loop through the additional modules
    if ( count($permissions) > 0 ) {
        foreach ( $additional_modules as $key => $module ) {
            // Go through core modules and add them to the menu.
            if ( substr($module,0,1) != '.' && $module != 'core' && is_dir(FS_ADMIN_ROOT.'modules/'.$module) && in_array($module, $permissions) ) {
                $additional[$module] = [
                    'code' => '<li class="'.( ( $module == basename(getcwd()) ) ? ' active' : '' ).'"><a href="/admin/modules/'.$module.'/"><i class="fa fa-'.$mod_settings[$module]['icon'].'"></i> '.ucwords(str_replace('-',' ',$module)).'</a></li>'
                ];
            }
        }
    }
    
    // Sort the core_primary menu section
    usort($core_primary, function($a, $b) {
        return $a['menu_order'] - $b['menu_order'];
    });
    
    // Sort the core_secondary menu section
    usort($core_secondary, function($a, $b) {
        return $a['menu_order'] - $b['menu_order'];
    });
    
    // Sort the additional modules menu section
    ksort($additional);
    
    // Go through each module list and build the nav
    foreach ( $core_primary as $module ) {
        $main_menu .= $module['code'];
    }
    
    foreach ( $additional as $module ) {
        $main_menu .= $module['code'];
    }
    
    foreach ( $core_secondary as $module ) {
        $main_menu .= $module['code'];
    }
    
    return $main_menu;
}

// Build home cards array
function homeCards() {
    $permissions = ( isset($_SESSION['permissions']) && (isSuper() || isAdmin()) ) ? $_SESSION['permissions'] : [];
    $homepage_cards = [];
    
    // Loop through permissions and create an array of home cards
    foreach ( $permissions as $module ) {
        
        
        // Check for core modules
        if ( file_exists(FS_ADMIN_ROOT.'modules/core/'.$module.'/mod_settings.php') ) {
            include(FS_ADMIN_ROOT.'modules/core/'.$module.'/mod_settings.php');
            if ( isset($homepage_card) ) {
                $homepage_cards[$module] = $homepage_card;
            }
        }
        // Check for additional modules
        if ( file_exists(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php') ) {
            include(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php');
            if ( isset($homepage_card) ) {
                $homepage_cards[$module] = $homepage_card;
            }
        }
        unset($homepage_card);
    }
    
    return $homepage_cards;
}

// Build main menu and populate the $module_form_settings variable if currently in a moduleif ( !function_exists('buildNav') ) {
    function buildNav() {
        global $mainMenu;
        global $module_form_settings;
        global $module_folder_links;
        global $module_homepage_cards;
        global $modules_by_type;
        
        //$mainMenu = '<li class="toplevel'.((basename(getcwd()) == 'admin')?' active':'').'"><a href="/admin/"><i class="fa fa-dashboard"></i> Dashboard</a></li>';
    
        $core_list = scandir(FS_ADMIN_ROOT.'modules/core');
        foreach ( $core_list as $module ) {
            if ( substr($module,0,1) != '.' ) {
                if ( ($module == 'users' && isset($_SESSION['permissions']) &&  in_array('users', $_SESSION['permissions'])) || $module != 'users' ) {
                    if ( file_exists(FS_ADMIN_ROOT.'modules/core/'.$module.'/mod_settings.php') ) {
                        include(FS_ADMIN_ROOT.'modules/core/'.$module.'/mod_settings.php');
                        
                        // If currently in this module, store settings
                        if ( basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'] ) {
                            $module_form_settings = $module_settings;
                        }
                        
                        if ( $module != 'settings' ) {
                            
                            // Add module name to array with post type as the key
                            if ( isset($module_settings['post_type']) ) {
                                $modules_by_type[$module_settings['post_type']] = $module_settings['module_name'];
                            }
                            
                            // Add link to module form to array
                            if ( isset($module_settings['post_type']) && !in_array($module_settings['post_type'], $module_folder_links) ) {
                                $module_folder_links[$module_settings['post_type']] = '/admin/modules/core/'.$module.'/'.$module_settings['form_link'];
                            }
                            
                            // Add module to menu
                            $mainMenu .= '<li class="nav__item core'.( (basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'])?' active':'').'"><a class="nav__link" href="/admin/modules/core/'.$module.'/"><i class="fa fa-'.$module_settings['icon'].'"></i> '.ucwords(str_replace('-',' ',$module_settings['module_name'])).'</a></li>';
                        }
                    }
                }
            }
            
            unset($module_settings);
        }
        
        // Hack to put Orders menu item into top section of the main nav
        if ( isset($_SESSION['permissions']) && in_array('orders',$_SESSION['permissions']) ) {
            
            $module = 'orders';
            
            if ( file_exists(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php') ) {
                include(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php');
            }
            
            $mainMenu .= '<li class="nav__item orders-menu-item '.( (basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'])?' active':'').'"><a class="nav__link" href="/admin/modules/'.$module.'/"><i class="fa fa-'.$module_settings['icon'].'"></i> '.ucwords(str_replace('-',' ',$module_settings['module_name'])).'</a></li>';
            
            if ( basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'] ) {
                $module_form_settings = $module_settings;
            }
            
            if ( isset($module_settings['post_type']) && !in_array($module_settings['post_type'], $module_folder_links) ) {
                $module_folder_links[$module_settings['post_type']] = '/admin/modules/'.$module.'/'.$module_settings['form_link'];
            }
            
            if ( isset($module_settings['post_type']) && !in_array($module_settings['post_type'], $module_homepage_cards) && isset($homepage_card) ) {
                $module_homepage_cards[$module_settings['post_type']] = $homepage_card;
                unset($homepage_card);
            }
            
            if ( isset($module_settings['post_type']) ) {
                // Add module name to array with post type as the key
                $modules_by_type[$module_settings['post_type']] = $module_settings['module_name'];
            }
            
            unset($module_settings);
        }
        
        if ( isset($_SESSION['permissions']) ) {
            foreach ( $_SESSION['permissions'] as $module ) {
                
                if ( $module != 'orders' ) {
                    if ( file_exists(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php') ) {
                        include(FS_ADMIN_ROOT.'modules/'.$module.'/mod_settings.php');
                        
                        $mainMenu .= '<li class="nav__item '.( (basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'])?' active':'').'"><a class="nav__link" href="/admin/modules/'.$module.'/"><i class="fa fa-'.$module_settings['icon'].'"></i> '.ucwords(str_replace('-',' ',$module_settings['module_name'])).'</a></li>';
                        
                        // If currently in this module, store settings
                        if ( basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'] ) {
                            $module_form_settings = $module_settings;
                        }
                        
                        if ( isset($module_settings['post_type']) && !in_array($module_settings['post_type'], $module_folder_links) ) {
                            $module_folder_links[$module_settings['post_type']] = '/admin/modules/'.$module.'/'.$module_settings['form_link'];
                        }
                        
                        if ( isset($module_settings['post_type']) && !in_array($module_settings['post_type'], $module_homepage_cards) && isset($homepage_card) ) {
                            $module_homepage_cards[$module_settings['post_type']] = $homepage_card;
                            unset($homepage_card);
                        }
                        
                        if ( isset($module_settings['post_type']) ) {
                            // Add module name to array with post type as the key
                            $modules_by_type[$module_settings['post_type']] = $module_settings['module_name'];
                        }
                        
                    }
                }
                
                unset($module_settings);
                
            }        
            foreach ( $_SESSION['permissions'] as $module ) {
                
                if ( $module != 'orders' ) {
                    if ( file_exists(FS_ADMIN_ROOT.'modules/menus/'.$module.'/mod_settings.php') ) {
                        include(FS_ADMIN_ROOT.'modules/menus/'.$module.'/mod_settings.php');
                        
                        $mainMenu .= '<li class="nav__item '.( (basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'])?' active':'').'"><a class="nav__link" href="/admin/modules/menus/'.$module.'/"><i class="fa fa-'.$module_settings['icon'].'"></i> '.ucwords(str_replace('-',' ',$module_settings['module_name'])).'</a></li>';
                        
                        // If currently in this module, store settings
                        if ( basename(getcwd()) == $module_settings['module_name'] || basename(dirname(__FILE__)) == $module_settings['module_name'] ) {
                            $module_form_settings = $module_settings;
                        }
                        
                        if ( isset($module_settings['post_type']) && !in_array($module_settings['post_type'], $module_folder_links) ) {
                            $module_folder_links[$module_settings['post_type']] = '/admin/modules/menus/'.$module.'/'.$module_settings['form_link'];
                        }
                        
                        if ( isset($module_settings['post_type']) && !in_array($module_settings['post_type'], $module_homepage_cards) && isset($homepage_card) ) {
                            $module_homepage_cards[$module_settings['post_type']] = $homepage_card;
                            unset($homepage_card);
                        }
                        
                        if ( isset($module_settings['post_type']) ) {
                            // Add module name to array with post type as the key
                            $modules_by_type[$module_settings['post_type']] = $module_settings['module_name'];
                        }
                        
                    }
                }
                
                unset($module_settings);
                
            }  
        }
        
        
        return $mainMenu;
    }


// Build the url to the page of the given ID
if ( !function_exists('buildPath') ) {
    function buildPath($post_id, $count = 0) {
        global $pdo;
        global $path;
            
        if ( $count == 0 ) {
            $path = [];
        }
        
        // Get the
        $getPost = $pdo->prepare("SELECT post_slug, parent_id FROM er_posts WHERE _id = ? LIMIT 1");
        $getPost->execute([$post_id]);
        
        while ( $post = $getPost->fetch() ) {
            // Add page slug to path array
            $path[] = $post['post_slug'];
            
            // Loop this function until parent_id reaches 1, then build the path
            if ( $post['parent_id'] > 1 ) {
                $count++;
                buildPath($post['parent_id'], $count);
            } else {
                $path = "/" . implode("/",array_reverse($path)) . "/";
            }   
        }
        return $path;
    }
}

// Create <option> list of pages for <select>
if ( !function_exists('page_options') ) {
    function page_options($parent_id,$indent,$value,$post_id,$type = 'page') {
        global $pdo;
        
        $getPages = $pdo->prepare("SELECT * FROM er_posts WHERE parent_id = :parent_id  AND post_type = :post_type ORDER BY menu_order ASC, post_title ASC, _id ASC");
        $getPages->execute(['parent_id'=>$parent_id, 'post_type'=>$type]);
        $pages = $getPages->fetchAll();		
        
        foreach ($pages as $row) {
            if ( $row['_id'] != 2 ) {
                ?><option<?=(strval($value)==strval($row['_id']))?' selected ':' ';?>value="<?=$row['_id'];?>" <? if($row['_id'] == $post_id) { echo " disabled"; } ?>><?=str_repeat("- ",$indent).$row['post_title'];?></option><?
            }
            page_options($row['_id'], $indent+1, $value, $post_id, $type);
        }
    }
}

if ( !function_exists('post_options') ) {
    function post_options($parent_id,$indent,$value) {
        global $pdo;
        
        $getPages = $pdo->prepare("SELECT * FROM er_posts WHERE parent_id = :parent_id  AND (post_type <> 'file' AND post_type <> 'image' AND post_type <> 'advertisement') AND post_status = 'live' ORDER BY menu_order ASC, post_title ASC, _id ASC");
        $getPages->execute(['parent_id'=>$parent_id]);
        $pages = $getPages->fetchAll();		
        
        foreach ($pages as $row) {
            if ( $row['_id'] != 2 ) {
                ?><option<?=(strval($value)==strval($row['_id']))?' selected ':' ';?>value="<?=$row['_id'];?>"><?=str_repeat("- ",$indent).$row['post_title'];?></option><?
            }
            post_options($row['_id'], $indent+1, $value);
        }
    }
}

// Create list of checkboxes
if ( !function_exists('page_checkboxes') ) {
    function page_checkboxes($parent_id,$indent,$variable) {
        global $pdo;
        global $page_checkboxes_counter;
        global $$variable;
        
        $counter = $variable . '_counter';
        global $$counter;
        
        if ( isset($$variable) ) {
            $checked = explode(',',$$variable);
        } else {
            $checked = [];
        }
        
        $getPages = $pdo->prepare("SELECT * FROM er_posts WHERE parent_id = :parent_id AND post_type = :post_type ORDER BY menu_order ASC, _id ASC");
        $getPages->execute(['parent_id'=>$parent_id, 'post_type'=>'page']);
        $pages = $getPages->fetchAll();		
        
        if ( $pages && $indent == 0 ) {
            ?><div class="wrap checkbox-list"><?
        }
        
        foreach ($pages as $row) {
            if ( $row['_id'] != 2 ) {
                ?><label for="<?=$variable;?>_<?=$$counter + 1;?>"><input type="checkbox" name="<?=$variable;?>[]" id="<?=$variable;?>_<?=$$counter + 1;?>" <?=(in_array($row['_id'],$checked))?' checked':'';?> value="<?=$row['_id'];?>"><?=str_repeat("- ",$indent).$row['post_title'];?></label><?
                $$counter++;
            }
            page_checkboxes($row['_id'], $indent+1, $variable);
        }
        
        if ( $pages && $indent == 0 ) {
            ?></div><?
        }
        
    }
}


// create list of products

if ( !function_exists('product_checkboxes') ) {
    function product_checkboxes($parent_id,$indent,$variable,$post_id,$sortLive = false) {
        global $pdo;
        global $page_checkboxes_counter;
        global $$variable;

        // echo '<pre>';
        // var_dump(str_replace('"','',str_replace('[','',str_replace(']','',$products_in_group))));
        // echo '</pre>';

        $counter = $variable . '_counter';
        global $$counter;
        
        if ( isset($$variable) ) {
            $checked = explode(',',$$variable);
        } else {
            $checked = [];
        }

        // echo '<pre>';
        //  print_r($checked);
        // echo '</pre>';

        if ( $sortLive ) {
            $extraSql = "ORDER BY post_status = 'hidden', post_status = 'draft', post_status = 'live', post_title ASC";
        } else {
            $extraSql = "";
        }

        $getProducts = $pdo->prepare("SELECT * FROM er_posts WHERE parent_id = :parent_id AND post_type = :post_type $extraSql");
        $getProducts->execute(['parent_id'=>$parent_id, 'post_type'=>'product']);
        $products = $getProducts->fetchAll();
        
        if ( $products && $indent == 0 ) {
            ?><div class="wrap checkbox-list"><?
        }

        
        foreach ($products as $row) {

            $icon = $row['post_status']=='live' ? 'check' : ( $row['post_status'] == 'hidden' ? 'eye' : 'times' );


            // MAY NEED TO DELETE 
            $temp = preg_replace("/[^a-zA-Z 0-9]+/", "", $checked );

            ?><label for="<?=$variable;?>_<?=$$counter + 1;?>"><input type="checkbox" data-row-number="<?=$$counter + 1;?>" data-id="<?=$row['_id']?>" data-product-name="<?=$row['post_title']?>" name="<?=$variable;?>[]" id="<?=$variable;?>_<?=$$counter + 1;?>" data-post-id="<?=$post_id?>" <?=(in_array($row['_id'],$temp))?' checked':'';?> value="<?=$row['_id'];?>"><?=str_repeat("- ",$indent).$row['post_title'];?><span class="mark_as_<?=$row['post_status'];?>"><?=ucwords($row['post_status']);?></span></label><?
            $$counter++;
            page_checkboxes($row['_id'], $indent+1, $variable);

        }
        
        if ( $products && $indent == 0 ) {
            ?></div><?
        }
        
    }
}


if ( !function_exists('user_list') ) {
    function user_list() {
        
        global $pdo;
        global $post_title;
        
        $get_users = $pdo->prepare("SELECT _id, username, level, created_at, (SELECT meta_value FROM er_usermeta WHERE user_id=er_users._id AND meta_name = 'first_name') AS first_name,(SELECT meta_value FROM er_usermeta WHERE user_id=er_users._id AND meta_name = 'surname') AS surname FROM er_users WHERE level = ? ORDER BY first_name ASC");
        $get_users->execute(['customer']);
        $users = $get_users->fetchAll();
        
        foreach($users as $row) {?>
            <option value="<?=$row['_id'];?>" <?=($row['_id']== $post_title)?' selected':'';?>>
            <?=$row['first_name'];?> <?=$row['surname'];?> | <?=$row['username'];?></option>
            
        <?}
    }
}

// Format file size nicely
if ( !function_exists('formatBytes') ) {
    function formatBytes($size, $precision = 2) {
        $base = log($size, 1024);
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');   
    
        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }
}

// Before each row in "what you last worked on", check if there's a module function to modify the output first
if ( !function_exists('beforeLastUpdate') ) {
    function beforeLastUpdate($LastUpdate, $module_folder) {
        if ( is_dir(FS_ROOT.'admin/modules/'.$module_folder) && file_exists(FS_ROOT.'admin/modules/'.$module_folder.'/mod_funcs.php') ) {
            require_once(FS_ROOT.'admin/modules/'.$module_folder.'/mod_funcs.php');
            if ( function_exists('modifyLastUpdate') ) {
                $LastUpdate = modifyLastUpdate($LastUpdate);
            }
        }
        
        return $LastUpdate;
    }
}

// Get filename from slug
if ( !function_exists('getFilename') ) {
    function getFilename($post_slug) {
        return basename(FS_ROOT.$post_slug);
    }
}

// Get file icon from slug
if ( !function_exists('getFileIcon') ) {
    function getFileIcon($post_slug) {
        
        $ext = pathinfo($post_slug, PATHINFO_EXTENSION);
        
        switch ($ext) {
            case 'doc':
            case 'docx':
                $file_icon = 'file-word-o';
                break;
            case 'pdf':
                $file_icon = 'file-pdf-o';
                break;
            case 'csv':
            case 'xls':
            case 'xlsx':
                $file_icon = 'file-excel-o';
                break;
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'png':
            case 'tif':
            case 'psd':
                $file_icon = 'file-image-o';
                break;
            case 'zip':
            case 'rar':
            case '7z':
            case 's7z':
                $file_icon = 'file-archive-o';
                break;
            default:
                $file_icon = 'file-o';
        }
        
        return $file_icon;
        
    }
}

// Single Image Uploader
function single_image($variable_name, $title, $recommended_size = false, $description = false ) {
    global $$variable_name;

    if ( isset($$variable_name) ) {
        $var_decoded = json_decode($$variable_name,true);

        $filename = explode('/',$var_decoded['post_slug']);
        $filename = array_pop($filename);

        $filesize = filesize(FS_ROOT.$var_decoded['post_slug']);
        $filesize = formatBytes($filesize,0);

        $magick = new Imagick(FS_ROOT.$var_decoded['post_slug']);
        $geo = $magick->getImageGeometry();
    }
    ?>
        <div class="form__group frm_uploader" data-type="single_image">
            <div class="frm_section frm_section_large <?=$variable_name;?> card">
                <h4 class="form__group__legend"><?=$title;?></h4>
                
                <? if ( $description != false ) { ?><div class="uploader-image-note"><?=$description?></div><? } ?>
                    <? if ( $recommended_size != false ) { ?><div class="uploader-image-note"><i class="fa fa-info-circle"></i> Recommended size: <?=$recommended_size;?>.</div><? } ?>

                    <? // This is the postmeta that will store the image details (id, title, alt, etc.) as JSON ?>
                    <input type="hidden" name="<?=$variable_name;?>" id="<?=$variable_name;?>" value="<?=(isset($$variable_name))?htmlspecialchars($$variable_name):'';?>">

                    
                    <? // The box that shows the current image ?>
                    <div class="wrap current-image"<?=( !isset($$variable_name) ) ? ' style="display: none;"' : ''; ?>>
                        
                        <div class="wrap pad">
                            <? if ( isset($$variable_name) ){ ?>
                                <div class="current-image__image"><img  src="<?=$var_decoded['post_slug'];?>" alt="<?=$var_decoded['post_alt_title'];?>" /></div>
                                <div class="current-image__details">
                                    <p><b>Current Image</b></p>
                                    <p><i class="fa fa-image-o"></i> <span class="ci-filename"><?=$filename;?></span></p>
                                    <ul>
                                        <li><b>Dimensions:</b> <span class="geo-width"><?=$geo['width'];?></span> x <span class="geo-height"><?=$geo['height'];?></span> pixels</li>
                                        <li><b>Size:</b> <span class="ci-filesize"><?=$filesize;?></span></li>
                                    </ul>
                                    <button class="button action delete remove-single-image" type="button"><i class="fa fa-times"></i> Remove</button>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                <? // Show this message if there's already an image set ?>
                <div class="wrap update-image"<?=( !isset($$variable_name) ) ? ' style="display: none;"' : ''; ?>>
                    <h5><i class="fa fa-info-circle"></i> Update Image - Change the image above for another image.</h5>
                </div>
                <? // Links to open the image library modal ?>
                    <div class="image-options">
                        <div class="image-option">
                            <div class="image-option__title">
                                <b>Upload an image</b>
                            </div>
                            <div class="image-option__button">
                                <a id="upload-images" class="button open-images"><i class="fa fa-file-image-o"></i> Upload</a>
                            </div>
                        </div>
                        <div class="image-option">
                            <div class="image-option__title">
                                <b>or Select from library</b>
                            </div>
                            <div class="image-option__button">
                                <a id="manage-images" class="button open-images"><i class="fa fa-file-image-o"></i> Image Library</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    <?
}

// Multi Image Uploader
function multi_image($variable_name, $title) {
    global $$variable_name;
    global $module_form_settings;
    
    if ( isset($$variable_name) ) {
        if ( substr($$variable_name,0,1) == '{' ) {
            $$variable_name = '['.$$variable_name.']';
        }
        $var_decoded = json_decode($$variable_name, true);
    }
    
    $gallery_empty = true;
    
    if ( isset($$variable_name) && !empty($var_decoded) ) {
        $gallery_empty = false;
    }
    
    ?>
        <div class="form__group frm_uploader" data-type="gallery">
            <div class="frm_section frm_section_large <?=$variable_name;?> card">
                <h4 class="form__group__legend"><?=$title;?></h4>
                <div class="pad">
                <p class="gallery-drag-info"<?=($gallery_empty || (isset($var_decoded) && count($var_decoded) == 1))?' style="display: none;"':'';?>><i class="fa fa-info-circle"></i> Drag items with &nbsp;<i class="fa fa-sort"></i>&nbsp; to rearrange their order.</p>
                
                <input type="hidden" name="<?=$variable_name;?>" id="<?=$variable_name;?>" value="<?=(isset($$variable_name))?htmlspecialchars($$variable_name):'';?>">
                
                <ul class="gallery-list<?=($gallery_empty)?' empty-gallery':'';?>">
                    
                    <li class="empty-gallery-msg"<?=(!$gallery_empty)?' style="display: none;"':'';?>><i class=" fa fa-info-circle"></i> This <?=strtolower($module_form_settings['singular']).( (strtolower(substr($module_form_settings['singular'], -1)) == 's') ? "'" : "'s" );?> gallery is empty.</li>
                    <? if ( !$gallery_empty ) {
                        foreach ( $var_decoded as $img ) {
                            ?>
                            <li class="actual-gallery-item" data-item="<?=htmlspecialchars(json_encode($img, JSON_UNESCAPED_SLASHES));?>">
                                <div class="gallery-image-handle"><i class="fa fa-sort"></i></div>
                                <div class="gallery-image-preview" style="background-image: url(<?="'".$img['post_slug']."'";?>);"></div>
                                <div class="gallery-image-text">
                                    <p><strong><?=$img['post_title'];?></strong></p>
                                    <p><?=$img['post_alt_title'];?></p>
                                </div>
                                <div class="gallery-image-actions">
                                    <button type="button" class="button button--edit action edit"><i class="fa fa-pencil"></i> Edit</button>
                                    <button type="button" class="button button--delete action delete"><i class="fa fa-times"></i> Remove</button>
                                </div>
                            </li>
                            <?
                        }
                    } ?>
                </ul>
                
                <div class="image-options">
                    <div class="image-option">
                        <div class="image-option__title">
                            <b>Upload an image</b>
                        </div>
                        <div class="image-option__button">
                            <a id="upload-images" class="button open-images"><i class="fa fa-file-image-o"></i> Upload</a>
                        </div>
                    </div>
                    <div class="image-option">
                        <div class="image-option__title">
                            <b>or Select from library</b>
                        </div>
                        <div class="image-option__button">
                            <a id="manage-images" class="button open-images"><i class="fa fa-file-image-o"></i> Image Library</a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    <?
}

// File Uploader
function file_uploader($variable_name, $title) {
    global $$variable_name;
    global $module_form_settings;
    global $pdo;
    
    if ( isset($$variable_name) ) {
        $var_decoded = json_decode($$variable_name, true);
    }
    
    $no_files = true;
    
    if ( isset($$variable_name) && !empty($var_decoded) ) {            
        $in = str_repeat('?,', count($var_decoded) - 1) . '?';
        
        $file_list = [];
        
        // Create a file list with id's as keys and empty values to be populated
        foreach ( $var_decoded as $var ) {
            $file_list[$var] = '';
        }
        
        // Populate the file list values
        $getFiles = $pdo->prepare("SELECT _id, post_title, post_slug, parent_id FROM er_posts WHERE _id IN ($in)");
        $getFiles->execute($var_decoded);
        while ( $file = $getFiles->fetch() ) {
            $file_list[$file['_id']] = $file;
        }
        
        // Remove any non-existant files from the var_decoded variable
        foreach ( $file_list as $key => $val ) {
            if ( $val == '' || empty($val) ) {
                unset($file_list[$key]);
                
                foreach ( $var_decoded as $k => $v ) {
                    if ( $v == $key ) {
                        unset($var_decoded[$k]);
                    }
                }
                
            }
        }
        
        // Refresh the variable with any non-existant files removed
        $$variable_name = json_encode($var_decoded,true);
        
        // Check there are files
        if ( empty($file_list) ) {
            $no_files = true;
        } else {
            $no_files = false;
        }
    }
    
    ?>
        <div class="form__group frm_uploader" data-type="files">
            <div class="frm_section frm_section_large <?=$variable_name;?> card">
                <h4 class="form__group__legend"><?=$title;?></h4>

                <p class="files-drag-info"<?=($no_files || (isset($var_decoded) && count($var_decoded) == 1))?' style="display: none;"':'';?>><i class="fa fa-info-circle"></i> Drag items with &nbsp;<i class="fa fa-sort"></i>&nbsp; to rearrange their order.</p>
                
                <input type="hidden" name="<?=$variable_name;?>" id="<?=$variable_name;?>" value="<?=(isset($$variable_name)&&!empty($file_list))?htmlspecialchars($$variable_name):'';?>">
                
                <ul class="files-list<?=($no_files)?' no-files':'';?>">
                    
                    <li class="no-files-msg"<?=(!$no_files)?' style="display: none;"':'';?>><i class=" fa fa-info-circle"></i> This <?=strtolower($module_form_settings['singular']);?> has no files.</li>
                    <? if ( !$no_files ) {
                        foreach ( $file_list as $file_id => $file ) {
                            $file_icon = getFileIcon($file['post_slug']);
                            $file_name = getFileName($file['post_slug']);

                            if ($file_icon == 'file-word-o') {
                                $download_slug = '/files/doc/';
                            } else if ($file_icon == 'file-pdf-o') {
                                $download_slug = '/files/pdf/';
                            }


                            ?>
                            <li class="file-item" data-item="<?=$file_id;?>">
                                <div class="file-item-handle"><i class="fa fa-sort"></i></div>
                                <div class="file-item-icon"><i class="fa fa-<?=getFileIcon($file['post_slug']);?>"></i></div>
                                <div class="file-item-text">
                                    <a target="_blank" download href="<?=$download_slug?><?=$file_name?>"><p><strong><?=$file['post_title'];?></strong><br/><?=getFilename($file['post_slug']);?></p></a>
                                </div>
                                <div class="file-item-actions">
                                <button type="button" class="button button--delete action delete"><i class="fa fa-times"></i> Remove</button>
                                </div>
                            </li>
                            <?
                        }
                    } ?>
                </ul>
                
                <div class="file-options">
                    <div class="file-option">
                        <div class="file-option__title">
                            <b>Upload a file</b>
                        </div>
                        <div class="file-option__button">
                            <a id="upload-files" class="button action open-files"><i class="fa fa-file-o"></i> Upload</a>
                        </div>
                    </div>
                    <div class="file-option">
                        <div class="file-option__title">
                            <b>or Select from files</b>
                        </div>
                        <div class="file-option__button">
                            <a id="manage-files" class="button action open-files"><i class="fa fa-file-o"></i> File Manager</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?
}

// Select parent page (for articles, events, etc.)
if ( !function_exists('parentSelector') ) {
    function parentSelector($var_name, $var_value) {
        global $module_form_settings;
        ?><div id="parent-page-select" class="parent-page-select">
            <form id="set_module_parent" method="post" action="<?=$_SERVER['REQUEST_URI'];?>">
                <label for="<?=$var_name;?>">Set <?=ucwords($module_form_settings['module_name']);?> Page:</label>
                <select class="form__select" name="<?=$var_name;?>">
                    <? page_options(0,0,$var_value); ?>
                </select>
                <button type="submit" class="button"><i class="fa fa-save"></i> Save</button>
            </form>
        </div><?
    }
}

// Create a slug out of the input (usually post_title)
if ( !function_exists('rewriteName') ) {
    function rewriteName($the_name) {
        $the_name = strtolower(str_replace("--","-",str_replace("---","-",str_replace("/","-",str_replace(" ","-",str_replace("'","",str_replace(",","",str_replace(" ,","",preg_replace("/[^a-z0-9-',.]/i","-",str_replace("&","and",trim($the_name)))))))))));
        return $the_name;
    }
}


if ( !function_exists('product_list') ) {
    function product_list($product_id) {
        
        global $pdo;
        
        $get_products = $pdo->prepare("SELECT * FROM er_posts WHERE post_type = ? ORDER BY post_title ASC");
        $get_products->execute(['product']);
        $products = $get_products->fetchAll();
        
        foreach($products as $row) {?>
            <option value="<?=$row['_id']?>" <?=($row['_id'] == $product_id)?' selected':'';?>><?=$row['post_title'];?></option>
            
        <?}
    }
}



if ( !function_exists('user_list') ) {
    function user_list() {
        
        global $pdo;
        global $post_title;
        
        $get_users = $pdo->prepare("SELECT _id, username, level, created_at, (SELECT meta_value FROM er_usermeta WHERE user_id=er_users._id AND meta_name = 'first_name') AS first_name,(SELECT meta_value FROM er_usermeta WHERE user_id=er_users._id AND meta_name = 'surname') AS surname FROM er_users WHERE level = ? ORDER BY first_name ASC");
        $get_users->execute(['customer']);
        $users = $get_users->fetchAll();
        
        foreach($users as $row) {?>
            <option value="<?=$row['_id'];?>" <?=($row['_id']== $post_title)?' selected':'';?>>
            <?=$row['first_name'];?> <?=$row['surname'];?> | <?=$row['username'];?></option>
            
        <?}
    }
}



    /**
     * Converts an ID to the post_title
     * 
     * @param integer/array $id - ID to be converted
     * @return string - The Name associated with the ID
     */
if ( ! function_exists('idToNameConversion')) {
    function idToNameConversion( $id ) {
        global $pdo; // Get DB Object
        // Get a large amount of id/names from one query
        if ( is_array($id) ) {
            $qStr = 'SELECT _id, post_title FROM er_posts WHERE _id IN (';
            for($i=0;$i<count($id)-1;$i++){ $qStr .= '?,'; }
            $qStr .= '?)';
            $q = $pdo->prepare($qStr);
            $q->execute($id);
            $returnData = [];
            while($row = $q->fetch()){
                $returnData[$row['_id']] = $row['post_title'];
            }
            return $returnData;
        } else {
            // Query
            $q = $pdo->prepare('SELECT post_title FROM er_posts WHERE _id = ?');
            $q->execute([$id]);
            return $q->fetch()['post_title'];
        }

    }
}


/**
 * Check if string is in a JSON format
 * @param string - string to check
 * @return boolean
 */
function isJson($string) {
  json_decode($string);
  return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * Retrieve a record from the posts table
 * 
 * @param int $post_id The ID of the record to retrieve
 * @return array The record. Empty array on failure.
 */
function getPost( int $post_id ): array {
  global $pdo;

  // Prepare Query
  $get_post = $pdo->prepare('
    SELECT *
    FROM er_posts
    WHERE _id = :post_id
    LIMIT 1
  ');
  $get_post->execute([
    'post_id' => $post_id
  ]);

  // Handle failing to find anything
  if ( $get_post->rowCount() === 0 ) {
    return false;
  }

  // Return the record
  return $get_post->fetch();
}


/**
 * Retrieve a Meta Record
 * 
 * @param int $post_id The ID of the post record
 * @param string $meta_name The Meta Name to be retrived
 * @return mixed The value of the meta on success, will be json_decoded if need. Empty string on failure.
 */
function getMeta( int $post_id, string $meta_name ) {
  global $pdo;

  // Prepare Query
  $get_meta = $pdo->prepare('
    SELECT meta_value
    FROM er_postmeta
    WHERE post_id = :post_id
      AND meta_name = :meta_name
    LIMIT 1
  ');
  $get_meta->execute([
    'post_id' => $post_id,
    'meta_name' => $meta_name
  ]);

  // Handle Failing to find anything
  if ( $get_meta->rowCount() === 0 ) {
    return '';
  }

  // Get Meta Value
  $meta_value = $get_meta->fetch()['meta_value'];
  if ( isJson($meta_value) ) {
    $meta_value = json_decode($meta_value, true);
  }
  
  // Return Value
  return $meta_value;
}

/**
 * Retrieve all the meta for a er_post record
 * 
 * @param int $post_id The record's ID the meta is tied to
 * @param bool $autoDecode Whether to JSON decode strings it finds. Default true
 * @return array Key Value Pairs of the meta. Empty array on failure. 
 */
function getAllMeta( int $post_id, bool $autoDecode = true ): array {
  global $pdo;

  // Prepare Query
  $getMeta = $pdo->prepare('
    SELECT meta_name, meta_value
    FROM er_postmeta
    WHERE post_id = :post_id
  ');
  $getMeta->execute([
    'post_id' => $post_id
  ]);

  // Handle no meta results
  if ( $getMeta->rowCount() === 0 ) {
    return [];
  }

  // Save and store meta
  $meta = [];
  while ( $row = $getMeta->fetch() ) {
    $metaValue = $row['meta_value'];

    // Handle JSON Values
    if ( $autoDecode && isJson($metaValue) ) {
      $metaValue = json_decode($metaValue, true);
    }

    $meta[$row['meta_name']] = $metaValue;
  }

  // Return Data
  return $meta;
}



// Include Fat Hippo Specific Functions
include_once FS_ROOT . 'includes/inc_funcs--fathippo.php';