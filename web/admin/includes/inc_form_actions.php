<div class="wrap form-actions">
    <div class="msg change-alert"><i class="fa fa-warning"></i> Please <b>save</b> your <?=strtolower($module_form_settings['singular'])?> before navigating elsewhere or you will lose your changes.</div>
    <button class="form-actions__save" type="submit" name="save_form"><i class="fa fa-save"></i> Save <?=$module_form_settings['singular']?></button>
</div>