<?php
    
// Check if users table exists, if not create it and add record
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_users'")->fetch();
if ( !$tbl_exists ) {

    // Create the users table
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_users` (
        `_id` int(5) NOT NULL AUTO_INCREMENT,
        `username` varchar(255) NOT NULL,
        `password` varchar(255) NOT NULL,
        `active` enum('Yes','No') NOT NULL,
        `level` enum('superuser','admin','standard') DEFAULT 'standard' NOT NULL,
        `created_at` datetime DEFAULT NULL,
        `last_login` datetime DEFAULT NULL,
        PRIMARY KEY (`_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

    // Add our superuser account
    $createSuperUser = $pdo->prepare('INSERT INTO `er_users` (username, password, active, level, created_at) VALUES (:username, :password, :active, :level, NOW())');
    $createSuperUser->execute([
        'username' => $username,
        'password' => password_hash($password, PASSWORD_BCRYPT),
        'active' => 'Yes',
        'level' => 'superuser'
    ]);

}

// Check if usermeta table exists, if not create it
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_usermeta'")->fetch();
if ( !$tbl_exists ) {

    // Create the posts table
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_usermeta` (
        `_id` int(5) NOT NULL AUTO_INCREMENT,
        `user_id` int(5) DEFAULT NULL,
        `meta_name` text NOT NULL,
        `meta_value` text NOT NULL,
        PRIMARY KEY (`_id`),
        FOREIGN KEY (`user_id`) REFERENCES `er_users`(`_id`) ON DELETE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
}

// Check if user permissions table exists, if not create it and add record
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_permissions'")->fetch();
if ( !$tbl_exists ) {

    // Create permissions table
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_permissions` (
        `_id` int(5) NOT NULL,
        `module_name` varchar(255) NOT NULL,
        FOREIGN KEY (`_id`) REFERENCES `er_users` (`_id`) ON DELETE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
    
}

// Check if posts table exists, if not create it and add records
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_posts'")->fetch();
if ( !$tbl_exists ) {
    
    // Create the posts table
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_posts` (
        `_id` int(5) NOT NULL AUTO_INCREMENT,
        `date_created` datetime DEFAULT NULL,
        `post_status` varchar(255) NOT NULL,
        `post_type` varchar(255) NOT NULL,
        `parent_id` int(5) NOT NULL DEFAULT '0',
        `date_modified` datetime DEFAULT NULL,
        `post_slug` text NOT NULL,
        `menu_order` int(5) NOT NULL DEFAULT '0',
        `post_author` int(5) NOT NULL DEFAULT '0',
        `post_title` varchar(255) NOT NULL,
        `post_alt_title` varchar(255) NOT NULL,
        PRIMARY KEY (`_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
    
    // Add Home and 404 pages
    $initialisePages = $pdo->prepare('INSERT INTO `er_posts` (date_created, post_status, post_type, date_modified, post_slug, post_author, post_title, post_alt_title) VALUES (NOW(), :post_status, :post_type, NOW(), :post_slug, :post_author, :post_title, :post_alt_title)');
    
    $defaultPages = [
        ['post_slug' => 'index','post_title'=>'Home','post_alt_title'=>'Home'],
        ['post_slug' => '404','post_title'=>'404','post_alt_title'=>'404']
    ];
    
    foreach ( $defaultPages as $key => $page ) {
        $initialisePages->execute([
            'post_status' => 'live',
            'post_type' => 'page',
            'post_slug' => $page['post_slug'],
            'post_author' => 1,
            'post_title' => $page['post_title'],
            'post_alt_title' => $page['post_alt_title']
        ]);
    }
}

// Check if postmeta table exists, if not create it and add records
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_postmeta'")->fetch();
if ( !$tbl_exists ) {

    // Create the posts table
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_postmeta` (
        `_id` int(5) NOT NULL AUTO_INCREMENT,
        `post_id` int(5) DEFAULT NULL,
        `meta_name` text NOT NULL,
        `meta_value` text NOT NULL,
        PRIMARY KEY (`_id`),
        FOREIGN KEY (`post_id`) REFERENCES `er_posts`(`_id`) ON DELETE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

    $initialisePages = $pdo->prepare('INSERT INTO `er_postmeta` (post_id, meta_name, meta_value) VALUES (:post_id, :meta_name, :meta_value)');
    
    $defaultPages = [
        ['post_id'=>1, 'meta_name'=>'page_content', 'meta_value'=>'<p>This is the homepage.</p>'],
        ['post_id'=>2, 'meta_name'=>'page_content', 'meta_value'=>'<p>Oops! The page you are looking for does not exist. Head to our <a href="/">homepage</a> and go from there.']
    ];
    
    foreach ( $defaultPages as $key => $page ) {
        $initialisePages->execute([
            'post_id' => $page['post_id'],
            'meta_name' => $page['meta_name'],
            'meta_value' => $page['meta_value']
        ]);   
    }
}

// Check if config table exists, if not create it
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_config'")->fetch();
if ( !$tbl_exists ) {
    
    // Create the config table
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_config` (
        `site_name` varchar(255) NOT NULL,
        `admin_logo` varchar(255) NOT NULL,
        `append_meta_title` varchar(255) NOT NULL,
        `append_meta_description` varchar(255) NOT NULL,
        `site_address` text NOT NULL,
        `site_postcode` varchar(8) NOT NULL,
        `site_maplink` varchar(255) NOT NULL,
        `site_lat_long` varchar(255) NOT NULL,
        `site_telephone` varchar(255) NOT NULL,
        `site_email` varchar(255) NOT NULL,
        `ga_code` varchar(50) NOT NULL,
        `gst_code` varchar(50) NOT NULL,
        `reg_number` varchar(255) NOT NULL,
        `emailer_from` varchar(255) NOT NULL,
        `emailer_to` varchar(255) NOT NULL,
        `css_js_filename` varchar(255) NOT NULL,
        `database_live_date` varchar(255) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");

    // Insert a blank link so it can be updated via the settings page
    $pdo->query("INSERT INTO er_config (site_name,admin_logo,append_meta_title,append_meta_description,site_address,site_postcode,site_maplink,site_lat_long,site_telephone,site_email,ga_code,gst_code,reg_number,emailer_from,emailer_to,css_js_filename,database_live_date) VALUES ('','','','','','','','','','','','','','','','','')");
}

// Check if options table exists, if not create it
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_options'")->fetch();
if ( !$tbl_exists ) {
    
    // Create the options table
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_options` (
        `option_name` varchar(255) NOT NULL,
        `option_value` text NOT NULL,
        PRIMARY KEY (`option_name`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8");   
}