    <?php
        if ( isset($admin_page_type) && file_exists(FS_ADMIN_ROOT.'modules/core/image-library/image_library.php') ) { 
            
            echo '<div class="image_library_modal">';
                include_once(FS_ADMIN_ROOT.'modules/core/image-library/image_library.php');
            echo '</div>';
            echo '<div class="image_library_bg"></div>';
        }
    ?>
    
    <?php
        if ( isset($admin_page_type) && file_exists(FS_ADMIN_ROOT.'modules/core/file-manager/file_manager.php') ) { 
            
            echo '<div class="file_manager_modal">';
                include_once(FS_ADMIN_ROOT.'modules/core/file-manager/file_manager.php');
            echo '</div>';
            echo '<div class="file_manager_bg"></div>';
        }
    ?>
    
    <script src="/admin/js/jquery-3.2.1.min.js"></script>
    <script src="/admin/js/fancybox/source/jquery.fancybox.pack.js"></script>
    <? if ( isset($admin_page_title) ) { // Should only need these on form pages ?>
        <script src="/admin/js/validate.js"></script>
        <script src="/admin/js/tinymce/tinymce.min.js"></script>
    <? } ?>
    
    <script src="/admin/js/uploader.js"></script>
    
    <?
        if ( file_exists('mod_js.php') ) {
            include_once('mod_js.php');
        }
    ?>
    
    <script src="/admin/js/admin.js?v=<?=filemtime(FS_ADMIN_ROOT.'js/admin.js');?>"></script>
    
    <? 
        if ( file_exists(FS_ADMIN_ROOT.'modules/core/image-library/mod_js.php') ) {
            include_once(FS_ADMIN_ROOT.'modules/core/image-library/mod_js.php'); 
        }
        
        if ( file_exists(FS_ADMIN_ROOT.'modules/core/file-manager/mod_js.php') ) {
            include_once(FS_ADMIN_ROOT.'modules/core/file-manager/mod_js.php');
        }
    ?>

    <script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="/admin/js/search.js"></script>

</body>
</html>
<?php unset($_SESSION['success']); ?>