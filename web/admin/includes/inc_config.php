<?php
if ( !headers_sent() ) {
	header("Content-Type: text/html; charset=utf-8");
	session_start();
}

// Only show errors if they've been switched on in settings
if ( isset($_SESSION['show_errors']) ) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

date_default_timezone_set("Europe/London");

// If on our local IP, define DEV as true
define('DEV', ( ($_SERVER['SERVER_ADDR'] == '192.168.11.121') ? true : false));

define('FS_ROOT',$_SERVER['DOCUMENT_ROOT']."/");

define('ADMIN_FOLDER',"/admin/");
define('FS_ADMIN_ROOT',$_SERVER['DOCUMENT_ROOT'].ADMIN_FOLDER);
define('FS_ADMIN_INCLUDES',FS_ADMIN_ROOT."includes/");

require_once(FS_ROOT."../private/db_config.php");
require_once(FS_ADMIN_INCLUDES."inc_funcs.php");
//require_once(FS_ADMIN_INCLUDES."password.php");

$breadcrumb = [
    ['/admin/', 'Home']
];

// Specify that we're in the admin (for layout and field system)
$in_admin = true;

// Check that config table exists
$configExists = $pdo->query("SHOW TABLES LIKE 'er_config'")->fetch();

// If config table exists, get the Site Name, set module option arrays and build the Nav
if ( $configExists ) {
	$getSiteName = $pdo->query("SELECT site_name, admin_logo FROM er_config")->fetch();
	$site_name = ($getSiteName['site_name'] != '') ? $getSiteName['site_name'] : 'Edward Robertson CMS';
	$admin_logo = $getSiteName['admin_logo'];


	// Establish form settings for any currently active module
	$module_form_settings = [];
	$module_folder_links = [];
	$module_homepage_cards = [];
	$modules_by_type = [];
	
	// Build the main menu and populate $module_form_settings
	buildNav();
}

// Check that options table exists
$optionsExists = $pdo->query("SHOW TABLES LIKE 'er_options'")->fetch();

// If options table exists, create variables from contents for use across site if needed
if ( $optionsExists ) {
    $getOptions = $pdo->query("SELECT * FROM er_options");
    while ( $opt = $getOptions->fetch() ) {
        $option_name = $opt['option_name'];
        $$option_name = $opt['option_value'];
    }
}