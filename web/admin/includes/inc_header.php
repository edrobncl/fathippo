<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
    	<meta name="HandheldFriendly" content="True">
    	<meta name="MobileOptimized" content="320">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    	<meta name="author" content="Edward Robertson - www.edwardrobertson.co.uk" />
        <meta name="description" content="The Edward Robertson CMS Dashboard" />
        
        <title><?=str_replace("-"," ", ucwords(basename(getcwd()))) . ' : ' . $site_name;?> : Dashboard</title>
                
        <link rel="stylesheet" href="<?=ADMIN_FOLDER.'css/core.min.css?v='.filemtime(FS_ADMIN_ROOT.'css/core.min.css');?>" media="all" />
        <link rel="stylesheet" href="<?=ADMIN_FOLDER.'css/additional.min.css?v='.filemtime(FS_ADMIN_ROOT.'css/additional.min.css');?>" media="all" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">        
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/admin/js/fancybox/source/jquery.fancybox.css" media="screen" />
        
        <? if ( file_exists(FS_ROOT.'images/favicon.ico') ) { ?><link rel="shortcut icon" href="/images/favicon.ico" /><? } ?>
        <? if ( file_exists(FS_ROOT.'images/apple-touch-icon-precomposed.png') ) { ?><link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon-precomposed.png"><? } ?>
        <? if ( file_exists(FS_ROOT.'images/apple-touch-icon-precomposed.png') ) { ?><meta name="msapplication-TileImage" content="/images/apple-touch-icon-precomposed.png" /><? } ?>
        <meta name="msapplication-TileColor" content="#fff" />
        
        <meta name="robots" content="noindex">
    </head>
    <body class="<? if(DEV) { ?>dev-site<? } ?><? if($_SESSION['userlevel'] == "superuser") { ?> superuser<? } ?>">
        <div id="admin-search"></div>
        
        <div id="page-wrap">
            
            <header id="header">
                <a class="header__logo" target="_blank" href="https://www.edwardrobertson.co.uk"><img width="32" height="34" src="<?=ADMIN_FOLDER.'images/logo-white-blue-dots.png';?>" alt="Edward Robertson Website" /></a>
                <div id="nav-toggle"><i class="fa fa-bars"></i><b>Menu</b></div>
                <a class="header__link header__link--dashboard" href="<?=ADMIN_FOLDER;?>"><i class="fa fa-dashboard"></i><b>Dashboard</b></a>
                <a class="header__link" href="/admin/modules/core/settings/"><i class="fa fa-cog"></i><b>Settings</b></a>
                <a class="header__link" id="admin-search-toggle" target="_blank">
                    <div class="css-shape css-search" role="button"><span></span><span></span><span></span></div>
                    <b>Search</b>
                </a>
                <a class="header__link" href="/" target="_blank"><i class="fa fa-globe"></i><b>Website</b></a>
                <? if(DEV) { ?><div class="dev-site-notice">Dev Admin</div><? } ?>
                <a class="header__link header__link--signout" href="<?=ADMIN_FOLDER.'logout';?>"><i class="fa fa-sign-out"></i><b>Logout</b></a>
            </header>
            
            <? include(FS_ADMIN_ROOT.'includes/inc_nav.php'); ?>
			<div id="dashboard">