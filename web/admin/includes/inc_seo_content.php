<div class="form__group" style="position:relative;">
    <fieldset>
        <legend class="form__group__legend">Search engine listing preview</legend>        
        <div class="seo-toggle"><b>Edit SEO</b><i class="fa fa-chevron-down"></i></div>
        <div class="wrap seo-content">
            
            
            <div class="wrap seo-toggle-content">
                <p>Please <a href="https://www.edwardrobertson.co.uk/contact/" target="_blank">contact us</a> for a more detailed explanation of Search Engine Optimisation.</p>
                <div class="form__field with_help">
                    <label class="form__field__label" for="meta_title">Meta Title <a tabindex="-1" id="inline" href="#pop_meta_title"  class="admin-help"><i class="fa fa-question-circle"></i> Help?</a></label>
                    <input class="form__field__input" name="meta_title" id="meta_title" type="text" value="<?=(isset($meta_title))?$meta_title:'';?>" />
                </div>
            
                <div style="display:none">
                    <div id="pop_meta_title">
                        <h3>Meta Title</h3>
                        <p>The main tagline used for search engines and Social Media. Recommended 100 characters.</p>
                        <p>If you do not include a Meta Title, we will use your Title along with the "Append Meta Title" used in your Settings.</p>
                    </div>
                </div>
            
                <div class="form__field with_help">
                    <label class="form__field__label" for="meta_description">Meta Description <a tabindex="-1" id="inline" href="#pop_meta_description"  class="admin-help"><i class="fa fa-question-circle"></i> Help?</a></label>
                    <textarea  class="form__field__textarea" name="meta_description" id="meta_description"><?=(isset($meta_description))?$meta_description:'';?></textarea>
                </div>
            
                <div style="display:none">
                    <div id="pop_meta_description">
                        <h3>Meta Description</h3>
                        <p>A short summary of the page, used in the website, social media and SEO. Recommended 200 characters.</p>
                        <p>If you do not include a Meta Description, we will use your Title along with the "Append Meta Description" used in your Settings.</p>
                    </div>
                </div>

                <?php
                    single_image('og_image', 'Share Image', '1200px x 630px');
                    single_image('og_twitter_image', 'Twitter Share Image', '1200px x 686px');
                ?>

            </div>
            <div class="wrap seo-content__preview">
                <?php
                $og_url = rtrim($_SERVER['HTTP_HOST'],"/").(($post_id == 1)?'':buildPath($post_id));
                ?>
                <div class="seo-content__preview__metaurl"><?=$og_url;?></div>
                <div class="seo-content__preview__metatitle"><?=(isset($meta_title))?$meta_title:$post_title;?></div>
                <div class="seo-content__preview__metadescription"><?=(isset($meta_description))?$meta_description:'This meta description will be compiled automatically by the search engines unless you specify some text.';?></div>

            </div>
        </div>
    </fieldset>
</div>