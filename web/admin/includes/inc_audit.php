<?php
if ( !defined('FS_ROOT') ) {
	$redirect = (isset($return_to)) ? $return_to : '/admin/';
	header("Location: $redirect");
	exit();
}

// Create the audit trail table if it doesn't exist
$tbl_exists = $pdo->query("SHOW TABLES LIKE 'er_audit'")->fetch();
if ( !$tbl_exists ) {
    $pdo->query("CREATE TABLE IF NOT EXISTS `er_audit` (
        `_id` int(11) NOT NULL AUTO_INCREMENT,
		`user_id` int(11) NOT NULL,
		`post_id` int(11) NOT NULL,
		`post_type` varchar(255) NOT NULL,
		`action` varchar(7) NOT NULL,
		`post_content` mediumtext NOT NULL,
        `created_at` datetime DEFAULT NULL,
        PRIMARY KEY (`_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
}

try {
	$auditTime = date('Y-m-d H:i:s');
	$insertAudit = $pdo->prepare("INSERT INTO er_audit (user_id, post_id, post_type, action, post_content, created_at) VALUES (?,?,?,?,?,?)");
	$insertAudit->execute([$_SESSION['eruid'],$post_id,$post_type,$action,json_encode($postAudit),$auditTime]);
} catch ( Exception $e ) {
	// Should work fine. If not, don't throw errors. It's okay.
}