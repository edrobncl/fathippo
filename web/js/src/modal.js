(function () {


  /**
   * Tabs inside the modals in menu layouts
   */
  document.querySelectorAll('.modal__dialogue__toggles .tab').forEach(tab => {

    tab.addEventListener('click', () => {
      console.log('click');
      if ('data-toggle' in tab.attributes) {
        let selectedTab = tab.attributes['data-toggle'].value;

        // Update the Tab Selected
        [...tab.parentNode.children].forEach(sibling => sibling.classList.remove('active'));
        tab.classList.add('active');

        // Update The Tab Content
        [...tab.parentNode.nextSibling.nextSibling.children].forEach(sibling => {
          if (sibling.attributes['data-name'].value !== selectedTab) {
            sibling.classList.remove('active');
          } else {
            sibling.classList.add('active');
          }
        });
      }
    });
  });


  /**
   * Modal Close
   */
  document.querySelectorAll('.modal__dialogue__button[data-close]').forEach(closeButton => {
    closeButton.addEventListener('click', () => {
      if ('data-close' in closeButton.attributes) {
        let modalId = closeButton.attributes['data-close'].value;

        document.querySelector('.modal.active[data-modal="' + modalId + '"]').classList.remove('active');
      }
    }); 
  });

})();