google.maps.event.addDomListener(window, 'load', mapInit);

function mapInit() {
  const centerpoint = { lat: 55.3781, lng: 3.4360 };
  var mapOptions = {
    center: centerpoint,
    zoom: 17,
    minZoom: 5,
    mapTypeControl: false,
    disableDefaultUI:true,
    styles:   [
      {
        'featureType': 'all',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'saturation': 36
          },
          {
            'color': '#000000'
          },
          {
            'lightness': 40
          }
        ]
      },
      {
        'featureType': 'all',
        'elementType': 'labels.text.stroke',
        'stylers': [
          {
            'visibility': 'on'
          },
          {
            'color': '#000000'
          },
          {
            'lightness': 16
          }
        ]
      },
      {
        'featureType': 'all',
        'elementType': 'labels.icon',
        'stylers': [
          {
            'visibility': 'on'
          }
        ]
      },
      {
        'featureType': 'administrative',
        'elementType': 'geometry.fill',
        'stylers': [
          {
            'visibility':'off'
          }
        ]
      },
      {
        'featureType': 'administrative',
        'elementType': 'geometry.stroke',
        'stylers': [
          {
            'visibility':'off'
          }
        ]
      },
      {
        'featureType': 'landscape',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'lightness': 20
          }
        ]
      },
      {
        'featureType': 'poi',
        'elementType': 'geometry',
        'stylers': [
          {
            'visibility':'off'
          }
        ]
      },
      {
        'featureType': 'road.highway',
        'elementType': 'geometry.fill',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'lightness': 17
          }
        ]
      },
      {
        'featureType': 'road.highway',
        'elementType': 'geometry.stroke',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'lightness': 29
          },
          {
            'weight': 0.2
          }
        ]
      },
      {
        'featureType': 'road.arterial',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'lightness': 18
          }
        ]
      },
      {
        'featureType': 'road.local',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#000000'
          },
          
          {
            'lightness': 16
          }
        ]
      },
      {
        'featureType': 'transit',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'lightness': 19
          }
        ]
      },
      {
        'featureType': 'water',
        'elementType': 'geometry',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'lightness': 13
          }
        ]
      }
    ]
  };
  var mapElement = document.getElementById('google-map');
  var map = new google.maps.Map(mapElement, mapOptions);
  var mapBounds = new google.maps.LatLngBounds();
  var concessionmarker = {
    url: '/images/map/concessionmarker.svg',    
    scaledSize: new google.maps.Size(45, 45), // scaled size
    origin: new google.maps.Point(0, 0), // origin
    anchor: new google.maps.Point(20, 40) // anchor
  };
  var concessionselected = {
    url: '/images/map/concessionselected.svg',    
    scaledSize: new google.maps.Size(45, 45), // scaled size
    origin: new google.maps.Point(0, 0), // origin
    anchor: new google.maps.Point(20, 40) // anchor
  };
  var mapmarker = {
    url: '/images/map/mapmarker.svg',    
    scaledSize: new google.maps.Size(45, 45), // scaled size
    origin: new google.maps.Point(0, 0), // origin
    anchor: new google.maps.Point(20, 40) // anchor
  };
  var mapmarkerselected = {
    url: '/images/map/markerselected.svg',
    scaledSize: new google.maps.Size(45, 45), // scaled size
    origin: new google.maps.Point(0, 0), // origin
    anchor: new google.maps.Point(20, 40) // anchor
  };
  var mapBox = $('#map-wrap .map-box');
  
  var jsons = [];
  $('.location-json').each(function(){
    jsons.push(JSON.parse($(this).val()));
  });

  var markers = [];
  $.each(jsons, function( k, location ) {
    var position = new google.maps.LatLng(location.latitude, location.longitude);
    mapBounds.extend(position);

    if (location.type == 'concession') {
      var marker = new google.maps.Marker({
        position: position,
        map: map,
        locationData: location,
        icon: concessionmarker
      });
    }
    else {
      var marker = new google.maps.Marker({
        position: position,
        map: map,
        locationData: location,
        icon: mapmarker
      });
    }
    markers.push(marker);

    new google.maps.event.addListener(marker, 'click', function() {
      $.each(markers, function( kk, otherMarker ) {
        if (otherMarker.locationData.type == 'concession') {
          otherMarker.setIcon(concessionmarker);
        }
        else {
          otherMarker.setIcon(mapmarker);
        }
      });
      if (location.type == 'concession') {
        marker.setIcon(concessionselected);
        var target = 'article#'+marker.locationData.id;
        $('html,body').animate({
            scrollTop: $(target).offset().top - 110
        }, 1000);
      }
      else {
        marker.setIcon(mapmarkerselected);
        var target = 'article#'+marker.locationData.id;
        $('html,body').animate({
            scrollTop: $(target).offset().top - 110
        }, 1000);
      }
    });
  });

  var options = {
    textSize: 50,
    backgroundPosition:  0,
    width: 50,
    gridSize: 50,
    styles: [{
      textColor: '#30E49A',
      url: '/images/map/cluster1.png',
      height: 50,
      width: 50
    }]
  };
  var markerCluster = new MarkerClusterer(map, markers, options);

  function ZoomControl(controlDiv, map) {
    // zoomIn
    var zoomInButton = document.createElement('a');
    zoomInButton.className = 'zoom-in';
    var zoomInButtonIcon = document.createElement('i');
    zoomInButtonIcon.className = 'icon-magnifying-glass-plus';
    zoomInButton.appendChild(zoomInButtonIcon);
    controlDiv.appendChild(zoomInButton);
    google.maps.event.addDomListener(zoomInButton, 'click', function() {
      map.setZoom(map.getZoom() + 1);
    });
      
    // zoomOut
    var zoomOutButton = document.createElement('a');
    zoomOutButton.className = 'zoom-out';
    var zoomOutButtonIcon = document.createElement('i');
    zoomOutButtonIcon.className = 'icon-magnifying-glass-minus';
    zoomOutButton.appendChild(zoomOutButtonIcon);
    controlDiv.appendChild(zoomOutButton);
    google.maps.event.addDomListener(zoomOutButton, 'click', function() {
      map.setZoom(map.getZoom() - 1);
    });
  }
  var zoomControlDiv = document.createElement('p');
  zoomControlDiv.className = 'zoom-links';
  zoomControlDiv.index = 1;
  var zoomControl = new ZoomControl(zoomControlDiv, map);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(zoomControlDiv);

  map.setCenter(mapBounds.getCenter());
  map.fitBounds(mapBounds); 
  new google.maps.event.addDomListener(window, 'resize', function() {
    google.maps.event.trigger(map, 'resize');
    map.setCenter(mapBounds.getCenter());
    map.fitBounds(mapBounds); 
  });
}

$(document).ready(function() {
  $('.view-links .list').on('click', function(e) {
    e.preventDefault();
    Cookies.set('locationsIndexView', 'list');
    $('#map-wrap').addClass('d-none');
    $('body').removeClass('noscroll');
    $('#list-wrap, section.cms-element.page-header-locations-index').removeClass('d-none');
  });
  
  $('.view-links .map').on('click', function(e) {
    e.preventDefault();
    Cookies.set('locationsIndexView', 'map');
    $('#list-wrap, section.cms-element.page-header-locations-index').addClass('d-none');
    $('#map-wrap').removeClass('d-none');
    window.dispatchEvent(new Event('resize'));
  });
});