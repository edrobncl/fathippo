(function () {

  $.each($('[data-toggle]'), function (idx, toggle) {

    $(toggle).on('click', function (e) {
      e.preventDefault();
      $(e.target).addClass('open');

      // By default, book-online drawer display attribute is set to "none"
      // this is done to avoid Lighthouse rendering problem.
      $('.book-online').css('display', 'block');

      var target = $(e.target).attr('data-toggle');
      var venue = $(e.target).attr('data-venue');

      if (target == 'book-online' && venue == 'true') {

        // Grab href
        var href = $(e.target).attr('href');

        // Load source into iframe
        $('[data-iframe="book-venue"]').attr('src', href);

        // Show the iframe
        $('[data-iframe="book-venue"]').addClass('active');

        // Hide the venues container
        $('.book-online__venues').hide();

        // Hide the venues scrollbar
        $('.book-online__wrap').css('overflow-y', 'hidden');
      }

      else if (target == 'book-venue') {

        // Grab href
        var href = $(e.target).attr('href');

        // Load source into iframe
        if ($('[data-iframe="book-venue"]').attr('src') !== href) {
          $('[data-iframe="book-venue"]').attr('src', href);
        }

        // Show the iframe
        $('[data-iframe="book-venue"]').addClass('active');

        // Hide the venues container
        $('.book-online__venues').hide();

        // Hie the venues scrollbar
        $('.book-online__wrap').css('overflow-y', 'hidden');
      }

      if ($('[data-wrap="' + target + '"]').length) {
        var wrap_height = $('[data-wrap="' + target + '"]').outerHeight();
        var active_height = Number($('[data-content="' + target + '"]').css('height').replace('px', ''));

        if (active_height === 0) {
          $('[data-content="' + target + '"]').css('height', wrap_height);
          $('[data-toggle="' + target + '"]').find('button').css('transform', 'rotate(45deg)');
        }
        else {
          $('[data-content="' + target + '"]').css('height', 0);
          $('[data-toggle="' + target + '"]').find('button').css('transform', 'rotate(0deg)');
        }
      }

      else if ($('[data-modal="' + target + '"]').length) {

        if (target == 'book-online' && venue == undefined) {
          // Reset classes and attributes
          $('[data-iframe="book-venue"]').removeClass('active');
          $('[data-iframe="book-venue"]').attr('src', '');
          $('.book-online__venues').show();
        }

        $('[data-modal="' + target + '"]').addClass('active');
        $('body').addClass('locked');
  
        $('[data-close]').on('click', function (e) {
          $('[data-modal="' + target + '"]').removeClass('active');
          $('body').removeClass('locked');
          $('.book-online__wrap').css('overflow-y', 'auto');
        });
      }

      // if ($(e.target).hasClass('open')) {
      //   $('[data-iframe="book-venue"]').attr('src', null);
      //   $(e.target).removeClass('open');
      // }
    });
  });

})();