$(document).ready(function () {
  if (document.querySelectorAll('.parallax').length) {
    new simpleParallax(document.querySelectorAll('.parallax img'),{
      customWrapper:'.parallax',
      scale: '1',
    });
  }
});