$(document).ready(function() {
  $("#nav-menu-toggle").click(function() {
    $(this).toggleClass("open");
    $(this).find('i').toggleClass('icon-times');
    $(".nav").toggleClass("active");
    $("html,body").toggleClass("locked");
  });
});