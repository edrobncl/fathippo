$(document).ready(function () {
  if ($('#carousel').length) {
    $('#carousel').slick({
      lazyLoad: 'ondemand',
      speed: 600,
      infinite: true,
      dots : true,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 8000,
      adaptiveHeight: true,
      cssEase: 'ease-out',
      fade: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: $('#prev-arrow'),
      nextArrow: $('#next-arrow'),
      appendDots: $('#carousel-dots'),
    });
  }

  if ($('.locations--slick').length) {
    $('.locations--slick').slick({
      lazyLoad: 'ondemand',
      speed: 600,
      infinite: true,
      dots: false,
      arrows: true,
      autoplay: false,
      adaptiveHeight: false,
      cssEase: 'ease-out',
      fade: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('#l-prev-arrow'),
      nextArrow: $('#l-next-arrow'),
      responsive: [{
        breakpoint: 991,
        settings: {
          slidesToShow: 2
        }
      },{
        breakpoint: 769,
        settings: {
          slidesToShow: 1
        }
      }]
    });
  }

  if ($('.events--slick').length) {
    $('.events--slick').slick({
      lazyLoad: 'ondemand',
      speed: 600,
      infinite: true,
      dots: false,
      arrows: true,
      autoplay: false,
      adaptiveHeight: false,
      cssEase: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('#l-prev-arrow'),
      nextArrow: $('#l-next-arrow'),
      responsive: [{
        breakpoint: 991,
        settings: {
          slidesToShow: 2
        }
      },{
        breakpoint: 769,
        settings: 'unslick'
      }]
    });
  }
  
  if ($('.locations__listings').length) {
    $('.locations__listings').slick({
      lazyLoad: 'ondemand',
      speed: 600,
      infinite: true,
      dots: false,
      arrows: true,
      autoplay: false,
      adaptiveHeight: true,
      cssEase: 'ease-out',
      fade: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: $('.control-left i'),
      nextArrow: $('.control-right i')
    });
  }
});