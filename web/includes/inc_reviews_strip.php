<?php

$getTestimonials = '
  SELECT *,
    (SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = "testimonial_content") AS content,
    (SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = "testimonial_image") AS image
  FROM er_posts
  WHERE _id IN (
      SELECT post_id
      FROM er_postmeta
      WHERE meta_name = "testimonial_page_list"
        AND meta_value LIKE :post_id
    )
  AND post_type = "testimonial"
';

if ( ! empty($_SESSION['wsid']) ) {
  $getTestimonials .= ' AND post_status IN ("live", "private") ';
} else {
  $getTestimonials .= ' AND post_status = "live" ';
}

$getTestimonials .= '
ORDER BY menu_order ASC
';

$getTestimonials = $pdo->prepare($getTestimonials);
$getTestimonials->execute([
  'post_id' => '%"' . $post_id . '"%'
]);

if (isset($getTestimonials) && $getTestimonials->rowCount() > 0) {
  addAsset('carousel'); ?>

  <section class="section reviews">
    <div class="inner">
      <div id="carousel" class="reviews__carousel">
        <?php while ($review = $getTestimonials->fetch()) { ?>
          <div>
            <article class="container review">
              <div class="review__column review__column--images">
                <div class="review__image"><?php showPicture($review['image']); ?></div>
                <div class="review__illust"><img class="review__illustration review__illustration--mobile" loading="lazy" src="/images/megaphone.svg" alt="Megaphone" width="140" height="140" /></div>
              </div>
              <div class="review__column review__column--text">
                <img class="review__illustration review__illustration--desktop" loading="lazy" src="/images/megaphone.svg" alt="Megaphone" width="140" height="140" />
                <div class="review__content"><?php echo $review['content']; ?></div>
                <div class="review__author"><?php echo $review['post_title']; ?></div>
              </div>
            </article>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
<?php } ?>