<?php if (!empty($popup)) { ?>
  <div class="modal" data-modal="modal_<?php echo $popup['_id']; ?>">
    <div class="modal__dialogue modal__dialogue--lgtop">
      <div class="modal__dialogue__content">
        <button type="button" class="modal__dialogue__button modal__dialogue__button--toright inner-overlay" data-close="modal_<?php echo $popup['_id']; ?>"><i class="icon-times"></i></button>
        <h3 class="modal__dialogue__title modal__dialogue__title--center"><?php echo $popup['title']; ?></h3>
        <div class="modal__dialogue__desc modal__dialogue__desc--lgmargin"><?php echo $popup['body']; ?></div>
      </div>
      <div class="modal__dialogue__footer modal__dialogue__footer--white">
        <p><?php echo str_replace(['<p>', '</p>'], '', $popup['footer']); ?> <span data-close="modal_<?php echo $popup['_id']; ?>" class="faux-link">Click here</span></p>
      </div>
    </div>
  </div>
<?php } ?>