<?php
  $navOutput = '';

  $navQ = "
    SELECT _id, parent_id, post_alt_title,
      (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'post_nav_icon') AS nav_icon,
      (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'external_slug') AS external
    FROM er_posts
    WHERE post_type IN ('page', 'menu')
      AND parent_id = ?";

  // Handle Private Pages
  if ( ! empty($_SESSION['eruid']) ) {
    $navQ .= " AND post_status IN ('live', 'private') ";
  } else {
    $navQ .= " AND post_status = 'live' ";
  }

    $navQ .= "
      AND menu_order > 0
    ORDER BY menu_order ASC, post_alt_title ASC
  ";

  // First level
  $fetchNav = $pdo->prepare($navQ);
  $fetchNav->execute([1]);

  $nCount = 0;


  while ( $row = $fetchNav->fetch() ) {
    $nCount++;

    $navOutput .= '<li class="nav__item nv-'.$row['_id'].'"><a ' . ($row['_id'] == 3 ? 'data-toggle="book-online"' : '').' class="nav__link inner-overlay';
    if ( $row['_id'] == $post_id || (isset($parent_id) && $row['_id'] == $parent_id) ) {
      $navOutput .= ' nav__link--active ';
    }

    $href = !empty($row['external']) ? $row['external'] : buildPath($row['_id']);

    $navOutput .= '" href="'.$href.'" title="'.$row['post_alt_title'].'">' . showNavIcon($row['nav_icon']) . '<span class="nav__link__text">' . $row['post_alt_title'] . '</span>';

    // Second level
    $fetchNav2 = $pdo->prepare($navQ);
    $fetchNav2->execute([$row['_id']]);

    if ($fetchNav2->rowCount() > 0) {
      $nSubCount = 0;
      $navOutput .= '<i class="nav__parent-icon"></i></a><div class="__nav__group">';
      
      while ($row2 = $fetchNav2->fetch()) {
        $nSubCount++;
        // $href = buildPath($row2['_id']);
        $href = !empty($row2['external']) ? $row2['external'] : buildPath($row2['_id']);
        $navOutput .= '<a class="nav__group__link" href="'.$href.'"> '.$row2['post_alt_title'].'</a>';
      }

      $navOutput .= '</div>';
    }
    else {
      $navOutput .= '</a>';
    }

    $navOutput .= '</li>';
  }
?>

<nav id="nav" class="nav" data-name="nav">
  <ul class="reset--list nav__list nav__list--primary">
    <?php echo $navOutput;?>
  </ul>

  <button type="button" id="nav-menu-toggle" class="nav__toggle inner-overlay" title="Open main menu"><i class="icon-bars"></i></button>
  <?php $fetchNav = $pdo->query("SELECT * FROM er_posts WHERE _id IN(22,13,14,15,16,17,18) ORDER BY menu_order ASC, post_alt_title ASC"); ?>

  <div class="mobile-nav" data-name="mobile-nav" data-hidden="mobile-nav">
    <ul class="reset--list nav__list">
      <?php echo $navOutput; ?>
    </ul>

    <?php if ($fetchNav->rowCount() > 0) { ?>
      <ul class="reset-list nav__list nav__list--secondary">
        <?php while ($navLink = $fetchNav->fetch()) { ?>
          <li class="nav__item">
            <a href="<?php echo buildPath($navLink['_id']); ?>" title="<?php echo $navLink['post_title']; ?>" class="nav__link nav__link--small"><?php echo $navLink['post_alt_title']; ?></a>
          </li>
        <?php } ?>
      </ul>
    <?php } ?>

    <ul class="reset--list social-media">
      <?php if (!empty($twitter_handle)) { ?><li class="social-media__item"><a href="https://www.twitter.com/<?php echo $twitter_handle; ?>" title="Follow Fat Hippo on Twitter" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-twitter"></i></a></li><?php } ?>
      <?php if (!empty($instagram_handle)) { ?><li class="social-media__item"><a href="https://www.instagram.com/<?php echo $instagram_handle; ?>" title="Follow Fat Hippo on Instagram" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-instagram"></i></a></li><?php } ?>
      <?php if (!empty($facebook_handle)) { ?><li class="social-media__item"><a href="<?php echo $facebook_handle; ?>" title="Like Fat Hippo on Facebook" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-facebook"></i></a></li><?php } ?>
      <?php if (!empty($spotify_handle)) { ?><li class="social-media__item"><a href="<?php echo $spotify_handle; ?>" title="Play Fat Hippo pre-burger songs!" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-spotify"></i></a></li><?php } ?>
    </ul>
  </div>
</nav>