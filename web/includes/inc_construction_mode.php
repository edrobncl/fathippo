<div class="construction-mode-overlay" style="z-index:999999999999999;position:fixed;top:0;left:0;width:100%;height:100vh;background:rgba(0,0,0, .925);">
<div class="construction-mode-block" style="z-index:9999999999999999;position: fixed;top: 0;left: 5%;width: 90%;height: 100vh;display: flex;align-items: center;justify-content: center;">
    <div class="construction-mode-content" style="width: 100%;max-width: 600px;margin: 0 auto;position: relative; color:#fff;  text-align:center;">
        <div style="font-size:28px;"><i style="color:yellow" class="fa fa-warning"></i> <span style="font-weight:bold; color:#fff;">Sorry, this page is currently in development.</span></div>
        <a style="display:block;margin-top:2em;font-size:20px;" ref="#" onclick="history.go(-1);return false;">&crarr; Go back</a>
    </div>
</div>
</div>