<?php
	// Check the category to see if Call to action should show
	$showCTA = getMeta($menuCategoryId, 'show_cta');

	if ( ! empty($showCTA) && $showCTA == 1 ) { ?>		
		<div class="category-cta">
			<p class="category-cta__title">Ready To Eat?</p>
			<div class="category-cta__links">
				<div class="category-cta__item">
					<a href="<?= buildPath(3) ?>" title="Book Online" data-toggle="book-online" class="category-cta__link inner-overlay">
						<i class="icon-calendar-plus d-none d-lg-inline category-cta__link__icon"></i>
						<span class="category-cta__link__text"><?= showName(3) ?></span>
					</a>
				</div>
				<div class="category-cta__item">
					<a href="http://onelink.to/npwksw" target="_blank" title="Click and Collect" class="category-cta__link inner-overlay">
						<i class="icon-cursor-click d-none d-lg-inline category-cta__link__icon"></i>
						<span class="category-cta__link__text"><?= showName(95) ?></span>
					</a>
				</div>
				<div class="category-cta__item">
					<a href="https://fathippo.order.deliveroo.co.uk/" target="_blank" title="Order Delivery" class="category-cta__link inner-overlay">
						<i class="icon-deliveroo d-none d-lg-inline category-cta__link__icon"></i>
						<span class="category-cta__link__text"><?= showName(94) ?></span>
					</a>
				</div>
				<div class="category-cta__item">
					<a href="<?= buildPath(6); ?>" title="View all our locations" class="category-cta__link inner-overlay">
						<i class="icon-map-pin d-none d-lg-inline category-cta__link__icon"></i>
						<span class="category-cta__link__text"><?= showName(6) ?></span>
					</a>
				</div>
			</div>
		</div>
		<?php
	}
?>