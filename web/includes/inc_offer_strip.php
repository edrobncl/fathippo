<?php
  // ====================================
  //  Check for any popups on the page
  // ====================================
  $getPopups = '
    SELECT _id, post_alt_title AS unique_code,
      (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "popup_title") AS title,
      (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "popup_tagline") AS tagline,
      (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "popup_body") AS body,
      (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "popup_footer") AS footer
    FROM er_posts
    WHERE post_type = "popup" AND ';

  if ($post_layout !== 'location') {
    $getPopups .= '_id IN (
      SELECT post_id 
      FROM er_postmeta
      WHERE meta_name = "popup_page_list" 
      AND meta_value LIKE :page_id
    ) AND ';
  }

  $getPopups .= 'post_status ';
  $getPopups .= ((!empty($_SESSION['eruid'])) ? ' IN("private")' : ' = "private"' ).  ' LIMIT 1';

  // ====================================
  //  Execute Query
  // ====================================
  $getPopups = $pdo->prepare($getPopups);

  if ($post_layout !== 'location') {
    $getPopups->execute([
      'page_id' => '%"' . $post_id . '"%'
    ]);
  }
  else {
    $getPopups->execute();
  }

  // Only Add Popups if one is found
  if ($getPopups->rowCount() > 0) {
    addAsset('popup');
    $popup = $getPopups->fetch();
?>

<div id="offer-strip" class="offer-strip bg-diagonal">
  <div class="inner inner--narrow">
    <p class="offer-strip__text"><?php echo !empty($popup['tagline']) ? $popup['tagline'] : ''; ?>
    <a href="#" data-toggle="modal_<?php echo $popup['_id']; ?>" class="offer-strip__button inner-overlay">Book Now <i class="icon-arrow-right"></i></a></p>
  </div>
</div>

<?php } ?>