<?php

/**
 * Show the small icon in the nav
 * @param array - list of post details
 * @return void
 */
function showNavIcon($icon) {
  $icons = [
    'icon-calendar-plus',
    'icon-deliveroo',
    'icon-cursor-click',
  ];

  if (!empty($icon) && in_array($icon, $icons)) {
    return '<i class="'.$icon.' d-none d-lg-inline"></i>';
  }
}

/**
 * Function to store assets to load
 * @param string - name of the asset to store
 * @param string - type of the asset to store
 * @return void
 */
function addAsset($asset) {
  global $assets;

  if (!isset($assets[$asset])) {
    // $assets = [];
  }

  $assets[] = $asset;
}

/**
 * Function to check if the asset is required.
 * Unset the asset after the use.
 * @param string - type of the asset to return
 * @return boolean
 */
function checkAsset($asset) {
  global $assets;

  if (empty($assets)) {
    return false;
  }

  foreach ($assets as $key => $value) {
    if ($value == $asset) {
      unset($assets[$asset]);
      return true;
    }
  }

  return false;
}

/**
 * Function to print address
 * @param array - location array
 * @return void
 */
function showAddress($listing) {
  ?><address class="location-listing__detail__text"><?php
    echo !empty($listing['addr_line_1']) ? $listing['addr_line_1'] : '';
    echo !empty($listing['addr_line_2']) ? ', ' . $listing['addr_line_2'] : '';
    echo !empty($listing['addr_line_3']) ? ', ' . $listing['addr_line_3'] : '';
    echo !empty($listing['addr_line_3']) ? ', ' . $listing['addr_line_3'] : '';
    echo !empty($listing['city']) ? ', ' . $listing['city'] : '';
    echo !empty($listing['postcode']) ? ', ' . $listing['postcode'] : '';
  ?></address><?php
}

/**
 * Function to show location amenities
 * @param string - JSON string
 * @return void
 */
function showLocationAmenities($amenities) {
  $amenities = json_decode($amenities, 1);
  ?><div class="amenities">
    <div class="amenities__title">Amenities</div>
    <div class="amenities__icons">
      <?php
        foreach ( $amenities as $amenity ) {
          switch ( (int) $amenity ) {
            case 1: ?><span class="amenities__icon" data-toggle="tooltip" data-content="Child Friendly"><i class="icon-face"></i></span><?php break;
            case 2: ?><span class="amenities__icon" data-toggle="tooltip" data-content="Dog Friendly"><i class="icon-dog"></i></span><?php break;
            case 3: ?><span class="amenities__icon" data-toggle="tooltip" data-content="Free WiFi"><i class="icon-wifi"></i></span><?php break;
            case 4: ?><span class="amenities__icon" data-toggle="tooltip" data-content="Halal Options"><i class="icon-halal"></i></span><?php break;
            case 5: ?><span class="amenities__icon" data-toggle="tooltip" data-content="Outdoor Seating"><i class="icon-outdoor-seating"></i></span><?php break;
            case 6: ?><span class="amenities__icon" data-toggle="tooltip" data-content="Wheelchair Accessible"><i class="icon-disabled-access"></i></span><?php break;
          }      
        }
      ?>
    </div>
  </div><?php
}

/**
 * Function to show the find a hippo card
 * @param array - options
 * @return void
 */
function showFindAHippoCard($options = []) {
  ?><a href="<?php echo $options['url'] ?? buildPath(6); ?>" title="Find a Fat Hippo" class="card inner--overlay">
    <div class="card__icon">
      <img src="<?php echo $options['image'] ?? '/images/burgerpin.svg'; ?>" loading="lazy" alt="Burger Pin"  class="card__icon__image" width="140" height="140" />
    </div>
    <div class="card__content">
      <h2 class="card__title">
        <span class="card__title__one">Find</span><span class="card__title__two">a</span>
        <span class="card__title__three">Hippo</span>
      </h2>
    </div>
    <div class="card__arrow">
      <img src="/images/greencirclechevronright.svg" loading="lazy" alt="Chevron Right" class="card__arrow__image" width="50" height="50" />
    </div>
  </a><?php
}

/**
 * Function to show the book a table card
 * @param array - array of options
 * @return void
 */
function showBookTableCard($options = []) {
  ?><a data-toggle="book-online"<?php echo !empty($options['url']) ? ' data-venue="true"' : ''; ?> href="<?php echo $options['url'] ?? 'https://fathippo.co.uk/book-a-table'; ?>" title="Book your table" rel="noreferrer" class="card inner-overlay">
    <div class="card__icon">
      <img src="<?php echo $options['image'] ?? '/images/calendar.svg'; ?>" loading="lazy" alt="Calendar" class="card__icon__image" width="140" height="140" />
    </div>
    <div class="card__content">
      <h2 class="card__title">
        <span class="card__title__one">Book</span><span class="card__title__two">Your</span>
        <span class="card__title__three">Table</span>
      </h2>
    </div>
    <div class="card__arrow">
      <img data-toggle="book-online" src="/images/greencirclechevronright.svg" loading="lazy" alt="Chevron Right" class="card__arrow__image" width="50" height="50" />
    </div>
  </a><?php
}

/**
 * Function show show click and collect card
 * @param array - array of options
 * @return void
 */
function showClickCollectCard($options = []) {
  ?><a href="<?php echo $options['url'] ?? 'https://fathippo.co.uk/app'; ?>" title="Click and collect" rel="noreferrer" class="card inner-overlay">
    <div class="card__icon">
      <img src="<?php echo $options['image'] ?? '/images/bag.svg'; ?>" alt="Calendar" loading="lazy" class="card__icon__image" width="50" height="50" />
    </div>
    <div class="card__content">
      <h2 class="card__title">
        <span class="card__title__two">Click &amp;</span>
        <span class="card__title__three">Collect</span>
      </h2>
    </div>
    <div class="card__arrow">
      <img src="/images/greencirclechevronright.svg" loading="lazy" alt="Chevron Right" class="card__arrow__image" width="50" height="50" />
    </div>
  </a><?php
}

/**
 * Function show show order delivery card
 * @param array - array of options
 * @return void
 */
function showOrderDeliveryCard($options = []) {
  ?><a href="<?php echo $options['url'] ?? 'https://fathippo.5loyalty.com/order'; ?>" title="Click and collect" rel="noreferrer" class="card inner-overlay">
    <div class="card__icon">
      <img src="<?php echo $options['image'] ?? '/images/calendar.svg'; ?>" alt="Calendar" loading="lazy" class="card__icon__image" width="50" height="50" />
    </div>
    <div class="card__content">
      <h2 class="card__title">
        <span class="card__title__two">Order</span>
        <span class="card__title__three">Delivery</span>
      </h2>
    </div>
    <div class="card__arrow">
      <img src="/images/greencirclechevronright.svg" loading="lazy" alt="Chevron Right" class="card__arrow__image" width="50" height="50" />
    </div>
  </a><?php
}

/**
 * Function show show order delivery card
 * @param array - array of options
 * @return void
 */
function showMenuCard($options = []) {
  ?><a href="<?php echo $options['url'] ?? 'https://fathippo.5loyalty.com/order'; ?>" title="Click and collect" rel="noreferrer" class="card inner-overlay">
    <div class="card__icon">
      <img src="<?php echo $options['image'] ?? '/images/calendar.svg'; ?>" alt="Calendar" loading="lazy" class="card__icon__image" width="50" height="50" />
    </div>
    <div class="card__content">
      <h2 class="card__title">
        <span class="card__title__two">View</span><br/>
        <span class="card__title__three">Menu</span>
      </h2>
    </div>
    <div class="card__arrow">
      <img src="/images/greencirclechevronright.svg" loading="lazy" alt="Chevron Right" class="card__arrow__image" width="50" height="50" />
    </div>
  </a><?php
}

/**
 * Show menu section name
 * Becuase sections are hardcoded in the ADMIN, we need to figure out
 * what section is associated with the ID...
 * 
 * @param integer - section ID
 * @return string - section name
 */
function showMenuSectionName($id) {
  $sections = [
    1 => 'Starters',
    2 => 'Vegan Starters',
    3 => 'Beef',
    4 => 'Chicken',
    5 => 'Vegan',
    6 => 'Upgrades',
    7 => 'Fast Hippo',
    8 => 'Sides',
    9 => 'Sauces',
    10 => 'Desserts'
  ];

  return isset($sections[$id]) ? $sections[$id] : null;
}

/**
 * 
 */
function showMenuDetailIcon($id) {
  switch ($id) {
    case 1:
        return '/images/icon-vegetarian.svg';
    case 2:
        return '/images/icon-vegetarian-grey.svg';
    case 3:
        return '/images/icon-vegan.svg';
    case 4:
        return '/images/icon-vegan-grey.svg';
    case 5:
        return '/images/icon-gluten-free.svg';
    case 6:
        return '/images/icon-gluten-free-grey.svg';
    case 7:
        return '/images/icon-dairy-free.svg';
    case 8:
        return '/images/icon-dairy-free-grey.svg';
  }
}

function showMenuDetailName($id) {
  switch ($id) {
    case 1:
      return 'Vegetarian';
    case 2:
      return 'Vegetarian (on request)';
    case 3:
      return 'Vegan';
    case 4:
      return 'Vegan (on request)';
    case 5:
      return 'Gluten Free';
    case 6:
      return 'Gluten Free (on request)';
    case 7:
      return 'Dairy Free';
    case 8:
      return 'Dairy Free (on request)';
  }
}




/**
 * Output to the screen the Ingredient with any allergies
 * 
 * @param int $ingredient_id The ID of the ingredient
 */
function outputMenuItemIngredient( int $ingredient_id ) {
  $ingredient = getPost($ingredient_id);
  $allergns = getMeta($ingredient['_id'], 'ingredient_allergens');
  
  echo '<p>' . $ingredient['post_title'] . '<br />';

  if ( empty($allergns) ) {
    echo '</p>';
    return;
  }

  $allergnList = [];
  foreach ( $allergns as $allergn ) {
    $allergn = getPost($allergn);
    $allergnList[] = $allergn['post_title'];
  }

  echo '<span class="modal__dialogue__shaded-text">' . implode(', ', $allergnList) . '</span></p>';
}


/**
 * Retrieve all available menus
 * 
 * @param bool $includeDraft Whether or not to include draft post status in results
 * @return array The Query's Resultant Array or empty array on failure
 */
function getMenus( bool $includeDraft = false ): array {
  global $pdo;

  // Prepare Query
  $getMenusQuery = '
    SELECT *, post_alt_title AS name
    FROM er_posts
    WHERE post_type = "menu" 
  ';

  // Check whether user is an admin
  if ( ! empty($_SESSION['eruid']) ) {
    if ( ! $includeDraft ) {
      $getMenusQuery .= 'AND post_status IN ("live", "unlisted", "private") ';
    }
  } else {
    $getMenusQuery .= 'AND post_status IN("live", "unlisted") ';
  }

  // Finish Query
  $getMenusQuery .= 'ORDER BY menu_order ASC';

  // Execute Query
  $getMenusQuery = $pdo->prepare($getMenusQuery);
  $getMenusQuery->execute();

  // Handle Empty Result
  if ( $getMenusQuery->rowCount() === 0 ) {
    return [];
  }

  // Return Result
  return $getMenusQuery->fetchAll();
}

/**
 * Retrieve all the available categories for a menu
 * 
 * @param int $menuId The ID of the mneu that we are retrieving Categories for
 * @param bool $includeDraft Whether or not to include draft post status in results
 * @return array An array of categories for the menu. Empty array on failure
 */
function getMenuCategories( int $menuId, bool $includeDraft = false ): array {
  global $pdo;

  // Prepare Query
  $getMenuCategoriesQuery = '
    SELECT *, post_title AS name
    FROM er_posts
    WHERE parent_id = :menu_id
      AND post_type = "menu_category"
  ';

  // Determine the Post Status
  if ( ! empty($_SESSION['eruid']) ) {
    if ( ! $includeDraft ) {
      $getMenuCategoriesQuery .= 'AND post_status IN ("live", "private") ';
    }
  } else {
    $getMenuCategoriesQuery .= 'AND post_status = "live" ';
  }

  // Finish Query
  $getMenuCategoriesQuery .= 'ORDER BY menu_order ASC';

  // Execute Query
  $getMenuCategories = $pdo->prepare($getMenuCategoriesQuery);
  $getMenuCategories->execute([
    'menu_id' => $menuId
  ]);

  // Handle Failure
  if ( $getMenuCategories->rowCount() === 0 ) {
    return [];
  }

  // Return Results
  return $getMenuCategories->fetchAll();
}

/**
 * Retrieve the items that are within a category
 * 
 * @param int $categoryId The Category that we are retrieving the menu items of
 * @param bool $includeDraft Whether or not to include draft status items in results
 * @return array An array of category items. Empty array on failure
 */
function getCategoryItems( int $categoryId, bool $includeDraft = false ): array {
  global $pdo;

  // Prepare Query
  $getCategoryItems = '
    SELECT *
    FROM er_posts
    WHERE parent_id = :category_id
      AND post_type = "menu_item"
  ';

  // Handle the post status
  if ( ! empty($_SESSION['feuid']) ) {
    if ( ! $includeDraft ) {
      $getCategoryItems .= 'AND post_status IN ("live", "private") ';
    }
  } else {
    $getCategoryItems .= 'AND post_status = "live" ';
  }

  // Finish Query
  $getCategoryItems .= 'ORDER BY menu_order ASC';

  // Execute Query
  $getCategoryItems = $pdo->prepare($getCategoryItems);
  $getCategoryItems->execute([
    'category_id' => $categoryId
  ]);
  
  // Handle Failure
  if ( $getCategoryItems->rowCount() === 0  ) {
    return [];
  }

  // Return Data
  return $getCategoryItems->fetchAll();
}

/**
 * Retrieve the menu with categories and menu items formatted
 * 
 * @param int $menuId The ID of the menu to retrieve
 * @return array Array with all menu information in. Empty array on failure.
 */
function getMenu( int $menuId ): array {
  $menu = getPost($menuId);
  if ( empty($menu) ) {
    return [];
  }

  // Retrieve Menu Categories 
  foreach ( getMenuCategories($menuId) as $category ) {
    // Add Category Meta
    $category = array_merge($category, getAllMeta($category['_id']));
    
    // Add Category Items
    $category['category_items'] = array_map(function($item) {
      return array_merge($item, getAllMeta($item['_id']));
    }, getCategoryItems($category['_id']));

    // Add Category items to array
    $menu['categories'][] = $category;
  }


  return $menu;
}

/*
 * Function to check for special dates, i.e., Bank Holidays and Christmas
 * and then show correct times, if they were defined in the CMS.
 */
function show_correct_times($time, $country = 'england-and-wales') {
  global $pdo, $post_id;
  $today = date('Y-m-d');

  if (!isset($_SESSION['holidays'])) {
    $holidays = file_get_contents('https://www.gov.uk/bank-holidays.json');
    $_SESSION['holidays'] = json_decode($holidays, 1);
  }

  if ($country == 'england' || $country == 'wales') {
    $country = 'england-and-wales';
  }

  $stmt = $pdo->prepare("
    SELECT meta_value AS time
    FROM er_postmeta
    WHERE post_id = $post_id
    AND meta_name = ?
  ");

  foreach ($_SESSION['holidays'][$country]['events'] as $holiday) {
    if ($holiday['date'] == $today && in_array($holiday['title'], ['Christmas Day'])) {
      $stmt->execute(['opening_christmas']);

      if ($stmt->rowCount() > 0) {
        $opening = $stmt->fetch()['meta_value'];
        return $opening['time'] . ' (' . $holiday['title'] . ')';
      }
    }

    if ($holiday['date'] == $today) {
      $stmt->execute(['opening_bank_holiday']);

      if ($stmt->rowCount() > 0) {
        $opening = $stmt->fetch()['meta_value'];
        return $opening['time'] . ' (' . $holiday['title'] . ')';
      }
    }
  }

  return $time;
}

/**
 * Function to show page feature
 * @param string back
 * @param string title
 * @param string tagline
 */
function showPageFeature($back = 'Find Us', $title = 'Page Feature Title', $tagline = 'Page Feature Tagline') {
  ?><div class="feature">
    <p class="feature__back"><?php echo $back; ?></p>
    <div class="feature__front">
      <h2 class="feature__title"><?php echo $title; ?></h2>
      <div class="feature__tagline"><?php echo $tagline; ?></div>
    </div>
  </div><?php
}



