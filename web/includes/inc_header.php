<?php if (isset($construction_mode) && $construction_mode == "yes" && !DEV) include FS_ROOT.'includes/inc_construction_mode.php'; ?>
<header id="header" class="header">
  <div class="inner header__inner header__container">
    <div class="header__logo">
      <a title="Visit our Homepage" id="logo" href="/" class="header__logo__link">
        <img src="<?php echo $admin_logo; ?>" width="300" height="200" alt="<?php echo $site_name; ?>: click for homepage" class="header__logo__image" />
      </a>
    </div>
    <div class="header__nav">
      <?php include FS_ROOT.'includes/inc_nav.php';?>
    </div>
  </div>
</header>