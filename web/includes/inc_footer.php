
<?php 

$page_ids = $pdo->query("SELECT post_id FROM er_postmeta WHERE meta_name = 'show_footer' AND meta_value = 'yes'")->fetchAll();
$page_fetch = $pdo->prepare("SELECT *,
    (SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = 'footer_section') AS section,
    (SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = 'external_slug') AS ex_slug
  FROM er_posts WHERE post_status = 'live' AND _id = ?"
);
if (!empty($page_ids)) {
  $pages = [];
  foreach ($page_ids as $id) {
    $page_fetch->execute([$id['post_id']]);
    if ($page_fetch->rowCount() > 0) {
      while ($row = $page_fetch->fetch()) {
        $pages[] = $row;
      }
    }
  }
  usort($pages, function($a, $b){
    if ($a['menu_order'] == $b['menu_order']) {
      return strcmp($a['post_alt_title'], $b['post_alt_title']);
    } else if ($a['menu_order'] == 0) {
      return $b['menu_order'] - $a['menu_order'];
    } else if ($b['menu_order'] == 0) {
      return $b['menu_order'] - $a['menu_order'];
    } else {
      return $a['menu_order'] - $b['menu_order'];
    }
  });
}

// Fetch Concessions
$venues = fetchResource([
  'post_type' => 'location',
  'post_status' => 'live',
  'meta' => [
    'office_address1' => 'addr_line_1',
    'office_address2' => 'addr_line_2',
    'office_address3' => 'addr_line_3',
    'office_city' => 'city',
    'office_postcode' => 'postcode',
    'book_table',
  ]
]);

if (isset($venues) && $venues['count'] > 0) {
  addAsset('toggle'); ?>

  <div class="book-online" data-modal="book-online">
    <div class="book-online__wrap">
      <iframe data-iframe="book-venue" class="book-online__iframe" title="Book a table"></iframe>

      <div class="book-online__venues">
        <div class="book-online__title">Online Reservations</div>
        <?php foreach ($venues['data'] as $venue) { ?>
          <div class="book-online__venue">
            <a class="book-online__venue__title" href="<?php echo $venue['book_table']; ?>" title="Book <?php echo $venue['post_title']; ?>" data-toggle="book-venue"><?php echo $venue['post_alt_title']; ?></a>
            <?php if (false) { ?><div class="book-online__venue__address"><?php echo $venue['addr_line_1'] . (!empty($venue['addr_line_2']) ? ', ' . $venue['addr_line_2'] : '') . (!empty($venue['addr_line_3']) ? ', ' . $venue['addr_line_3'] : '') . (!empty($venue['city']) ? ', ' . $venue['city'] : '') . (!empty($venue['postcode']) ? ', ' . $venue['postcode'] : ''); ?></div><?php } ?>
          </div>
        <?php } ?>
      </div>

      <button class="book-online__button inner-overlay" data-close="book-online" title="Close Book Online Drawer"><i class="icon-times"></i></button>
    </div>

    <picture>
      <source media="(max-width: 575px)" srcset="/images/eat--600px.webp" type="image/webp">
      <source media="(max-width: 575px)" srcset="/images/eat--600px.png" type="image/png">
      <source srcset="/images/eat.webp" type="image/webp">
      <img src="/images/eat.png" loading="lazy" alt="Eat" class="book-online__image" width="190" height="200">
    </picture>
  </div>
<?php } ?>

<footer id="footer">
  <div class="footer__upper">
    <div class="inner">
      <div class="footer__container">

        <?php if (!empty($pages)) { ?>
          <div class="footer__col footer__col--eat">
            <h3 class="footer__title">Eat with us</h3>
            <ul class="reset--list footer__nav">
              <? foreach ($pages as $page) { 
                if (!empty($page['section']) && $page['section'] == 1) { 
                  if (!empty($page['ex_slug'])) { 
                    $link = $page['ex_slug'];
                  } else {
                    $link = buildPath($page['_id']);
                  } ?>
                  <li class="footer__nav__item">
                    <a href="<?=$link;?>" title="<?=$page['post_alt_title'];?>" class="footer__nav__link"<?php if ($page['post_alt_title'] == 'Book a Table') echo ' data-toggle="book-online"' ?>><?=$page['post_alt_title'];?></a>
                  </li>
                <? }
              } ?>
            </ul>
          </div>
        <?php } ?>

        <?php if (!empty($pages)) { ?>
          <div class="footer__col footer__col--learn">
            <h3 class="footer__title">Learn about us</h3>
            <ul class="reset--list footer__nav footer__nav--learn">
              <? foreach ($pages as $page) { 
                if (!empty($page['section']) && $page['section'] == 2) { 
                  if (!empty($page['ex_slug'])) { 
                    $link = $page['ex_slug'];
                  } else {
                    $link = buildPath($page['_id']);
                  } ?>
                  <li class="footer__nav__item">
                    <a href="<?=$link;?>" title="<?=$page['post_alt_title'];?>" class="footer__nav__link"><?=$page['post_alt_title'];?></a>
                  </li>
                <? }
              } ?>
            </ul>
          </div>
        <?php } ?>

        <div class="footer__col footer__col--social">
          <ul class="reset--list social-media">
            <?php if (!empty($twitter_handle)) { ?><li class="social-media__item"><a href="https://www.twitter.com/<?php echo $twitter_handle; ?>" title="Follow Fat Hippo on Twitter" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('twitter-brands'); ?></a></li><?php } ?>
            <?php if (!empty($instagram_handle)) { ?><li class="social-media__item"><a href="https://www.instagram.com/<?php echo $instagram_handle; ?>" title="Follow Fat Hippo on Instagram" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('instagram-brands'); ?></a></li><?php } ?>
            <?php if (!empty($facebook_handle)) { ?><li class="social-media__item"><a href="<?php echo $facebook_handle; ?>" title="Like Fat Hippo on Facebook" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('facebook-f-brands'); ?></a></li><?php } ?>
            <?php if (!empty($spotify_handle)) { ?><li class="social-media__item"><a href="<?php echo $spotify_handle; ?>" title="Play Fat Hippo pre-burger songs!" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('spotify-brands'); ?></a></li><?php } ?>
            <?php if (!empty($whatsapp_handle)) { ?><li class="social-media__item"><a href="https://wa.me/<?php echo $whatsapp_handle; ?>" title="Message Fat Hippo on WhatsApp" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('whatsapp-brands'); ?></a></li><?php } ?>
            <?php if (!empty($pinterest_handle)) { ?><li class="social-media__item"><a href="<?php echo $pinterest_handle; ?>" title="Follow Fat Hippo on Pinterest" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('pinterest-p-brands'); ?></a></li><?php } ?>
            <?php if (!empty($linkedin_handle)) { ?><li class="social-media__item"><a href="<?php echo $linkedin_handle; ?>" title="Follow Fat Hippo on LinkedIn" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('linkedin-in-brands'); ?></a></li><?php } ?>
            <?php if (!empty($flickr_handle)) { ?><li class="social-media__item"><a href="<?php echo $flickr_handle; ?>" title="Follow Fat Hippo on Flickr" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('flickr-brands'); ?></a></li><?php } ?>
            <?php if (!empty($youtube_handle)) { ?><li class="social-media__item"><a href="<?php echo $youtube_handle; ?>" title="Subscribe to Fat Hippo on YouTube" target="_blank" class="social-media__link" rel="noreferrer"><?php showIcon('youtube-brands'); ?></a></li><?php } ?>
          </ul>
          <img src="/images/bag-1.svg" loading="lazy" alt="Bag" class="footer__illustration" width="120" height="120" />
        </div>

      </div>
    </div>
  </div>

  <div class="footer__lower">
    <div class="inner footer__container">
      <div class="footer__copyright">
        <p>&copy; Copyright 2010<?php echo (date('Y') > 2020) ? ' - ' . date('Y') : ''; ?> <a class="copyright-link" href="/"><?=$site_name?></a>.<?php if (!empty($reg_number)) { ?> Registered in England - No. <?php echo $reg_number; ?>.<?php } ?> <?php if (!empty($site_address)) echo $site_address; if (!empty($site_postcode)) echo ', ' . $site_postcode; ?></p>
      </div>
      <p class="footer__credit">Website by <a href="https://www.edwardrobertson.co.uk/" title="Edward Robertson Digital Design" target="_blank" rel="noreferrer">Edward Robertson</a>.</p>
    </div>
  </div>
</footer>

<?php
  // Include the Popups
  include_once FS_ROOT . 'includes/inc_popup.php';
?>
