<article class="location-listing">
  <a href="<?php echo buildPath($location['_id']); ?>" title="Visit <?php echo $location['post_title']; ?>" class="location-listing__thumbnail">
    <picture>
      <?php if (!empty($location['large_thumbnail'])) { ?>
        <source media="(max-width: 768px)" srcset="<?php echo showPostImage($location['large_thumbnail'], 'webp'); ?>" type="image/webp" />
        <source media="(max-width: 768px)" srcset="<?php echo showPostImage($location['large_thumbnail']); ?>" type="image/jpg" />
      <?php } ?>
      <source srcset="<?php echo showPostImage($location['thumbnail'], 'webp'); ?>" type="image/webp" />
      <img src="<?php echo showPostImage($location['thumbnail']); ?>" alt="<?php echo $location['post_title']; ?>" width="320" height="211" />
    </picture>
  </a>

  <div class="location-listing__content">

    <div class="location-listing__content__title">
      <?php if (!empty($location['concession']) && $location['concession'] == 'yes') { ?>
        <div class="location-listing__concession">Concession</div>
      <?php } ?>
      <div class="location-listing__title-wrap">
        <h3 class="location-listing__title"><?php echo $location['post_alt_title']; ?></h3>
      </div>
      <?php if (!empty($location['coming_soon']) && $location['coming_soon'] == 'yes') { ?>
        <div class="location-listing__coming-soon">Coming Soon!</div>
      <?php } ?>
    </div>

    <?php if (!empty($location['addr_line_1']) && !empty($location['opening'])) { ?>
      <div class="location-listing__detail">
        <i class="location-listing__detail__icon icon-map-pin"></i>
        <?php showAddress($location); ?>
      </div>
      <div class="location-listing__detail">
        <i class="location-listing__detail__icon icon-clock"></i>
        <div class="location-listing__detail__text">Today: <?php echo $location['opening']; ?></div>
      </div>
    <?php } ?>
    <?php if ( ! empty($location['email_addr']) ) { ?>
      <div class="location-listing__detail">
        <i class="location-listing__detail__icon icon-envelope"></i>
        <a class="location-listing__detail__text" href="mailto:<?= $location['email_addr'] ?>"><?= $location['email_addr'] ?></a>
      </div>
    <?php } ?>
    <?php if ( ! empty($location['telephone']) ) { ?>
      <div class="location-listing__detail">
        <i class="location-listing__detail__icon icon-phone"></i>
        <a class="location-listing__detail__text" href="tel:<?= str_replace(' ', '', $location['telephone']) ?>"><?= $location['telephone'] ?></a>
      </div>
    <?php } ?>
  </div>
  
  <div class="location-listing__actions">
    <?php if (!empty($location['book_table'])) { ?>
      <a data-toggle="book-online" data-venue="true" href="<?php echo $location['book_table']; ?>" title="Book a table at <?php echo $location['post_title']; ?>" rel="noreferrer" target="_blank" class="location-listing__action inner-overlay">
        <i class="icon-calendar-plus"></i>
        Book a Table
      </a>
    <?php } ?>

    <?php if (!empty($location['click_collect'])) { ?>
      <a href="https://fathippo.co.uk/app" title="Order and Collect FatHippo" target="_blank" rel="noreferrer" class="location-listing__action inner-overlay">
        <i class="icon-cursor-click"></i>
        Click &amp; Collect
      </a>
    <?php } ?>

    <?php if (!empty($location['order_delivery'])) { ?>
      <a href="<?php echo $location['order_delivery']; ?>" title="Order FatHippo delivery" target="_blank" rel="noreferrer" class="location-listing__action inner-overlay">
        <i class="icon-deliveroo"></i>
        Order Delivery
      </a>
    <?php } ?>

    <a href="<?php echo buildPath($location['_id']); ?>" title="" class="location-listing__action inner-overlay">
      <i class="icon-chevron-right"></i>
      Find out More
    </a>
  </div>
</article>