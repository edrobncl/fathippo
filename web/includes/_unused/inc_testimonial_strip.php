<?php
	// ====================================
	// Check for any testimonials for this page
	// ====================================
	$getTestimonials = '
		SELECT *,
			(SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = "testimonial_content") AS content,
			(SELECT meta_value FROM er_postmeta WHERE er_posts._id = post_id AND meta_name = "testimonial_image") AS image
		FROM er_posts
		WHERE _id IN (
				SELECT post_id
				FROM er_postmeta
				WHERE meta_name = "testimonial_page_list"
					AND meta_value LIKE :post_id
			)
			AND post_type = "testimonial" ';

	if ( ! empty($_SESSION['wsid']) ) {
		$getTestimonials .= ' AND post_status IN ("live", "private") ';
	} else {
		$getTestimonials .= ' AND post_status = "live" ';
	}

	$getTestimonials .= '
		ORDER BY menu_order ASC
	';

	$getTestimonials = $pdo->prepare($getTestimonials);
	$getTestimonials->execute([
		'post_id' => '%"' . $post_id . '"%'
	]);


	// ====================================
	//  Show Testimonials Strip
	// ====================================
	if ( $getTestimonials->rowCount() > 0 ) { ?>
		<section class="section strip testimonial-strip">
			<h3 class="strip__title">Testimonials</h3>
			<p class="strip__content">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laborum eum cupiditate delectus ipsam quam quaerat mollitia libero.</p>

			<div class="testimonials">
				<?php
					while ( $testimonial = $getTestimonials->fetch() ) {
						$testimonialImage = json_decode($testimonial['image'] ?? '[]', true);
						?>
						<div class="testimonial">
							<?php if ( ! empty($testimonialImage) ) { ?>
								<div class="testimonial__image" style="background:url('<?= $testimonialImage['post_slug'] ?>') center;"></div>
							<?php } ?>
							<div class="testimonial__content"><?= $testimonial['content'] ?></div>
							<div class="testimonial__footer">
								<span class="testimonial__author"><?= $testimonial['post_title'] ?></span> -
								<span class="testimonial__published"><?= date('jS F Y', strtotime($testimonial['post_alt_title'])) ?></span>
							</div>
						</div>
						<?php
					}
				?>
			</div>
		</section>
		<?php
	}
?>