<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]--><head>
	<meta charset="utf-8" />  
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="author" content="Edward Robertson - www.edwardrobertson.co.uk" />
  <meta name="description" content="<?php if (isset($meta_description)) echo $meta_description; ?>" />
  <meta name="msapplication-TileImage" content="/images/apple-touch-icon-precomposed.png" />
  <meta name="msapplication-TileColor" content="#fff" />

  <title><?php if (DEV) echo $post_layout.":: "; ?><?php if (isset($meta_title)) echo $meta_title; ?></title>

  <style><?php include 'css/critical.min.css'; ?></style>

  <? // Preload the general CSS and load after critical - to improve loading times ?>
  <link rel="preload" href="/css/<?=$css_js_filename;?>.min.css" as="style" onload="this.load=null;this.rel='stylesheet'" />
  <?php if ($post_id !== 1) { ?><link rel="preload" href="/css/additional.min.css?v=<?php echo filemtime(FS_ROOT.'css/additional.min.css'); ?>" as="style" onload="this.load=null;this.rel='stylesheet'" /><?php } ?>

  <noscript>
    <link rel="stylesheet" href="/css/<?=$css_js_filename;?>.min.css" />
    <?php if ($post_id !== 1) { ?><link href="/css/additional.min.css" /><?php } ?>
    <link rel="stylesheet" href="https://use.typekit.net/bii3mhn.css" />
  </noscript>

  <? // Some web browsers (i.e. Firefox) don't support preload so put in as a fallback ?>
  <link rel="stylesheet" href="/css/<?php echo $css_js_filename; ?>.min.css?v=<?php echo filemtime(FS_ROOT.'css/'.$css_js_filename.'.min.css'); ?>" media="all" />
  <?php if ($post_id !== 1) { ?><link rel="stylesheet" href="/css/additional.min.css" media="all" /><?php } ?>

  <?php if (file_exists(FS_ROOT.'css/custom.css')) { ?><link rel="stylesheet" href="/css/custom.css" media="all"><?php } ?>
  <?php if (checkAsset('slick')) { ?><link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@accessible360/accessible-slick@1.0.1/slick/slick.min.css" media="all" /><?php } ?>
  <link rel="shortcut icon" href="/favicon.ico" />
  <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
   
  <!--[if lte IE 7]><script src="/js/lte-ie7.js"></script><![endif]-->
  <!--[if lt IE 9]>
      <script src="/js/respond.min.js"></script>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script src="/js/modernizr-2.5.3.min.js"></script>
  <![endif]-->

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5VGFQWX');</script>
  <!-- End Google Tag Manager -->

	<?php if (($ga_code!='' && !is_null($ga_code)) ) { ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', '<?php echo $ga_code; ?>', 'auto');
      ga('send', 'pageview');
    </script>
  <?php } ?>

  <?php if (!DEV && $gst_code != '' && !is_null($gst_code)) { ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $gst_code; ?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '<?php echo $gst_code; ?>');
    </script>
  <?php } ?>

  <?php include_once(FS_ROOT."includes/inc_open_graph.php"); ?>
  <?php if (isset($custom_header_js)) echo '<script>'.$custom_header_js.'</script>'; ?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VGFQWX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <?php if (isset($facebook_pixel)) echo $facebook_pixel; ?>
  <div id="skip-to-main"><a href="#main">skip to main content</a></div>
  <div id="page-wrap">