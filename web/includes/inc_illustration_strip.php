<?php
  switch ($post_id) {
    case 879:
      $image_name = 'stuff-face-burger';
      break;

    case 880:
      $image_name = 'beer';
      break;

    case 881:
      $image_name = 'hungry-hippo';
      break;
    
    case 19:
    case 21:
      $image_name = 'megaphone';
      break;

    case 20:
      $image_name = 'burger-bury-head';
      break;
  }

  if (isset($image_name)) {
?>

<div class="illustration illustration">
  <div class="inner illustration__inner">
    <img src="/images/<?php echo $image_name; ?>.svg" alt="Stuff Face Burger" width="140" height="140" class="illustration__image" />
  </div>
</div>

<?php } ?>