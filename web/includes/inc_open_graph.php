<?
	switch ( $post_type ) {
    	case 'page':
    	    $og_set_image = 'header_image';
    	    break;
        case 'product':
    	    $og_set_image = 'product_thumbnail';
    	    break;
        default:
    	    $og_set_image = $post_type . '_image';
	}
	
	if ( isset($og_set_image) ) {
    	$getOgImage = $pdo->prepare("SELECT meta_value FROM er_postmeta WHERE post_id = ? AND meta_name = ?");
    	$getOgImage->execute([$post_id,$og_set_image]);
    	
    	if ( $has_og_image = $getOgImage->fetch() ) {
        	$og_image_json = json_decode($has_og_image['meta_value'],true);
        	$og_image = rtrim(SITE_URL,"/").$og_image_json['post_slug'];
    	}
	}
	
	$og_title = (isset($meta_title)) ? $meta_title : $post_title . ' ' . $append_meta_title;
	$og_desc = (isset($meta_description)) ? $meta_description : $post_title . ' ' . $append_meta_description;
	$og_url = rtrim(SITE_URL,"/").(($post_id == 1)?'':buildPath($post_id));
	$og_type = $post_type;
?>
        
    <meta property="og:title" content="<?=$og_title;?>"> 
    <meta property="og:description" content="<?=$og_desc;?>"> 
    <meta property="og:type" content="<?=$og_type;?>">
    <meta property="og:url" content="<?=$og_url;?>">
    <? if ( isset($og_image) ) { ?><meta property="og:image" content="<?=$og_image?>">
    <? } ?><meta property="og:site_name" content="<?=$site_name?>">
    
    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="<?=$og_url?>">
    <meta name="twitter:title" content="<?=$og_title?>">
    <meta name="twitter:description" content="<?=$og_desc;?>">
    <? if ( isset($og_image) || isset($og_twitter_image) ) { ?><meta name="twitter:image" content="<?=$og_twitter_image ?? $og_image?>">
    <? } ?><? if( isset($twitter_handle) &&  $twitter_handle != "") { ?><meta name="twitter:creator" content="<?=$twitter_handle;?>">
    <? } ?><? if ( isset($og_image) ) { ?><link rel="image_src" href="<?=$og_image?>"/>
    <? } ?>

	<?php if($post_layout == "contact") { ?>
	<script type='application/ld+json'> 
	{
	"@context": "http://www.schema.org",
	"@type": "Organization",
	"name": "<?=$site_name?>",
	"url": "<?=SITE_URL?>",
	"logo": "<?=SITE_URL?>images/company-logo.svg",
	"foundingDate": "2005",
	"founders": [
	{
	"@type": "Person",
	"name": "Director Name"
	}],
	"contactPoint": {
	"@type": "ContactPoint",
	"email": "<?=$site_email?>",
	"telephone": "<?=$site_telephone?>",
	"url": "<?=SITE_URL?>contact-us/",
	"contactType": "Customer Service"
	},
	"description": "General contact description",
	"address": {
	"@type": "PostalAddress",
	"streetAddress": "Address Line 1",
	"addressLocality": "Address Line 2",
	"addressRegion": "Region",
	<? if($site_postcode) { ?>"postalCode": "<?=$site_postcode?>",<? } ?>
	"addressCountry": "United Kingdom"
	},
	"sameAs": [ 
	<? if(isset($twitter_handle) && $twitter_handle != '') { ?>"https://www.twitter.com/<?=$twitter_handle?>",<? } ?>
	<? if(isset($instagram_handle) && $instagram_handle != '') { ?>"https://www.instagram.com/<?=$instagram_handle?>",<? } ?>
	<? if(isset($linkedin_handle) && $linkedin_handle != '') { ?>"<?=$linkedin_handle?>",<? } ?>
	<? if(isset($pinterest_handle) && $pinterest_handle != '') { ?>"<?=$pinterest_handle?>",<? } ?>
	<? if(isset($googleplus_handle) && $googleplus_handle != '') { ?>"<?=$googleplus_handle?>",<? } ?>
	<? if(isset($flickr_handle) && $flickr_handle != '') { ?>"<?=$flickr_handle?>",<? } ?>
	<? if(isset($youtube_handle) && $youtube_handle != '') { ?>"<?=$youtube_handle?>",<? } ?>
	<? if(isset($facebook_handle) && $facebook_handle != '') { ?>"<?=$facebook_handle?>"<? } ?>
	]
	}
	</script>
	<?php } ?>