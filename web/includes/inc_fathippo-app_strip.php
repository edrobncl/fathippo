<section class="section cta">
  <div class="inner">
    <div class="cta__container">
      <div class="cta__image">
        <picture>
          <source media="(max-width: 575px)" srcset="/images/mobile-app--600px.webp" type="image/webp" />
          <source media="(max-width: 575px)" srcset="/images/mobile-app--600px.png" type="image/png" />
          <source srcset="/images/mobile-app.webp" type="image/webp" />
          <img src="/images/mobile-app.png" loading="lazy" alt="FatHippo Mobile App" width="75" height="138" />
        </picture>
      </div>
      <div class="cta__content">
        <h2 class="cta__content__title">Get Burger 'Appy With Our New App!</h2>
        <p class="cta__content__tagline">Fat Hippo rewards and Click + Collect at the press of a button? Download our brand new app available on iOS or Google Play now!</p>
      </div>
      <div class="cta__action">
        <?php showButton('https://fathippo.co.uk/app', 'button--outlined cta__action__button', 'Download', 'a'); ?>
      </div>
    </div>
  </div>
</section>