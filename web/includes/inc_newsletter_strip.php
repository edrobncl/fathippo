<?php $newsletter = true; ?>
<section class="section newsletter">
  <div class="inner newsletter__wrap">
    <div><h2 class="newsletter__title"><span>Stay in the loop</span></h2></div>
    <div><p class="newsletter__tagline">Want us to slide in with breaking burger news? Sign up for exclusive Fat Hippo goss straight to your inbox!</p></div>

    <!-- Need a link to their MailChimp newsletter sign up form -->
    <form class="container newsletter__form js-cm-form" id="subForm" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="5B5E7037DA78A748374AD499497E309EA4E11E358334A9B0DB8549E2D157C4565EE9C49935ECEC8C497ECE8546E39B0A9E72F74E8FC6C33617CF2F9245FB6491">
      <div class="newsletter__field">
        <label class="newsletter__field__label" for="cm-name">Full Name</label>
        <input aria-label="Name" maxlength="200" class="newsletter__field__input" name="cm-name" id="fieldName" required />
      </div>
      <div class="newsletter__field">
        <label class="newsletter__field__label" for="cm-ukkdhdi-ukkdhdi">Email Address</label>
        <input autocomplete="Email" aria-label="Email" maxlength="200" class="newsletter__field__input js-cm-email-input qa-input-email" name="cm-ukkdhdi-ukkdhdi" id="fieldEmail email" type="email" required />
      </div>
      <div class="newsletter__action">
        <?php showButton(null, 'button--outlined', 'Sign me up', 'button'); ?>
      </div>
    </form>
  </div>
</section>