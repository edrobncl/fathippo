<?php
  if ($post_layout !== 'location') {

    switch ($post_layout) {
      case 'fleet':
        $carousel_slider = 'event_slider';
        break;

      case 'location':
        $carousel_slider = 'location_gallery';
        break;

      default:
        $carousel_slider = 'homepage_slider';
        break;
    }

    $carousel = fetchResource([
      'post_type' => $carousel_slider,
      'post_status' => 'private',
      'meta' => [
        'homepage_slide_image' => 'image',
        'homepage_slide_href' => 'slide_href',
        'text_color'
      ]
    ]);

  } else if (!empty($location_gallery)) {
    $carousel['data'] = json_decode($location_gallery, 1);
    $carousel['count'] = count($carousel['data']);

    foreach ($carousel['data'] as $key => $image) {
      $carousel['data'][$key]['image'] = $image['post_slug'];
    }
  }

  if (isset($carousel) && $carousel['count'] > 0) {
    // We're "telling" inc_js.php what assets we want for this layout.
    addAsset('carousel'); ?>

  <div class="section section--hero">
    <div class="carousel carousel--<?php echo $post_layout; ?> bottom-edge">
      <div id="carousel" class="carousel__slides">
        <?php foreach ($carousel['data'] as $slide) { ?>
          <div class="carousel__slide carousel__slide--<?php echo $post_layout; ?>">
            <picture>
              <source srcset="<?php echo showPostImage($slide['image'], 'webp'); ?>" type="image/webp" />
              <img src="<?php echo showPostImage($slide['image']); ?>" alt="<?php echo $slide['post_title']; ?>" />
            </picture>
            
            <?php if ($post_layout !== 'location') { ?>
              <div class="carousel__slide__overlay">
                <div class="carousel__slide__content">
                  <p class="carousel__slide__title"<?php echo !empty($slide['text_color']) ? ' style="color:'.$slide['text_color'].'"' : ''; ?>><?php echo $slide['post_title'];?></p>
                  <?php showButton($slide['slide_href'], 'carousel__slide__button', $slide['post_alt_title'], 'a'); ?>
                </div>
              </div>
            <?php } ?>
          </div>
        <?php } ?>
      </div>

      <div class="carousel__arrows">
        <div class="inner">
          <button type="button" id="prev-arrow" title="Previous slide" class="slick-arrow slick-prev"><?php showIcon('arrow-left-regular'); ?></button>
          <button type="button" id="next-arrow" title="Next slide" class="slick-arrow slick-next"><?php showIcon('arrow-right-regular'); ?></button>
        </div>
      </div>
      <div class="carousel__dots">
        <div id="carousel-dots" class="inner"><!-- js will append dots here --></div>
      </div>
    </div>
  </div>
<?php } ?>