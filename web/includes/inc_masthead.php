<?php
  $header_image = !empty($header_image) 
    ? json_decode($header_image, 1)
    : ['post_slug' => '/images/fallback.jpg'];

  if (!empty($thumbnail_image)) {
    $header_image = !empty($thumbnail_image)
      ? json_decode($thumbnail_image, 1)
      : ['post_slug' => '/images/fallback.jpg'];
  }
?>

<section id="masthead" class="masthead masthead--<?php echo $post_layout; ?>">
	<div class="inner">
    <div class="container">
      <div class="masthead__column--text masthead__wrap">
        <div class="masthead__text">
          <?php array_pop($breadcrumb); if ($parent_id > 1) buildBreadcrumb($breadcrumb); ?>
		      <?php if (!empty($post_title)) { ?><h1 class="masthead__title"><?php echo $post_title; ?></h1><?php } ?>

          <?php if ($post_layout == 'article') { ?>
            <div class="masthead__date">Published on <?php echo date('d.m.Y', strtotime($post_alt_title)); ?></div>
            <div class="masthead__date">Updated on <?php echo date('d.m.Y', strtotime($date_updated)); ?></div>
          <?php } ?>

          <?php if ($post_layout == 'job') { ?>
            <div class="masthead__date"><i class="icon-map-pin"></i><?php echo showName($job_location, 'post_alt_title'); ?></div>
          <?php } ?>
        </div>
      </div>
      <div class="masthead__column--image masthead__wrap">
        <?php showPicture([
          'src' => $header_image['post_slug'],
          'class' => 'masthead__image'
        ]); ?>
      </div>
    </div>
	</div>
</section>