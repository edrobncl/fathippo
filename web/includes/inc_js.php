    </div><!--page-wrap-->
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script>
      jQuery.event.special.touchstart = { setup: function( _, ns, handle ) { this.addEventListener("touchstart", handle, { passive: !ns.indexOf("noPreventDefault") }); }};
      jQuery.event.special.touchmove = { setup: function( _, ns, handle ) { this.addEventListener("touchmove", handle, { passive: !ns.indexOf("noPreventDefault") }); }};
    </script>
    <script defer src="/js/nav-toggle.min.js?v=<?php echo filemtime(FS_ROOT.'js/nav-toggle.min.js'); ?>"></script>
    <? if (isset($newsletter) && $newsletter == true) { ?>
        <script src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>  
    <? } ?>

    <?php if (checkAsset('map')) { ?>
      <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB56Gy_mk-y1BoR3uKTLYhig-v1HrLd6JQ&libraries=places"></script>
      <script defer src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
      <script defer src="/js/map.min.js"></script>
    <?php } ?>

    <?php if (checkAsset('carousel')) { ?>
      <script defer src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
      <script defer src="/js/carousel.min.js"></script>
    <?php } ?>

    <?php if (checkAsset('parallax')) { ?>
      <script defer src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.5.1/dist/simpleParallax.min.js"></script>
      <script defer src="/js/parallax.min.js"></script>
    <?php } ?>

    <?php if (checkAsset('toggle')) { ?>
      <script defer src="/js/toggle.min.js"></script>
    <?php } ?>

    <?php if (checkAsset('modal')) { ?>
      <script defer src="/js/modal.min.js"></script>
    <?php  } ?>
  </body>
</html>