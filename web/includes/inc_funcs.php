<?php

use PHPMailer\PHPMailer\PHPMailer;
use WebPConvert\WebPConvert;

// isAdmin
function isAdmin() {
    global $pdo;

    if ( isset($_SESSION['eruid']) && (int)$_SESSION['eruid'] > 0 ) {
        $id = $_SESSION['eruid'];

        $adminCheck = $pdo->prepare("SELECT level FROM er_users WHERE _id = ? LIMIT 1");
        $adminCheck->execute([$id]);
        $is_admin = $adminCheck->fetch();

        if ( $is_admin['level'] == 'superuser' || $is_admin['level'] == 'admin' ) {
            return true;
        }
    }

    return false;
}

// XSSTrapper
function XSSTrapper($TheString) {
	if (is_null($TheString) || trim($TheString) == "") {
		$XSSTrapper = $TheString;
	} else {
		$XSSTrapper = trim($TheString);
		$XSSTrapper = str_replace(
			array("<",">","(",")","'","\""),
			array("&lt;","&gt;","&#x28;","&#x29;","&apos;","&#x22;"),
			$XSSTrapper
		);
	}
	return $XSSTrapper;
}

// Check XSSTrapper output
function trapCheck($input) {    
    if ( $input != '' && !is_null($input) ) {
        return true;
    }
    return false;
}

// buildPath
function buildPath($post_id, $count = 0) {
    global $pdo;
    global $path;

    if ( $count == 0 ) {
        $path = [];
    }

    $getPost = $pdo->prepare("SELECT post_slug, parent_id FROM er_posts WHERE _id = ? LIMIT 1");
    $getPost->execute([$post_id]);

    if ( $post = $getPost->fetch() ) {
        // Add page slug to path array
        $path[] = $post['post_slug'];

        // Loop this function until parent_id reaches 1, then build the path
        if ( $post['parent_id'] > 1 ) {
            $count++;
            buildPath($post['parent_id'], $count);
        } else {
            $path = "/" . implode("/",array_reverse($path)) . "/";
        }
    }
    return $path;
}

function buildBreadcrumb($thearray) {
    global $NormPath;
    echo '<ul id="breadcrumb" class="reset--list" itemscope itemtype="http://schema.org/BreadcrumbList">';
    $combined_url="";
    $arrayCount = 0;
    foreach ($thearray as $value) {
        $arrayCount++;
        $combined_url .= $value[0];
        echo '<li class="breadcrumb__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a class="breadcrumb__link" href="'.$combined_url.'" itemprop="item"><span itemprop="name">'.ucfirst($value[1]).'</span><meta itemprop="position" content="'.$arrayCount.'"></a></li>';
    }
    echo '</ul>';
}

// Emailer - PHPMailer
function emailer(
    $fromname,
    $fromemail,
    $toemail,
    $ccemail,
    $bccemail,
    $subject,
    $mailbody,
    $htmlmailbody,
    $attachments,
    $replyTo = "",
    $smtp = false
) {
    global $env;

    $mail = new PHPMailer();

    // If using SMTP, set details here
    if ( $smtp ) {
        // Set up SMTP
        $mail->isSMTP();
        $mail->Host = 'smtp.sparkpostmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'SMTP_Injection';
        $mail->Password = $env['general']['SPARKPOST_API_KEY'];
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
    }

	// Addresses
	$mail->setFrom($fromemail, $fromname);
	$mail->addAddress($toemail);
	$mail->addReplyTo($replyTo ?: $fromemail);

	if ($ccemail) {
		$mail->addCC($ccemail);
	}

	if ($bccemail) {
		$mail->addBCC($bccemail);
	}

	// Content
    $mail->Subject = $subject;

	if ($mailbody != '' && !is_null($mailbody)) {
		$mail->Body = $mailbody;
	}

	if ($htmlmailbody != '' && !is_null($htmlmailbody)) {
	    $mail->isHTML(true);
	    $mail->Body = $htmlmailbody;
	    $mail->AltBody = $mailbody;
	}

	if (!empty($attachments)) {
		foreach ($attachments as $attachment){
			$mail->addAttachment($attachment);
		}
	}

	$mail->send();
}

function showName($post_id, $attr = 'post_title') {
    global $pdo;

    $getPost = $pdo->prepare("SELECT post_title, post_alt_title FROM er_posts WHERE _id = ? LIMIT 1");
    $getPost->execute([$post_id]);
    
    $post = $getPost->fetch();
    return $post[$attr];
}

function showAltName($post_id, $attr = 'post_alt_title') {
    global $pdo;

    $getPost = $pdo->prepare("SELECT post_title, post_alt_title FROM er_posts WHERE _id = ? LIMIT 1");
    $getPost->execute([$post_id]);
    
    $post = $getPost->fetch();
    return $post[$attr];
}

function showLink($post_id, $class_name, $title_type) {
    global $pdo;
	// Example use - showLink(1,"button","post_title") - id of post and class name

    $getPost = $pdo->prepare("SELECT _id, post_title FROM er_posts WHERE _id = ? LIMIT 1");
    $getPost->execute([$post_id]);
    
    $post = $getPost->fetch();
    return '<a class="'.$class_name.'" href="'.buildPath($post['_id']).'">'.$post[$title_type].'</a>';
}

function pullMeta($post,$prefs = []) {
	global $pdo;
	// Example use - pullMeta($row,["image_thumbnail","header_image"]) - id of post and array of items needed
	// Example use - pullMeta($row) - this will grab all meta_value

    if ( !empty($prefs) ) {
        $in = str_repeat('?,',count($prefs) - 1) . '?';
        $pullMeta = $pdo->prepare("SELECT * FROM er_postmeta WHERE post_id = ? AND meta_name IN ($in)");
        $pullMeta->execute(array_merge([$post['_id']],$prefs));
    } else {
        $pullMeta = $pdo->prepare("SELECT * FROM er_postmeta WHERE post_id = ?");
        $pullMeta->execute([$post['_id']]);
    }

    if ( $pullMeta->rowCount() > 0 ) {
        $meta = $pullMeta->fetchAll();

        foreach ( $meta as $m ) {
            if ( substr($m['meta_value'],0,1) == '{' || substr($m['meta_value'],0,2) == '[{' ) {
                $post[$m['meta_name']] = json_decode($m['meta_value'],true);
            } else {
                $post[$m['meta_name']] = $m['meta_value'];
            }
        }
    }

    return $post;
}

function showIcon($name) {
    if (file_exists(FS_ROOT.'icons/'.$name.'.svg')) {
      include FS_ROOT.'icons/'.$name.'.svg';
    } else {
      throw new Exception('Icon "' . $name . '" does not exists. Did you add it to the icons directory?');
    }
  }

/**
 * Convert Image Path into a Base 64 Encoded Image. Returns Image Path if fails to find image.
 * 
 * @param String $imagePath The Slug to the Image
 * @return String The encoded string if successful, the image path if failed
 */
function encodeImage(String $imagePath): String {
    // Set Full Path to Image
    $fullPath = $_SERVER['DOCUMENT_ROOT'] . $imagePath;

    // Check File Exists
    if ( ! file_exists($fullPath) ) {
        return $imagePath;
    }

    // Base 64 Encode Image
    $imageExtension = pathinfo($fullPath, PATHINFO_EXTENSION);
    $imageEncoded = "data:image/$imageExtension;base64," . base64_encode(file_get_contents($fullPath));

    return $imageEncoded;
}


/**
 * Automagically build a fetched resource pagination. Hassle-free usage.
 * 
 * Example usage: 
 * $articles = fetchResource(['post_type' => 'article', 'parent_id' => 4, 'limit' => 9]);
 * echo $articles['pagination'];
 * 
 * @return string
 */
function buildPagination($current_page, $before_and_after, $count, $page_length) {
  $pagination = '
    <ul class="reset--list pagination">';

      if ($current_page > 2) {
        $pagination .= '<!-- jump to first page -->
        <li class="pagination__item">
          <a href="?pg=1" class="pagination__link">&#171;</a>
        </li>';
      }

      if ($current_page > 1) {
        $pagination .= '<!-- jump to previous page -->
        <li class="pagination__item">
          <a href="?pg='.($current_page - 1).'" class="pagination__link">&#8249;</a>
        </li>';
      }

      $pagination .= '<!-- x before -->';
      for ($pg = $current_page - $before_and_after; $pg < $current_page; $pg++) {
        if ($pg >= 1) {
          $pagination .= '<li class="pagination__item">
            <a href="?pg='.$pg.'" class="pagination__link">'.$pg.'</a>
          </li>';
        }
      }

      $pagination .= '<!-- currently active page -->
      <li class="pagination__item">
        <a href="?pg='.$pg.'" class="pagination__link active">
          '.$pg.'
        </a>
      </li>
      
      <!-- x after -->';
      for ($pg = $current_page + 1; $pg <= $current_page + $before_and_after; $pg++) {
        if ($pg <= $count / $page_length) {
          $pagination .= '<li class="pagination__item">
            <a href="?pg='.$pg.'" class="pagination__link">'.$pg.'</a>
          </li>';
        }
      }

      if ($count / $page_length > $current_page) {
        $pagination .= '<!-- jump to next page -->
        <li class="pagination__item">
          <a href="?pg='.($current_page + 1).'" class="pagination__link">&#8250;</a>
        </li>';
      }

      if ($current_page < $count / $page_length - 2) {
        $pagination .= '<!-- jump to the last page -->
        <li class="pagination__item">
          <a href="?pg='.number_format($count/$page_length, 0).'" class="pagination__link">&#187;</a>
        </li>';
      }

    $pagination .= '</ul>
  ';

  return $pagination;
}

/**
 * Build line of text to indicate how many rows 
 * out of the total found is being shown to the user.
 * 
 * Example usage:
 * $projects = fetchResource(['post_type' => 'projects', 'parent_id' => 12, 'limit' => 12]);
 * echo $articles['showing'];
 * 
 * @param string - $page_offset
 * @param string - $page_length
 * @param string - $count
 * @return string
 */
function buildShowing($page_offset, $page_length, $count) {
  $min = (1 + $page_offset);
  $max = ($page_length + $page_offset);

  if ($max > $count) {
    $max = $count;
  }

  return '<div class="showing">
    Showing '.$min.' - '.$max.' of '.$count.' results.
  </div>';
}

/**
 * Fetch specified resource from the er_posts table
 * and return all results, including pagination and showing.
 * 
 * Example usage:
 * $caseStudies = fetchResource(['post_type' => 'case-study', 'paginate' => true, 'parent_id' => 11, 'limit' => 6]);
 * if ($caseStudies['count'] > 0) foreach ($caseStudies['data'] as $caseStudy) include FS_ROOT.'includes/inc_casestudy_listing.php';
 * echo $caseStudies['pagination'];
 * echo $caseStudies['showing'];
 * 
 * Using "LIKE" when fetching meta to improve reusability of this function.
 * Use meta => array("product_thumbnail" => "LIKE thumbnail") to specify that you want to
 * fetch meta values that contain word "thumbnail" in their meta_name. Helpful, if you have
 * two post types with different meta_name for the thumbnail image. For example, page could
 * have thumbnail_image but product could have product_thumbnail. Using "LIKE thumbnail" will
 * return either thumbnail_image, product_thumbnail or both.
 * 
 * @param array - options/binding parameters
 * @return array
 */
function fetchResource($options) {
  global $pdo, $post_id;

  if (!empty($options)) {
    extract($options);
  }

  $current_page = 1;
  $before_and_after = 2;
  $paging_qstr = '?pg=2';
  $binds = [];

  if (isset($paginate) && isset($_GET['pg'])) {
    $page_num = XSSTrapper($_GET['pg']);
    if ((strlen(trim($page_num)) > 0) && is_numeric($page_num)) {
      $current_page = $page_num;
      $paging_qstr = ($current_page == 1) ? '' : '?pg=' . $current_page;
    }
  }

  $offset = ($limit ?? 100) * ($current_page - 1);

  $query = '
    SELECT SQL_CALC_FOUND_ROWS *,
      (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name = "post_tagline") AS tagline,
      (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name = "page_summary") AS summary,
      (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name = "thumbnail_image") AS thumbnail';

  if (!empty($meta)) {
    $query .= ', ';

    foreach ($meta as $key => $value) {
      $meta_name = !is_integer($key) ? $key : $value;
      $meta_operator = '=';

      if (strpos($value, 'LIKE') !== false) {
        $value = str_replace('LIKE ', '', $value);
        $meta_name = "%$value%";
        $meta_operator = 'LIKE';
      }

      $query .= '
      (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name '.$meta_operator.' "'.$meta_name.'") AS ' . $value . ',';
    }

    $query = rtrim($query, ',');
  }

  $query .= '
    FROM er_posts AS post
      LEFT JOIN er_posts AS child_post
        ON post._id = child_post._id

    WHERE post.post_status = "'.(isset($post_status) ? $post_status : 'live').'"
  ';

  // if (isset($post_id)) {
  //   $query .= '
  //     AND post._id != '.$post_id;
  // }

  if (!empty($post_type)) {
    $query .= '
      AND post.post_type = :post_type ';
    $binds['post_type'] = $post_type;
  }

  if (!empty($post_title)) {
    $query .= '
      AND post.post_title = :post_title ';
    $binds['post_title'] = $post_title;
  }

  if (!empty($id)) {
    $query .= '
      AND post._id = :id ';
    $binds['id'] = $id;
  }

  if (!empty($post_alt_title)) {
    $query .= '
      AND post.post_alt_title = :post_alt_title ';
    $binds['post_alt_title'] = $post_alt_title;
  }

  if (!empty($parent_id)) {
    $query .= '
      AND post.parent_id = :parent_id ';
    $binds['parent_id'] = $parent_id;
  }

  if (!empty($in)) {
    $query .= '
      AND post.parent_id IN('.implode(', ', $in).')';
  }

  if (!empty($in_category)) {
    $query .= '
      AND (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name = "category_id" AND meta_value IN('.implode(', ', $in_category).'))';
  }

  if (!empty($id_in)) {
    $query .= '
      AND post._id IN('.implode(', ', $id_in).')';
  }

  if (!empty($exclude)) {
    if (is_array($exclude)) {
      $query .= '
        AND post._id NOT IN ('.implode(', ', $exclude).')';
    }
    else {
      $query .= '
        AND post._id != ' . $exclude;
    }
  }

  if (!empty($exclude_meta)) {
    if (is_array($exclude_meta)) {
      foreach ($exclude_meta as $exl_meta_name => $exl_meta_value) {
        $query .= '
          AND (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name = :exl_meta_name) NOT LIKE :exl_meta_value';
        $binds['exl_meta_name'] = "$exl_meta_name";
        $binds['exl_meta_value'] = "%$exl_meta_value%";
      }
    }
  }

  if (!empty($has_meta)) {
    $query .= '
      AND (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name = :has_meta) != 0';
    $binds['has_meta'] = $has_meta;
  }

  if (!empty($where_meta)) {
    foreach ($where_meta as $where_meta_key => $where_meta_value) {
      $query .= ' 
        AND (SELECT meta_value FROM er_postmeta WHERE post_id = post._id AND meta_name = :where_meta_name) = :where_meta_value';
      $binds['where_meta_name'] = $where_meta_key;
      $binds['where_meta_value'] = $where_meta_value;
    }
  }

  if (!empty($order)) {
    $query .= '
      ORDER BY ' . $order;
  } else {
    $query .= ' 
      ORDER BY post.menu_order ASC ';
  }

  if (!empty($group_by)) {
    $query .= '
      GROUP BY ' . $group_by;
  }

  if (!empty($limit)) {
    $query .= '
      LIMIT :limit';
    $binds['limit'] = $limit;
  }

  if (!empty($offset)) {
    $query .= ' 
      OFFSET :offset ';
    $binds['offset'] = $offset;
  }

  $stmt = $pdo->prepare($query);

  if (!empty($binds)) {
    foreach ($binds as $key => $value) {
      $stmt->bindValue(":$key", $value, PDO::PARAM_STR);
    }
  }

  $stmt->execute();

  $count = $pdo->query('SELECT FOUND_ROWS() AS total')->fetch()['total'];

  return [
    'count' => $count,
    'current_page' => $current_page,
    'offset' => $offset,
    'data' => $stmt->fetchAll(),
    'query' => $query,
    'pagination' => buildPagination(
      $current_page,
      $before_and_after,
      $count,
      $limit ?? 100
    ),
    'showing' => buildShowing(
      $offset, 
      $limit ?? 100, 
      $count
    )
  ];
}

/**
 * Check if string is in a JSON format
 * @param string - string to check
 * @return boolean
 */
function isJson($string) {
  json_decode($string);
  return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * Function to show image (don't create Webp format)
 * Use for displaying PNG images for now, please.
 * 
 * @param string - json string containing image details
 * @param string - the fallback image url
 * @return string - either uploaded image or fallback
 */
function showPostImage($image, $ext = null, $fallback = null) {
  if (isJson($image)) {
    $image = json_decode($image, 1);

    if (!empty($image['post_slug'])) {
      $image = $image['post_slug'];
    }
  }

  if (!empty($ext) && $ext == 'webp') {
    $webpImage = str_replace(['.jpg', '.jpeg'], '.webp', $image);

    if (!file_exists(FS_ROOT.ltrim($webpImage, '/'))) {
      create_webp_image($image);
    }

    return $webpImage;
  }

  if (is_null($image) || !file_exists(FS_ROOT.ltrim($image, '/'))) {
    $image = isset($fallback) ? $fallback : '/images/fallback.jpg';
  }

  return $image;
}

/**
 * Convert image to webp format
 * 
 * Uses WebPConvert package to convert images to WebP format.
 * @source https://github.com/rosell-dk/webp-convert
 * @todo Figure out why PNG have white or black background after conversions.
 *  
 * @param string - filepath
 * @return void
 */
function create_webp_image($file) {

  // Don't convert images with specified file extensions.
  // @todo: figure out why png files after conversion have white or black background.
  if (!in_array(pathinfo($file, PATHINFO_EXTENSION), ['webp', 'png'])) {
    $source = FS_ROOT.ltrim($file, '/');
    $dest = str_replace(['.jpeg', '.jpg', '.png', '.gif'], '', $source) . '.webp';
    $options = [];

    // Convert image and save it in the same directory as the original image
    WebPConvert::convert($source, $dest, [
      'fail' => 'original',
      'convert' => [
        'quality' => 100,
      ],
      'png' => [
        'encoding' => 'auto',
        'near-lossless' => 60,
        'quality' => 85,
        'sharp-yuv' => true
      ]
    ]);
  }
}

/**
 * Show picture tag
 * 
 * Accepts image slug or array of options
 * 
 * Example usage:
 * Pass image slug as a parameter
 * showPicture('/images/example.jpeg');
 * 
 * Pass array with options
 * showPicture([
 *    'src' => '/images/example.jpeg',
 *    'alt' => 'Alt title of the image', // auto-generated from the filename
 *    'media' => 'max-width: 768px', // default
 *    'loading' => 'lazy' // default
 *    'width' => 480, // default
 *    'height' => 320, // default
 *    'class' => 'example__image' // optional
 *    'preview' => true // use fancybox plugin to enable larger image preview
 * ]);
 * 
 */
function showPicture($params) {

  // Extract params array into variables
  if (is_array($params)) {
    extract($params);
  }
  else {
    $src = $params;
  }

  if (isJson($src)) {
    $src = json_decode($src, 1)['post_slug'];
  }

  // Check if src image exists
  if (is_null($src) || !file_exists(FS_ROOT.ltrim($src, '/'))) {
    $src = isset($fallback) ? $fallback : '/images/fallback.jpg';
  }

  // Set original and modified file extensions
  $original_ext = ['.jpg', '.png', '.jpeg', '.gif'];
  $modified_ext = ['--600px.jpg', '--600px.png', '--600px.jpeg', '--600px.gif'];
  
  // Get original and small image
  $originalImage = isJson($src) ? showImage($src) : $src;
  $smallImage = str_replace($original_ext, $modified_ext, $originalImage);

  // Get original extension
  $originalExt = explode('.', $originalImage);
  $originalExt = $originalExt[count($originalExt) - 1];

  // Get webp and small webp image
  $webpImage = str_replace($original_ext, '.webp', $originalImage);
  $smallWebpImage = str_replace(['.webp'], '--600px.webp', $webpImage);

  // If image has not been converted before
  if (!file_exists(FS_ROOT.ltrim($webpImage, '/'))) {

    // If the original image exists, then convert to WebP
    if (file_exists(FS_ROOT.ltrim($originalImage))) {
      create_webp_image($originalImage);
    }

    // If the small image exists, then convert to WebP
    if (file_exists(FS_ROOT.ltrim($smallImage))) {
      create_webp_image($smallImage);
    }
  }

  // Get image alt title
  if (empty($alt)) {
    $alt = explode('/', $originalImage);
    $alt = explode('.', $alt[count($alt) - 1]);
    $alt = ucwords(str_replace('-', ' ', $alt[0]));
  }

  // Create HTML
  if (isset($preview) && $preview == true) {
    addAsset('fancybox'); ?>
    <a data-fancybox="gallery" href="<?php echo $originalImage; ?>" class="fancybox">
  <?php } ?>
    <picture>
      <?php if (!empty($smallWebpImage) && file_exists(FS_ROOT.ltrim($smallWebpImage, '/'))) { ?>
        <source media="(max-width: <?php echo isset($media) ? $media : '475'; ?>px)" srcset="<?php echo $smallWebpImage; ?>" type="image/webp" />
      <?php } ?>
      <?php if (!empty($smallImage) && file_exists(FS_ROOT.ltrim($smallImage, '/'))) { ?>
        <source media="(max-width: <?php echo isset($media) ? $media : '475'; ?>px)" srcset="<?php echo $smallImage; ?>" type="image/<?php echo $originalExt; ?>" />
      <?php } ?>
      <?php if (!empty($webpImage) && file_exists(FS_ROOT.ltrim($webpImage, '/'))) { ?>
        <source srcset="<?php echo $webpImage; ?>" type="image/webp" />
      <?php } ?>
      <img data-src="<?php echo $originalImage; ?>" src="<?php echo $originalImage; ?>" alt="<?php echo $alt; ?>" class="<?php echo isset($class) ? $class : ''; ?>" loading="<?php echo isset($loading) ? $loading : 'lazy'; ?>" width="<?php echo isset($width) ? $width : '480'; ?>" height="<?php echo isset($height) ? $height : '320'; ?>" />
    </picture>
  <?php if (isset($preview) && $preview == true) { ?>
    </a>
  <?php }
}

/**
 * Show button
 * @param integer - id of the post
 * @param string  - class modifier
 * @param string  - text for the label
 * @param array   - dataset
 * @return void
 */
function showButton($id = null, $modifier = null, $label = null, $tag = 'a', $dataset = []) {
  $thePath = is_numeric($id) ? buildPath($id) : $id; 
  $showName =  $label ?? showName($id);

  if ($tag == 'a') { ?><a <?php echo isset($dataset['data-toggle']) ? 'data-toggle="'. $dataset['data-toggle'] .'"' : ''; ?> href="<?php echo $thePath; ?>"<?php } else { ?><button type="submit"<?php } ?> title="<?php echo $showName; ?>" class="button<?php echo isset($modifier) ? ' ' . $modifier : ''; ?>">
    <?php echo $showName; ?>
  <?php if ($tag == 'a') { ?></a><?php } else { ?></button><?php }
}


/**
 * Retrieve a record from the posts table
 * 
 * @param int $post_id The ID of the record to retrieve
 * @return array The record. Empty array on failure.
 */
function getPost( int $post_id ): array {
  global $pdo;

  // Prepare Query
  $get_post = $pdo->prepare('
    SELECT *
    FROM er_posts
    WHERE _id = :post_id
    LIMIT 1
  ');
  $get_post->execute([
    'post_id' => $post_id
  ]);

  // Handle failing to find anything
  if ( $get_post->rowCount() === 0 ) {
    return false;
  }

  // Return the record
  return $get_post->fetch();
}


/**
 * Retrieve a Meta Record
 * 
 * @param int $post_id The ID of the post record
 * @param string $meta_name The Meta Name to be retrived
 * @return mixed The value of the meta on success, will be json_decoded if need. Empty string on failure.
 */
function getMeta( int $post_id, string $meta_name ) {
  global $pdo;

  // Prepare Query
  $get_meta = $pdo->prepare('
    SELECT meta_value
    FROM er_postmeta
    WHERE post_id = :post_id
      AND meta_name = :meta_name
    LIMIT 1
  ');
  $get_meta->execute([
    'post_id' => $post_id,
    'meta_name' => $meta_name
  ]);

  // Handle Failing to find anything
  if ( $get_meta->rowCount() === 0 ) {
    return '';
  }

  // Get Meta Value
  $meta_value = $get_meta->fetch()['meta_value'];
  if ( isJson($meta_value) ) {
    $meta_value = json_decode($meta_value, true);
  }
  
  // Return Value
  return $meta_value;
}

/**
 * Retrieve all the meta for a er_post record
 * 
 * @param int $post_id The record's ID the meta is tied to
 * @param bool $autoDecode Whether to JSON decode strings it finds. Default true
 * @return array Key Value Pairs of the meta. Empty array on failure. 
 */
function getAllMeta( int $post_id, bool $autoDecode = true ): array {
  global $pdo;

  // Prepare Query
  $getMeta = $pdo->prepare('
    SELECT meta_name, meta_value
    FROM er_postmeta
    WHERE post_id = :post_id
  ');
  $getMeta->execute([
    'post_id' => $post_id
  ]);

  // Handle no meta results
  if ( $getMeta->rowCount() === 0 ) {
    return [];
  }

  // Save and store meta
  $meta = [];
  while ( $row = $getMeta->fetch() ) {
    $metaValue = $row['meta_value'];

    // Handle JSON Values
    if ( $autoDecode && isJson($metaValue) ) {
      $metaValue = json_decode($metaValue, true);
    }

    $meta[$row['meta_name']] = $metaValue;
  }

  // Return Data
  return $meta;
}

/**
 * FatHippo extension
 */
include 'inc_funcs--fathippo.php';

