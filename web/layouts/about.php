<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "page_content",
      "page_content2",
      "header_image"
    ]);

  } else {
?>
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include FS_ROOT.'includes/inc_masthead.php'; ?>

  <section class="section intro">
    <div class="inner">
      <div class="container">
        <div class="container__column">
          <img loading="lazy" src="/images/group-of-burgers.svg" alt="Group of Burgers" width="458" height="257" class="intro__image" />
        </div>
        <div class="container__column">
          <div class="page-content">
            <?php if (!empty($page_content)) echo $page_content; ?>
            <?php showButton('https://fathippo.co.uk/book-a-table', 'button--outlined', 'Book a Table', 'a', $dataset=['data-toggle'=>'book-online']); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php if (!empty($page_content2)) { ?>
    <section class="section about-content">
      <div class="inner inner--narrow">
        <div class="page-content"><?php echo $page_content2; ?></div>
      </div>
    </section>
  <?php } ?>

  <?php include_once FS_ROOT . 'includes/inc_newsletter_strip.php'; ?>
  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>

</main>
<?php } ?>