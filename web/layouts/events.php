<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "page_content",
      "page_content2",
      "header_image"
    ]);

  } else {
?>
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include FS_ROOT.'includes/inc_masthead.php'; ?>

  <section class="section about-intro">
    <div class="inner">
      <div class="container">
        <div class="container__column">
          <img loading="lazy" src="/images/fleet-chevy.svg" alt="Group of Burgers" width="458" height="257" />
        </div>
        <div class="container__column">
          <div class="page-content page-content--pad-top">
            <?php if (!empty($page_content)) echo $page_content; ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php if (!empty($page_content2)) { ?>
    <section class="section about-content">
      <div class="inner inner--narrow">
        <div class="page-content"><?php echo $page_content2; ?></div>
      </div>
    </section>
  <?php } ?>

  <section class="section event-hire">
    <div class="inner event-hire__wrap">
      <div><h2 class="event-hire__title"><span>What are you waiting for?</span></h2></div>
      <div><p class="event-hire__tagline">If the Fat Hippo Fleet sounds like a bit of what you need at your event, get in touch now. We'd love to hear from you.</p></div>
      <?php showButton('mailto:fleet@fathippo.co.uk?subject=Fleet+Enquiry', 'button--outlined', 'Hire the Fleet'); ?>
    </div>
  </section>

  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>
<?php } ?>