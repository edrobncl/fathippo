<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "post_tagline",
      "page_content",
      "header_image",
      "thumbnail_image"
    ]);

  } else {

    /**
     * Fetch page portals
     */
    if ($post_id == 7) {

      $portals = fetchResource([
        'post_type' => 'menu',
        'parent_id' => $post_id,
        'order' => 'post.menu_order ASC',
        'meta' => [
          'thumbnail_image' => 'thumbnail',
        ]
      ]);

    } else {

      $portals = fetchResource([
        'post_type' => 'page',
        'parent_id' => $post_id,
        'order' => 'post.menu_order ASC',
        'meta' => [
          'thumbnail_image' => 'thumbnail',
          'external_slug',
        ]
      ]);

    }
?> 
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include_once FS_ROOT.'includes/inc_illustration_strip.php'; ?>

  <?php if (!empty($post_tagline) || !empty($post_title)) { ?>
    <section class="section plain-masthead">
      <div class="inner inner--narrow">
        <div class="feature">
          <h1 class="feature__title feature__title--large"><?php echo $post_tagline ?? $post_title; ?></h1>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($page_content)) { ?>
    <section class="section page">
      <div class="inner inner--narrow">
        <div class="page-content"><?php echo $page_content; ?></div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($portals['data'])) { ?>
    <div class="section page-portals">
      <div class="inner inner--narrow">
        <div class="page-portals__list">
          <?php foreach ($portals['data'] as $key => $portal) {
            $thePath = !empty($portal['external_slug']) ? $portal['external_slug'] : buildPath($portal['_id']); ?>


            <article class="page-portals__listing">
              <a href="<?php echo $thePath; ?>"<?php if (!empty($portal['external_slug'])) { ?> target="_blank"<?php } ?> title="View <?php echo $portal['post_title']; ?> menu" class="page-portals__listing__link">
                <h2 class="page-portals__listing__title"><?php echo $portal['post_title']; ?></h2>
                <i class="page-portals__listing__icon icon-arrow-right"></i>
              </a>
            </article>
          <?php } ?>
        </div>
      </div>

      <div class="page-portal__illustrations">
        <img loading="lazy" src="/images/Menu-part1.svg" alt="Hands full of burgers" class="page-portal__illustration first" />
        <img loading="lazy" src="/images/Menu-part2.svg" alt="Smashing burger" class="page-portal__illustration second" />
      </div>
    </div>
  <?php } ?>

  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>
<?php } ?>