<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include_once FS_ROOT.'includes/inc_masthead.php'; ?>

  <section class="section article">
    <div class="inner">
      <div class="container article__container">
        <div class="article__aside">
          <ul class="reset--list social-media">
            <?php $socialLink = rtrim(SITE_URL, '/') . $_SERVER['REQUEST_URI']; ?>
            <li class="social-media__item"><a href="https://twitter.com/intent/tweet?text=<?= $socialLink ?>" title="Share <?php echo $post_title; ?> on Twitter" rel="noreferrer" class="social-media__link" target="_blank"><?php showIcon('twitter-brands'); ?></a></li>
            <li class="social-media__item"><a href="https://www.facebook.com/sharer/sharer.php?u=<?= $socialLink ?>" title="Share <?php echo $post_title; ?> on Facebook" rel="noreferrer" class="social-media__link" target="_blank"><?php showIcon('facebook-f-brands'); ?></a></li>            
            <li class="social-media__item social-media__item--cta">Share Article</li>
          </ul>
        </div>
        <div class="article__main">

          <?php if (!empty($article_content)) { ?>
            <div class="page-content"><?php echo str_replace('<a href="/book-a-table', '<a data-toggle="book-online" href="/book-a-table', $article_content); ?></div>
          <?php } ?>

          <?php if (!empty($article_content2)) { ?>
            <div class="page-content"><?php echo str_replace('<a href="/book-a-table', '<a data-toggle="book-online" href="/book-a-table', $article_content2); ?></div>
          <?php } ?>

          <?php if ( ! empty($show_cta) ) { ?>
            <div class="cta cta--article">
              <div class="cta-text"><?= $cta_text ?? '' ?></div>
              <a class="button button--outlined inner-overlay" data-toggle="book-online" href="<?= buildPath($cta_linked_page) ?>"><?= $cta_button_text ?? 'Find out more' ?></a>
            </div>
          <?php } ?>
          
        </div>
      </div>
    </div>
  </section>

  <?php include_once FS_ROOT . 'includes/inc_newsletter_strip.php'; ?>
  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>