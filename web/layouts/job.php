<main id="main" class="main job-page">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include_once FS_ROOT.'includes/inc_masthead.php'; ?>
  <?php
    $link = 'mailto:' . $job_email . '?subject=Application For: '. $post_title .' - '. (showAltName($job_location) ?? 'Other');
  ?>

  <section class="section section--job">
    <div class="inner">
      <div class="container container--job">
        <div class="article__aside">
          <ul class="reset--list social-media">
            <?php
              $socialLink = rtrim(SITE_URL, '/') . $_SERVER['REQUEST_URI'];
            ?>
            <li class="social-media__item"><a href="https://twitter.com/intent/tweet?text=<?= $socialLink ?>" title="Share <?php echo $post_title; ?> on Twitter" rel="noreferrer" class="social-media__link" target="_blank"><i class="icon-twitter"></i></a></li>
            <li class="social-media__item"><a href="https://www.facebook.com/sharer/sharer.php?u=<?= $socialLink ?>" title="Share <?php echo $post_title; ?> on Facebook" rel="noreferrer" class="social-media__link" target="_blank"><i class="icon-facebook"></i></a></li>
            <?php
              if ( 1 == 2 ) { ?>
                <li class="social-media__item"><a href="https://www.facebook.com/" title="Share <?php echo $post_title; ?> on Messenger" rel="noreferrer" class="social-media__link"><i class="icon-messenger"></i></a></li>
                <li class="social-media__item"><a href="https://www.linkedin.com/" title="Share <?php echo $post_title; ?> on LinkedIn" rel="noreferrer" class="social-media__link"><i class="icon-linkedin"></i></a></li>
                <li class="social-media__item"><a href="https://www.facebook.com/" title="Share <?php echo $post_title; ?> on elsewhere" rel="noreferrer" class="social-media__link"><img src="/images/dots.svg" alt="Dots" /></a></li>
                <?php
              }
            ?>
            <li class="social-media__item social-media__item--cta">Share &nbsp;</li>
          </ul>
        </div>
        <div class="article__main">
          <?php if (!empty($job_description)) { ?>
            <div class="page-content"><?php echo $job_description; ?></div>
          <?php } ?>
          <div class="job-details">
            <div class="job-content__department">Department: <?php echo $job_department; ?></div>
            <div class="job-content__location">Restaurant: <?php echo showAltName($job_location) ?? 'Other'; ?></div>
            <div class="job-content__type">Job Type: <?php echo $job_type; ?></div>
            <div class="job-content__salary">Salary: <?php echo $job_salary; ?></div>
            <div class="job-content__type">Email: <a href="<?= $link ?>" target="_blank"><?php echo $job_email; ?></a></div>
          </div>
          <?php 
            showButton($link, 'button--outlined', 'Apply for Vacancy');
          ?>
        </div>
      </div>
    </div>
  </section>
</main>