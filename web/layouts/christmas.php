<?php
  if (isset($in_admin)) {

    $post_editables = json_encode([
      "post_tagline",
      "page_content",
      "post_tagline2",
      "page_content2",
      "page_content3",
      "post_tagline4",
      "page_content4",
      "header_image",
    ]);

  } else {

    // ====================================
    //  Getting Menu 
    // ====================================
    $menu = getMenu(1248); // Christmas Menu ID
    addAsset('modal');
    addAsset('carousel');
?>

<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>

  <?php if (!empty($header_image)) { ?>
    <section class="section hero bottom-edge">
      <?php showPicture(['src'=>$header_image,'class'=>'hero__image']); ?>
      <div class="hero__overlay">
        <div class="hero__content">
          <h1 class="hero__title"><?php echo $post_title; ?></h1>
          <?php showButton('https://menus.preoday.com/Fat-Hippo#/main/choose-branch', null, 'Click+Collect', 'a'); ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($page_content)) { ?>
    <section class="section xmas">
      <div class="inner">
        <div class="container">
          <div class="container__column xmas__title">
            <?php if (!empty($post_tagline)) { ?><h2 class="xmas__title"><?php echo $post_tagline; ?></h2><?php } ?>
            <p class="xmas__time"><i class="icon-calendar-plus"></i> 09.12.<?php echo date('y'); ?> - 31.12.<?php echo date('y'); ?></p>
            <img loading="lazy" src="/images/seasonal-xmas-burger-cake.svg" alt="Seasonal Xmas Burger Cake" width="458" height="257" class="xmas__image" />
          </div>
          <div class="container__column xmas__content">
            <div class="page-content">
              <?php if (!empty($page_content)) echo $page_content; ?>
              <?php showButton('https://menus.preoday.com/Fat-Hippo#/main/choose-branch', 'button--outlined', 'Click+Collect', 'a'); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <section class="section xmas-specials">
    <div class="inner">

      <?php showPageFeature('The Specials', $post_tagline2, $page_content2 ?? ''); ?>

      <div class="locations__actions">
        <div class="locations__controls">
          <button type="button" id="l-prev-arrow" title="Previous slide" class="slick-arrow slick-arrow--circle slick-prev"><?php showIcon('arrow-left-regular'); ?></button>
          <button type="button" id="l-next-arrow" title="Next slide" class="slick-arrow slick-arrow--circle slick-next"><?php showIcon('arrow-right-regular'); ?></button>
        </div>
      </div>

      <div class="food-carousel locations--slick">
        <?php foreach (array_column($menu['categories'], 'category_items') as $items) {
          foreach ($items as $item) {
            $item['name'] = $item['post_title'];
            $item['price'] = $item['post_alt_title'];
            $item['description'] = $item['food_description'];
            $item['thumbnail'] = json_encode($item['food_image']);
            $item['details'] = $item['food_details'] ?? '';
            $item['ingredients'] = json_encode($item['ingredients']);
            if (isset($item['veg_ingredients'])) $item['veg_ingredients'] = json_encode($item['veg_ingredients']);
            if (isset($item['vegan_ingredients'])) $item['vegan_ingredients'] = json_encode($item['vegan_ingredients']);
            if (isset($item['df_ingredients'])) $item['df_ingredients'] = json_encode($item['df_ingredients']);
            if (isset($item['gf_ingredients'])) $item['gf_ingredients'] = json_encode($item['gf_ingredients']);
            ?><div><?php
                // Split Food Details up as What it is vs what can be requested
                $standardDetails = $requestDetails = [];
                if ( ! empty($item['details']) ) {
                  foreach ( $item['details'] as $detail ) {
                    // If Not Requested
                    if ( in_array($detail, [1,3,5,7]) ) {
                      $standardDetails[] = $detail;
                    } else {
                      $requestDetails[] = $detail;
                    }
                  }
                }
              ?><article class="menu-item">
                <?php if ( ! empty($item['thumbnail']) ) { ?>
                  <div class="menu-item__thumbnail inner-overlay" data-toggle="modal_<?php echo $item['_id']; ?>">
                    <img src="<?php echo showPostImage($item['thumbnail']); ?>" alt="<?php echo $item['name']; ?>" />
                  </div>
                <?php } ?>
                <div class="menu-item__content">
                  <h3 class="menu-item__title"><?php echo $item['name']; ?></h3>

                  <div class="menu-item__details">
                    <div class="menu-item__price">
                      <?php if ( $item['price'] > 0 ) { ?>
                        &pound;<?php echo number_format($item['price'], 2, '.', ','); ?>
                      <?php } ?>
                    </div>
                    <div class="menu-item__info">

                      <div class="menu-item__info__wrap menu-item__info__wrap--toggle" role="button" data-toggle="modal_<?php echo $item['_id']; ?>">
                        <img src="/images/circle-green-info.svg" alt="Information" />
                      </div>

                      <?php if ( ! empty($standardDetails) ) {
                        foreach ( $standardDetails as $detail ) { ?>
                          <div class="menu-item__info__wrap menu-item__info__wrap--tooltip" data-content="<?= showMenuDetailName($detail) ?>">
                            <img src="<?= showMenuDetailIcon($detail) ?>" alt="" class="menu-item__info__icon" />
                          </div>
                          <?php
                        }
                      } ?>

                    </div>
                  </div>

                  <div class="menu-item__description">
                    <?php echo $item['description']; ?>
                  </div>

                  <?php if ( ! empty($requestDetails) ) { ?>
                    <p class="menu-item__details">
                      <span class="menu-item__detail">On Request:</span>
                      <?php foreach ($requestDetails as $detail) { ?>
                        <span class="menu-item__detail menu-item__detail--tooltip" data-toggle="tooltip" data-content="<?= showMenuDetailName($detail) ?>">
                          <img src="<?= showMenuDetailIcon($detail) ?>" alt="" class="menu-item__detail__icon" />
                        </span><?php
                      } ?>
                    </p>
                  <?php } ?>
                </div>
              </article>
            </div>
          <?php }
        } ?>
      </div>

      <?php foreach (array_column($menu['categories'], 'category_items') as $items) {
        foreach ($items as $item) {
          $item['name'] = $item['post_title'];
          $item['price'] = $item['post_alt_title'];
          $item['description'] = $item['food_description'];
          $item['thumbnail'] = json_encode($item['food_image']);
          $item['details'] = $item['food_details'] ?? '';
          $item['ingredients'] = json_encode($item['ingredients']);
          if (isset($item['veg_ingredients'])) $item['veg_ingredients'] = json_encode($item['veg_ingredients']);
          if (isset($item['vegan_ingredients'])) $item['vegan_ingredients'] = json_encode($item['vegan_ingredients']);
          if (isset($item['df_ingredients'])) $item['df_ingredients'] = json_encode($item['df_ingredients']);
          if (isset($item['gf_ingredients'])) $item['gf_ingredients'] = json_encode($item['gf_ingredients']);

          include 'menu/menu__modal.php';
        }
      } ?>
    </div>
  </section>

  <?php if (!empty($page_content3)) { ?>
    <section class="section xmas-content">
      <div class="inner inner--narrow">
        <div class="page-content"><?php echo $page_content3; ?></div>
      </div>
    </section>  
  <?php } ?>

  <?php if (!empty($post_tagline4) && !empty($page_content4)) { ?>
    <section class="section newsletter">
      <div class="inner newsletter__wrap">
        <div><h2 class="newsletter__title"><span><?php echo $post_tagline4; ?></span></h2></div>
        <div class="newsletter__tagline"><span><?php echo $page_content4; ?></span></div>
        <div class="newsletter__actions">
          <?php showButton('https://fathippo.co.uk/book-a-table', 'button--outlined', 'Book a Table', 'a', $dataset=["data-toggle"=>"book-online"]); ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php include_once FS_ROOT.'includes/inc_reviews_strip.php'; ?>
</main>

<?php } ?>