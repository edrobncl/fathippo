<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "post_tagline",
      "page_content",
      "header_image",
      "image_gallery",
      "downloads"
    ]);

  } else {

    $articles = fetchResource([
      'post_type' => 'article',
      'order' => 'post.post_alt_title DESC',
      'paginate' => true,
      'limit' => 10,
      'meta' => [
        'thumbnail_image' => 'thumbnail',
        'article_abstract' => 'abstract'
      ]
    ]);
?> 
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>

  <?php if (!empty($post_title)) { ?>
    <section class="section plain-masthead">
      <div class="inner">
        <div class="feature feature--main">
          <h1 class="feature__title feature__title--large"><?php echo $post_title; ?></h1>
          <?php if (!empty($post_tagline)) { ?><div class="feature__tagline"><?php echo $post_tagline; ?></div><?php } ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <section class="section blog">
    <div class="inner">

		  <?php if (!empty($page_content)) { ?>
        <div class="page-content"><?php echo $page_content;?></div>
      <?php } ?>

      <?php if ($articles['count'] > 0) { ?>
        <div class="blog__articles">
          <?php foreach ($articles['data'] as $article) {
            $thePath = buildPath($article['_id']); ?>

            <article class="container article-listing">
              <div class="article-listing__image">
                <a href="<?php echo $thePath; ?>" title="Read full story">
                  <img loading="lazy" src="<?php echo showPostImage($article['thumbnail']); ?>" alt="<?php echo $article['post_title']; ?>" />
                </a>
              </div>
              <div class="article-listing__content">
                <h3 class="article-listing__title"><a href="<?php echo $thePath; ?>" title="Read full story"><?php echo $article['post_title']; ?></a></h3>
                <div class="article-listing__abstract"><?php echo $article['abstract']; ?></div>
                <a href="<?php echo $thePath; ?>" title="Read full story" class="inline-button">Read article <i class="icon-chevron-right"></i></a>
              </div>
            </article>
          <?php } ?>
        </div>

        <?php if ($articles['count'] > 10) { ?>
          <div class="container blog__summary">
            <?php echo $articles['showing']; ?>
            <?php echo $articles['pagination']; ?>
          </div>
        <?php } ?>
      <?php } else { ?>
        <div class="blog__not-found">Couldn't find any articles at the moment, please check again soon!</div>
      <?php } ?>

    </div>
  </section>

  <?php include_once FS_ROOT . 'includes/inc_newsletter_strip.php'; ?>
  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>
<?php } ?>