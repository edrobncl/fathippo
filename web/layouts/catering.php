<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "page_content",
      "post_tagline2",
      "page_content2",
      "post_tagline3",
      "page_content3",
      "post_tagline4",
      "page_content4",
      "post_tagline5",
      "page_content5",
      "header_image"
    ]);

  } else {
?>
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include FS_ROOT.'includes/inc_masthead.php'; ?>

  <section class="section about-intro">
    <div class="inner">
      <div class="container">
        <div class="container__column">
          <img loading="lazy" src="/images/fleet-chevy.svg" alt="Group of Burgers" width="458" height="257" />
        </div>
        <div class="container__column">
          <div class="page-content page-content--pad-top">
            <?php if (!empty($page_content)) echo $page_content; ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section why-us">
    <div class="inner">
      <div class="feature">
        <p class="feature__back">Why Us?</p>
        <div class="feature__front">
          <h2 class="feature__title"><?php echo $post_tagline2; ?></h2>
          <div class="feature__tagline"><?php echo $page_content2; ?></div>
        </div>
      </div>

      <div class="container why-us__cards">
        <?php if (!empty($post_tagline3) && !empty($page_content3)) { ?>
          <article class="why-us__card">
            <div class="why-us__icon"><i class="icon-calendar"></i></div>
            <h3 class="why-us__title"><?php echo $post_tagline3; ?></h3>
            <div class="why-us__tagline"><?php echo $page_content3; ?></div>
          </article>
        <?php } ?>

        <?php if (!empty($post_tagline4) && !empty($page_content4)) { ?>
          <article class="why-us__card">
            <div class="why-us__icon"><i class="icon-burger"></i></div>
            <h3 class="why-us__title"><?php echo $post_tagline4; ?></h3>
            <div class="why-us__tagline"><?php echo $page_content4; ?></div>
          </article>
        <?php } ?>

        <?php if (!empty($post_tagline5) && !empty($page_content5)) { ?>
          <article class="why-us__card">
            <div class="why-us__icon"><i class="icon-van"></i></div>
            <h3 class="why-us__title"><?php echo $post_tagline5; ?></h3>
            <div class="why-us__tagline"><?php echo $page_content5; ?></div>
          </article>
        <?php } ?>
      </div>
    </div>
  </section>

  <?php include_once FS_ROOT.'includes/inc_reviews_strip.php'; ?>

  <section class="section event-hire">
    <div class="inner event-hire__wrap">
      <div><h2 class="event-hire__title"><span>Get in Touch</span></h2></div>
      <div><p class="event-hire__tagline">'Cause if you like it, then you should put an ONION ring on it! Hit us up if you'd like a Fat Hippo burger to have and to hold on your special day.</p></div>
      <?php showButton('mailto:weddings@fathippo.co.uk?subject=Wedding+Enquiry', 'button--outlined', 'Make Enquiry'); ?>
    </div>
  </section>

</main>
<?php } ?>