<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "post_tagline",
      "page_content",
      "header_image",
      "thumbnail_image"
    ]);

  } else {
    // Prepare Get Menu Query
    $getMenuItems = '
      SELECT _id, post_title AS name, post_alt_title AS price, menu_order,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "food_description") AS description,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "menu_section") AS section_id,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "food_details") AS details,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "ingredients") AS ingredients,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "veg_ingredients") AS veg_ingredients,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "vegan_ingredients") AS vegan_ingredients,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "gf_ingredients") AS gf_ingredients,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "df_ingredients") AS df_ingredients,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = "food_image") AS thumbnail
      FROM er_posts
      WHERE post_type = "menu_item"
        AND parent_id = :category_id
      ';
    if ( ! empty($_SESSION['eruid']) ) {
      $getMenuItems .= ' AND post_status IN ("live", "private") ';
    } else {
      $getMenuItems .= ' AND post_status = "live" ';
    }
    $getMenuItems .= '
      ORDER BY menu_order ASC
    ';
    $getMenuItems = $pdo->prepare($getMenuItems);

    // Fetch Categories
    $getCategories = '
      SELECT er_posts._id, er_posts.post_title,
        max(case when er_postmeta.meta_name = "menu_category_description" then er_postmeta.meta_value end) AS description
      FROM er_posts
        LEFT JOIN er_postmeta
          ON er_posts._id = er_postmeta.post_id
      WHERE er_posts.post_type = "menu_category"
        AND er_posts.parent_id = :post_id ';
	if ( ! empty($_SESSION['eruid']) ) {
      $getCategories .= ' AND er_posts.post_status IN ("live", "private") ';
    } else {
      $getCategories .= ' AND er_posts.post_status = "live" ';
    }
	$getCategories .= '
      GROUP BY er_posts._id
      ORDER BY er_posts.menu_order ASC
    ';
	$getCategories = $pdo->prepare($getCategories);
    $getCategories->execute([
      'post_id' => $post_id
    ]);

    $categoriesList = [];
    while ( $category = $getCategories->fetch() ) {
      $getMenuItems->execute([
        'category_id' => $category['_id']
      ]);
      
      $categoriesList[$category['_id']] = [
        '_id' => $category['_id'],
        'title' => $category['post_title'],
        'description' => $category['description'] ?? '',
        'menu_items' => []
      ];

      if ( $getMenuItems->rowCount() > 0 ) {
        while ( $menuItem = $getMenuItems->fetch() ) {
          // Decode Meta
          $menuItem['details'] = json_decode($menuItem['details'], true);
          $categoriesList[$category['_id']]['menu_items'][] = $menuItem;
        }			
      }
    }

    // Fetch Menus
    $menus = fetchResource([
      'post_type' => 'menu',
      'post_status' => 'live',
      'meta' => [
        'menu_icon' => 'icon'
      ]
    ]);


    if (!empty($categoriesList)) {
      addAsset('toggle');
      addAsset('modal');
    }

?>
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>

  <?php if (isset($menus) && $menus['count'] > 0) { ?>
    <div class="menu__nav">
      <div class="inner menu__nav__list">
        <?php foreach ($menus['data'] as $menu) { ?>
          <a href="<?php echo buildPath($menu['_id']); ?>" title="View the <?php echo $menu['post_title']; ?>" class="menu__nav__button<?php echo $post_id == $menu['_id'] ? ' active' : '';?>"><i class="icon-<?php echo $menu['icon'] ?? 'burger'; ?>"></i> <?php echo $menu['post_alt_title']; ?></a>
        <?php } ?>
      </div>
    </div>
  <?php } ?>

  <?php include_once FS_ROOT.'includes/inc_illustration_strip.php'; ?>

  <?php if (!empty($header_image) && !empty($post_tagline)) { ?>
    <section class="section hero bottom-edge">
      <?php showPicture(['src'=>$header_image,'class'=>'hero__image']); ?>
      <div class="hero__overlay">
        <div class="hero__content">
          <h1 class="hero__title"><?php echo $post_tagline; ?></h1>
          <?php showButton('https://booking.favouritetable.com/?cc=229&skin=fathippo', 'button', 'Book Now', 'a', $dataset=['data-toggle'=>'book-online']); ?>
        </div>
      </div>
    </section>

  <?php } else { ?>
    
    <section class="section plain-masthead">
      <div class="inner inner--narrow">
        <?php if (empty($header_image) && !empty($post_tagline)) { ?>
          <div class="feature">
            <h1 class="feature__title feature__title--large"><?php echo $post_tagline; ?></h1>
            <?php if (!empty($page_content)) { ?><div class="feature__tagline"><?php echo $page_content; ?></div><?php } ?>
          </div>
        <?php } else if (!empty($header_image) && !empty($page_content)) { ?>
          <div class="feature">
            <div class="feature__tagline"><?php echo str_replace('<a href="/book-a-table', '<a data-toggle="book-online" href="/book-a-table', $page_content); ?></div>
          </div>
        <?php } ?>
      </div>
    </section>
  <?php } ?>

  <div class="section menu">
    <div class="inner inner--narrow">

      <?php if ($post_slug == 'secret-menu') { ?>
        <?php showPageFeature('Top Secret', 'Secret Menu', 'Get your hands around these menu items... exclusively for The Herd!'); ?>
      <?php } ?>

      <div class="menu__allergy">
        <div class="menu__allergy-info">
          <img src="/images/circle-green-info.svg" alt="Allergy information icon" /> For allergy info, hit the icon shown to the left here
        </div>
      </div>

      <?php 
	  	  if (!empty($categoriesList)) {
          // Check how many categories have products in
          $numberOfValidCategories = 0;
          foreach ($categoriesList as $key => $section) {
            if ( empty($section['menu_items']) ) continue;
            $numberOfValidCategories++;
          }

          // Handle Categories the normal way
          if ( $numberOfValidCategories > 1 ) {
            foreach ($categoriesList as $key => $section) {
              if ( empty($section['menu_items']) ) continue;
              include 'menu/menu__section.php';
            }
          } else {
            // Handle CTA
            $menuCategoryId = array_keys($categoriesList)[0];
            include FS_ROOT . 'includes/inc_menu_category_cta.php';

            // Manually Output Category=
            foreach ( $categoriesList as $key => $section ) {
              if ( empty($section['menu_items']) ) continue; ?>

              <h2 class="menu-section__title menu-section__title--solo"><?= $section['title'] ?></h2>
              <div class="menu-section__description"><?= $section['description'] ?? '' ?></div>
              <div class="menu-section__items" style="display:flex;">
                <?php foreach ($section['menu_items'] as $key => $item) include 'menu/menu__item.php'; ?>
              </div>
              <?php
            }
          }
        } 
		  ?>
    </div>

    <?php if (!empty($hh_title) && !empty($hh_content)) { ?>
      <div class="inner">
        <div class="menu__hippo-hour">
          <div class="feature">
            <p class="feature__back"><?php echo str_replace('!', '', $hh_title); ?></p>
            <div class="feature__front">
              <h2 class="feature__title"><?php echo $hh_title; ?></h2>
              <div class="feature__tagline"><?php echo $hh_content; ?></div>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>
<?php } ?>