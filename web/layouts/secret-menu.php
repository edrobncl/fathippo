<?php

  if (isset($in_admin)) {

		$post_editables = json_encode([
      "post_tagline",
      "page_content",
      "header_image",
      "thumbnail_image"
    ]);

  } else {

  $menu = fetchResource([
    'post_type' => 'menu',
    'post_status' => 'unlisted',
    'id' => 910,
    'limit' => 1,
    'meta' => [
      'header_image',
      'post_tagline',
      'page_content',
    ]
  ]);

  if ($menu['count'] > 0) {
    foreach ($menu['data'][0] as $key => $value) {
      $$key = $value;
    }

    // ====================================
    //  Getting Menu 
    // ====================================
    $foodMenu = getMenu(910); // Secret Menu ID
    addAsset('modal');
    addAsset('carousel');
?>
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  
  <?php if (!empty($header_image)) { ?>
    <section class="section hero bottom-edge">
      <?php showPicture(['src'=>$header_image,'class'=>'hero__image']); ?>
      <div class="hero__overlay">
        <div class="hero__content">
          <h1 class="hero__title"><?php echo $post_title; ?></h1>
          <?php showButton('https://booking.favouritetable.com/?cc=229&skin=fathippo', null, 'Book a Table', 'a', $dataset=['data-toggle'=>'book-online']); ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($post_tagline) || !empty($page_content)) { ?>
    <section class="section intro">
      <div class="inner inner--narrow">
        <div class="page-content">
          <?php if (!empty($post_tagline)) { ?><h2><?php echo $post_tagline; ?></h2><?php } ?>
          <?php if (!empty($page_content)) echo $page_content; ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($foodMenu['categories'])) { ?>
    <section class="section xmas-specials">
      <div class="inner">
        <?php showPageFeature('Top Secret', 'Secret Menu', 'Get your hands around these menu items... exclusively for The Herd!'); ?>

        <div class="locations__actions">
          <div class="locations__controls">
            <button type="button" id="l-prev-arrow" title="Previous slide" class="slick-arrow slick-arrow--circle slick-prev"><?php showIcon('arrow-left-regular'); ?></button>
            <button type="button" id="l-next-arrow" title="Next slide" class="slick-arrow slick-arrow--circle slick-next"><?php showIcon('arrow-right-regular'); ?></button>
          </div>
        </div>

        <div class="food-carousel locations--slick">
          <?php foreach (array_column($foodMenu['categories'], 'category_items') as $items) {
            foreach ($items as $item) {
              $item['name'] = $item['post_title'];
              $item['price'] = $item['post_alt_title'];
              $item['description'] = $item['food_description'];
              $item['thumbnail'] = json_encode($item['food_image']);
              $item['details'] = $item['food_details'] ?? '';
              $item['ingredients'] = json_encode($item['ingredients']);
              if (isset($item['veg_ingredients'])) $item['veg_ingredients'] = json_encode($item['veg_ingredients']);
              if (isset($item['vegan_ingredients'])) $item['vegan_ingredients'] = json_encode($item['vegan_ingredients']);
              if (isset($item['df_ingredients'])) $item['df_ingredients'] = json_encode($item['df_ingredients']);
              if (isset($item['gf_ingredients'])) $item['gf_ingredients'] = json_encode($item['gf_ingredients']);
              ?><div><?php
                  // Split Food Details up as What it is vs what can be requested
                  $standardDetails = $requestDetails = [];
                  if ( ! empty($item['details']) ) {
                    foreach ( $item['details'] as $detail ) {
                      // If Not Requested
                      if ( in_array($detail, [1,3,5,7]) ) {
                        $standardDetails[] = $detail;
                      } else {
                        $requestDetails[] = $detail;
                      }
                    }
                  }
                ?><article class="menu-item">
                  <?php if ( ! empty($item['thumbnail']) ) { ?>
                    <div class="menu-item__thumbnail inner-overlay" data-toggle="modal_<?php echo $item['_id']; ?>">
                      <img src="<?php echo showPostImage($item['thumbnail']); ?>" alt="<?php echo $item['name']; ?>" />
                    </div>
                  <?php } ?>
                  <div class="menu-item__content">
                    <h3 class="menu-item__title"><?php echo $item['name']; ?></h3>

                    <div class="menu-item__details">
                      <div class="menu-item__price">
                        <?php if ( $item['price'] > 0 ) { ?>
                          &pound;<?php echo number_format($item['price'], 2, '.', ','); ?>
                        <?php } ?>
                      </div>
                      <div class="menu-item__info">

                        <div class="menu-item__info__wrap menu-item__info__wrap--toggle" role="button" data-toggle="modal_<?php echo $item['_id']; ?>">
                          <img src="/images/circle-green-info.svg" alt="Information" />
                        </div>

                        <?php if ( ! empty($standardDetails) ) {
                          foreach ( $standardDetails as $detail ) { ?>
                            <div class="menu-item__info__wrap menu-item__info__wrap--tooltip" data-content="<?= showMenuDetailName($detail) ?>">
                              <img src="<?= showMenuDetailIcon($detail) ?>" alt="" class="menu-item__info__icon" />
                            </div>
                            <?php
                          }
                        } ?>

                      </div>
                    </div>

                    <div class="menu-item__description">
                      <?php echo $item['description']; ?>
                    </div>

                    <?php if ( ! empty($requestDetails) ) { ?>
                      <p class="menu-item__details">
                        <span class="menu-item__detail">On Request:</span>
                        <?php foreach ($requestDetails as $detail) { ?>
                          <span class="menu-item__detail menu-item__detail--tooltip" data-toggle="tooltip" data-content="<?= showMenuDetailName($detail) ?>">
                            <img src="<?= showMenuDetailIcon($detail) ?>" alt="" class="menu-item__detail__icon" />
                          </span><?php
                        } ?>
                      </p>
                    <?php } ?>
                  </div>
                </article>
              </div>
            <?php }
          } ?>
        </div>

        <?php foreach (array_column($foodMenu['categories'], 'category_items') as $items) {
          foreach ($items as $item) {
            $item['name'] = $item['post_title'];
            $item['price'] = $item['post_alt_title'];
            $item['description'] = $item['food_description'];
            $item['thumbnail'] = json_encode($item['food_image']);
            $item['details'] = $item['food_details'] ?? '';
            $item['ingredients'] = json_encode($item['ingredients']);
            if (isset($item['veg_ingredients'])) $item['veg_ingredients'] = json_encode($item['veg_ingredients']);
            if (isset($item['vegan_ingredients'])) $item['vegan_ingredients'] = json_encode($item['vegan_ingredients']);
            if (isset($item['df_ingredients'])) $item['df_ingredients'] = json_encode($item['df_ingredients']);
            if (isset($item['gf_ingredients'])) $item['gf_ingredients'] = json_encode($item['gf_ingredients']);

            include 'menu/menu__modal.php';
          }
        } ?>

        <?php if (false) { ?><div id="locations" class="food-carousel locations--slick">
          <?php foreach ($menu['data'] as $item) { ?>
            <article class="menu-item">
              <?php if (!empty($item['thumbnail'])) { ?>
                <div class="menu-item__thumbnail inner-overlay" data-toggle="modal_<?php $item['_id'];?>">
                  <img src="<?php echo showPostImage($item['thumbnail']); ?>" alt="<?php echo $item['post_title']; ?>" />
                </div>
                
                <div class="menu-item__content">
                  <h3 class="menu-item__title"><?php echo $item['post_title']; ?></h3>

                  <div class="menu-item__details">
                    <div class="menu-item__price">
                      <?php if ($item['post_alt_title'] > 0) { ?>
                        &pound;<?php echo number_format($item['post_alt_title'], 2, '.', ','); ?>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </article>
          <?php } ?>
        </div><?php } ?>
      </div>
    </section>
  <?php } ?>
  <?php
  // Include Testimonials
  include_once FS_ROOT . 'includes/inc_reviews_strip.php';
?>

</main>
<?php } 
} ?>