<?php
	if (isset($in_admin)) {
        
		$post_editables = json_encode([
      "post_tagline",
      "page_content"
    ]);

	} else {
?>
<main id="main" class="main">
  <section class="section page-not-found">  
    <div class="inner inner--narrow">
      <h1 class="page-not-found__title">Error 404</h1>
      <?php if (!empty($post_tagline)) { ?><p class="page-not-found__tagline"><?php echo $post_tagline; ?></p><?php } ?>
      <?php if (!empty($page_content)) { ?><div class="page-not-found__content"><?php echo $page_content; ?></div><?php } ?>
      <div class="page-not-found__actions"><?php showButton('/', 'button--outlined', 'Home Page'); ?></div>
    </div>
  </section>
</main>
<?php } ?>