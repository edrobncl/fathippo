<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "page_content",
      "post_tagline2",
      "page_content2",
      "post_tagline3",
      "page_content3",
      "post_tagline4",
      "page_content4",
      "header_image",
      "page_content2_image",
      "page_content3_image",
      "page_content4_image"
    ]);

  } else {
?>
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include_once FS_ROOT.'includes/inc_masthead.php'; ?>

  <section class="section intro">
    <div class="inner">
      <div class="container">
        <div class="container__column">
          <img loading="lazy" src="/images/first-aid-kit-landscape.svg" alt="Group of Burgers" width="458" height="257" class="intro__image" />
        </div>
        <div class="container__column">
          <div class="page-content">
            <?php if (!empty($page_content)) echo $page_content; ?>
            <?php showButton('https://fathippo.co.uk/book-a-table', 'button--outlined', 'Book a Table', 'a', $dataset=['data-toggle'=>'book-online']); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php if (!empty($page_content2)) { ?>
    <section class="section about-intro">
      <div class="inner">
        <div class="container">
          <div class="container__column">
            <?php if (!empty($page_content2_image)) showPicture([
              'src' => $page_content2_image,
              'class' => 'allergens__image'
            ]); ?>
          </div>
          <div class="container__column allergens__content allergens__content--right">
            <h2 class="allergens__title"><?php echo $post_tagline2; ?></h2>
          </div>
        </div>
        <div class="container">
          <div class="container__column">
            <img loading="lazy" src="/images/smashing-burger.png" alt="Smashing Burger" width="500" height="356" />
          </div>
          <div class="container__column">
            <div class="page-content page-content--pad-top">
              <?php echo $page_content2; ?>
              <?php showButton('/menus/food/', 'button--outlined', 'Browse Menu'); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <section class="section ready-to-eat">
    <div class="inner ready-to-eat__inner">
      <h2 class="ready-to-eat__title">Ready to Eat?</h2>
      <div class="container">
        <div class="ready-to-eat__action">
          <a href="https://booking.favouritetable.com/?cc=229&skin=fathippo" class="ready-to-eat__link inner-overlay" title="Book a table" rel="noreferrer" data-toggle="book-online"><i class="icon-calendar-plus"></i> <span>Book a Table</span></a>
        </div>
        <div class="ready-to-eat__action">
          <a href="http://fathippo.co.uk/app" class="ready-to-eat__link" title="Click & Collect"><i class="icon-cursor-click" rel="noreferrer"></i> <span>Click & Collect</span></a>
        </div>
        <div class="ready-to-eat__action">
          <a href="https://fathippo.order.deliveroo.co.uk/" class="ready-to-eat__link" title="Order Delivery" rel="noreferrer"><i class="icon-deliveroo"></i> <span>Order Delivery</span></a>
        </div>
        <div class="ready-to-eat__action">
          <a href="/locations/" class="ready-to-eat__link inner-overlay" title="Find a Hippo"><i class="icon-map-pin"></i> <span>Find a Hippo</span></a>
        </div>
      </div>
    </div>
  </section>

  <?php if (!empty($page_content3)) { ?>
    <section class="section about-intro">
      <div class="inner">
        <div class="container">
          <div class="container__column allergens__content allergens__content--left">
            <h2 class="allergens__title"><?php echo $post_tagline3; ?></h2>
          </div>
          <div class="container__column allergens__image-wrap">
            <?php if (!empty($page_content3_image)) showPicture([
              'src' => $page_content3_image,
              'class' => 'allergens__image allergens__image--rev'
            ]); ?>
          </div>
        </div>
        <div class="container allergens__text-strip">
          <div class="container__column allergens__illustration-wrap">
            <img loading="lazy" src="/images/walking-bag.png" alt="Walking Bag" width="350" height="206" class="allergens__illustration" />
          </div>
          <div class="container__column">
            <div class="page-content page-content--pad-top">
              <?php echo $page_content3; ?>
              <div class="page-content__actions">
                <?php showButton('https://fathippo.order.deliveroo.co.uk/', 'button--outlined', 'Order Delivery'); ?>
                <?php showButton('http://fathippo.co.uk/app', 'button--outlined', 'Click & Collect'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>  
  <?php } ?>

  <?php if (!empty($page_content4)) { ?>
    <section class="section about-intro">
      <div class="inner">
        <div class="container">
          <div class="container__column">
            <?php if (!empty($page_content4_image)) showPicture([
              'src' => $page_content4_image,
              'class' => 'allergens__image'
            ]); ?>
          </div>
          <div class="container__column allergens__content allergens__content--right">
            <h2 class="allergens__title"><?php echo $post_tagline4; ?></h2>
          </div>
        </div>
        <div class="container allergens__text-strip">
          <div class="container__column">
            <div class="page-content page-content--pad-top">
              <?php echo $page_content4; ?>
              <div class="page-content__actions">
                <?php showButton('/menus/fat-hippo-fleet-allergens/', 'button--outlined', 'Fleet Allergens'); ?>
              </div>
            </div>
          </div>
          <div class="container__column">
            <img loading="lazy" src="/images/fleet-chevy.png" alt="Fleet Chevy" width="500" height="356" />
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>
<?php } ?>