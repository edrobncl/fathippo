<?php
  if (isset($in_admin)) {
    $post_editables = json_encode([
      "post_tagline",
      "page_content",
    ]);
  } else {

    $locations = fetchResource([
      'post_type' => 'location',
      'post_status' => 'live',
      'meta' => [
        'coming_soon',
        'location_concession' => 'concession',
        'office_address1' => 'addr_line_1',
        'office_address2' => 'addr_line_2',
        'office_address3' => 'addr_line_3',
        'office_city' => 'city',
        'office_postcode' => 'postcode',
        'office_phone' => 'telephone',
        'office_email' => 'email_addr',
        'opening_'.strtolower(date('l', strtotime(date('Y-m-d')))) => 'opening',
        'location_image2' => 'thumbnail',
        'location_image3' => 'illustration',
        'office_latitude' => 'latitude',
        'office_longitude' => 'longitude',
        'menu',
        'book_table',
        'click_collect',
        'order_delivery',
      ],
      'exclude_meta' => [
        'coming_soon' => 'yes'
      ]
    ]);

?>

<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>

  <?php if (true) { ?><div class="section map">
    <div class="location__arrays">
      <? foreach ($locations['data'] as $location) { 
        if (!empty($location['_id']) && !empty($location['latitude']) && !empty($location['longitude'])) { 
          $address = trim($location['addr_line_1']);
          if (!empty($location['addr_line_2'])) {
            $address .= ', '.trim($location['addr_line_2']);
          }
          if (!empty($location['addr_line_3'])) {
            $address .= ', '.trim($location['addr_line_3']);
          }
          $address .= ', '.trim($location['city']).', '.trim($location['postcode']);
          $thumb = (isset($location['thumbnail']))?json_decode($location['thumbnail'], true):['post_slug' => '/images/fallback.jpg'];
          if (!empty($location['concession']) && $location['concession'] == 'yes') { $type = 'concession'; } else { $type = 'restaurant'; }
          $details = [
            'id' => $location['_id'],
            'latitude' => $location['latitude'], 
            'longitude' => $location['longitude'],
            'type' => $type
          ]; ?>
          <input class="location-json" type="hidden" value='<?=json_encode($details);?>' />
        <? }
      } ?>
    </div>
    <?php addAsset('map'); ?>
    <div id="google-map" class="embed-responsive google-map embed-responsive-21by9 map__canvas"></div>
  </div><?php } ?>

  <?php if ($locations['count'] > 0) { ?>
    <section class="section locations">
      <div class="inner">
        <div class="feature feature--left">
          <h1 class="feature__title feature__title--large">Find a Hippo</h1>
          <img src="/images/burgerpin.svg" alt="Burger Pin" class="feature__image" />
        </div>

        <?php foreach ($locations['data'] as $location) {
          $thePath = buildPath($location['_id']); 
          $thumb = (isset($location['thumbnail']))?$location['thumbnail']:null; ?>
          <article class="location-card" id="<?=$location['_id'];?>">

            <a href="<?php echo $thePath; ?>" class="location-card__thumbnail">
              <?php if (!empty($thumb)) showPicture($thumb); ?>
            </a>
            <div class="location-card__content">
              <div class="location-card__title-wrap">
                <h2 class="location-card__title"><?php echo $location['post_alt_title']; ?></h2>
              </div>
              <?php if (!empty($location['coming_soon']) && $location['coming_soon'] == 'yes') { ?>
                <p class="location-card__coming-soon">Coming Soon!</p>
              <?php } ?>
              <?php if (!empty($location['concession']) && $location['concession'] == 'yes') { ?>
                <p class="location-card__concession">Concession</p>
              <?php } ?>
              <div class="location-card__details">
                <?php if (!empty($location['addr_line_1'])) { ?>
                  <div class="location-card__detail">
                    <div class="location-card__detail__icon"><i class="icon-map-pin"></i></div>
                    <div class="location-card__detail__text"><?php showAddress($location); ?></div>
                  </div>
                <? } ?>
                <? if (!empty($location['opening'])) {  ?>
                  <div class="location-card__detail">
                    <div class="location-card__detail__icon"><i class="icon-clock"></i></div>
                    <div class="location-card__detail__text">
                      Today: <?php echo $location['opening']; ?>
                    </div>
                  </div>
                <?php } ?>
                <?php if (!empty($location['telephone'])) { ?>
                  <div class="location-card__detail">
                    <div class="location-card__detail__icon"><i class="icon-phone"></i></div>
                    <div class="location-card__detail__text">
                      <a href="tel:<?php echo str_replace(' ', '', $location['telephone']); ?>" title="Call <?php echo $location['post_title']; ?>"><?php echo $location['telephone']; ?></a>
                    </div>
                  </div>
                <?php } ?>
              </div>

              <div class="location-card__actions">
                <?php if (!empty($location['book_table'])) { ?>
                  <a data-toggle="book-online" data-venue="true" href="<?php echo $location['book_table']; ?>" title="Book a table at <?php echo $location['post_title']; ?>" rel="noreferrer" target="_blank" class="location-card__action inner-overlay">
                    <span class="location-card__action__inner">
                      <i class="icon-calendar-plus"></i>
                      <span>Book a Table</span>
                    </span>
                  </a>
                <?php } ?>

                <?php if (!empty($location['click_collect'])) { ?>
                  <a href="https://fathippo.co.uk/app" title="Order and Collect FatHippo" target="_blank" rel="noreferrer" class="location-card__action inner-overlay">
                    <span class="location-card__action__inner">
                      <i class="icon-cursor-click"></i>
                      <span>Click &amp; Collect</span>
                    </span>
                  </a>
                <?php } ?>

                <?php if (!empty($location['order_delivery'])) { ?>
                  <a href="<?php echo $location['order_delivery']; ?>" title="Order FatHippo delivery" target="_blank" rel="noreferrer" class="location-card__action inner-overlay">
                    <span class="location-card__action__inner">
                      <i class="icon-deliveroo"></i>
                      <span>Order Delivery</span>
                    </span>
                  </a>
                <?php } ?>

                 <?php if (!empty($location['menu'])) { ?>
                  <a href="<?php echo $location['menu']; ?>" title="View Menu" target="_blank" rel="noreferrer" class="location-card__action inner-overlay">
                    <span class="location-card__action__inner">
                      <i class="icon-burger"></i>
                      <span>View Menu</span>
                    </span>
                  </a>
                <?php } ?>

                <a href="<?php echo $thePath; ?>" title="" class="location-card__action inner-overlay">
                  <span class="location-card__action__inner">
                    <i class="icon-chevron-right"></i>
                    <span>Find out More</span>
                  </span>
                </a>
              </div>

              <?php if (!empty($location['illustration'])) { ?>
                <img src="<?php echo showPostImage($location['illustration']); ?>" alt="<?php echo $location['post_title']; ?>" class="location-card__illustration" />
              <?php } ?>
            </div>
          </article>
        <?php } ?>
      </div>
    </section>
  <?php } ?>

  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>


<?php } ?>