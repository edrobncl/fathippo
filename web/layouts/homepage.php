<?php
	if (isset($in_admin)) {
        
		$post_editables = json_encode([
      "post_tagline",
      "post_tagline2",
      "page_content2",
      "post_tagline3",
      "page_content3_image",
      "page_content3",
      "post_tagline4",
      "page_content4",
    ]);

  } else {

    /**
     * Fetch all available locations from the database
     */

    $opening_times = 'opening_'.strtolower(date('l', strtotime(date('Y-m-d'))));

    $fetchLocations = $pdo->prepare("
      SELECT _id, post_title, post_alt_title,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'coming_soon') AS coming_soon,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'location_concession') AS concession,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'office_address1') AS addr_line_1,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'office_address2') AS addr_line_2,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'office_address3') AS addr_line_3,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'office_city') AS city,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'office_postcode') AS postcode,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'office_phone') AS telephone,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'office_email') AS email_addr,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = '$opening_times') AS opening,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'location_image') AS thumbnail,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'location_image2') AS large_thumbnail,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'book_table') AS book_table,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'click_collect') AS click_collect,
        (SELECT meta_value FROM er_postmeta WHERE post_id = er_posts._id AND meta_name = 'order_delivery') AS order_delivery
      FROM er_posts
      WHERE post_type = ?
      AND post_status = ?
      ORDER BY menu_order ASC
    ");

    $fetchLocations->execute(['location', 'live']);
?> 

<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include_once FS_ROOT.'includes/inc_carousel.php'; ?>

  <section class="section quick-links homepage-quick-links">
    <div class="inner">

      <?php if (!empty($post_tagline)) { ?>
        <div class="feature quick-links__title" style="display:none;">
          <h1 class="feature__title"><?php echo $post_tagline; ?></h1>
        </div>
      <?php } ?>

      <div class="flex-container flex-container--quick-links">
        <?php showFindAHippoCard(); ?>
        <?php showBookTableCard(); ?>
        <?php showClickCollectCard(); ?>
      </div>
    </div>
  </section>

  <?php if ($fetchLocations->rowCount() > 0) { ?>
    <section class="section locations">
      <div class="inner">
        <?php showPageFeature('Find Us', $post_tagline2, $page_content2 ?? ''); ?>

        <div class="locations__actions">
          <div class="locations__controls">
            <button type="button" id="l-prev-arrow" title="Previous slide" class="slick-arrow slick-arrow--circle slick-prev"><?php showIcon('arrow-left-regular'); ?></button>
            <button type="button" id="l-next-arrow" title="Next slide" class="slick-arrow slick-arrow--circle slick-next"><?php showIcon('arrow-right-regular'); ?></button>
          </div>
          <div class="locations__link">
            <a href="/locations/" title="View all Locations" class="inline-button">
              <span>All Locations</span>
              <?php showIcon('chevron-right-regular'); ?>
            </a>
          </div>
        </div>

        <div id="locations" class="flex-container flex-container--locations locations__container locations--slick">
          <?php while ($location = $fetchLocations->fetch()) {
            include FS_ROOT.'includes/inc_location_listing.php';
          } ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($post_tagline3)) { ?>
    <section class="section large-image">
      <div class="inner large-image__inner">
        <picture>
          <source media="(max-width: 575px)" srcset="/images/eat--600px.webp" type="image/webp" />
          <source media="(max-width: 575px)" srcset="/images/eat--600px.png" type="image/png" />
          <source srcset="/images/eat.webp" type="image/webp" />
          <img src="/images/eat.png" loading="lazy" alt="Eat" class="large-image__illustration" width="90" height="100" />
        </picture>
      </div>
      <div class="large-image__photo">
        <?php showPicture($page_content3_image ?? '/images/the-good-kind-of-gluttony.jpg'); ?>
        <div class="large-image__text">
          <div class="inner">
            <h2 class="large-image__title"><?php echo $post_tagline3; ?></h2>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($page_content3)) { ?>
    <div class="section about about--homepage">
      <div class="inner">
        <div class="about__content">
          <div class="about__text"><?php echo $page_content3; ?></div>
          <?php showButton('https://fathippo.co.uk/book-a-table', 'button--outlined', 'Book a Table', 'a', $dataset=['data-toggle'=>'book-online']); ?>
        </div>
      </div>
    </div>
  <?php } ?>

  <?php if (!empty($page_content4)) { ?>
    <section class="section join">
      <div class="inner">
        <div class="flex-container flex-container--join">
          <div class="join__illustration">
            <picture>
              <source media="(max-width: 575px)" srcset="/images/join-the-herd--600px.webp" type="image/webp" />
              <source media="(max-width: 575px)" srcset="/images/join-the-herd--600px.png" type="image/png" />
              <source srcset="/images/join-the-herd.webp" type="image/webp" />
              <img src="/images/join-the-herd.png" loading="lazy" alt="Join the Herd" class="join__illustration__image" width="330" height="235" />
            </picture>
          </div>
          <div class="join__text">
            <div class="feature feature--left">
              <h2 class="feature__title"><?php echo $post_tagline4 ?? 'Join Our Herd'; ?></h2>
              <div class="feature__tagline"><?php echo $page_content4; ?></div>
              <?php showButton(22, 'button--outlined', 'Tell Me More'); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php include_once FS_ROOT.'includes/inc_fathippo-app_strip.php'; ?>
</main>
<?php } ?>

