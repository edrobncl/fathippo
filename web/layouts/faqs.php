<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "post_tagline",
      "page_content",
      "header_image",
      "thumbnail_image"
    ]);

  } else {

    $faqs = fetchResource([
      'post_type' => 'faq',
      'post_alt_title' => $post_id,
      'meta' => [
        'answer'
      ]
    ]);
?>

<main id="main" class="main">
  <!-- <div class="inner main__illustration main__illustration--convenience">
    <img src="/images/hands-up-shorter.svg" alt="Stuff Face Burger" width="140" height="140" class="main__illustration" />
  </div> -->

  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>

  <?php if (!empty($post_tagline)) { ?>
    <section class="section plain-masthead">
      <div class="inner inner--narrow">
        <div class="feature">
          <h1 class="feature__title feature__title--large"><?php echo $post_title; ?></h1>
          <?php if (!empty($post_tagline)) { ?><div class="feature__tagline"><?php echo $post_tagline; ?></div><?php } ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if ($faqs['count'] > 0) {
    addAsset('toggle'); ?>
    
    <section class="section faqs">
      <div class="inner inner--narrow">
        <?php foreach ($faqs['data'] as $faq) { ?>
          <article class="faq">
            <div class="faq__header inner-overlay" data-toggle="faq_<?php echo $faq['_id']; ?>">
              <h3 class="faq__title"><?php echo $faq['post_title']; ?></h3>
              <button type="button" class="small-button"><i class="icon-plus"></i></button>
            </div>
            <div class="faq__answer" data-content="faq_<?php echo $faq['_id']; ?>">
              <div class="faq__wrap page-content" data-wrap="faq_<?php echo $faq['_id']; ?>"><?php echo $faq['answer']; ?></div>
            </div>
          </article>
        <?php } ?>
      </div>
    </section>
  <?php } ?>
</main>

<?php } ?>