<?php
	if (isset($in_admin)) {
		$post_editables = json_encode([
      "page_content",
      "post_tagline2",
      "page_content2",
      "post_tagline3",
      "page_content3",
      "post_tagline4",
      "page_content4",
      "post_tagline5",
      "page_content5",
      "post_tagline6",
      "page_content6",
      "post_tagline7",
      "page_content7",
      "post_tagline8",
      "page_content8",
      "header_image",
    ]);
  } else {
?> 
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include FS_ROOT.'includes/inc_masthead.php'; ?>

  <section class="section intro">
    <div class="inner">
      <div class="container">
        <div class="container__column">
          <img loading="lazy" src="/images/join-the-herd.png" alt="Group of Burgers" width="458" height="257" class="intro__image" />
        </div>
        <div class="container__column">
          <div class="page-content">
            <?php if (!empty($page_content)) echo $page_content; ?>
            <?php showButton('#rewards', 'button--outlined', 'Join the Herd'); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section why-us">
    <div class="inner">
      <?php showPageFeature('Why Join?', $post_tagline2, $page_content2); ?>

      <div class="container why-us__cards">
        <?php if (!empty($post_tagline3) && !empty($page_content3)) { ?>
          <article class="why-us__card">
            <div class="why-us__icon"><i class="icon-calendar"></i></div>
            <h3 class="why-us__title"><?php echo $post_tagline3; ?></h3>
            <div class="why-us__tagline"><?php echo $page_content3; ?></div>
          </article>
        <?php } ?>

        <?php if (!empty($post_tagline4) && !empty($page_content4)) { ?>
          <article class="why-us__card">
            <div class="why-us__icon"><i class="icon-burger"></i></div>
            <h3 class="why-us__title"><?php echo $post_tagline4; ?></h3>
            <div class="why-us__tagline"><?php echo $page_content4; ?></div>
          </article>
        <?php } ?>

        <?php if (!empty($post_tagline5) && !empty($page_content5)) { ?>
          <article class="why-us__card">
            <div class="why-us__icon"><i class="icon-van"></i></div>
            <h3 class="why-us__title"><?php echo $post_tagline5; ?></h3>
            <div class="why-us__tagline"><?php echo $page_content5; ?></div>
          </article>
        <?php } ?>
      </div>
    </div>
  </section>

  <section id="rewards" class="section rewards">
    <div class="inner rewards__wrap bg-diagonal">
      <h2 class="rewards__title"><span>Join the Herd to Reap the Rewards</span></h2>

      <div class="container rewards__cards">
        <?php if (!empty($page_content6)) { ?>
          <article class="reward-card">
            <div class="reward-card__icon">1</div>
            <div class="reward-card__tagline"><?php echo str_replace(['<p>', '</p>'], ['<p><span>', '</span></p>'], $page_content6); ?></div>
          </article>
        <?php } ?>

        <?php if (!empty($page_content7)) { ?>
          <article class="reward-card reward-card--middle">
            <div class="reward-card__icon">2</div>
            <div class="reward-card__tagline"><?php echo str_replace(['<p>', '</p>'], ['<p><span>', '</span></p>'], $page_content7); ?></div>
          </article>
        <?php } ?>

        <?php if (!empty($page_content8)) { ?>
          <article class="reward-card">
            <div class="reward-card__icon">3</div>
            <div class="reward-card__tagline"><?php echo str_replace(['<p>', '</p>'], ['<p><span>', '</span></p>'], $page_content8); ?></div>
          </article>
        <?php } ?>
      </div>

      <div class="rewards__action">
        <?php showButton('http://onelink.to/npwksw', 'button--outlined', 'Join the Herd'); ?>
      </div>
    </div>
  </section>

  <?php include_once FS_ROOT.'includes/inc_fathippo-app_strip.php'; ?>
  <?php
  // Include Testimonials
  include_once FS_ROOT . 'includes/inc_reviews_strip.php';
?>

</main>
<?php } ?>