<?php
  if (isset($in_admin)) {

  } else {
?>

<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>

  <?php if (empty($location_gallery)) { ?>
    <div class="section location">
      <picture>
        <source srcset="<?php echo showPostImage($location_header ?? $location_image2, 'webp'); ?>" type="image/webp" />
        <img src="<?php echo showPostImage($location_header ?? $location_image2); ?>" alt="<?php echo $post_title; ?>" width="1920" height="1024" />
      </picture>
      <div class="location__overlay">
        <div class="inner">
          <a href="/locations/" class="button button--small">
            <?php showIcon('arrow-left-regular'); ?> Back
          </a>
        </div>
      </div>
    </div>
  <?php } else {
    include_once FS_ROOT.'includes/inc_carousel.php';
  } ?>

  <section class="section">
    <div class="inner">
      <div class="location-details">
        <div class="location-details__main">
          <h1 class="location-details__title">
            <span class="location-details__title__first">Fat Hippo</span><span class="location-details__title__second"><?php echo $post_alt_title; ?></span>
          </h1>
          <?php if (!empty($location_concession) && $location_concession == 'yes') { ?>
            <p class="location-details__concession">Concession</p>
          <?php } ?>
        </div>
        <div class="location-details__amenities">
          <?php if (!empty($location_amenities)) showLocationAmenities($location_amenities); ?>
        </div>
      </div>

      <div class="flex-container flex-container--quick-links location-details__links">
        <?php
          if (!empty($book_table)) {
            showBookTableCard([
              'url' => $book_table,
            ]);
          }
          if (!empty($click_collect)) {
            showClickCollectCard([
              'url' => $click_collect,
              'image' => '/images/burgerpin.svg'
            ]);
          }
          if (!empty($order_delivery)) {
            showOrderDeliveryCard([
              'url' => $order_delivery,
              'image' => '/images/bag.svg'
            ]);
          }
          if (!empty($menu)) {
            showMenuCard([
              'url' => $menu,
              'image' => '/images/click-burger.png'
            ]);
          }
        ?>
      </div>
    </div>
  </section>

  <section class="section contact-details">
    <div class="inner">
      <div class="contact-details__container">
        <div class="contact-details__group">
          <div class="contact-details__group-wrap">
            <h2 class="contact-details__title">Contact Details</h2>

            <div class="contact-details__detail">
              <div class="contact-details__detail__icon"><i class="icon-map-pin"></i></div>
              <div class="contact-details__detail__text">
                <?php
                  showAddress([
                    'addr_line_1' => $office_address1 ?? null,
                    'addr_line_2' => $office_address2 ?? null,
                    'addr_line_3' => $office_address3 ?? null,
                    'city' => $office_city ?? null,
                    'postcode' => $office_postcode ?? null,
                  ]);

                  if (!empty($office_map_link)) { ?>
                    <a href="<?php echo $office_map_link; ?>" title="Open in Google Map" target="_blank" rel="noreferrer" class="contact-details__detail__link">Get Directions <i class="icon-chevron-right"></i></a>
                <?php } ?>
              </div>
            </div>

            <?php if (!empty($office_email)) { ?>
              <div class="contact-details__detail">
                <div class="contact-details__detail__icon"><i class="icon-envelope"></i></div>
                <div class="contact-details__detail__text">
                  <a href="mailto:<?php echo $office_email; ?>" title="Send us an email"><?php echo $office_email; ?></a>
                </div>
              </div>
            <?php } ?>

            <?php if (!empty($office_phone)) { ?>
              <div class="contact-details__detail">
                <div class="contact-details__detail__icon"><i class="icon-phone"></i></div>
                <div class="contact-details__detail__text">
                  <a href="mailto:<?php echo $office_phone; ?>" title="Call us"><?php echo $office_phone; ?></a>
                </div>
              </div>
            <?php } ?>

            <ul class="reset--list social-media social-media--align-left">
              <?php if (!empty($twitter_link) || !empty($twitter_handle)) { ?><li class="social-media__item"><a href="https://www.twitter.com/<?php echo $twitter_link ?? $twitter_handle; ?>" title="Follow Fat Hippo on Twitter" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-twitter"></i></a></li><?php } ?>
              <?php if (!empty($instagram_link) || !empty($instagram_handle)) { ?><li class="social-media__item"><a href="https://www.instagram.com/<?php echo $instagram_link ?? $instagram_handle; ?>" title="Follow Fat Hippo on Instagram" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-instagram"></i></a></li><?php } ?>
              <?php if (!empty($facebook_link) || !empty($facebook_handle)) { ?><li class="social-media__item"><a href="<?php echo $facebook_link ?? $facebook_handle; ?>" title="Like Fat Hippo on Facebook" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-facebook"></i></a></li><?php } ?>
              <?php if (!empty($spotify_link) || !empty($spotify_handle)) { ?><li class="social-media__item"><a href="<?php echo $spotify_link ?? $spotify_handle; ?>" title="Listen to playlist curated by Fat Hippo" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-spotify"></i></a></li><?php } ?>
            </ul>
          </div>

          <?php if (!empty($location_image3)) { ?>
            <img src="<?php echo showPostImage($location_image3); ?>" loading="lazy" alt="" class="contact-details__illustration" />  
          <?php } ?>
        </div>

        <div class="contact-details__group">
          <h2 class="contact-details__title">Opening Hours</h2>
          <div class="contact-details__detail">
            <div class="contact-details__detail__icon"><i class="icon-clock"></i></div>
            <div class="contact-details__detail__text">
              <div>Mon: <?php echo show_correct_times($opening_monday, $office_country); ?></div>
              <div>Tue: <?php echo show_correct_times($opening_tuesday, $office_country); ?></div>
              <div>Wed: <?php echo show_correct_times($opening_wednesday, $office_country); ?></div>
              <div>Thu: <?php echo show_correct_times($opening_thursday, $office_country); ?></div>
              <div>Fri: <?php echo show_correct_times($opening_friday, $office_country); ?></div>
              <div>Sat: <?php echo show_correct_times($opening_saturday, $office_country); ?></div>
              <div>Sun: <?php echo show_correct_times($opening_sunday, $office_country); ?></div>
            </div>
          </div>
          <?php 
            if ( ! empty($opening_bank_holiday) ) { ?>
              <h2 class="contact-details__title">Bank Holiday Hours</h2>
              <div class="contact-details__detail">
                <div class="contact-details__detail__icon"><i class="icon-clock"></i></div>
                <div class="contact-details__detail__text">
                  <div><?= $opening_bank_holiday ?></div>
                </div>
              </div>
              <?php
            }

            if ( ! empty($opening_christmas) ) { ?>
              <h2 class="contact-details__title">Christmas Hours</h2>
              <div class="contact-details__detail">
                <div class="contact-details__detail__icon"><i class="icon-clock"></i></div>
                <div class="contact-details__detail__text">
                  <div><?= $opening_christmas ?></div>
                </div>
              </div>
              <?php
            }
          ?>
        </div>
      </div>
    </div>
  </section>

  <section class="section location-cta">
    <div class="inner">

      <div class="page-content__container">
        <div class="page-content__group">
          <h2 class="page-content__title"><?php echo $page_tagline; ?></h2>
          <img src="<?php echo showPostImage($location_image4); ?>" loading="lazy" alt="Edinburgh" class="page-content__illustration" />
        </div>

        <div class="page-content__group">
          <div class="page-content__text"><?php echo $page_content; ?></div>
          <div class="page-content__actions">
            <?php if (!empty($book_table)) {
              showButton($book_table, 'button button--outlined', 'Book a Table', 'a', $dataset=['data-toggle'=>'book-online']);
            } ?>
            <?php if (!empty($click_collect)) {
              showButton($click_collect, 'button button--outlined', 'Order C+C', 'a');
            } ?>
          </div>
        </div>
      </div>

      <div class="page-content__socials">
        <div>Follow <?php echo $post_title; ?></div>
        <ul class="reset--list social-media social-media--align-left">
          <?php if (!empty($twitter_link) || !empty($twitter_handle)) { ?><li class="social-media__item"><a href="https://www.twitter.com/<?php echo $twitter_link ?? $twitter_handle; ?>" title="Follow Fat Hippo on Twitter" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-twitter"></i></a></li><?php } ?>
          <?php if (!empty($instagram_link) || !empty($instagram_handle)) { ?><li class="social-media__item"><a href="https://www.instagram.com/<?php echo $instagram_link ?? $instagram_handle; ?>" title="Follow Fat Hippo on Instagram" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-instagram"></i></a></li><?php } ?>
          <?php if (!empty($facebook_link) || !empty($facebook_handle)) { ?><li class="social-media__item"><a href="<?php echo $facebook_link ?? $facebook_handle; ?>" title="Like Fat Hippo on Facebook" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-facebook"></i></a></li><?php } ?>
          <?php if (!empty($spotify_link) || !empty($spotify_handle)) { ?><li class="social-media__item"><a href="<?php echo $spotify_link ?? $spotify_handle; ?>" title="Listen to playlist curated by Fat Hippo" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-spotify"></i></a></li><?php } ?>
        </ul>
      </div>

    </div>
  </section>
  <?php
  // Include Testimonials
  include_once FS_ROOT . 'includes/inc_reviews_strip.php';
?>

</main>

<?php } ?>