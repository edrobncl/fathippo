<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "page_content",
      "post_tagline2",
      "page_content2",
      "post_tagline3",
      "page_content3",
      "page_content4",
      "header_image",
      "page_content3_image",
    ]);

  } else {

    $locations = fetchResource([
      'post_type' => 'location',
      'post_status' => 'live',
      'meta' => [
        'coming_soon',
        'location_concession' => 'concession',
        'office_address1' => 'addr_line_1',
        'office_address2' => 'addr_line_2',
        'office_address3' => 'addr_line_3',
        'office_city' => 'city',
        'office_postcode' => 'postcode',
        'office_phone' => 'telephone',
        'office_email' => 'email_addr',
        'opening_'.strtolower(date('l', strtotime(date('Y-m-d')))) => 'opening',
        'location_image' => 'thumbnail',
        'book_table',
        'click_collect',
        'order_delivery',
      ]
    ]);

?> 
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include FS_ROOT.'includes/inc_masthead.php'; ?>

  <?php if (!empty($page_content)) { ?>
    <section class="section intro intro--contact">
      <div class="inner">
        <div class="container">
          <div class="container__column intro__image-wrap">
            <img loading="lazy" src="/images/contact-paper-plane.svg" alt="Group of Burgers" width="458" height="257" class="intro__image" />
          </div>
          <div class="container__column">
            <div class="page-content">
              <?php if (!empty($page_content)) echo $page_content; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if ($locations['count'] > 0) {
    addAsset('carousel'); ?>
    <section class="section about-intro">
      <div class="inner">
        <?php showPageFeature('Find Us', $post_tagline2, $page_content2); ?>

        <div class="locations__actions">
          <div class="locations__controls">
            <button type="button" id="l-prev-arrow" title="Previous slide" class="slick-arrow slick-arrow--circle slick-prev"><?php showIcon('arrow-left-regular'); ?></button>
            <button type="button" id="l-next-arrow" title="Next slide" class="slick-arrow slick-arrow--circle slick-next"><?php showIcon('arrow-right-regular'); ?></button>
          </div>
          <div class="locations__link">
            <a href="/locations/" title="View all Locations" class="inline-button">
              <span>All Locations</span>
              <?php showIcon('chevron-right-regular'); ?>
            </a>
          </div>
        </div>

        <div id="locations" class="flex-container flex-container--locations locations__container locations--slick">
          <?php foreach ($locations['data'] as $location) {
            include FS_ROOT.'includes/inc_location_listing.php';
          } ?>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php if (!empty($page_content3)) { ?>
    <section class="section about-intro">
      <div class="inner">
        <div class="container">
          <div class="container__column">
            <?php if (!empty($page_content3_image)) showPicture([
              'src' => $page_content3_image,
              'class' => 'allergens__image'
            ]); ?>
          </div>
          <div class="container__column allergens__content allergens__content--right">
            <h2 class="allergens__title"><?php echo $post_tagline3; ?></h2>
          </div>
        </div>
        <div class="container allergens__text-strip">
          <div class="container__column">
            <div class="page-content page-content--pad-top">
              <?php echo $page_content3; ?>
            </div>
          </div>
          <div class="container__column">
            <img loading="lazy" src="/images/fleet-contact-image.png" alt="Raised Burgers!" width="500" height="356" />
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <section class="section contact-cta">
    <div class="inner inner--narrow">
      <?php showPageFeature('Holla at HQ', 'Beefy Question?', 'For all enquiries, please get in touch.'); ?>

      <div class="container contact-cta__actions">
        <article class="contact-cta__card">
          <img src="/images/calendar.svg" alt="Calendar" width="140" height="140" class="contact-cta__image" />
          <h3 class="contact-cta__title">Drop us an email</h3>
          <a href="mailto:<?php echo $site_email; ?>" title="Drop us an email" class="contact-cta__email"><?php echo $site_email; ?></a>
        </article>

        <article class="contact-cta__card">
          <img src="/images/bag-1.svg" alt="Walking Bag" width="140" height="140" class="contact-cta__image" />
          <h3 class="contact-cta__title">Give us a follow</h3>
          <ul class="reset--list social-media">
            <?php if (!empty($twitter_handle)) { ?><li class="social-media__item"><a href="https://www.twitter.com/<?php echo $twitter_handle; ?>" title="Follow Fat Hippo on Twitter" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-twitter"></i></a></li><?php } ?>
            <?php if (!empty($instagram_handle)) { ?><li class="social-media__item"><a href="https://www.instagram.com/<?php echo $instagram_handle; ?>" title="Follow Fat Hippo on Instagram" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-instagram"></i></a></li><?php } ?>
            <?php if (!empty($facebook_handle)) { ?><li class="social-media__item"><a href="<?php echo $facebook_handle; ?>" title="Like Fat Hippo on Facebook" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-facebook"></i></a></li><?php } ?>
            <?php if (!empty($spotify_handle)) { ?><li class="social-media__item"><a href="<?php echo $spotify_handle; ?>" title="Play Fat Hippo pre-burger songs!" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-spotify"></i></a></li><?php } ?>
          </ul>
        </article>
      </div>
    </div>
  </section>
</main>
<?php } ?>