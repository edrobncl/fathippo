<div class="modal" data-modal="modal_<?php echo $item['_id']; ?>">
  <div class="modal__dialogue">
    <div class="modal__dialogue__header">
      <div class="modal__dialogue__details">
        <?php
          if ( ! empty($item['thumbnail']) ) { ?>
            <div class="modal__dialogue__image"><?php showPicture($item['thumbnail']); ?></div>
            <?php
          }
        ?>
        <div class="modal__dialogue__text">
          <h3 class="modal__dialogue__title"><?php echo $item['name']; ?></h3>
          <?php
            if ( ! empty($item['price']) && $item['price'] != 0 ) { ?>
              <p class="modal__dialogue__price">&pound;<?php echo number_format((float) $item['price'], 2); ?></p>
              <?php
            }
          ?>
          <?php echo $item['description']; ?>
        </div>
      </div>
      <div class="modal__dialogue__action">
        <button type="button" class="modal__dialogue__button" data-close="modal_<?php echo $item['_id']; ?>"><i class="icon-times"></i></button>
      </div>
    </div>
    <div class="modal__dialogue__content">
    <?php 
        // Show Food Details 
        if ( ! empty($item['details']) ) { ?> 
          <p>Suitable for:</p>
          <?php
            foreach ( $item['details'] as $detail ) {
              // Always use green icons when detail is in modal
              $iconDetailId = $detail; 
              if ( in_array($detail, [2,4,6,8]) ) $iconDetailId--; 
              ?>
              <p><img src="<?php echo showMenuDetailIcon($iconDetailId); ?>" alt="<?= showMenuDetailName($detail) ?>" class="menu-item__detail__icon" /> <?= showMenuDetailName($detail) ?></p>
              <?php
            }
          }
      ?>
      <h4 class="modal__dialogue__subtitle">Allergy Information</h4>
      <p class="modal__dialogue__shaded-text">Select the requirements to see the allergy information:</p>

      <div class="modal__dialogue__toggles">
        <?php if (!empty($item['ingredients'])) { ?> 
          <button type="button" class="button button--outlined button--toggle tab active" data-toggle="tab_<?php echo $item['_id']; ?>_standard">Standard</button>
        <?php } ?>
        <?php if (!empty($item['veg_ingredients'])) { ?>
          <button type="button" class="button button--outlined button--toggle tab" data-toggle="tab_<?php echo $item['_id']; ?>_veg">Vegetarian</button>
        <?php } ?>
        <?php if (!empty($item['vegan_ingredients'])) { ?>
          <button type="button" class="button button--outlined button--toggle tab" data-toggle="tab_<?php echo $item['_id']; ?>_vegan">Vegan</button>
        <?php } ?>
        <?php if (!empty($item['df_ingredients'])) { ?>
          <button type="button" class="button button--outlined button--toggle tab" data-toggle="tab_<?php echo $item['_id']; ?>_df">Dairy Free</button>
        <?php } ?>
        <?php if (!empty($item['gf_ingredients'])) { ?>
          <button type="button" class="button button--outlined button--toggle tab" data-toggle="tab_<?php echo $item['_id']; ?>_gf">Gluten Free</button>
        <?php } ?>
      </div>

      <div class="modal__dialogue__tabs">
        <?php if (!empty($item['ingredients'])) { ?>
          <div class="modal__dialogue__tab active" data-name="tab_<?php echo $item['_id']; ?>_standard">
            <?php 
              foreach ( json_decode($item['ingredients'], true) as $ingredient ) {
                outputMenuItemIngredient($ingredient);
              }
            ?>
          </div>
        <?php } ?>

        <?php if (!empty($item['veg_ingredients'])) { ?>
          <div class="modal__dialogue__tab" data-name="tab_<?php echo $item['_id']; ?>_veg">
            <?php 
              foreach ( json_decode($item['veg_ingredients'], true) as $ingredient ) {
                outputMenuItemIngredient($ingredient);
              }
            ?>
          </div>
        <?php } ?>

        <?php if (!empty($item['vegan_ingredients'])) { ?>
          <div class="modal__dialogue__tab" data-name="tab_<?php echo $item['_id']; ?>_vegan">
            <?php 
              foreach ( json_decode($item['vegan_ingredients'], true) as $ingredient ) {
                outputMenuItemIngredient($ingredient);
              }
            ?>
          </div>
        <?php } ?>

        <?php if (!empty($item['df_ingredients'])) { ?>
          <div class="modal__dialogue__tab" data-name="tab_<?php echo $item['_id']; ?>_df">
            <?php 
              foreach ( json_decode($item['df_ingredients'], true) as $ingredient ) {
                outputMenuItemIngredient($ingredient);
              }
            ?>
          </div>
        <?php } ?>

        <?php if (!empty($item['gf_ingredients'])) { ?>
          <div class="modal__dialogue__tab" data-name="tab_<?php echo $item['_id']; ?>_gf">
           <?php 
              foreach ( json_decode($item['gf_ingredients'], true) as $ingredient ) {
                outputMenuItemIngredient($ingredient);
              }
            ?>
          </div>
        <?php } ?>
      </div>
    </div>
    <div class="modal__dialogue__footer">
      <p>If you'd like to discuss your dietary requirements with us, or you need more information, please email our executive chef, Dickie on <a href="mailto:dickie@fathippo.co.uk" target="_blank" title="Send email to dickie@fathippo.co.uk">dickie@fathippo.co.uk</a>.</p>
    </div>
  </div>
</div>