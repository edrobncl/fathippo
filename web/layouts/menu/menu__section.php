<?php
  $menuCategoryId = $key;
?>
<section class="menu-section">
  <div class="menu-section__header inner-overlay" data-toggle="<?php echo $key; ?>">
    <h2 class="menu-section__title"><?= $section['title'] ?></h2>
    <button class="menu-section__button"><i class="icon-plus"></i></button>
  </div>
  
  <div class="menu-section__content" data-content="<?php echo $key; ?>">
    <div class="menu-section__hidden" data-wrap="<?= $key ?>">
      <?php if ( ! empty($section['description']) ) { ?>
        <div class="menu-section__description"><?= $section['description'] ?></div>
      <?php } ?>

      <div class="menu-section__items">
        <?php foreach ($section['menu_items'] as $key => $item) include 'menu__item.php'; ?>
      </div>

      <?php
        include FS_ROOT . 'includes/inc_menu_category_cta.php';
      ?>
    </div>

  </div>

</section> 