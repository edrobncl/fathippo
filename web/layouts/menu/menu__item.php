<?php
  // Split Food Details up as What it is vs what can be requested
  $standardDetails = $requestDetails = [];
  if ( ! empty($item['details']) ) {
    foreach ( $item['details'] as $detail ) {
      // If Not Requested
      if ( in_array($detail, [1,3,5,7]) ) {
        $standardDetails[] = $detail;
      } else {
        $requestDetails[] = $detail;
      }
    }
  }
?>
<article class="menu-item">
  <?php
    if ( ! empty($item['thumbnail']) ) { ?>
      <div class="menu-item__thumbnail inner-overlay" data-toggle="modal_<?php echo $item['_id']; ?>">
        <img src="<?php echo showPostImage($item['thumbnail']); ?>" alt="<?php echo $item['name']; ?>" />
      </div>
      <?php
    }
  ?>
  <div class="menu-item__content">
    <h3 class="menu-item__title"><?php echo $item['name']; ?></h3>

    <div class="menu-item__details">
      <div class="menu-item__price">
        <?php
          if ( $item['price'] > 0 ) { ?>
            &pound;<?php echo number_format($item['price'], 2, '.', ','); ?>
            <?php
          }
        ?>
      </div>
      <div class="menu-item__info">

        <div class="menu-item__info__wrap menu-item__info__wrap--toggle" role="button" data-toggle="modal_<?php echo $item['_id']; ?>">
          <img src="/images/circle-green-info.svg" alt="Information" />
        </div>

        <?php include 'menu__modal.php'; ?>

        <?php 
          if ( ! empty($standardDetails) ) {
            foreach ( $standardDetails as $detail ) { ?>
              <div class="menu-item__info__wrap menu-item__info__wrap--tooltip" data-content="<?= showMenuDetailName($detail) ?>">
                <img src="<?= showMenuDetailIcon($detail) ?>" alt="" class="menu-item__info__icon" />
              </div>
              <?php
            }
          }
        ?>

      </div>
    </div>

    <div class="menu-item__description">
      <?php echo $item['description']; ?>
    </div>

    <?php if ( ! empty($requestDetails) ) { ?>
      <p class="menu-item__details">
        <span class="menu-item__detail">On Request:</span>
        <?php foreach ($requestDetails as $detail) { ?>
          <span class="menu-item__detail menu-item__detail--tooltip" data-toggle="tooltip" data-content="<?= showMenuDetailName($detail) ?>">
            <img src="<?= showMenuDetailIcon($detail) ?>" alt="" class="menu-item__detail__icon" />
          </span><?php
        } ?>
      </p>
    <?php } ?>
  </div>
</article>