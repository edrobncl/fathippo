<?php
	if (isset($in_admin)) {
        
		$post_editables = json_encode([
      "post_tagline",
      "page_content",
      "post_tagline2",
      "page_content2",
      "post_tagline3",
      "page_content3",
      "post_tagline4",
      "page_content4",
      "post_tagline5",
      "page_content5",
      "header_image"
    ]);

  } else {

    $locations = fetchResource([
      'post_type' => 'location',
      'post_status' => 'live',
      'order' => 'post.post_alt_title'
    ]);

    $jobs = fetchResource([
      'post_type' => 'job',
      'post_status' => 'live',
      'meta' => [
        'job_location',
        'job_type',
        'job_department',
        'job_salary',
        'job_summary',
        'job_email'
      ]
    ]);

    if ($jobs['count'] > 0) {
      foreach ($jobs['data'] as $job) {
        if ( $job['job_location'] > 0 ) {
          $location = str_replace(' ', '_', showAltName($job['job_location']));
        } else {
          $location = 'Other';
          // Add Location Data for Other
          $locations['data'][] = [
            '_id' => -1,
            'date_created' => date('Y-m-d H:i:s'),
            'post_status' => 'live',
            'post_type' => 'location',
            'parent_id' => 6,
            'date_modified' => date('Y-m-d H:i:s'),
            'post_slug' => 'other',
            'menu_order' => 999,
            'post_author' => 1,
            'post_title' => 'Other',
            'post_alt_title' => 'Other',
            'tagline' => '',
            'summary' => '',
            'thumbnail' => '',
          ]; 
        }
        if (empty($$location)) {
          $$location = [];
        }
        $$location[] = $job;
      }
      addAsset('toggle');
    }

  ?> 
  <main id="main" class="main">
    <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
    <?php include_once FS_ROOT.'includes/inc_masthead.php'; ?>

    <section class="section intro">
      <div class="inner">
        <div class="container">
          <div class="container__column intro__image-wrap">
            <img loading="lazy" src="/images/burger-heads-group.svg" alt="Group of Burgers" width="458" height="257" class="intro__image" />
          </div>
          <div class="container__column intro__content-wrap">
            <div class="page-content">
              <?php if (!empty($page_content)) echo $page_content; ?>
              <?php showButton('#vacancies', 'button--outlined', 'Vacancies'); ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section featured-perks">
      <div class="inner">
        <?php showPageFeature('Why Us?', $post_tagline2, $page_content2); ?>

        <div class="container why-us__cards">
          <?php if (!empty($post_tagline3) && !empty($page_content3)) { ?>
            <article class="why-us__card">
              <div class="why-us__icon"><i class="icon-plus"></i></div>
              <h3 class="why-us__title"><?php echo $post_tagline3; ?></h3>
              <div class="why-us__tagline"><?php echo $page_content3; ?></div>
            </article>
          <?php } ?>

          <?php if (!empty($post_tagline4) && !empty($page_content4)) { ?>
            <article class="why-us__card">
              <div class="why-us__icon"><i class="icon-face"></i></div>
              <h3 class="why-us__title"><?php echo $post_tagline4; ?></h3>
              <div class="why-us__tagline"><?php echo $page_content4; ?></div>
            </article>
          <?php } ?>

          <?php if (!empty($post_tagline5) && !empty($page_content5)) { ?>
            <article class="why-us__card">
              <div class="why-us__icon"><i class="icon-burger"></i></div>
              <h3 class="why-us__title"><?php echo $post_tagline5; ?></h3>
              <div class="why-us__tagline"><?php echo $page_content5; ?></div>
            </article>
          <?php } ?>
        </div>
      </div>

      <div class="inner inner--narrow">
        <div class="vacancies" id="vacancies">
          <?php if ($locations['count'] > 0) { ?>
            <div class="career-locations">
              <?php foreach ($locations['data'] as $location) { 
                $title = str_replace(' ', '_', $location['post_alt_title']);
                if (!empty($$title)) { ?>
                  <div class="career-location">
                    <h2 class="career-location__title"><?php echo $location['post_alt_title'];?></h2>
                    <?php foreach ($$title as $key => $job) { ?>
                      <div class="job">
                        <div class="job-title__header inner-overlay" data-toggle="<?php echo $job['_id'];?>">
                          <h3 class="job-title"><?php echo $job['post_title'];?></h3>
                          <button class="menu-section__button"><i class="icon-plus"></i></button>
                        </div>
                        <div class="job-content" data-content="<?php echo $job['_id'];?>">
                          <div class="job-content__wrap" data-wrap="<?php echo $job['_id'];?>">
                            <div class="job-content__summary"><?php echo $job['job_summary'];?></div>
                            <div class="job-content__department">Department: <?php echo $job['job_department'];?></div>
                            <div class="job-content__type">Job Type: <?php echo $job['job_type'];?></div>
                            <div class="job-content__salary">Salary: <?php echo $job['job_salary'];?></div>
                            <div class="job-content__location">Restaurant: <?php echo showAltName($job['job_location']);?></div>
                            <div class="job-content__actions">
                              <?php showButton($job['_id'], 'button--outlined', 'View Position'); ?>
                              <a href="mailto:<?php echo $job['job_email'];?>?subject=Application For: <?php echo $job['post_title'];?> - <?php echo showAltName($job['job_location']) ?? 'Other';?>" class="inline-button">
                                Apply For Position <i class="icon-chevron-double-right"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                <?php }
              } ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </section>
  </main>
  <?php } ?>