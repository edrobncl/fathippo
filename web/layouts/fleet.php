<?php
	if (isset($in_admin)) {

    $post_editables = json_encode([
      "page_content",
      "page_content2",
      "page_content3",
      "page_content4",
      "page_content5",
      "post_tagline5",
    ]);

  }
  else {

    // Get All UPCOMING events and events that HAVENT FINISHED yet
    $events = '
      SELECT er_posts.*,
        max(case when er_postmeta.meta_name = "event_time" then er_postmeta.meta_value end) AS time,
        max(case when er_postmeta.meta_name = "event_end_date" then er_postmeta.meta_value end) AS end_date,
        max(case when er_postmeta.meta_name = "event_address" then er_postmeta.meta_value end) AS address,
        max(case when er_postmeta.meta_name = "event_link_text" then er_postmeta.meta_value end) AS link_text,
        max(case when er_postmeta.meta_name = "event_link" then er_postmeta.meta_value end) AS link,
        max(case when er_postmeta.meta_name = "pdf_for_menu" then er_postmeta.meta_value end) AS pdf_for_menu,
        max(case when er_postmeta.meta_name = "menu_pdf" then er_postmeta.meta_value end) AS menu_pdf
        
      FROM er_posts
        LEFT JOIN er_postmeta 
          ON er_posts._id = er_postmeta.post_id
      WHERE er_posts.post_type = "event"
        AND (
          DATE(er_posts.post_alt_title) >= DATE(NOW())
          OR
          er_posts._id IN (
            SELECT post_id
            FROM er_postmeta
            WHERE meta_name = "event_end_date"
              AND DATE(meta_value) >= DATE(NOW())
          )
        ) ';

    // Handle Status
    // if ( ! empty($_SESSION['eruid']) ) {
    //   $events .= 'AND er_posts.post_status IN ("live", "private")';
    // } else {
      $events .= 'AND er_posts.post_status = "live"';
    // }

    $events .= '
      GROUP BY er_posts._id
      ORDER BY DATE(er_posts.post_alt_title) ASC
    ';

    // Execute Query
    $events = $pdo->query($events)->fetchAll();


    $fleets = fetchResource([
      'post_type' => 'fleet-member',
      'meta' => [
        'fleet_member_description' => 'content',
        'fleet_member_image' => 'thumbnail'
      ]
    ]);

?>
<main id="main" class="main">
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>
  <?php include FS_ROOT.'includes/inc_carousel.php'; ?>

  <section class="section fleet">

    <?php if (!empty($page_content)) { ?>
      <div class="inner inner--narrow">
        <div class="feature feature--medium">
          <div class="feature__tagline"><?php echo $page_content; ?></div>
        </div>
      </div>
    <?php } ?>

    <div class="inner">
      <?php if (!empty($page_content2)) { ?>
        <div class="feature">
          <p class="feature__back">Find Us</p>
          <div class="feature__front"><?php echo $page_content2; ?></div>
        </div>
      <?php } ?>


      <?php if (!empty($events)) { ?>
        <?php if (count($events) > 3) { ?>
          <div class="locations__actions">
            <div class="locations__controls">
              <button type="button" id="l-prev-arrow" title="Previous slide" class="slick-arrow slick-arrow--circle slick-prev"><?php showIcon('arrow-left-regular'); ?></button>
              <button type="button" id="l-next-arrow" title="Next slide" class="slick-arrow slick-arrow--circle slick-next"><?php showIcon('arrow-right-regular'); ?></button>
            </div>
          </div>
        <?php } ?>

        <div id="locations" class="flex-container flex-container--locations events__container<?php if (!empty($events) && count($events) > 2) { ?> events--slick<?php } ?>">
          <?php foreach ($events as $event) { 
            if (!empty($event['menu_pdf'])) {
              $d_id = json_decode($event['menu_pdf'], true)[0];
              $pdf = $pdo->query("SELECT * FROM er_posts WHERE post_type = 'file' AND _id = $d_id")->fetch();
            } ?>
            <article class="event-listing">
              <div class="event-listing__content">
                <div class="event-listing__title-wrap">
                  <h3 class="event-listing__title"><?php echo $event['post_title']; ?></h3>
                </div>
                <div class="event-listing__detail">
                  <div class="event-listing__detail__icon"><i class="icon-map-pin"></i></div>
                  <div class="event-listing__detail__text"><?php echo $event['address']; ?></div>
                </div>
                <div class="event-listing__detail">
                  <div class="event-listing__detail__icon"><i class="icon-calendar-plus"></i></div>
                  <div class="event-listing__detail__text">
                    <?php echo date('d.m.Y', strtotime($event['post_alt_title'])); ?>
                    <?php if (!empty($event['end_date']) && $event['post_alt_title'] != $event['end_date']) { ?>
                      - <?php echo date('d.m.Y', strtotime($event['end_date'])); ?>
                    <?php } ?>
                  </div>
                </div>
                <div class="event-listing__detail">
                  <div class="event-listing__detail__icon"><i class="icon-clock"></i></div>
                  <div class="event-listing__detail__text"><?php echo $event['time']; ?></div>
                </div>
              </div>
              <? if (!empty($event['pdf_for_menu']) && !empty($pdf)) { ?>
                <div class="event-listing__action">
                  <a href="<?=$pdf['post_slug'];?>" title="View our menu" rel="noreferrer"><i class="icon-van"></i> <?=$event['link_text'];?></a>
                </div>
              <? } else if (!empty($event['link'])) { ?>
                <div class="event-listing__action">
                  <a href="<?php echo $event['link']; ?>" title="Order here" rel="noreferrer"><i class="icon-van"></i> <?php echo $event['link_text']; ?></a>
                </div>
              <? } ?>
            </article>
          <?php } ?>
        </div>
      <?php } else {  ?>
        <div class="events events--no-upcoming">
          <div class="events__front">
            <p>There are no upcoming events currently. Check back here later to see all upcoming events near you.</p>
            <p>Or keep up to date with us on our social media</p>
            <ul class="reset--list social-media">
              <li class="social-media__item"><a href="https://twitter.com/fathippofleet" title="Follow Fat Hippo on Twitter" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-twitter"></i></a></li>
              <li class="social-media__item"><a href="https://www.facebook.com/fathippofleet/" title="Like Fat Hippo on Facebook" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-facebook"></i></a></li>
            </ul>
          </div>
        </div>
      
      <?php } ?>

      <?php if (!empty($page_content3)) { ?>
        <div class="feature">
          <p class="feature__back">The Fleet</p>
          <div class="feature__front"><?php echo $page_content3; ?></div>
        </div>
      <?php } ?>

      <?php if ($fleets['count'] > 0) foreach ($fleets['data'] as $key => $fleet) { ?>
        <article class="fleet-listing">
          <div class="flex-container flex-container--fleet">
            <div class="fleet-listing__thumb"><?php showPicture($fleet['thumbnail']); ?></div>
            <div class="fleet-listing__content">
              <h3 class="fleet-listing__title"><?php echo $fleet['post_title']; ?></h3>
              <?php echo $fleet['content'];?>
            </div>
          </div>
        </article>
      <?php } ?>
    </div>
  </section>

  <section class="section fleet-allergens">
    <div class="inner inner--narrow">
      <?php if (!empty($page_content4)) { ?>
        <div class="page-content">
          <?php echo $page_content4; ?>
          <?php showButton('/menus/fat-hippo-fleet-allergens/', 'button--outlined', 'Fleet Allergens'); ?>
        </div>
      <?php } ?>
    </div>
  </section>

  <section class="section fleet-cta">
    <div class="inner">
      <div class="fleet-cta__container">
        <div class="fleet-cta__container-col">
          <h2 class="fleet-cta__title"><?php echo $post_tagline5; ?></h2>
          <img src="/images/fleet-chevy.svg" alt="Fleet Chevy" class="fleet-cta__image" />
        </div>
        <div class="fleet-cta__container-col">
          <div class="page-content">
            <?php echo $page_content5; ?>
            <?php showButton('mailto:fleet@fathippo.co.uk?subject=Fleet Enquiry', 'button--outlined', 'Hire the Fleet'); ?>
          </div>
        </div>
      </div>
      <div class="fleet-cta__socials">
        <span>Follow the Fat Hippo Fleet</span>
        <ul class="reset--list social-media">
          <li class="social-media__item"><a href="https://twitter.com/fathippofleet" title="Follow Fat Hippo on Twitter" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-twitter"></i></a></li>
          <li class="social-media__item"><a href="https://www.facebook.com/fathippofleet/" title="Like Fat Hippo on Facebook" target="_blank" class="social-media__link" rel="noreferrer"><i class="icon-facebook"></i></a></li>
        </ul>
      </div>
    </div>
  </section>

  <?php include_once FS_ROOT.'includes/inc_reviews_strip.php'; ?>

</main>
<?php } ?>