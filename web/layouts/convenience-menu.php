<?php
	if (isset($in_admin)) {

		$post_editables = json_encode([
      "post_tagline",
      "page_content",
      "header_image",
      "thumbnail_image",
      "menus" // List of menus to show
    ]);

  } else {
    addAsset('modal');
    addAsset('toggle');
?>
<main id="main" class="main">
  <!-- <div class="inner main__illustration main__illustration--convenience">
    <img src="/images/stuff-face-burger.svg" alt="Stuff Face Burger" width="140" height="140" class="main__illustration" />
  </div> -->
  
  <?php include_once FS_ROOT.'includes/inc_offer_strip.php'; ?>

  <section class="section menu menu--convenience">
    <div class="inner inner--narrow">
      

      <?php 
        // Menus
        $menuList = isset($menu_list) ? json_decode($menu_list, true) : [];
        
        foreach ( $menuList as $index => $menuId ) {
          $menu = getPost($menuId);
          $menuMeta = getAllMeta($menuId);

          // Show Menu Headers
         ?>
            <div class="feature">
              <?php
                if ( ! empty($menuMeta['post_tagline']) ) { ?>
                  <h1 class="feature__title feature__title--large"><?= $menuMeta['post_tagline'] ?></h1>
                  <?php 
                }

                if ( ! empty($menuMeta['page_content']) ) { ?>
                  <div class="feature__tagline"><?= $menuMeta['page_content'] ?></div>
                  <?php
                }
              ?>
            </div>
            <?php
              if ( $index === 0 ) { ?>
                <section class="section">
                  <div class="inner inner--narrow">
        
                    <div class="menu__allergy-info">
                      <img src="/images/circle-green-info.svg" alt="Allergy information icon" />For allergy info, hit the icon shown to the left here
                    </div>
                  </div>
                </section>
                <?php
              }
            ?>
          <?php
          
          

          $categoriesList = getMenuCategories($menuId);
          foreach ( $categoriesList as $key => $section ) {
              $key = uniqid();
              $section['menu_items'] = getCategoryItems($section['_id']); // Need to sort this
              if ( empty($section['menu_items']) ) continue;

              $section['menu_items'] = array_map(function($item) {
                $meta = getAllMeta($item['_id'], false);
                return [
                  'description' => $meta['food_description'] ?? '',
                  'section_id' => $meta['menu_section'] ?? '',
                  'details' => json_decode($meta['food_details'] ?? '[]', true),
                  'ingredients' => $meta['ingredients'] ?? '',
                  'veg_ingredients' => $meta['veg_ingredients'] ?? '',
                  'vegan_ingredients' => $meta['vegan_ingredients'] ?? '',
                  'gf_ingredients' => $meta['gf_ingredients'] ?? '',
                  'df_ingredients' => $meta['df_ingredients'] ?? '',
                  'thumbnail' => $meta['food_image'] ?? '',
                  '_id' => $item['_id'],
                  'name' => $item['post_title'],
                  'price' => $item['post_alt_title'],
                  'menu_order' => $item['menu_order'] 
                ];
              }, $section['menu_items']);

              $section['title'] = $section['post_title'];
              $section['description'] = getMeta($section['_id'], 'menu_category_description');

              include 'menu/menu__section.php';
          }
        }

		  ?>
    </div>

    <?php if (!empty($hh_title) && !empty($hh_content)) { ?>
      <div class="inner">
        <div class="menu__hippo-hour">
          <div class="feature">
            <p class="feature__back"><?php echo str_replace('!', '', $hh_title); ?></p>
            <div class="feature__front">
              <h2 class="feature__title"><?php echo $hh_title; ?></h2>
              <div class="feature__tagline"><?php echo $hh_content; ?></div>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </section>

  <?php include_once FS_ROOT . 'includes/inc_reviews_strip.php'; ?>
</main>
<?php } ?>


<?php

?>